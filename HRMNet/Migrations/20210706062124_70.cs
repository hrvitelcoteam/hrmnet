﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _70 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Candidate_CandidateId",
                table: "Attachments");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Candidate_CandidateId",
                table: "CandidateCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositionHistory_Candidate_CandidateId",
                table: "CandidatePositionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositionHistory_Positions_PositionId",
                table: "CandidatePositionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions");

            migrationBuilder.DropForeignKey(
                name: "FK_Disciplin_Candidate_CandidateId",
                table: "Disciplin");

            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Candidate_CandidateId",
                table: "Meetings");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Candidate_CandidateId",
                table: "Offers");

            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetails_Candidate_CandidateId",
                table: "StuffDetails");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Candidate_CandidateId",
                table: "Attachments",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Candidate_CandidateId",
                table: "CandidateCourse",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse",
                column: "CourseId",
                principalTable: "Course",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures",
                column: "FeatureId",
                principalTable: "Features",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories",
                column: "InventoryId",
                principalTable: "Inventories",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositionHistory_Candidate_CandidateId",
                table: "CandidatePositionHistory",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositionHistory_Positions_PositionId",
                table: "CandidatePositionHistory",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Disciplin_Candidate_CandidateId",
                table: "Disciplin",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Candidate_CandidateId",
                table: "Meetings",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Candidate_CandidateId",
                table: "Offers",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetails_Candidate_CandidateId",
                table: "StuffDetails",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Candidate_CandidateId",
                table: "Attachments");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Candidate_CandidateId",
                table: "CandidateCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositionHistory_Candidate_CandidateId",
                table: "CandidatePositionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositionHistory_Positions_PositionId",
                table: "CandidatePositionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions");

            migrationBuilder.DropForeignKey(
                name: "FK_Disciplin_Candidate_CandidateId",
                table: "Disciplin");

            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Candidate_CandidateId",
                table: "Meetings");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Candidate_CandidateId",
                table: "Offers");

            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetails_Candidate_CandidateId",
                table: "StuffDetails");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Candidate_CandidateId",
                table: "Attachments",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Candidate_CandidateId",
                table: "CandidateCourse",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse",
                column: "CourseId",
                principalTable: "Course",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures",
                column: "FeatureId",
                principalTable: "Features",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories",
                column: "InventoryId",
                principalTable: "Inventories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositionHistory_Candidate_CandidateId",
                table: "CandidatePositionHistory",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositionHistory_Positions_PositionId",
                table: "CandidatePositionHistory",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Disciplin_Candidate_CandidateId",
                table: "Disciplin",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Candidate_CandidateId",
                table: "Meetings",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Candidate_CandidateId",
                table: "Offers",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetails_Candidate_CandidateId",
                table: "StuffDetails",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
