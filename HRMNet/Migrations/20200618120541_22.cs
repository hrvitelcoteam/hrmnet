﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsClosed",
                table: "Positions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "EvaluatedDate",
                table: "CurrentPositions",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<DateTime>(
                name: "CVSharingDateWithCustomer",
                table: "CurrentPositions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BriefPersonalInfomation",
                table: "Candidate",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsClosed",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "CVSharingDateWithCustomer",
                table: "CurrentPositions");

            migrationBuilder.DropColumn(
                name: "BriefPersonalInfomation",
                table: "Candidate");

            migrationBuilder.AlterColumn<DateTime>(
                name: "EvaluatedDate",
                table: "CurrentPositions",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
