﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _24 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertise_Candidate_CandidateId",
                table: "CandidateExpertise");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertise_Expertises_ExpertiseId",
                table: "CandidateExpertise");

            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Customers_CustomerId",
                table: "Meetings");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Customers_CustomerId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_CustomerId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_CustomerId",
                table: "Meetings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateExpertise",
                table: "CandidateExpertise");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Meetings");

            migrationBuilder.RenameTable(
                name: "CandidateExpertise",
                newName: "CandidateExpertises");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateExpertise_ExpertiseId",
                table: "CandidateExpertises",
                newName: "IX_CandidateExpertises_ExpertiseId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateExpertise_CandidateId",
                table: "CandidateExpertises",
                newName: "IX_CandidateExpertises_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateExpertises",
                table: "CandidateExpertises",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "PositionType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PositionId = table.Column<int>(nullable: false),
                    ExpertiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PositionType_Expertises_ExpertiseId",
                        column: x => x.ExpertiseId,
                        principalTable: "Expertises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionType_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PositionType_ExpertiseId",
                table: "PositionType",
                column: "ExpertiseId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionType_PositionId",
                table: "PositionType",
                column: "PositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises");

            migrationBuilder.DropTable(
                name: "PositionType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateExpertises",
                table: "CandidateExpertises");

            migrationBuilder.RenameTable(
                name: "CandidateExpertises",
                newName: "CandidateExpertise");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateExpertises_ExpertiseId",
                table: "CandidateExpertise",
                newName: "IX_CandidateExpertise_ExpertiseId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateExpertises_CandidateId",
                table: "CandidateExpertise",
                newName: "IX_CandidateExpertise_CandidateId");

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Offers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Meetings",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateExpertise",
                table: "CandidateExpertise",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_CustomerId",
                table: "Offers",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_CustomerId",
                table: "Meetings",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertise_Candidate_CandidateId",
                table: "CandidateExpertise",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertise_Expertises_ExpertiseId",
                table: "CandidateExpertise",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Customers_CustomerId",
                table: "Meetings",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Customers_CustomerId",
                table: "Offers",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
