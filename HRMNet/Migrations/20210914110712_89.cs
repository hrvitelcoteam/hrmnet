﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _89 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SopId",
                table: "Attachments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_SopId",
                table: "Attachments",
                column: "SopId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Sops_SopId",
                table: "Attachments",
                column: "SopId",
                principalTable: "Sops",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Sops_SopId",
                table: "Attachments");

            migrationBuilder.DropIndex(
                name: "IX_Attachments_SopId",
                table: "Attachments");

            migrationBuilder.DropColumn(
                name: "SopId",
                table: "Attachments");
        }
    }
}
