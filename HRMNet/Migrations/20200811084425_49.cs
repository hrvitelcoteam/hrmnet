﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _49 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetail_Titles_TitleId",
                table: "StuffDetail");

            migrationBuilder.AlterColumn<int>(
                name: "TitleId",
                table: "StuffDetail",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetail_Titles_TitleId",
                table: "StuffDetail",
                column: "TitleId",
                principalTable: "Titles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetail_Titles_TitleId",
                table: "StuffDetail");

            migrationBuilder.AlterColumn<int>(
                name: "TitleId",
                table: "StuffDetail",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetail_Titles_TitleId",
                table: "StuffDetail",
                column: "TitleId",
                principalTable: "Titles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
