﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CandidateFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CandidateId = table.Column<int>(nullable: false),
                    FeatureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CandidateFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CandidateFeatures_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "Candidate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CandidateFeatures_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PositionFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PositionId = table.Column<int>(nullable: false),
                    FeatureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PositionFeatures_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionFeatures_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PositionMandatoryFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PositionId = table.Column<int>(nullable: false),
                    FeatureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionMandatoryFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PositionMandatoryFeatures_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionMandatoryFeatures_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CandidateFeatures_CandidateId",
                table: "CandidateFeatures",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CandidateFeatures_FeatureId",
                table: "CandidateFeatures",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionFeatures_FeatureId",
                table: "PositionFeatures",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionFeatures_PositionId",
                table: "PositionFeatures",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionMandatoryFeatures_FeatureId",
                table: "PositionMandatoryFeatures",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionMandatoryFeatures_PositionId",
                table: "PositionMandatoryFeatures",
                column: "PositionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CandidateFeatures");

            migrationBuilder.DropTable(
                name: "PositionFeatures");

            migrationBuilder.DropTable(
                name: "PositionMandatoryFeatures");
        }
    }
}
