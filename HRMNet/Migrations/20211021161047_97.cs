﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _97 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TitleId",
                table: "AppUsers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_TitleId",
                table: "AppUsers",
                column: "TitleId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppUsers_Titles_TitleId",
                table: "AppUsers",
                column: "TitleId",
                principalTable: "Titles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppUsers_Titles_TitleId",
                table: "AppUsers");

            migrationBuilder.DropIndex(
                name: "IX_AppUsers_TitleId",
                table: "AppUsers");

            migrationBuilder.DropColumn(
                name: "TitleId",
                table: "AppUsers");
        }
    }
}
