﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _42 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetail_Candidate_ManagerId",
                table: "StuffDetail");

            migrationBuilder.DropIndex(
                name: "IX_StuffDetail_ManagerId",
                table: "StuffDetail");

            migrationBuilder.DropColumn(
                name: "ManagerId",
                table: "StuffDetail");

            migrationBuilder.AddColumn<int>(
                name: "CandidateId",
                table: "StuffDetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Manager",
                table: "StuffDetail",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_StuffDetail_CandidateId",
                table: "StuffDetail",
                column: "CandidateId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetail_Candidate_CandidateId",
                table: "StuffDetail",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetail_Candidate_CandidateId",
                table: "StuffDetail");

            migrationBuilder.DropIndex(
                name: "IX_StuffDetail_CandidateId",
                table: "StuffDetail");

            migrationBuilder.DropColumn(
                name: "CandidateId",
                table: "StuffDetail");

            migrationBuilder.DropColumn(
                name: "Manager",
                table: "StuffDetail");

            migrationBuilder.AddColumn<int>(
                name: "ManagerId",
                table: "StuffDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_StuffDetail_ManagerId",
                table: "StuffDetail",
                column: "ManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetail_Candidate_ManagerId",
                table: "StuffDetail",
                column: "ManagerId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
