﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SummaryInformation",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TotalITexperience",
                table: "Candidate",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SummaryInformation",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "TotalITexperience",
                table: "Candidate");
        }
    }
}
