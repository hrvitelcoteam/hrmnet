﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _79 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Timesheet_AppUsers_UserId",
                table: "Timesheet");

            migrationBuilder.DropIndex(
                name: "IX_Timesheet_UserId",
                table: "Timesheet");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Timesheet");

            migrationBuilder.AddColumn<int>(
                name: "PositionId",
                table: "Timesheet",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Timesheet_PositionId",
                table: "Timesheet",
                column: "PositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Timesheet_Positions_PositionId",
                table: "Timesheet",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Timesheet_Positions_PositionId",
                table: "Timesheet");

            migrationBuilder.DropIndex(
                name: "IX_Timesheet_PositionId",
                table: "Timesheet");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "Timesheet");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Timesheet",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Timesheet_UserId",
                table: "Timesheet",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Timesheet_AppUsers_UserId",
                table: "Timesheet",
                column: "UserId",
                principalTable: "AppUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
