﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _72 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures",
                column: "FeatureId",
                principalTable: "Features",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures",
                column: "FeatureId",
                principalTable: "Features",
                principalColumn: "Id");
        }
    }
}
