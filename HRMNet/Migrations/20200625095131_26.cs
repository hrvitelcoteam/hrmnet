﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _26 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PositionType_Expertises_ExpertiseId",
                table: "PositionType");

            migrationBuilder.DropForeignKey(
                name: "FK_PositionType_Positions_PositionId",
                table: "PositionType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PositionType",
                table: "PositionType");

            migrationBuilder.RenameTable(
                name: "PositionType",
                newName: "PositionTypes");

            migrationBuilder.RenameIndex(
                name: "IX_PositionType_PositionId",
                table: "PositionTypes",
                newName: "IX_PositionTypes_PositionId");

            migrationBuilder.RenameIndex(
                name: "IX_PositionType_ExpertiseId",
                table: "PositionTypes",
                newName: "IX_PositionTypes_ExpertiseId");

            migrationBuilder.AlterColumn<string>(
                name: "PositionType",
                table: "Positions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "CustomerName",
                table: "Positions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PositionTypes",
                table: "PositionTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PositionTypes_Expertises_ExpertiseId",
                table: "PositionTypes",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PositionTypes_Positions_PositionId",
                table: "PositionTypes",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PositionTypes_Expertises_ExpertiseId",
                table: "PositionTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_PositionTypes_Positions_PositionId",
                table: "PositionTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PositionTypes",
                table: "PositionTypes");

            migrationBuilder.RenameTable(
                name: "PositionTypes",
                newName: "PositionType");

            migrationBuilder.RenameIndex(
                name: "IX_PositionTypes_PositionId",
                table: "PositionType",
                newName: "IX_PositionType_PositionId");

            migrationBuilder.RenameIndex(
                name: "IX_PositionTypes_ExpertiseId",
                table: "PositionType",
                newName: "IX_PositionType_ExpertiseId");

            migrationBuilder.AlterColumn<string>(
                name: "PositionType",
                table: "Positions",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CustomerName",
                table: "Positions",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PositionType",
                table: "PositionType",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PositionType_Expertises_ExpertiseId",
                table: "PositionType",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PositionType_Positions_PositionId",
                table: "PositionType",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
