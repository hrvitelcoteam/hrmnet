﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _39 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "Inventories");

            migrationBuilder.AddColumn<int>(
                name: "InventoryTypeId",
                table: "Inventories",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StuffDetailId",
                table: "Inventories",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "InventoryTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Titles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Titles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkLocations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkLocations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StuffDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    TCNumber = table.Column<string>(nullable: true),
                    AccommodationAddress = table.Column<string>(nullable: true),
                    MilitaryStatus = table.Column<int>(nullable: false),
                    PostponementdDate = table.Column<DateTime>(nullable: false),
                    ContractType = table.Column<int>(nullable: false),
                    ContractDuration = table.Column<string>(nullable: true),
                    TitleId = table.Column<int>(nullable: false),
                    WorkLocationId = table.Column<int>(nullable: false),
                    SchoolOfGraduation = table.Column<string>(nullable: true),
                    DepartmentOfGraduation = table.Column<string>(nullable: true),
                    DateOfGraduation = table.Column<DateTime>(nullable: false),
                    IBANNumber = table.Column<string>(nullable: true),
                    CompanyEmail = table.Column<string>(nullable: true),
                    ManagerId = table.Column<int>(nullable: false),
                    BloodGroup = table.Column<string>(nullable: true),
                    EmergencyContactInformation = table.Column<string>(nullable: true),
                    BirthCertificateNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StuffDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StuffDetail_Candidate_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "Candidate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StuffDetail_Titles_TitleId",
                        column: x => x.TitleId,
                        principalTable: "Titles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StuffDetail_WorkLocations_WorkLocationId",
                        column: x => x.WorkLocationId,
                        principalTable: "WorkLocations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Inventories_InventoryTypeId",
                table: "Inventories",
                column: "InventoryTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Inventories_StuffDetailId",
                table: "Inventories",
                column: "StuffDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffDetail_ManagerId",
                table: "StuffDetail",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffDetail_TitleId",
                table: "StuffDetail",
                column: "TitleId");

            migrationBuilder.CreateIndex(
                name: "IX_StuffDetail_WorkLocationId",
                table: "StuffDetail",
                column: "WorkLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Inventories_InventoryTypes_InventoryTypeId",
                table: "Inventories",
                column: "InventoryTypeId",
                principalTable: "InventoryTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Inventories_StuffDetail_StuffDetailId",
                table: "Inventories",
                column: "StuffDetailId",
                principalTable: "StuffDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inventories_InventoryTypes_InventoryTypeId",
                table: "Inventories");

            migrationBuilder.DropForeignKey(
                name: "FK_Inventories_StuffDetail_StuffDetailId",
                table: "Inventories");

            migrationBuilder.DropTable(
                name: "InventoryTypes");

            migrationBuilder.DropTable(
                name: "StuffDetail");

            migrationBuilder.DropTable(
                name: "Titles");

            migrationBuilder.DropTable(
                name: "WorkLocations");

            migrationBuilder.DropIndex(
                name: "IX_Inventories_InventoryTypeId",
                table: "Inventories");

            migrationBuilder.DropIndex(
                name: "IX_Inventories_StuffDetailId",
                table: "Inventories");

            migrationBuilder.DropColumn(
                name: "InventoryTypeId",
                table: "Inventories");

            migrationBuilder.DropColumn(
                name: "StuffDetailId",
                table: "Inventories");

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Inventories",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
