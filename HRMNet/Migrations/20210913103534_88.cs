﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _88 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Sops",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sops_CustomerId",
                table: "Sops",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sops_Customers_CustomerId",
                table: "Sops",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sops_Customers_CustomerId",
                table: "Sops");

            migrationBuilder.DropIndex(
                name: "IX_Sops_CustomerId",
                table: "Sops");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Sops");
        }
    }
}
