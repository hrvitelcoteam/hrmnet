﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _55 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
