﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _74 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DisciplinId",
                table: "Attachments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_DisciplinId",
                table: "Attachments",
                column: "DisciplinId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Disciplin_DisciplinId",
                table: "Attachments",
                column: "DisciplinId",
                principalTable: "Disciplin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Disciplin_DisciplinId",
                table: "Attachments");

            migrationBuilder.DropIndex(
                name: "IX_Attachments_DisciplinId",
                table: "Attachments");

            migrationBuilder.DropColumn(
                name: "DisciplinId",
                table: "Attachments");
        }
    }
}
