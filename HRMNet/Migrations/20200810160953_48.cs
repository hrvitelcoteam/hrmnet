﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _48 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventory_Candidate_CandidateId",
                table: "CandidateInventory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventory_Inventories_InventoryId",
                table: "CandidateInventory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateInventory",
                table: "CandidateInventory");

            migrationBuilder.RenameTable(
                name: "CandidateInventory",
                newName: "CandidateInventories");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateInventory_InventoryId",
                table: "CandidateInventories",
                newName: "IX_CandidateInventories_InventoryId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateInventory_CandidateId",
                table: "CandidateInventories",
                newName: "IX_CandidateInventories_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateInventories",
                table: "CandidateInventories",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories",
                column: "InventoryId",
                principalTable: "Inventories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateInventories",
                table: "CandidateInventories");

            migrationBuilder.RenameTable(
                name: "CandidateInventories",
                newName: "CandidateInventory");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateInventories_InventoryId",
                table: "CandidateInventory",
                newName: "IX_CandidateInventory_InventoryId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateInventories_CandidateId",
                table: "CandidateInventory",
                newName: "IX_CandidateInventory_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateInventory",
                table: "CandidateInventory",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventory_Candidate_CandidateId",
                table: "CandidateInventory",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventory_Inventories_InventoryId",
                table: "CandidateInventory",
                column: "InventoryId",
                principalTable: "Inventories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
