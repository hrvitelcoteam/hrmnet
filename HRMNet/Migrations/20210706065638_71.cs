﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _71 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Candidate_CandidateId",
                table: "CandidateCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositionHistory_Candidate_CandidateId",
                table: "CandidatePositionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Candidate_CandidateId",
                table: "CandidateCourse",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositionHistory_Candidate_CandidateId",
                table: "CandidatePositionHistory",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Candidate_CandidateId",
                table: "CandidateCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositionHistory_Candidate_CandidateId",
                table: "CandidatePositionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Candidate_CandidateId",
                table: "CandidateCourse",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Candidate_CandidateId",
                table: "CandidateInventories",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositionHistory_Candidate_CandidateId",
                table: "CandidatePositionHistory",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");
        }
    }
}
