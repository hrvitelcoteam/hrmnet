﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _58 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CandidateId",
                table: "Course",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Course_CandidateId",
                table: "Course",
                column: "CandidateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Course_Candidate_CandidateId",
                table: "Course",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_Candidate_CandidateId",
                table: "Course");

            migrationBuilder.DropIndex(
                name: "IX_Course_CandidateId",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "CandidateId",
                table: "Course");
        }
    }
}
