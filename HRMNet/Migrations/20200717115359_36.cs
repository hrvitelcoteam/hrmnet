﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _36 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Candidate_CandidateId",
                table: "Attachments");

            migrationBuilder.AlterColumn<int>(
                name: "CandidateId",
                table: "Attachments",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Attachments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_CustomerId",
                table: "Attachments",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Candidate_CandidateId",
                table: "Attachments",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Customers_CustomerId",
                table: "Attachments",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Candidate_CandidateId",
                table: "Attachments");

            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_Customers_CustomerId",
                table: "Attachments");

            migrationBuilder.DropIndex(
                name: "IX_Attachments_CustomerId",
                table: "Attachments");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Attachments");

            migrationBuilder.AlterColumn<int>(
                name: "CandidateId",
                table: "Attachments",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_Candidate_CandidateId",
                table: "Attachments",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
