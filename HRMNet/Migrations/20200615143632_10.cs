﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateFeatures",
                table: "CandidateFeatures");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateExpertises",
                table: "CandidateExpertises");

            migrationBuilder.RenameTable(
                name: "CandidateFeatures",
                newName: "CandidateFeature");

            migrationBuilder.RenameTable(
                name: "CandidateExpertises",
                newName: "CandidateExpertise");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateFeatures_FeatureId",
                table: "CandidateFeature",
                newName: "IX_CandidateFeature_FeatureId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateFeatures_CandidateId",
                table: "CandidateFeature",
                newName: "IX_CandidateFeature_CandidateId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateExpertises_ExpertiseId",
                table: "CandidateExpertise",
                newName: "IX_CandidateExpertise_ExpertiseId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateExpertises_CandidateId",
                table: "CandidateExpertise",
                newName: "IX_CandidateExpertise_CandidateId");

            migrationBuilder.AddColumn<string>(
                name: "Features",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateFeature",
                table: "CandidateFeature",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateExpertise",
                table: "CandidateExpertise",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Meetings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    PositionName = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    ProjectName = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    SpeakingPerson = table.Column<string>(nullable: true),
                    Result = table.Column<string>(nullable: true),
                    CandidateId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Meetings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Meetings_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "Candidate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Meetings_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Offers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    PositionName = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    ProjectName = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Result = table.Column<string>(nullable: true),
                    CandidateId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Offers_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "Candidate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Offers_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_CandidateId",
                table: "Meetings",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_CustomerId",
                table: "Meetings",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_CandidateId",
                table: "Offers",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_CustomerId",
                table: "Offers",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertise_Candidate_CandidateId",
                table: "CandidateExpertise",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertise_Expertises_ExpertiseId",
                table: "CandidateExpertise",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeature_Candidate_CandidateId",
                table: "CandidateFeature",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeature_Features_FeatureId",
                table: "CandidateFeature",
                column: "FeatureId",
                principalTable: "Features",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertise_Candidate_CandidateId",
                table: "CandidateExpertise");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertise_Expertises_ExpertiseId",
                table: "CandidateExpertise");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeature_Candidate_CandidateId",
                table: "CandidateFeature");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateFeature_Features_FeatureId",
                table: "CandidateFeature");

            migrationBuilder.DropTable(
                name: "Meetings");

            migrationBuilder.DropTable(
                name: "Offers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateFeature",
                table: "CandidateFeature");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateExpertise",
                table: "CandidateExpertise");

            migrationBuilder.DropColumn(
                name: "Features",
                table: "Candidate");

            migrationBuilder.RenameTable(
                name: "CandidateFeature",
                newName: "CandidateFeatures");

            migrationBuilder.RenameTable(
                name: "CandidateExpertise",
                newName: "CandidateExpertises");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateFeature_FeatureId",
                table: "CandidateFeatures",
                newName: "IX_CandidateFeatures_FeatureId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateFeature_CandidateId",
                table: "CandidateFeatures",
                newName: "IX_CandidateFeatures_CandidateId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateExpertise_ExpertiseId",
                table: "CandidateExpertises",
                newName: "IX_CandidateExpertises_ExpertiseId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateExpertise_CandidateId",
                table: "CandidateExpertises",
                newName: "IX_CandidateExpertises_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateFeatures",
                table: "CandidateFeatures",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateExpertises",
                table: "CandidateExpertises",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Candidate_CandidateId",
                table: "CandidateExpertises",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Candidate_CandidateId",
                table: "CandidateFeatures",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateFeatures_Features_FeatureId",
                table: "CandidateFeatures",
                column: "FeatureId",
                principalTable: "Features",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
