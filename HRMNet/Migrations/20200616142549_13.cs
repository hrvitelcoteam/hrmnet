﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CandidateFeature");

            migrationBuilder.DropTable(
                name: "PositionTypes");

            migrationBuilder.DropTable(
                name: "Types");

            migrationBuilder.DropColumn(
                name: "ResponsibleId",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Positions");

            migrationBuilder.AddColumn<string>(
                name: "CustomerName",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Features",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MustToHave",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ResponsibleMan",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tipe",
                table: "Positions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerName",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Features",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "MustToHave",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "ResponsibleMan",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Tipe",
                table: "Positions");

            migrationBuilder.AddColumn<int>(
                name: "ResponsibleId",
                table: "Positions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Positions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CandidateFeature",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CandidateId = table.Column<int>(type: "int", nullable: false),
                    FeatureId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CandidateFeature", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CandidateFeature_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "Candidate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CandidateFeature_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Types",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeName = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Types", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PositionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PositionId = table.Column<int>(type: "int", nullable: false),
                    TipeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PositionTypes_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionTypes_Types_TipeId",
                        column: x => x.TipeId,
                        principalTable: "Types",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CandidateFeature_CandidateId",
                table: "CandidateFeature",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CandidateFeature_FeatureId",
                table: "CandidateFeature",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionTypes_PositionId",
                table: "PositionTypes",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionTypes_TipeId",
                table: "PositionTypes",
                column: "TipeId");
        }
    }
}
