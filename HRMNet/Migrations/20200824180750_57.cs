﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _57 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inventories_StuffDetail_StuffDetailId",
                table: "Inventories");

            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetail_Candidate_CandidateId",
                table: "StuffDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetail_Titles_TitleId",
                table: "StuffDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetail_WorkLocations_WorkLocationId",
                table: "StuffDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StuffDetail",
                table: "StuffDetail");

            migrationBuilder.RenameTable(
                name: "StuffDetail",
                newName: "StuffDetails");

            migrationBuilder.RenameIndex(
                name: "IX_StuffDetail_WorkLocationId",
                table: "StuffDetails",
                newName: "IX_StuffDetails_WorkLocationId");

            migrationBuilder.RenameIndex(
                name: "IX_StuffDetail_TitleId",
                table: "StuffDetails",
                newName: "IX_StuffDetails_TitleId");

            migrationBuilder.RenameIndex(
                name: "IX_StuffDetail_CandidateId",
                table: "StuffDetails",
                newName: "IX_StuffDetails_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StuffDetails",
                table: "StuffDetails",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Course",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    Name = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Professor = table.Column<string>(nullable: true),
                    Duration = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Course", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Inventories_StuffDetails_StuffDetailId",
                table: "Inventories",
                column: "StuffDetailId",
                principalTable: "StuffDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetails_Candidate_CandidateId",
                table: "StuffDetails",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetails_Titles_TitleId",
                table: "StuffDetails",
                column: "TitleId",
                principalTable: "Titles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetails_WorkLocations_WorkLocationId",
                table: "StuffDetails",
                column: "WorkLocationId",
                principalTable: "WorkLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inventories_StuffDetails_StuffDetailId",
                table: "Inventories");

            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetails_Candidate_CandidateId",
                table: "StuffDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetails_Titles_TitleId",
                table: "StuffDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_StuffDetails_WorkLocations_WorkLocationId",
                table: "StuffDetails");

            migrationBuilder.DropTable(
                name: "Course");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StuffDetails",
                table: "StuffDetails");

            migrationBuilder.RenameTable(
                name: "StuffDetails",
                newName: "StuffDetail");

            migrationBuilder.RenameIndex(
                name: "IX_StuffDetails_WorkLocationId",
                table: "StuffDetail",
                newName: "IX_StuffDetail_WorkLocationId");

            migrationBuilder.RenameIndex(
                name: "IX_StuffDetails_TitleId",
                table: "StuffDetail",
                newName: "IX_StuffDetail_TitleId");

            migrationBuilder.RenameIndex(
                name: "IX_StuffDetails_CandidateId",
                table: "StuffDetail",
                newName: "IX_StuffDetail_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StuffDetail",
                table: "StuffDetail",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Inventories_StuffDetail_StuffDetailId",
                table: "Inventories",
                column: "StuffDetailId",
                principalTable: "StuffDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetail_Candidate_CandidateId",
                table: "StuffDetail",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetail_Titles_TitleId",
                table: "StuffDetail",
                column: "TitleId",
                principalTable: "Titles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StuffDetail_WorkLocations_WorkLocationId",
                table: "StuffDetail",
                column: "WorkLocationId",
                principalTable: "WorkLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
