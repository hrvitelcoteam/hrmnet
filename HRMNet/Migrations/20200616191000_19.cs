﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBringAppropriateCandidateStarted",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "CustomerCVSharing",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "CustomerInterview",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "CustomerInterviewResult",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "CustomerMeetingDatetime",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "IsRecruitmentStarted",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "SummaryInformation",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "VitelcoInterview",
                table: "Candidate");

            migrationBuilder.AddColumn<string>(
                name: "BlackListReason",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CVFileName",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Expertises",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsBringAppropriateStarted",
                table: "Positions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOnBlackList",
                table: "Positions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PersonalCharacteristics",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RelatedPositionExperience",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Expertises",
                table: "Candidate",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BlackListReason",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "CVFileName",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Expertises",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "IsBringAppropriateStarted",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "IsOnBlackList",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "PersonalCharacteristics",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "RelatedPositionExperience",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Expertises",
                table: "Candidate");

            migrationBuilder.AddColumn<bool>(
                name: "IsBringAppropriateCandidateStarted",
                table: "Positions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "CustomerCVSharing",
                table: "Candidate",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "CustomerInterview",
                table: "Candidate",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "CustomerInterviewResult",
                table: "Candidate",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CustomerMeetingDatetime",
                table: "Candidate",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsRecruitmentStarted",
                table: "Candidate",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SummaryInformation",
                table: "Candidate",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "VitelcoInterview",
                table: "Candidate",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
