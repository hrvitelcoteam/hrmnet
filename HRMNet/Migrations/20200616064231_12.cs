﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Content",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "CustomerName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "PositionName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ProjectName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "Result",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "CustomerName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "PositionName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "ProjectName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "Result",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "SpeakingPerson",
                table: "Meetings");

            migrationBuilder.AddColumn<string>(
                name: "OContent",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OCustomerName",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ODate",
                table: "Offers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "OPositionName",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OProjectName",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OResult",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MCustomerName",
                table: "Meetings",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "MDate",
                table: "Meetings",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "MPositionName",
                table: "Meetings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MProjectName",
                table: "Meetings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MResult",
                table: "Meetings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MSpeakingPerson",
                table: "Meetings",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OContent",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OCustomerName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ODate",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OPositionName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OProjectName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OResult",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "MCustomerName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MDate",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MPositionName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MProjectName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MResult",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MSpeakingPerson",
                table: "Meetings");

            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerName",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Offers",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "PositionName",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectName",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Result",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerName",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Meetings",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "PositionName",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectName",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Result",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SpeakingPerson",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
