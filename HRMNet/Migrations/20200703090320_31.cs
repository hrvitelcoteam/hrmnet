﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _31 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Positions_MPositionId",
                table: "Meetings");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Positions_PositionId",
                table: "Offers");

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Positions_MPositionId",
                table: "Meetings",
                column: "MPositionId",
                principalTable: "Positions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Positions_PositionId",
                table: "Offers",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Positions_MPositionId",
                table: "Meetings");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Positions_PositionId",
                table: "Offers");

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Positions_MPositionId",
                table: "Meetings",
                column: "MPositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Positions_PositionId",
                table: "Offers",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
