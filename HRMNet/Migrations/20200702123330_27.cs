﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _27 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerName",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "MCustomerName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MDate",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MPositionName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MProjectName",
                table: "Meetings");

            migrationBuilder.AddColumn<string>(
                name: "ProjectName",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PositionId",
                table: "Meetings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_PositionId",
                table: "Meetings",
                column: "PositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Positions_PositionId",
                table: "Meetings",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Positions_PositionId",
                table: "Meetings");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_PositionId",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "ProjectName",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "Meetings");

            migrationBuilder.AddColumn<string>(
                name: "CustomerName",
                table: "Positions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MCustomerName",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "MDate",
                table: "Meetings",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "MPositionName",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MProjectName",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
