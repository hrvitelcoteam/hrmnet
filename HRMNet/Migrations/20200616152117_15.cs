﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PositionFeatures");

            migrationBuilder.DropColumn(
                name: "Tipe",
                table: "Positions");

            migrationBuilder.AddColumn<string>(
                name: "PositionType",
                table: "Positions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PositionType",
                table: "Positions");

            migrationBuilder.AddColumn<string>(
                name: "Tipe",
                table: "Positions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PositionFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeatureId = table.Column<int>(type: "int", nullable: false),
                    IsMandatory = table.Column<bool>(type: "bit", nullable: false),
                    PositionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PositionFeatures_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionFeatures_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PositionFeatures_FeatureId",
                table: "PositionFeatures",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionFeatures_PositionId",
                table: "PositionFeatures",
                column: "PositionId");
        }
    }
}
