﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePosition_Candidate_CandidateId",
                table: "CandidatePosition");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePosition_Position_PositionId",
                table: "CandidatePosition");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidatePosition",
                table: "CandidatePosition");

            migrationBuilder.RenameTable(
                name: "CandidatePosition",
                newName: "CandidatePositions");

            migrationBuilder.RenameIndex(
                name: "IX_CandidatePosition_PositionId",
                table: "CandidatePositions",
                newName: "IX_CandidatePositions_PositionId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidatePosition_CandidateId",
                table: "CandidatePositions",
                newName: "IX_CandidatePositions_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidatePositions",
                table: "CandidatePositions",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Position_PositionId",
                table: "CandidatePositions",
                column: "PositionId",
                principalTable: "Position",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Candidate_CandidateId",
                table: "CandidatePositions");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Position_PositionId",
                table: "CandidatePositions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidatePositions",
                table: "CandidatePositions");

            migrationBuilder.RenameTable(
                name: "CandidatePositions",
                newName: "CandidatePosition");

            migrationBuilder.RenameIndex(
                name: "IX_CandidatePositions_PositionId",
                table: "CandidatePosition",
                newName: "IX_CandidatePosition_PositionId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidatePositions_CandidateId",
                table: "CandidatePosition",
                newName: "IX_CandidatePosition_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidatePosition",
                table: "CandidatePosition",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePosition_Candidate_CandidateId",
                table: "CandidatePosition",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePosition_Position_PositionId",
                table: "CandidatePosition",
                column: "PositionId",
                principalTable: "Position",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
