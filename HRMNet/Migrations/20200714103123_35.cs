﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _35 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CandidateId",
                table: "Positions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Positions_CandidateId",
                table: "Positions",
                column: "CandidateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Candidate_CandidateId",
                table: "Positions",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Candidate_CandidateId",
                table: "Positions");

            migrationBuilder.DropIndex(
                name: "IX_Positions_CandidateId",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "CandidateId",
                table: "Positions");
        }
    }
}
