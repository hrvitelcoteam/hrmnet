﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ClosingDate",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosingOrSuspendingReason",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ExistOnKariyer",
                table: "Positions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ExistOnLinkedIn",
                table: "Positions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsBringAppropriateCandidateStarted",
                table: "Positions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "JobDescription",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ResponsibleId",
                table: "Positions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "StatringDate",
                table: "Positions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Positions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Positions",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "Candidate",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit");

            migrationBuilder.AddColumn<string>(
                name: "BlacklistReason",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CVFilename",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CustomerCVSharing",
                table: "Candidate",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "CustomerInterview",
                table: "Candidate",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "CustomerInterviewResult",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CustomerMeetingDatetime",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsApproved",
                table: "Candidate",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOnBlackList",
                table: "Candidate",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsRecruitmentStarted",
                table: "Candidate",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PositionRelatedExperience",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "VitelcoInterview",
                table: "Candidate",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Expertises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CreatedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatorIp = table.Column<string>(maxLength: 20, nullable: true),
                    ModifierId = table.Column<int>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: true),
                    ModifiedByBrowserName = table.Column<string>(maxLength: 1000, nullable: true),
                    ModifierIp = table.Column<string>(maxLength: 20, nullable: true),
                    ExperiseName = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expertises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PositionFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PositionId = table.Column<int>(nullable: false),
                    FeatureId = table.Column<int>(nullable: false),
                    IsMandatory = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PositionFeatures_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionFeatures_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Types",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeName = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Types", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CandidateExpertises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CandidateId = table.Column<int>(nullable: false),
                    ExpertiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CandidateExpertises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CandidateExpertises_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "Candidate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                        column: x => x.ExpertiseId,
                        principalTable: "Expertises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PositionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PositionId = table.Column<int>(nullable: false),
                    TipeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PositionTypes_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PositionTypes_Types_TipeId",
                        column: x => x.TipeId,
                        principalTable: "Types",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Positions_CustomerId",
                table: "Positions",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CandidateExpertises_CandidateId",
                table: "CandidateExpertises",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CandidateExpertises_ExpertiseId",
                table: "CandidateExpertises",
                column: "ExpertiseId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionFeatures_FeatureId",
                table: "PositionFeatures",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionFeatures_PositionId",
                table: "PositionFeatures",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionTypes_PositionId",
                table: "PositionTypes",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_PositionTypes_TipeId",
                table: "PositionTypes",
                column: "TipeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Customers_CustomerId",
                table: "Positions",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Customers_CustomerId",
                table: "Positions");

            migrationBuilder.DropTable(
                name: "CandidateExpertises");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "PositionFeatures");

            migrationBuilder.DropTable(
                name: "PositionTypes");

            migrationBuilder.DropTable(
                name: "Expertises");

            migrationBuilder.DropTable(
                name: "Types");

            migrationBuilder.DropIndex(
                name: "IX_Positions_CustomerId",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "ClosingDate",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "ClosingOrSuspendingReason",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "ExistOnKariyer",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "ExistOnLinkedIn",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "IsBringAppropriateCandidateStarted",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "JobDescription",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "ResponsibleId",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "StatringDate",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "BlacklistReason",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "CVFilename",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "CustomerCVSharing",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "CustomerInterview",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "CustomerInterviewResult",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "CustomerMeetingDatetime",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "IsApproved",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "IsOnBlackList",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "IsRecruitmentStarted",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "PositionRelatedExperience",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "VitelcoInterview",
                table: "Candidate");

            migrationBuilder.AlterColumn<bool>(
                name: "Status",
                table: "Candidate",
                type: "bit",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
