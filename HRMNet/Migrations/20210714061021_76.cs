﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _76 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "PreCandidateMettings");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "PreCandidate",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "PreCandidate");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "PreCandidateMettings",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
