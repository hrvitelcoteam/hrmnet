﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _28 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OContent",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OCustomerName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ODate",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OPositionName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OProjectName",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OResult",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "MResult",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MSpeakingPerson",
                table: "Meetings");

            migrationBuilder.AddColumn<string>(
                name: "OfferContent",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OfferDate",
                table: "Offers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "OfferResult",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PositionId",
                table: "Offers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "MeetingResult",
                table: "Meetings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MeetingSpeakingPerson",
                table: "Meetings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offers_PositionId",
                table: "Offers",
                column: "PositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Positions_PositionId",
                table: "Offers",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Positions_PositionId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_PositionId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OfferContent",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OfferDate",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OfferResult",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "MeetingResult",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MeetingSpeakingPerson",
                table: "Meetings");

            migrationBuilder.AddColumn<string>(
                name: "OContent",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OCustomerName",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ODate",
                table: "Offers",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "OPositionName",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OProjectName",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OResult",
                table: "Offers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MResult",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MSpeakingPerson",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
