﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _73 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositionHistory_Positions_PositionId",
                table: "CandidatePositionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse",
                column: "CourseId",
                principalTable: "Course",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories",
                column: "InventoryId",
                principalTable: "Inventories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositionHistory_Positions_PositionId",
                table: "CandidatePositionHistory",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositionHistory_Positions_PositionId",
                table: "CandidatePositionHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse",
                column: "CourseId",
                principalTable: "Course",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateExpertises_Expertises_ExpertiseId",
                table: "CandidateExpertises",
                column: "ExpertiseId",
                principalTable: "Expertises",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateInventories_Inventories_InventoryId",
                table: "CandidateInventories",
                column: "InventoryId",
                principalTable: "Inventories",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositionHistory_Positions_PositionId",
                table: "CandidatePositionHistory",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidatePositions_Positions_PositionId",
                table: "CandidatePositions",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id");
        }
    }
}
