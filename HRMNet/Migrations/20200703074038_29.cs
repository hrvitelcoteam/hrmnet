﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _29 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Positions_PositionId",
                table: "Meetings");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_PositionId",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "Meetings");

            migrationBuilder.AddColumn<int>(
                name: "MPositionId",
                table: "Meetings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_MPositionId",
                table: "Meetings",
                column: "MPositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Positions_MPositionId",
                table: "Meetings",
                column: "MPositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Positions_MPositionId",
                table: "Meetings");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_MPositionId",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MPositionId",
                table: "Meetings");

            migrationBuilder.AddColumn<int>(
                name: "PositionId",
                table: "Meetings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_PositionId",
                table: "Meetings",
                column: "PositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Positions_PositionId",
                table: "Meetings",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
