﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _41 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmailPassword",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PortalPassword",
                table: "Candidate",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PortalUsername",
                table: "Candidate",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailPassword",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "PortalPassword",
                table: "Candidate");

            migrationBuilder.DropColumn(
                name: "PortalUsername",
                table: "Candidate");
        }
    }
}
