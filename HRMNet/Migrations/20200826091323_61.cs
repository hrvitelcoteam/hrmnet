﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMNet.Migrations
{
    public partial class _61 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse",
                column: "CourseId",
                principalTable: "Course",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCourse_Course_CourseId",
                table: "CandidateCourse",
                column: "CourseId",
                principalTable: "Course",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
