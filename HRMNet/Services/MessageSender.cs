﻿using HRMNet.Models.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Services
{
    public class MessageSender : IEmailSender, ISmsSender
    {
        private readonly IWebMailService _webMailService;
        //private readonly SmtpConfig _smtp;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IOptionsSnapshot<SiteSettings> _siteOptions;
        public MessageSender(IWebMailService webMailService, IWebHostEnvironment hostingEnvironment, IOptionsSnapshot<SiteSettings> siteOptions)
        {
            _webMailService = webMailService ?? throw new ArgumentNullException(nameof(webMailService));
            _hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
            _siteOptions = siteOptions;
            _siteOptions.CheckArgumentIsNull(nameof(_siteOptions));

            //_smtp = new SmtpConfig()
            //{
            //    Server = _siteOptions.Value.Smtp.Server,
            //    Username = _siteOptions.Value.Smtp.Username,
            //    Password = _siteOptions.Value.Smtp.Password,
            //    //Port = 25,
            //    //Server = "mail.vitelco.com.tr",
            //    //Username = "noreply@vitelco.com.tr",
            //    //Password = "Wb7&+m?r",
            //    Port = 25,
            //    LocalDomain = "",
            //    UsePickupFolder = true,
            //    PickupFolder = "C:\\smtppickup",
            //    FromName = "Vitelco",
            //    FromAddress = "noreply@vitelco.com.tr"
            //};
        }

        public Task SendEmailAsync<T>(string email, string subject, string viewNameOrPath, T model)
        {
            return _webMailService.SendEmailAsync(
                _siteOptions.Value.Smtp,
                new[] { new MailAddress { ToName = "", ToAddress = email } },
                subject,
                viewNameOrPath,
                model
            );
        }


        public async Task SendEmailAsync<T>(string email, string subject, string viewNameOrPath, IEnumerable<string> attachmentFiles, T model)
        {
            var attachs = new List<string>();
            foreach (var item in attachmentFiles)
            {
                attachs.Add(_hostingEnvironment.WebRootPath + item);
            }

            await _webMailService.SendEmailAsync(
                _siteOptions.Value.Smtp,
                new[] { new MailAddress { ToName = "", ToAddress = email } },
                subject,
                viewNameOrPath,
                model,
                null,
                attachs
            );
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            return _webMailService.SendEmailAsync(
                _siteOptions.Value.Smtp,
                new[] { new MailAddress { ToName = "", ToAddress = email } },
                subject,
                message
            );
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
