﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using HRMNet.Models.Identity;
using HRMNet.Models.ViewModel.Identity;

namespace HRMNet.Services.Contracts.Identity
{
    public interface ISiteStatService
    {
        Task<List<User>> GetOnlineUsersListAsync(int numbersToTake, int minutesToTake);

        Task<List<User>> GetTodayBirthdayListAsync();

        Task UpdateUserLastVisitDateTimeAsync(ClaimsPrincipal claimsPrincipal);

        Task<AgeStatViewModel> GetUsersAverageAge();
    }
}