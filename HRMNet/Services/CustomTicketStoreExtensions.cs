﻿using HRMNet.Models.Common;
using HRMNet.Services.Identity;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Services
{
    public static class CustomTicketStoreExtensions
    {
        public static IServiceCollection AddCustomTicketStore(
            this IServiceCollection services, SiteSettings siteSettings)
        {
            var cookieOptions = siteSettings.CookieOptions;
            if (!cookieOptions.UseDistributedCacheTicketStore)
            {
                return services;
            }

            //services.AddDistributedSqlServerCache(options =>
            //{
            //    var cacheOptions = cookieOptions.DistributedSqlServerCacheOptions;
            //    options.ConnectionString = string.IsNullOrWhiteSpace(cacheOptions.ConnectionString) ?
            //            siteSettings.GetMsSqlDbConnectionString() :
            //            cacheOptions.ConnectionString;
            //    options.SchemaName = cacheOptions.SchemaName;
            //    options.TableName = cacheOptions.TableName;
            //});
            //services.AddScoped<ITicketStore, DistributedCacheTicketStore>();

            return services;

        }
    }
}
