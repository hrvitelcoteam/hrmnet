﻿using HRMNet.Data;
using HRMNet.Models.Common;
using HRMNet.Services.Contracts.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Services
{
    public static class DbContextOptionsExtensions
    {
        public static IServiceCollection AddConfiguredDbContext(
            this IServiceCollection services, SiteSettings siteSettings)
        {

            services.AddScoped<IUnitOfWork>(serviceProvider => serviceProvider.GetRequiredService<HRMNetContext>());

            services.AddDbContext<HRMNetContext>(options =>
                options.UseSqlServer(siteSettings.ConnectionStrings.SqlServer.ApplicationDbContextConnection));
            
            return services;

        }

        public static void InitializeDb(this IServiceProvider serviceProvider)
        {
            var scopeFactory = serviceProvider.GetRequiredService<IServiceScopeFactory>();
            using (var scope = scopeFactory.CreateScope())
            {
                var identityDbInitialize = scope.ServiceProvider.GetRequiredService<IIdentityDbInitializer>();
                identityDbInitialize.Initialize();
                identityDbInitialize.SeedData();
            }
        }
    }
}
