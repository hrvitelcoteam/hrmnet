﻿using HRMNet.Models.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Services
{
    public class ColorGenerator : IEnumerable<Color>
    {
        private readonly IEnumerable<int> _indexGenerator;
        public ColorGenerator(IEnumerable<int> indexGenerator)
        {
            _indexGenerator = indexGenerator;
        }

        public IEnumerator<Color> GetEnumerator()
        {
            foreach (var index in _indexGenerator)
            {
                yield return GetColorFromIndex(index);
            }
        }

        private Color GetColorFromIndex(int index)
        {
            byte red = (byte)(index & 0x000000FF);
            byte green = (byte)((index & 0x0000FF00) >> 08);
            byte blue = (byte)((index & 0x00FF0000) >> 16);
            return Color.FromArgb(red, green, blue);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public static class RandomNumber
    {
        static Random random = new Random();
        public static List<int> GenerateRandom(int count, int min, int max)
        {
            
            //  initialize set S to empty
            //  for J := N-M + 1 to N do
            //    T := RandInt(1, J)
            //    if T is not in S then
            //      insert T in S
            //    else
            //      insert J in S
            //
            // adapted for C# which does not have an inclusive Next(..)
            // and to make it from configurable range not just 1.

            if (max <= min || count < 0 ||
                    // max - min > 0 required to avoid overflow
                    (count > max - min && max - min > 0))
            {
                // need to use 64-bit to support big ranges (negative min, positive max)
                throw new ArgumentOutOfRangeException("Range " + min + " to " + max +
                        " (" + ((Int64)max - (Int64)min) + " values), or count " + count + " is illegal");
            }

            // generate count random values.
            HashSet<int> candidates = new HashSet<int>();

            // start count values before max, and end at max
            for (int top = max - count; top < max; top++)
            {
                // May strike a duplicate.
                // Need to add +1 to make inclusive generator
                // +1 is safe even for MaxVal max value because top < max
                if (!candidates.Add(random.Next(min, top + 1)))
                {
                    // collision, add inclusive max.
                    // which could not possibly have been added before.
                    candidates.Add(top);
                }
            }

            // load them in to a list, to sort
            List<int> result = candidates.ToList();

            // shuffle the results because HashSet has messed
            // with the order, and the algorithm does not produce
            // random-ordered results (e.g. max-1 will never be the first value)
            for (int i = result.Count - 1; i > 0; i--)
            {
                int k = random.Next(i + 1);
                int tmp = result[k];
                result[k] = result[i];
                result[i] = tmp;
            }
            return result;
        }
    }


    public static class StringToolkit
    {

        public static List<string> GetRandomColorList(int count)
        {
            List<string> randomColors = new List<string>();
            Color[] colors =
            {
                Color.Aqua, Color.Blue, Color.BlueViolet, Color.Brown, Color.Chartreuse,
                Color.Chocolate, Color.Crimson, Color.DarkGreen,
                Color.DarkMagenta, Color.CornflowerBlue, Color.DarkSalmon, Color.DeepPink,
                Color.DarkGoldenrod, Color.Indigo, Color.LimeGreen, Color.Olive, Color.Red,
                Color.SlateBlue,Color.Yellow
            };

            if (count <= colors.Count())
            {
                while (randomColors.Count != count)
                {
                    Random randomGen = new Random();
                    var randomNumber = randomGen.Next(0, colors.Count()-1);
                    Color c = colors[randomNumber];
                    string cs = HexConverter(c); 
                    if (!randomColors.Contains(cs))
                    {
                        randomColors.Add(cs);
                    }
                }
            }
            else
            {
                while (randomColors.Count<count)
                {
                    Random randomGen = new Random();
                    Color c = Color.FromArgb(randomGen.Next(256), randomGen.Next(256), randomGen.Next(256));
                    string cs = HexConverter(c);
                    if (!randomColors.Contains(cs))
                    {
                        randomColors.Add(cs);
                    }

                }
            }

            
            return randomColors;
        }

        public static string RandomColor()
        {
            Random randomGen = new Random();
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(MyFavoriteKnownColor));
            KnownColor randomColorName = names[randomGen.Next(names.Length)];
            Color randomColor = Color.FromKnownColor(randomColorName);
            return HexConverter(randomColor);
        }

        private static String HexConverter(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
    }
}
