﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Services
{
    public static class DateToolkit
    {
        public static Tuple<DateTime, DateTime> SpliteDateRange(string range)
        {
            if (string.IsNullOrWhiteSpace(range) || !range.Contains('-'))
                return null;

            var dates = range.Split('-');

            if (dates.Length != 2)
                return null;


            var start = DateTime.ParseExact(dates[0].Trim(), "MM/dd/yyyy",
                System.Globalization.CultureInfo.InvariantCulture);

            var end = DateTime.ParseExact(dates[1].Trim(), "MM/dd/yyyy",
                System.Globalization.CultureInfo.InvariantCulture);

            return new Tuple<DateTime, DateTime>(start, end.AddDays(1));
        }
    }
}
