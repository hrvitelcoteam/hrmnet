﻿using System;
using System.Security.Claims;
using HRMNet.Services.Contracts.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using HRMNet.Models.Identity;
using HRMNet.Data;

namespace HRMNet.Services.Identity
{
    public class ApplicationRoleStore :
        RoleStore<Role, HRMNetContext, int, UserRole, RoleClaim>,
        IApplicationRoleStore
    {
        private readonly IdentityErrorDescriber _describer;
        private readonly IUnitOfWork _uow;

        public ApplicationRoleStore(
            IUnitOfWork uow,
            IdentityErrorDescriber describer)
            : base((HRMNetContext)uow, describer)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _describer = describer ?? throw new ArgumentNullException(nameof(_describer));
        }

        #region BaseClass

        protected override RoleClaim CreateRoleClaim(Role role, Claim claim)
        {
            return new RoleClaim
            {
                RoleId = role.Id,
                ClaimType = claim.Type,
                ClaimValue = claim.Value
            };
        }

        #endregion

        #region CustomMethods

        #endregion
    }
}