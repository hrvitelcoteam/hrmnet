﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);

        Task SendEmailAsync<T>(string email, string subject, string viewNameOrPath, T model);

        Task SendEmailAsync<T>(string email, string subject, string viewNameOrPath, IEnumerable<string> attachmentFiles, T model);

    }
}
