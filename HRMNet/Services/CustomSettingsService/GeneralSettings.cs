﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Services.CustomSettingsService
{
    public class GeneralSettings: SettingsBase
    {
        public GeneralSettings()
        {
            
        }
        [Display(Name = "Sending Email Body")]
        public virtual string SendingEmailGeneralBody { get; set; }

        public virtual string AttachFile { get; set; }

    }
}
