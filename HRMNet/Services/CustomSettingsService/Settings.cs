﻿using HRMNet.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Services.CustomSettingsService
{
    public interface ISettings
    {
        GeneralSettings General { get; }
        
        Task<bool> Save();
    }

    public class Settings : ISettings
    {
        private readonly Lazy<GeneralSettings> _generalSettings;
        public GeneralSettings General { get { return _generalSettings.Value; } }
        private readonly IUnitOfWork _uow;

        public Settings(IUnitOfWork uow)
        {
            // ARGUMENT CHECKING SKIPPED FOR BREVITY
            _uow = uow;
            _generalSettings = new Lazy<GeneralSettings>(CreateSettings<GeneralSettings>);
        }
        
        public async Task<bool> Save()
        {
            // only save changes to settings that have been loaded
            if (_generalSettings.IsValueCreated)
                _generalSettings.Value.Save(_uow);

            var result = await _uow.SaveChangesAsync();
            if (result>0) return true;

            return false;

        }
        private T CreateSettings<T>() where T : SettingsBase, new()
        {
            var settings = new T();
            settings.Load(_uow);
            return settings;
        }
    }
}
