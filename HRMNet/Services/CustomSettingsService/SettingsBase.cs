﻿using HRMNet.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using HRMNet.Models.Common;
using Microsoft.EntityFrameworkCore;

namespace HRMNet.Services.CustomSettingsService
{
    public abstract class SettingsBase
    {
        private readonly string _name;
        private readonly PropertyInfo[] _properties;
        private DbSet<CustomSetting> _settings;
        public SettingsBase()
        {
            var type = this.GetType();
            _name = type.Name;
            _properties = type.GetProperties();
        }

        public virtual void Load(IUnitOfWork uow)
        {
            _settings = uow.Set<CustomSetting>();
            var settings = _settings.Where(w => w.Type == _name).ToList();

            foreach (var propertyInfo in _properties)
            {
                // get the setting from the settings list
                var setting = settings.SingleOrDefault(s => s.Name == propertyInfo.Name);
                if (setting != null)
                {
                    propertyInfo.SetValue(this, Convert.ChangeType(setting.Value, propertyInfo.PropertyType));
                }
            }
        }

        public virtual void Save(IUnitOfWork uow)
        {
            _settings = uow.Set<CustomSetting>();
            var settings = _settings.Where(w => w.Type == _name).ToList();

            foreach (var propertyInfo in _properties)
            {
                object propertyValue = propertyInfo.GetValue(this, null);
                string value = (propertyValue == null) ? null : propertyValue.ToString();

                var setting = settings.SingleOrDefault(s => s.Name == propertyInfo.Name);
                if (setting != null)
                {
                    setting.Value = value;
                }
                else
                {
                    var newSetting = new CustomSetting()
                    {
                        Name = propertyInfo.Name,
                        Type = _name,
                        Value = value,
                    };

                    _settings.Add(newSetting);
                    //uow.SaveChanges(true);
                }
            }
        }
    }
}
