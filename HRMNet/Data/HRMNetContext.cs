﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using System.Threading;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.AspNetCore.Http;
using HRMNet.Models.Positions;
using HRMNet.Models.Customers;
using System.Security.Cryptography.X509Certificates;
using HRMNet.Models.Learning;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using HRMNet.Models.Identity;
using System.Globalization;
using Microsoft.EntityFrameworkCore.Storage;
using HRMNet.Models.AuditableEntity;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using HRMNet.Services;
using Microsoft.EntityFrameworkCore.DataEncryption;
using Microsoft.EntityFrameworkCore.DataEncryption.Providers;
using DNTCommon.Web.Core;
using HRMNet.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using HRMNet.Models.Timesheet;
using HRMNet.Models.Recruit;
using HRMNet.Models.Workflow;
using HRMNet.Models.Sops;

namespace HRMNet.Data
{
    public class HRMNetContext : AuditableIdentityContext,
        IUnitOfWork
    {

        private IDbContextTransaction _transaction;
        private readonly IProtectionProviderService _protectionProviderService;
        private readonly IEncryptionProvider _provider;
        private readonly IHttpContextAccessor _contextAccessor;
        public HRMNetContext(DbContextOptions options, IProtectionProviderService protectionProviderService, IHttpContextAccessor contextAccessor)
            : base(options)
        {
            _protectionProviderService = protectionProviderService ?? throw new ArgumentNullException(nameof(_protectionProviderService));
            _contextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(_contextAccessor));
            //this._provider = new CustomEncryptionProvider(_protectionProviderService);
            this._provider = new CustomEncryptionProvider();
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseLoggerFactory(loggerFactory);
        //}


        public virtual DbSet<AppLogItem> AppLogItems { get; set; }
        public virtual DbSet<AppSqlCache> AppSqlCache { get; set; }

        public virtual DbSet<AppDataProtectionKey> AppDataProtectionKeys { get; set; }

        public void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            Set<TEntity>().AddRange(entities);
        }

        public void BeginTransaction()
        {
            _transaction = Database.BeginTransaction();
        }

        public void RollbackTransaction()
        {
            if (_transaction == null)
            {
                throw new NullReferenceException("Please call `BeginTransaction()` method first.");
            }
            _transaction.Rollback();
        }

        public void CommitTransaction()
        {
            if (_transaction == null)
            {
                throw new NullReferenceException("Please call `BeginTransaction()` method first.");
            }
            _transaction.Commit();
        }

        public override void Dispose()
        {
            _transaction?.Dispose();
            base.Dispose();
        }

        public void ExecuteSqlInterpolatedCommand(FormattableString query)
        {
            Database.ExecuteSqlInterpolated(query);
        }

        public void ExecuteSqlRawCommand(string query, params object[] parameters)
        {
            Database.ExecuteSqlRaw(query, parameters);
        }

        public T GetShadowPropertyValue<T>(object entity, string propertyName) where T : IConvertible
        {
            var value = this.Entry(entity).Property(propertyName).CurrentValue;
            return value != null
                ? (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture)
                : default;
        }

        public object GetShadowPropertyValue(object entity, string propertyName)
        {
            return this.Entry(entity).Property(propertyName).CurrentValue;
        }

        public void MarkAsChanged<TEntity>(TEntity entity) where TEntity : class
        {
            Update(entity);
        }

        public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            Set<TEntity>().RemoveRange(entities);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            ChangeTracker.DetectChanges();

            beforeSaveTriggers();

            ChangeTracker.AutoDetectChangesEnabled = false; // for performance reasons, to avoid calling DetectChanges() again.
            var result = base.SaveChanges(acceptAllChangesOnSuccess);
            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges(); //NOTE: changeTracker.Entries<T>() will call it automatically.

            beforeSaveTriggers();

            ChangeTracker.AutoDetectChangesEnabled = false; // for performance reasons, to avoid calling DetectChanges() again.
            var result = base.SaveChanges();
            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            ChangeTracker.DetectChanges();

            beforeSaveTriggers();

            ChangeTracker.AutoDetectChangesEnabled = false; // for performance reasons, to avoid calling DetectChanges() again.
            var result = base.SaveChangesAsync(_contextAccessor?.HttpContext?.User?.Identity?.Name);
            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            ChangeTracker.DetectChanges();

            beforeSaveTriggers();

            ChangeTracker.AutoDetectChangesEnabled = false; // for performance reasons, to avoid calling DetectChanges() again.
            var result = base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }

        private void beforeSaveTriggers()
        {
            validateEntities();
            setShadowProperties();
        }

        private void setShadowProperties()
        {
            // we can't use constructor injection anymore, because we are using the `AddDbContextPool<>`
            var props = this.GetService<IHttpContextAccessor>()?.GetShadowProperties();
            ChangeTracker.SetAuditableEntityPropertyValues(props);
        }

        private void validateEntities()
        {
            var errors = this.GetValidationErrors();
            if (!string.IsNullOrWhiteSpace(errors))
            {
                // we can't use constructor injection anymore, because we are using the `AddDbContextPool<>`
                var loggerFactory = this.GetService<ILoggerFactory>();
                var logger = loggerFactory.CreateLogger<HRMNetContext>();
                logger.LogError(errors);
                throw new InvalidOperationException(errors);
            }
        }


        public virtual DbSet<Candidate> Candidate { get; set; }
        public virtual DbSet<CandidateFeature> CandidateFeatures { get; set; }
        public virtual DbSet<CandidatePosition> CandidatePositions { get; set; }

        public virtual DbSet<CandidateExpertise> CandidateExpertises { get; set; }

        public virtual DbSet<Feature> Features { get; set; }

        public virtual DbSet<Position> Positions { get; set; }

        public virtual DbSet<Expertise> Expertises { get; set; }


        public virtual DbSet<Offer> Offers { get; set; }

        public virtual DbSet<Meeting> Meetings { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }

        //public virtual DbSet<Tipe> Types { get; set; }

        public virtual DbSet<PositionFeature> PositionFeatures { get; set; }

        public virtual DbSet<PositionMandatoryFeature> PositionMandatoryFeatures { get; set; }

        public virtual DbSet<PositionType> PositionTypes { get; set; }

        public virtual DbSet<CurrentPosition> CurrentPositions { get; set; }

        public virtual DbSet<Attachment> Attachments { get; set; }

        public virtual DbSet<Inventory> Inventories { get; set; }

        public virtual DbSet<WorkLocation> WorkLocations { get; set; }

        public virtual DbSet<Title> Titles { get; set; }

        public virtual DbSet<InventoryType> InventoryTypes { get; set; }

        public virtual DbSet<StuffDetail> StuffDetails { get; set; }


        public virtual DbSet<CandidateInventory> CandidateInventories { get; set; }

        public DbSet<Leaves> Leaves { get; set; }

        public DbSet<Course> Course { get; set; }

        public DbSet<CandidateCourse> CandidateCourse { get; set; }

        public DbSet<Disciplin> Disciplin { get; set; }

        public DbSet<Target> RecrutingTargets { get; set; }


        public virtual DbSet<PreCandidateAndCandidateViewModel> PreCandidateAndCandidateViewModel { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            // it should be placed here, otherwise it will rewrite the following settings!
            base.OnModelCreating(builder);

            builder.UseEncryption(this._provider);
            // we can't use constructor injection anymore, because we are using the `AddDbContextPool<>`
            // Adds all of the ASP.NET Core Identity related mappings at once.
            builder.AddCustomIdentityMappings(this.GetService<IOptionsSnapshot<SiteSettings>>()?.Value);

            builder.Entity<Candidate>(build =>
            {
                build.Property(c => c.Name).HasMaxLength(450).IsRequired();
                build.Property(c => c.Family).HasMaxLength(450).IsRequired();
                build.HasMany(x => x.Offers).WithOne(v => v.Candidate).HasForeignKey(b => b.CandidateId).OnDelete(DeleteBehavior.Cascade);
                build.HasMany(x => x.Meetings).WithOne(v => v.Candidate).HasForeignKey(b => b.CandidateId).OnDelete(DeleteBehavior.Cascade);
                build.HasMany(x => x.Attachments).WithOne(x => x.Candidate).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
                build.HasOne(x => x.StuffDetail).WithOne(z => z.Candidate).OnDelete(DeleteBehavior.Cascade);
                //build.HasMany(x=> x.PositionsHistory).onthOne(y=> y)
                //build.HasMany(x => x.CandidatePositions).WithOne(z => z.Candidate).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
                //build.HasMany(x => x.CandidateFeatures).WithOne(z => z.Candidate).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
                //build.HasMany(x => x.CandidateExpertises).WithOne(z => z.Candidate).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
                //build.HasMany(x => x.CandidateInventories).WithOne(z => z.Candidate).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
                //build.HasMany(x => x.CandidateCourses).WithOne(z => z.Candidate).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
                build.HasMany(x => x.Disciplins).WithOne(z => z.Candidate).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
                //build.HasMany(x => x.CandidatePositionsHistory).WithOne(z => z.Candidate).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);

            });
            
            //builder.Entity<Position>(build =>
            //{
            //    //build.HasMany(x => x.CandidatePositionsHistory).WithOne(z => z.Position).HasForeignKey(x => x.PositionId).OnDelete(DeleteBehavior.NoAction);
            //    //build.HasMany(x => x.CandidatePositions).WithOne(z => z.Position).HasForeignKey(x => x.PositionId).OnDelete(DeleteBehavior.NoAction);

            //});

            //builder.Entity<Course>(build =>
            //{
            //    //build.HasMany(x => x.CandidateCourses).WithOne(z => z.Course).HasForeignKey(x => x.CourseId).OnDelete(DeleteBehavior.NoAction);
            //});

            //builder.Entity<Inventory>(build =>
            //{
            //    //build.HasMany(x => x.CandidateInventories).WithOne(z => z.Inventory).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.NoAction);
            //});

            //builder.Entity<Expertise>(build =>
            //{
            //    //build.HasMany(x => x.CandidateExpertises).WithOne(z => z.Expertise).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.NoAction);
            //});

            builder.Entity<Feature>(build =>
            {
                //build.HasMany(x => x.CandidateFeatures).WithOne(z => z.Feature).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
                build.Property(c => c.FeatureName).HasMaxLength(450).IsRequired();
            });

            builder.Entity<Customer>(build =>
            {
                build.Property(c => c.Name).HasMaxLength(450).IsRequired();
                build.HasMany(x => x.Docs).WithOne(x => x.Customer).HasForeignKey(x => x.CustomerId).OnDelete(DeleteBehavior.Cascade);
                build.HasOne(x => x.Target).WithOne(x => x.Customer).OnDelete(DeleteBehavior.Cascade);

            });

            //builder.Entity<Target>(build =>
            //{
            //    build.HasOne(x => x.Customer).WithOne(z => z.Target).HasForeignKey("TargetId").OnDelete(DeleteBehavior.ClientSetNull);

            //});

            builder.Entity<Meeting>(build =>
            {
                build.HasOne(d => d.MPosition)
                    .WithMany(p => p.Meetings)
                    .HasForeignKey(d => d.MPositionId);
                //.OnDelete(DeleteBehavior.NoAction);
            });

            builder.Entity<Offer>(build =>
            {
                build.HasOne(d => d.Position)
                    .WithMany(p => p.Offers)
                    .HasForeignKey(d => d.PositionId);
                //.OnDelete(DeleteBehavior.NoAction);
            });

            builder.Entity<Position>(build =>
            {
                build.Property(c => c.PositionName).HasMaxLength(450).IsRequired();
                //build.HasMany(x => x.TimesheetItems)
                //    .WithOne(x => x.Position)
                //    .HasForeignKey(x => x.PositionId)
                //    .OnDelete(DeleteBehavior.SetNull);

            });

            builder.Entity<Expertise>(build =>
            {
                build.Property(c => c.ExpertiseName).HasMaxLength(450).IsRequired();
            });

            builder.Entity<CandidateFeature>(build =>
            {
                build.HasOne(x => x.Feature).WithMany(z => z.CandidateFeatures).HasForeignKey(x => x.FeatureId).OnDelete(DeleteBehavior.Cascade);
                build.HasOne(x => x.Candidate).WithMany(z => z.CandidateFeatures).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<CandidateInventory>(build =>
            {
                build.HasOne(x => x.Inventory).WithMany(z => z.CandidateInventories).HasForeignKey(x => x.InventoryId).OnDelete(DeleteBehavior.Cascade);
                build.HasOne(x => x.Candidate).WithMany(z => z.CandidateInventories).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<CandidateCourse>(build =>
            {
                build.HasOne(x => x.Course).WithMany(z => z.CandidateCourses).HasForeignKey(x => x.CourseId).OnDelete(DeleteBehavior.Cascade);
                build.HasOne(x => x.Candidate).WithMany(z => z.CandidateCourses).HasForeignKey(x => x.CandidateId).OnDelete(DeleteBehavior.Cascade);
            });


            builder.Entity<CandidateExpertise>(build =>
            {

                build.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidateExpertises)
                    .HasForeignKey(d => d.CandidateId).OnDelete(DeleteBehavior.Cascade);


                build.HasOne(d => d.Expertise)
                    .WithMany(p => p.CandidateExpertises)
                    .HasForeignKey(d => d.ExpertiseId).OnDelete(DeleteBehavior.Cascade);

            });

            builder.Entity<CandidatePosition>(build =>
            {
                build.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidatePositions)
                    .HasForeignKey(d => d.CandidateId).OnDelete(DeleteBehavior.Cascade);

                build.HasOne(d => d.Position)
                    .WithMany(p => p.CandidatePositions)
                    .HasForeignKey(d => d.PositionId).OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<CandidatePositionHistory>(build =>
            {
                build.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidatePositionsHistory)
                    .HasForeignKey(d => d.CandidateId).OnDelete(DeleteBehavior.Cascade);

                build.HasOne(d => d.Position)
                    .WithMany(p => p.CandidatePositionsHistory)
                    .HasForeignKey(d => d.PositionId).OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<Sop>(build =>
            {
                build
                .HasOne(d => d.Customer)
                .WithMany(x => x.Sops)
                .HasForeignKey(x => x.CustomerId)
                .OnDelete(DeleteBehavior.NoAction);

            });

                //builder.Entity<PositionFeature>(build =>
                //{

                //    build.HasOne(d => d.Position)
                //        .WithMany(p => p.PositionFeatures)
                //        .HasForeignKey(d => d.PositionId);


                //    build.HasOne(d => d.Feature)
                //        .WithMany(p => p.PositionFeatures)
                //        .HasForeignKey(d => d.FeatureId);

                //});

                //builder.Entity<PositionMandatoryFeature>(build =>
                //{

                //    build.HasOne(d => d.Position)
                //        .WithMany(p => p.PositionMandatoryFeatures)
                //        .HasForeignKey(d => d.PositionId);


                //    build.HasOne(d => d.Feature)
                //        .WithMany(p => p.PositionMandatoryFeatures)
                //        .HasForeignKey(d => d.FeatureId);

                //});

                //builder.Entity<PositionType>(build =>
                //{

                //    build.HasOne(d => d.Position)
                //        .WithMany(p => p.PositionTypes)
                //        .HasForeignKey(d => d.PositionId);


                //    build.HasOne(d => d.Expertise)
                //        .WithMany(p => p.PositionTypes)
                //        .HasForeignKey(d => d.ExpertiseId);

                //});

                //builder.Entity<CandidateCourse>(build =>
                //{

                //    build.HasOne(d => d.Candidate)
                //        .WithMany(p => p.CandidateCourses)
                //        .HasForeignKey(d => d.CandidateId);
                //    //.OnDelete(DeleteBehavior.Cascade);


                //    build.HasOne(d => d.Course)
                //        .WithMany(p => p.CandidateCourses)
                //        .HasForeignKey(d => d.CourseId);
                //        //.OnDelete(DeleteBehavior.Cascade);
                //});

                // This should be placed here, at the end.
                builder.AddAuditableShadowProperties();

        }

        public DbSet<PreCandidate> PreCandidate { get; set; }

        public DbSet<TimesheetItem> Timesheet { get; set; }

        public DbSet<WorkflowStateDateTime> WorkflowStateDateTimes { get; set; }

        public DbSet<Sop> Sops { get; set; }

        public DbSet<SopPosition> SopPositions { get; set; }

        public DbSet<CustomSetting> CustomSettings { get; set; }
    }
}
