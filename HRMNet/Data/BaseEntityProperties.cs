﻿using HRMNet.Models.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Data
{
    public static class BaseEntityProperties
    {
        public static void DefineBaseEntityProperties(this ModelBuilder modelBuilder)
        {
            foreach (var entityType in modelBuilder.Model
                                                   .GetEntityTypes()
                                                   .Where(e => typeof(BaseEntity).IsAssignableFrom(e.ClrType)))
            {
                modelBuilder.Entity(entityType.ClrType)
                            .Property<string>("CreatorIp").HasMaxLength(20);
                modelBuilder.Entity(entityType.ClrType)
                            .Property<string>("ModifierIp").HasMaxLength(20);

                modelBuilder.Entity(entityType.ClrType)
                            .Property<string>("CreatedByBrowserName").HasMaxLength(1000);
                modelBuilder.Entity(entityType.ClrType)
                            .Property<string>("ModifiedByBrowserName").HasMaxLength(1000);

            }
        }

        public static void SetBaseEntityPropertyValues(
            this ChangeTracker changeTracker,
            IHttpContextAccessor httpContextAccessor)
        {
            var httpContext = httpContextAccessor?.HttpContext;
            var userAgent = httpContext?.Request?.Headers["User-Agent"].ToString();
            var userIp = httpContext?.Connection?.RemoteIpAddress?.ToString();
            var now = DateTime.UtcNow;
            var userId = getUserId(httpContext);

            var modifiedEntries = changeTracker.Entries<BaseEntity>()
                                               .Where(x => x.State == EntityState.Modified);
            foreach (var modifiedEntry in modifiedEntries)
            {
                modifiedEntry.Property("ModifiedDateTime").CurrentValue = now;
                modifiedEntry.Property("ModifiedByBrowserName").CurrentValue = userAgent;
                modifiedEntry.Property("ModifierIp").CurrentValue = userIp;
                modifiedEntry.Property("ModifierId").CurrentValue = userId;
            }

            var addedEntries = changeTracker.Entries<BaseEntity>()
                                            .Where(x => x.State == EntityState.Added);
            foreach (var addedEntry in addedEntries)
            {
                addedEntry.Property("CreatedDateTime").CurrentValue = now;
                addedEntry.Property("CreatedByBrowserName").CurrentValue = userAgent;
                addedEntry.Property("CreatorIp").CurrentValue = userIp;
                addedEntry.Property("CreatorId").CurrentValue = userId;
            }
        }

        private static int? getUserId(HttpContext httpContext)
        {
            // should be comment after authentication is active
            return 1;
            //int? userId = null;
            //var userIdValue = httpContext?.User?.Identity?.GetUserId();
            //if (!string.IsNullOrWhiteSpace(userIdValue))
            //{
            //    userId = int.Parse(userIdValue);
            //}
            //return userId;
        }

        
    }
}
