﻿using HRMNet.Models.Candidates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Data
{
    public class PreCandidateAndCandidateViewModelConfiguration : IEntityTypeConfiguration<PreCandidateAndCandidateViewModel>
    {
        public void Configure(EntityTypeBuilder<PreCandidateAndCandidateViewModel> build)
        {
            build.HasNoKey().ToView(null);
        }
    }
}
