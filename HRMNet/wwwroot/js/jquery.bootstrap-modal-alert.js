﻿// <![CDATA[
(function ($) {
    $.bootstrapModalAlert = function (options) {
        var defaults = {
            caption: 'Confirm action',
            body: 'Do you want to perform the action?'
        };
        options = $.extend(defaults, options);

        var alertContainer = "#alertContainer";
        var html = '<div class="modal fade" id="alertContainer">' +
                   '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">' +
                        '<a class="close" data-dismiss="modal">&times;</a>'
                        + '<h5>' + options.caption + '</h5></div>' +
                   '<div class="modal-body">'
                        + options.body + '</div></div></div></div></div>';

        $(alertContainer).modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        $(alertContainer).remove();
        $(html).appendTo('body');
        $(alertContainer).modal('show');
    };
})(jQuery);
// ]]>