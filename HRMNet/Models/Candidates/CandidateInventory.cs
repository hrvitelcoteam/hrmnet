﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class CandidateInventory: BaseEntity
    {

        public virtual Candidate Candidate { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        [DisplayName("Employee")]
        public string EmployeeName { get; set; }
        public int CandidateId { get; set; }

        public virtual Inventory Inventory { get; set; }

        public int InventoryId { get; set; }

        [Required(ErrorMessage = "(*)")]
        [DisplayName("Delivery Date")]
        public virtual DateTime DeliveryDate { get; set; }

        [DisplayName("Operation Type")]
        public virtual OperationType OperationType { get; set; }

        [Display(Name = "Status")]
        public virtual InventoryStatus Status { get; set; }
    }
}
