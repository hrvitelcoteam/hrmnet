﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    [NotMapped]
    public class PreCandidateAndCandidateViewModel
    {
        public int Id { get; set; }

        public int CandidateId { get; set; }

        public int PreCandidateId { get; set; }

        [Display(Name = "Name")]
        
        public string Name { get; set; }

        [Display(Name = "Surename")]
        
        public string Family { get; set; }

        [Display(Name = "Name Surename")]
        public string DisplayName { get; set; }

        [Display(Name = "Phone")]
        public string Telephone { get; set; }

        public string Email { get; set; }

        [Display(Name = "CV Source")]
        public CVSource CVSource { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        [Display(Name = "Candidate / Precandidate")]
        public CandidatePrecandidate CandidatePrecandidate { get; set; }

        [Display(Name = "Is Candidate")]
        public int IsCandidate { get; set; }


        [Display(Name = "Last Interview Date")]
        public DateTime? LastInterviewDate { get; set; }


        [Display(Name = "Customer")]
        public string CustomerName { get; set; }

        [Display(Name = "Position")]
        public string PositionName { get; set; }

        [Display(Name = "Status")]
        public PreCandidateStatusByType GeneralStatus { get; set; }

        public PreCandidateStatus Status { get; set; }

        public int IsInBlackList { get; set; }

        public string Result { get; set; }

        [Display(Name = "Candidate Owner")]
        public string CandidateOwner { get; set; }

        [Display(Name = "Creator")]
        public string Creator { get; set; }

        public int CreatorId { get; set; }

        [Display(Name = "Modifier")]
        public string Modifier { get; set; }

        public int? ModifierId { get; set; }

        public int RecordCount { get; set; }

        public long RowNumber { get; set; }




    }

    public class PreCandidateViewModel
    {
        public int Id { get; set; }

        public int CandidateId { get; set; }

        public int PreCandidateId { get; set; }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Surename")]
        public string Family { get; set; }

        [Display(Name = "Name Surename")]
        public string DisplayName
        {
            get => Name + " " + Family;

            set => DisplayName = value;
        }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Phone")]
        public string Telephone { get; set; }

        [Required(ErrorMessage = "(*)")]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string Email { get; set; }

        [Display(Name = "CV Source")]
        public CVSource CVSource { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        [Display(Name = "Candidate / Precandidate")]
        public CandidatePrecandidate CandidatePrecandidate { get; set; }

        [Display(Name = "Is Candidate")]
        public bool IsCandidate { get; set; }

        //[DataType(DataType.Date)]
        [Display(Name = "First Interview Date")]
        public DateTime? FirstInterviewDate { get; set; }

        [Display(Name = "Last Interview Date")]
        public DateTime? LastInterviewDate { get; set; }


        [Display(Name = "Interview Date")]
        public DateTime? InterviewDate { get; set; }

        [Display(Name = "Customer")]
        public string CustomerName { get; set; }

        [Display(Name = "Position")]
        public string PositionName { get; set; }

        [Display(Name = "Status")]
        public PreCandidateStatusByType GeneralStatus { get; set; }

        public PreCandidateStatus Status { get; set; }

        public bool IsInBlackList { get; set; }

        public string Result { get; set; }

        [Display(Name = "Candidate Owner")]
        public string CandidateOwner { get; set; }

        [Display(Name = "Creator")]
        public string Creator { get; set; }

        public int CreatorId { get; set; }

        [Display(Name = "Modifier")]
        public string Modifier { get; set; }

        public int? ModifierId { get; set; }




    }
}
