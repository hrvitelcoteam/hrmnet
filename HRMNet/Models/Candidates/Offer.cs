﻿using HRMNet.Models.Common;
using HRMNet.Models.Customers;
using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class Offer : BaseEntity
    {
        [DataType(DataType.Date)]
        [Display(Name = "Offer Date")]
        public virtual DateTime OfferDate { get; set; }

        //[Display(Name = "Position Name")]
        //public virtual string OPositionName {get; set;}

        //[Display(Name = "Customer Name")]
        //public virtual string OCustomerName { get; set; }

        //[Display(Name = "Project Name")]
        //public virtual string OProjectName { get; set; }

        [Display(Name = "Offer Content")]
        public virtual string OfferContent { get; set; }

        [Display(Name = "Offer Result")]
        public virtual string OfferResult { get; set; }

        public virtual Position Position { get; set; }

        public int PositionId { get; set; }

        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }


    }
}
