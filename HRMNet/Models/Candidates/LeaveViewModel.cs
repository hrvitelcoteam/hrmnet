﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class LeaveViewModel
    {

        public int CandidateId { get; set; }

        [Display(Name = "Name Surename")]
        public string CandidateName { get; set; }

        [Display(Name = "Total Legal Leave (Day)")]
        public double TotalLegalLeave { get; set; }

        [Display(Name = "Total DayOff Leave (Day)")]
        public double TotalDayOffLeave { get; set; }

        [Display(Name = "Total Leave (Day)")]
        public double TotalLeave { get; set; }

        [Display(Name = "Employee Id")]
        public string BirthNumber { get; set; }
    }
}
