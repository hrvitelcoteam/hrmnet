﻿using HRMNet.Models.Common;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class Disciplin : BaseEntity
    {
        public Disciplin()
        {
            Attachments = new HashSet<Attachment>();
        }
        [Required]
        [Display(Name = "Result")]
        public virtual DisciplinResult ActionType { get; set; }

        [Required]
        [Display(Name = "Subject")]
        public virtual string Subject { get; set; }

        [Required]
        [Display(Name = "Defense Date")]
        [DataType(DataType.Date)]
        public virtual DateTime DefenseDate { get; set; }

        [Required]
        [Display(Name = "Notification Date")]
        [DataType(DataType.Date)]
        public virtual DateTime NotificationDate { get; set; }

        [Display(Name = "Description")]
        public virtual string Description { get; set; }

        public virtual Candidate Candidate {get; set; }

        public int CandidateId { get; set; }

        [Display(Name = "Total")]
        [NotMapped]
        public int Total { get; set; }

        [Display(Name = "Name Surename")]
        [NotMapped]
        public string CandidateName { get; set; }

        [NotMapped]
        public AttachmentViewModel AttachmentViewModel { get; set; }
        
        [NotMapped]
        [Display(Name = "Employee Id")]
        public string EmployeeId { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }

    }
}
