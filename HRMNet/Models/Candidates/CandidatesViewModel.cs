﻿using cloudscribe.Web.Pagination;
using HRMNet.Models.ViewModel.Candidate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class CandidatesViewModel
    {
        public CandidatesViewModel()
        {
            Candidates = new HashSet<Candidate>();
            Paging = new PaginationSettings();
            //CandidateViewModel = new HashSet<CandidateViewModel>();
        }

        public CandidateSearchViewModel Search { get; set; }

        public PaginationSettings Paging { get; set; }

        public ICollection<Candidate> Candidates { get; set; }

        //public ICollection<CandidateViewModel> CandidateViewModel { get; set; }
    }
}
