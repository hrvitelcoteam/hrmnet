﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class PositionKeyValue
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
