﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class InventoryType: BaseEntity
    {
        [DisplayName("Category Name")]
        public virtual string Name { get; set; }
    }
}
