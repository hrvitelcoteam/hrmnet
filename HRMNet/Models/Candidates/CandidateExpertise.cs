﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class CandidateExpertise
    {
        public virtual int Id { get; set; }

        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }

        public virtual Expertise Expertise { get; set; }

        public int ExpertiseId { get; set; }


    }
}
