﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class StuffDetail: BaseEntity
    {
        [Display(Name = "TC. Number")]
        public virtual string TCNumber { get; set; }

        [Display(Name = "Accommodation Address")]
        public virtual string AccommodationAddress { get; set; }

        [Display(Name = "Military Status")]
        public virtual MilitaryStatus MilitaryStatus { get; set; }

        [Display(Name = "Postponemented Date")]

        public virtual DateTime? PostponementdDate { get; set; }

        [Display(Name = "Contract Type")]
        public virtual ContractType ContractType { get; set; }

        [Display(Name = "Contract Duration")]
        public virtual string ContractDuration { get; set; }

        public virtual Title Title { get; set; }

        [Display(Name = "Title")]
        public int? TitleId { get; set; }

        [Display(Name = "Working Location")]
        public virtual WorkLocation WorkingLocation { get; set; }

        [Display(Name = "Working Location")]
        public int? WorkLocationId { get; set; }

        [Display(Name = "School Of Graduation")]
        public virtual string SchoolOfGraduation { get; set; }

        [Display(Name = "Department Of Graduation")]
        public virtual string DepartmentOfGraduation { get; set; }

        [Display(Name = "Date Of Graduation")]
        public virtual DateTime DateOfGraduation { get; set; }

        public virtual ICollection<Inventory> Inventories { get; set; }

        [Display(Name = "IBAN Number")]
        public virtual string IBANNumber { get; set; }

        [Display(Name = "Company Email")]
        public virtual string CompanyEmail { get; set; }

        [Display(Name = "Manager")]
        public virtual string Manager {  get; set; }

        //public int ManagerId { get; set; }
        [Display(Name = "Blood Group")]
        public virtual string BloodGroup { get; set; }

        [Display(Name = "Emergency Contact Information")]
        public virtual string EmergencyContactInformation { get; set; }

        [Display(Name = "Employee Id")]
        public virtual string BirthCertificateNumber { get; set; }

        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }
    }
}
