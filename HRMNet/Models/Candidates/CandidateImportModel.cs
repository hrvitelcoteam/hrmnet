﻿using CsvHelper.Configuration;
using System.Globalization;

namespace HRMNet.Models.Candidates
{
    public class CandidateImportModel
    {
        public CandidateImportModel()
        {
        }
        public string Name { get; set; }
        public string Family { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string StartPeriod { get; set; }
        public string CurrentWages { get; set; }
        public string ExpectedWages { get; set; }
        public string TotalITexperience { get; set; }
        public string PositionRelatedExperience { get; set; }
        public string Customer { get; set; }

        public string Position { get; set; }

        public string ProjectName { get; set; }
        public string InterviewDate { get; set; }
        public string MeetingSpeakingPerson { get; set; }
        public string MeetingResult { get; set; }
        public bool IsHavingCV { get; set; }
        public string CVfilename { get; set; }
        public bool IsOnBlackList { get; set; }
        public string BlacklistReason { get; set; }
        public bool HasError { get; set; } = false;
        public string ErrorMessage { get; set; }
    }
    public sealed class CandidateImportModelMap : ClassMap<CandidateImportModel>
    {
        public CandidateImportModelMap()
        {
            AutoMap(CultureInfo.InvariantCulture);
            Map(m => m.CVfilename).Name("CV filename");
            Map(m => m.TotalITexperience).Name("TotalITExperience");
            Map(m => m.PositionRelatedExperience).Name("PositionRelated Experience");
            Map(m => m.MeetingResult).Ignore();
            Map(m => m.HasError).Ignore();
            Map(m => m.ErrorMessage).Ignore();
        }
    }
}