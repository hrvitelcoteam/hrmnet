﻿
using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.Learning;
using HRMNet.Models.Positions;
using HRMNet.Models.Workflow;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Policy;
using System.Text;

namespace HRMNet.Models.Candidates
{
    public class Candidate: BaseEntity
    {
        public Candidate()
        {
            CandidatePositions = new HashSet<CandidatePosition>();
            Offers = new HashSet<Offer>();
            Meetings = new HashSet<Meeting>();
            VitelcoDocs = true;
            Attachments = new HashSet<Attachment>();
            //StuffDetail = new StuffDetail();
            Leaves = new HashSet<Leaves>();
            WorkflowStateDateTimes = new HashSet<WorkflowStateDateTime>();
        }

        [NotMapped]
        [Display(Name = "Name")]
        public string NameAlone { get; set; }

        [NotMapped]
        [Display(Name = "Name Surename")]
        public string DisplayName
        {
            get => Name + " " + Family;

            set => DisplayName = value;
        }

        
        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Name Surename")]
        [Encrypted]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Surename")]
        [Encrypted]
        public virtual string Family { get; set; }


        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Phone")]
        //[Encrypted]
        [Remote("ValidatePhone", "Candidates",
            AdditionalFields = nameof(Telephone) + "," + ViewModelConstants.AntiForgeryToken + "," + nameof(Id),
            HttpMethod = "POST")]
        [StringLength(50)]
        public virtual string Telephone { get; set; }

        [Required(ErrorMessage = "(*)")]
        //[RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format")]
        //[EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        //[Encrypted]
        //[MaxLength(50, ErrorMessage = "Max length is 50")]
        [Remote("ValidateEmail", "Candidates",
            AdditionalFields = nameof(Email) + "," + ViewModelConstants.AntiForgeryToken + "," + nameof(Id),
            HttpMethod = "POST")]
        [Display(Name = "Email")]
        [StringLength(250)]
        public virtual string Email { get; set; }

        [Display(Name = "Notice Period(Week)")]
        [RegularExpression(@"^\d+$", ErrorMessage = "You must enter integer value")]
        //[MaxLength(2, ErrorMessage = "Max length is 2")]
        public virtual string StartPeriod { get; set; }

        [Display(Name = "Current Salary(TL)")]
        [Encrypted]
        //[Required]
        //[MaxLength(6,ErrorMessage ="Max is 999999")]
        [RegularExpression(@"^\d+$", ErrorMessage = "You must enter integer value")]
        //[MinLength(4, ErrorMessage = "Minimum is 1000")]
        public virtual string CurrentWages { get; set; }

        [Display(Name = "Expected Salary(TL)")]
        [Encrypted]
        [Required]
        [MinLength(4,ErrorMessage = "Minimum is 1000")]
        //[MaxLength(6, ErrorMessage = "Max is 999999")]
        [RegularExpression(@"^\d+$", ErrorMessage = "You must enter integer value")]
        public virtual string ExpectedWages { get; set; }

        [Display(Name = "Status")]
        public virtual CandidateStatus Status { get; set; }

        [Display(Name = "Having CV")]
        public virtual bool IsHavingCV { get; set; }

        [Display(Name = "Total IT Exp(Year)")]
        [RegularExpression(@"^\d+$",ErrorMessage ="You must enter integer value")]
        //[MaxLength(2, ErrorMessage = "Max is 99")]
        public virtual string TotalITexperience { get; set; }

        [Display(Name = "Position Related Exp(Year)")]
        [RegularExpression(@"^\d+$", ErrorMessage = "You must enter integer value")]
        //[MaxLength(2, ErrorMessage = "Max is 99")]
        public virtual string PositionRelatedExperience { get; set; }

        [Display(Name = "CV Upload")]
        public virtual string CVFilename { get; set; }

        [Display(Name = "Candidate Summary")]
        public virtual string BriefPersonalInfomation { get; set; }

        //[Display(Name = "Job Description")]
        //public virtual string JobDescription { get; set; }

        //[Display(Name = "Customer CV Sharing")]
        //public virtual bool CustomerCVSharing { get; set; }

        //[Display(Name = "Has Customer Interview been done?")]
        //public virtual bool CustomerInterview { get; set; }

        [Display(Name = "Position")]
        [NotMapped]
        public string Positions { get; set; }

        [Display(Name = "Vitelco Docs")]
        [NotMapped]
        public bool VitelcoDocs { get; set; }

        [Display(Name = "Customer Docs")]
        [NotMapped]
        public bool CustomerDocs { get; set; }

        [Display(Name = "Customer")]
        [NotMapped]
        public string CustomerId { get; set; }

        //[Display(Name = "Customer Meeting Datetime")]
        //public virtual DateTime? CustomerMeetingDatetime { get; set; }

        //[Display(Name = "Customer Interview Result")]
        //public virtual string CustomerInterviewResult { get; set; }

        //[Display(Name = "Is the Vitelco Interview done?")]
        //public virtual bool VitelcoInterview { get; set; }

        [Display(Name = "Is candidate on the Black List?")]
        public virtual bool IsOnBlackList { get; set; }

        [Display(Name = "Blacklist Reason")]
        public virtual string BlacklistReason { get; set; }

        [Display(Name = "Is Approved")]
        public virtual bool IsApproved { get; set; }

        //[Display(Name = "Is Recruitment Started")]
        //public virtual bool IsRecruitmentStarted { get; set; }

        [Display(Name = "Expertises")]
        public virtual string Expertises { get; set; }

        [Display(Name = "Features")]
        public virtual string Features { get; set; }

        [NotMapped]
        public string InitialType { get; set; }

        [NotMapped]
        public string TypesSource { get; set; }

        [NotMapped]
        [Display(Name = "Expertise Types")]
        public string[] SelectedTypes { get; set; }

        [NotMapped]
        public string InitialPosition { get; set; }

        [NotMapped]
        public string PositionsSource { get; set; }

        [NotMapped]
        [Display(Name = "Positions")]
        public string[] SelectedPositions { get; set; }

        [NotMapped]
        public string InitialFeature { get; set; }

        [NotMapped]
        public string FeaturesSource { get; set; }

        [NotMapped]
        [Display(Name = "Features")]
        public string[] SelectedFeatures { get; set; }

        [NotMapped]
        [Display(Name = "Creator")]
        public string Creator { get; set; }

        [NotMapped]
        [Display(Name = "Modifier")]
        public string Modifier { get; set; }

        [NotMapped]
        public AttachmentViewModel AttachmentViewModel { get; set; }

        //[ProtectedPersonalData]
        //[Display(Name = "Portal Username")]
        //[Remote("ValidateUsername", "Candidates",
        //    AdditionalFields = nameof(Email) + "," + ViewModelConstants.AntiForgeryToken,
        //    HttpMethod = "POST")]
        [Encrypted]
        [Display(Name = "Portal Username")]
        public virtual string PortalUsername { get; set; }

        //[ProtectedPersonalData]
        [Display(Name = "Portal Password")]
        //[StringLength(100, ErrorMessage = "{0} Must be at least {2} and at most {1} characters.", MinimumLength = 6)]
        //[Remote("ValidatePassword", "Candidates",
        //    AdditionalFields = nameof(PortalUsername) + "," + ViewModelConstants.AntiForgeryToken, HttpMethod = "POST")]
        [DataType(DataType.Password)]
        [Encrypted]
        public virtual string PortalPassword { get; set; }

        //[ProtectedPersonalData]
        [Display(Name = "Email Password")]
        //[StringLength(100, ErrorMessage = "{0} Must be at least {2} and at most {1} characters.", MinimumLength = 6)]
        //[Remote("ValidatePassword", "Candidates",
        //    AdditionalFields = nameof(VitelcoEmail) + "," + ViewModelConstants.AntiForgeryToken, HttpMethod = "POST")]
        [DataType(DataType.Password)]
        [Encrypted]
        public virtual string EmailPassword { get; set; }

        public virtual StuffDetail StuffDetail { get; set; }

        [NotMapped]
        public List<SelectListItem> WorkflowBasedStatusList { get; set; }

        [NotMapped]
        [Display(Name = "Current Status")]
        public int CurrentStatus { get; set; }

        [NotMapped]
        [Display(Name = "Next Status")]
        public int NextStatus { get; set; }

        [NotMapped]
        [Display(Name = "Vitelco Email")]
        [EmailAddress(ErrorMessage = "Please enter valid email address.")]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format")]
        public string VitelcoEmail { get; set; }

        [NotMapped]
        [Display(Name = "Do you want to create a user?")]
        public bool CreateAUser { get; set; }

        public virtual int RelatedUserId { get; set; }

        [NotMapped]
        public string StatusName { get; set; }

        [NotMapped]
        public string PositionListString { get; set; }

        //[NotMapped]
        //public int TotalLegalLeave { get; set; }

        //[NotMapped]
        //public int TotalDayOffLeave { get; set; }

        //[NotMapped]
        //public int TotalLeave { get; set; }

        //[NotMapped]
        //public string BirthNumber { get; set; }


        //public int StuffDetailId { get; set; }

        //[HiddenInput]
        //public string Pid { set; get; }

        //public virtual User User { get; set; }

        //public int? UserId { get; set; }

        public virtual ICollection<CandidatePosition> CandidatePositions { get; set; }

        public virtual ICollection<CandidatePositionHistory> CandidatePositionsHistory { get; set; }

        public virtual ICollection<CandidateFeature> CandidateFeatures { get; set; }

        //public virtual ICollection<Position> PositionsHistory { get; set; }

        public virtual ICollection<Meeting> Meetings { get; set; }

        public virtual ICollection<Offer> Offers { get; set; }

        public virtual ICollection<CandidateExpertise> CandidateExpertises { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }

        //public virtual ICollection<Inventory> Inventories{ get; set; }

        public virtual ICollection<Leaves> Leaves { get; set; }

        public virtual ICollection<CandidateInventory> CandidateInventories { get; set; }

        public virtual ICollection<CandidateCourse> CandidateCourses { get; set; }

        public virtual ICollection<Disciplin> Disciplins { get; set; }

        public virtual ICollection<WorkflowStateDateTime> WorkflowStateDateTimes { get; set; }
        
    }
}
