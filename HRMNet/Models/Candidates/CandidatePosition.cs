﻿
using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.Text;

namespace HRMNet.Models.Candidates
{
    public class CandidatePosition
    {
        public int Id { get; set; }
        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }

        public virtual Position Position {get; set;}

        public int PositionId { get; set; }
    }

    public class CandidatePositionHistory
    {
        public int Id { get; set; }
        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }

        public virtual Position Position { get; set; }

        public int PositionId { get; set; }
    }

    
}
