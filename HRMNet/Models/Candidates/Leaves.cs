﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class Leaves: BaseEntity
    {
        [Display(Name = "Start Date")]
        public virtual DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        public virtual DateTime EndDate { get; set; }

        [NotMapped]
        [Display(Name = "Duration")]
        public string Duration {
            get{

                return (EndDate - StartDate).Hours.ToString() + " Hours";
            }
            set { Duration = value; }
        }

        [NotMapped]
        [Display(Name = "Total Legal Leave")]
        public string TotalLegalLeave { get; set; }

        [NotMapped]
        [Display(Name = "Total DayOff Leave")]
        public string TotalDayOffLeave { get; set; }

        [NotMapped]
        [Display(Name = "Total Leave")]
        public string TotalLeave { get; set; }

        [Display(Name = "Permission Type")]
        public virtual PermissionType PermissionType { get; set; }

        [Display(Name = "Leave Type")]
        public virtual LeaveType LeaveType { get; set; }

        [Display(Name = "Description")]
        public virtual string Description { get; set; }

        public virtual Candidate Candidate { get; set; }

        [Display(Name = "Employee")]
        public int CandidateId { get; set; }

        
    }
}
