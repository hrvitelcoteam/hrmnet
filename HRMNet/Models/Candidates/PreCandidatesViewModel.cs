﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class PreCandidatesViewModel
    {
        public PreCandidatesViewModel()
        {
            PreCandidates = new HashSet<PreCandidateViewModel>();
            PreCandidate = new PreCandidate();
        }
        public ICollection<PreCandidateViewModel> PreCandidates { get; set; }

        public PreCandidateSearchViewModel Search { get; set; }

        public PreCandidate PreCandidate { get; set; }
    }
}
