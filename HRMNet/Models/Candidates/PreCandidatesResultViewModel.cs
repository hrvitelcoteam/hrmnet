﻿using cloudscribe.Web.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class PreCandidatesAndCandidateResultViewModel
    {
        public PreCandidatesAndCandidateResultViewModel()
        {
            PreCandidatesAndCandidate = new HashSet<PreCandidateAndCandidateViewModel>();
            Paging = new PaginationSettings();
            Search = new PreCandidateSearchViewModel();
        }
        public ICollection<PreCandidateAndCandidateViewModel> PreCandidatesAndCandidate { get; set; }

        public PreCandidate PreCandidate { get; set; }

        public string Result { get; set; }

        public PaginationSettings Paging { get; set; }

        public PreCandidateSearchViewModel Search { get; set; }
    }

    public class PreCandidatesResultViewModel
    {
        public PreCandidatesResultViewModel()
        {
            PreCandidates = new HashSet<PreCandidateViewModel>();
            Paging = new PaginationSettings();
            Search = new PreCandidateSearchViewModel();
        }
        public ICollection<PreCandidateViewModel> PreCandidates { get; set; }

        public PreCandidate PreCandidate { get; set; }

        public string Result { get; set; }

        public PaginationSettings Paging { get; set; }

        public PreCandidateSearchViewModel Search { get; set; }
    }
}
