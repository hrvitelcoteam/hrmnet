﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class CurrentPosition : BaseEntity
    {
        [Display(Name = "Position")]
        public virtual string PositionName { get; set; }

        [Display(Name = "Customer")]
        public virtual string CustomerName {get; set;}

        [Display(Name = "Project")]
        public virtual string ProjectName { get; set; }

        [Display(Name = "Evaluated Date")]
        public virtual DateTime? EvaluatedDate { get; set; }

        public virtual DateTime? CVSharingDateWithCustomer { get; set; }

        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }




    }
}
