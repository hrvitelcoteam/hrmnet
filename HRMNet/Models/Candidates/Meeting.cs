﻿using HRMNet.Models.Common;
using HRMNet.Models.Customers;
using HRMNet.Models.Positions;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class Meeting: BaseEntity
    {

        //[Display(Name = "Position Name")]
        //[NotMapped]
        //public virtual string MPositionName { get; set; }

        //[Display(Name = "Customer Name")]
        //[NotMapped]
        //public virtual string MCustomerName { get; set; }

        //[Display(Name = "Project Name")]
        //[NotMapped]
        //public virtual string MProjectName { get; set; }

        //[NotMapped]
        [Display(Name = "Meeting Date")]
        [DataType(DataType.Date)]
        public virtual DateTime MeetingDate { get; set; }

        [Display(Name = "Person")]
        public virtual string MeetingSpeakingPerson { get; set; }

        [Display(Name = "Meeting Result")]
        public virtual string MeetingResult { get; set; }


        public virtual Position MPosition { get; set; }

        public int MPositionId { get; set; }

        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }
    }
}
