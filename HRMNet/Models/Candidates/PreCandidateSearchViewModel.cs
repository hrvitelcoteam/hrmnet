﻿using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class PreCandidateSearchViewModel
    {

        [Display(Name = "Customer Name")]
        [Required(ErrorMessage = "*")]
        public int CustomerId { get; set; }

        [Display(Name = "Position Name")]
        [Required(ErrorMessage = "*")]
        public int PositionId { get; set; }

        [Display(Name = "Features")]
        public string Features { get; set; }

        [Display(Name = "Must To Have")]
        public string MustToHave { get; set; }

        [Display(Name = "Expertises")]
        public string Expertises { get; set; }

        [Display(Name = "Candidate Owner")]
        public string OwnerId { get; set; }

        [Display(Name = "Date Range")]
        public string DateRange { get; set; }

        public string Phone { get; set; }
    }
}
