﻿using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class PreCandidateMettings: BaseEntity
    {
        public virtual PreCandidate PreCandidate { get; set; }

        public int PreCandidateId { get; set; }

        public virtual Position Position { get; set; }

        public int PositionId { get; set; }

        [Display(Name = "Note")]
        [StringLength(2000)]
        public virtual string Note { get; set; }
    }
}
