﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class CandidateSearchViewModel
    {
        [Display(Name = "Date Range")]
        public string DateRange { get; set; }

        [Display(Name = "  ")]
        //[Required]
        public string KeySearch { get; set; }

        [Display(Name = "  ")]
        public int? KeyType { get; set; }

        public string Sort { get; set; } = "Id";

        public SortOrder Order { get; set; } = SortOrder.Descending;




    }
}
