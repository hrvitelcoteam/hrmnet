﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class AssingedPositionsData
    {
        public int PositionId { get; set; }

        public string Title { get; set; }

        public bool Assigned { get; set; }
    }
}
