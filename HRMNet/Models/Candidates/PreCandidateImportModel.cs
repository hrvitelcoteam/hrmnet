﻿using CsvHelper.Configuration;
using System.Globalization;

namespace HRMNet.Models.Candidates
{
    public class PreCandidateImportModel
    {
        public PreCandidateImportModel()
        {
        }

        public string Name { get; set; }

        public string Family { get; set; }

        public string Telephone { get; set; }
        public string Email { get; set; }
        public int CVSource { get; set; }
        public int Status { get; set; }
        public string Position { get; set; }

        public string Customer { get; set; }

        public string ProjectName { get; set; }

        public string PositionNote { get; set; }
        public string InterviewDate { get; set; }

        public bool HasError { get; set; } = false;
        public string ErrorMessage { get; set; }

    }

    public sealed class PreCandidateImportModelMap : ClassMap<PreCandidateImportModel>
    {
        public PreCandidateImportModelMap()
        {
            AutoMap(CultureInfo.InvariantCulture);
            Map(m => m.PositionNote).Name("Position Note");
            Map(m => m.HasError).Ignore();
            Map(m => m.ErrorMessage).Ignore();
        }
    }
}
