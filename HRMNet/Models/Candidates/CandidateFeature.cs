﻿
using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HRMNet.Models.Candidates
{
    public class CandidateFeature
    {

        public virtual int Id { get; set; }
        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }

        public virtual Feature Feature { get; set; } 

        public int FeatureId { get; set; }
    }
}
