﻿using HRMNet.Models.Learning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class CandidateCourse
    {
        public virtual int Id { get; set; }

        public virtual Candidate Candidate { get; set; }

        [Display(Name = "Employee Name")]
        public int CandidateId { get; set; }

        public virtual Course Course { get; set; }

        [Display(Name = "Course Name")]
        public int? CourseId { get; set; }

        [Display(Name = "Trainer")]
        public virtual string Professor { get; set; }

        [Display(Name = "Course Name")]
        public virtual string Name { get; set; }

        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        public virtual DateTime? StartDate { get; set; }

        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        public virtual DateTime? EndDate { get; set; }

        [Display(Name = "Description")]
        public virtual string Description { get; set; }

        [Display(Name = "Is Internal")]
        [NotMapped]
        public bool IsInternal { get; set; }

        [Display(Name = "Internal Course")]
        [NotMapped]
        public int TotalInternal { get; set; }

        [Display(Name = "External Course")]
        [NotMapped]
        public int TotalExternal { get; set; }


        [Display(Name = "Total")]
        [NotMapped]
        public int Total { get; set; }

        [NotMapped]
        [Display(Name = "Name Surename")]
        public string CandidateDisplayName { get; set; }


        [NotMapped]
        [Display(Name = "Employee Id")]
        public string EmployeeId { get; set; }

    }
}
