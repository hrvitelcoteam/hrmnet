﻿using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.Recruit;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class Title : BaseEntity
    {
        //[MaxLength(450, ErrorMessage = "Maximum length is 450")]
        [Display(Name = "Title")]
        public virtual string Name { get; set; }

        [Display(Name = "Target (Month)")]
        public virtual int Target { get; set; }
        //public virtual RecruitingTarget RecruitingTarget { get; set; }

        //public int RecruitingTargetId { get; set; }

        public virtual ICollection<StuffDetail> StuffDetails {get; set;}

        public virtual ICollection<User> Users { get; set; }


    }
}
