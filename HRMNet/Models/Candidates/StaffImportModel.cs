﻿using CsvHelper.Configuration;
using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class StaffImportModel
    {
        public StaffImportModel()
        {
        }
        public string Name { get; set; }
        public string Family { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string StartPeriod { get; set; }
        public string CurrentWages { get; set; }
        public string ExpectedWages { get; set; }
        public string TotalITexperience { get; set; }
        public string PositionRelatedExperience { get; set; }

        public string Customer { get; set; }
        public string Position { get; set; }

        public string ProjectName { get; set; }
        public string InterviewDate { get; set; }
        public string MeetingSpeakingPerson { get; set; }
        public string MeetingResult { get; set; }
        public bool IsHavingCV { get; set; }
        public string CVfilename { get; set; }
        public bool IsOnBlackList { get; set; }
        public string BlacklistReason { get; set; }

        public string TCNumber { get; set; }

        public string AccommodationAddress { get; set; }

        public MilitaryStatus MilitaryStatus { get; set; }

        public DateTime? PostponementdDate { get; set; }

        public ContractType ContractType { get; set; }

        public string ContractDuration { get; set; }

        public string SchoolOfGraduation { get; set; }

        public string DepartmentOfGraduation { get; set; }

        public DateTime? DateOfGraduation { get; set; }

        public string IBANNumber { get; set; }

        public string CompanyEmail { get; set; }

        public string BloodGroup { get; set; }

        public string EmergencyContactInformation { get; set; }

        public string BirthCertificateNumber { get; set; }

        public string WorkLocation { get; set; }

        public string Title { get; set; }

        public bool ActiveWorker { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public bool HasError { get; set; } = false;
        public string ErrorMessage { get; set; }
    }

    public sealed class StaffImportModelMap : ClassMap<StaffImportModel>
    {
        public StaffImportModelMap()
        {
            AutoMap(CultureInfo.InvariantCulture);
            Map(m => m.CVfilename).Name("CV filename");
            Map(m => m.HasError).Ignore();
            Map(m => m.ErrorMessage).Ignore();
            Map(m => m.StartPeriod).Ignore();
            Map(m => m.InterviewDate).Ignore();
            Map(m => m.MeetingSpeakingPerson).Ignore();
            Map(m => m.MeetingResult).Ignore();
            Map(m => m.IsHavingCV).Ignore();
            Map(m => m.CVfilename).Ignore();
            Map(m => m.IsOnBlackList).Ignore();
            Map(m => m.BlacklistReason).Ignore();
        }
    }
}
