﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class Inventory: BaseEntity
    {
        [DisplayName("Place Of Purchase(Date)")]
        public virtual string PurchasedLocationAndDate { get; set; }

        
        [DisplayName("Operating System Key")]
        public virtual string WindowsPassword { get; set; }

        [DisplayName("Operating System")]
        public virtual string Windows { get; set; }

        [DisplayName("Antivirus Software Key")]
        public virtual string VirusProgramPassword { get; set; }

        [DisplayName("Antivirus Software")]
        public virtual string VirusProgram { get; set; }
        
        [DisplayName("Office Software Key")]
        public virtual string OfficeProgramKey { get; set; }

        [DisplayName("Office Software")]
        public virtual string OfficeProgram { get; set; }

        [DisplayName("Password Hint")]
        public virtual string PasswordHint { get; set; }

        [DisplayName("Windows User Password")]
        public virtual string WindowsUserPassword { get; set; }

        [DisplayName("Windows Username")]
        public virtual string WindowsUsername { get; set; }

        [Required(ErrorMessage = "(*)")]
        [DisplayName("Serial Number")]
        public virtual string SerialNumber { get; set; }

        [Required(ErrorMessage = "(*)")]
        [DisplayName("Model")]
        public virtual string Model { get; set; }

        [Required(ErrorMessage = "(*)")]
        [DisplayName("Brand")]
        public virtual string Brand { get; set; }

        

        [DisplayName("Inventory Number")]
        [Required(ErrorMessage = "(*)")]
        public virtual string InventoryNumber { get; set; }
        
        public virtual InventoryType InventoryType { get; set; }


        [DisplayName("Inventory Type")]
        public int InventoryTypeId { get; set; }

        [NotMapped]
        [DisplayName("Is Used")]
        public bool IsUsed { get; set; }
        

        //[DisplayName("Employee")]
        //public virtual Candidate Candidate { get; set; }

        //[DisplayName("Employee")]
        //public int? CandidateId { get; set; }

        public virtual ICollection<CandidateInventory> CandidateInventories { get; set; }
    }
}
