﻿using HRMNet.Models.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Candidates
{
    public class PreCandidate: BaseEntity
    {
        public PreCandidate()
        {
            PreCandidateMeetings = new HashSet<PreCandidateMettings>();
            CreatedDateTime = DateTime.Now;

        }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Name")]
        [Encrypted]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Surename")]
        [Encrypted]
        public virtual string Family { get; set; }

        [NotMapped]
        [Display(Name = "Name Surename")]
        public string DisplayName
        {
            get => Name + " " + Family;

            set => DisplayName = value;
        }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Phone")]
        [StringLength(50)]
        //[Encrypted]
        //[Remote("ValidatePhone", "Candidates",
        //    AdditionalFields = nameof(Telephone) + "," + ViewModelConstants.AntiForgeryToken + "," + nameof(Id),
        //    HttpMethod = "POST")]
        public virtual string Telephone { get; set; }

        //[Required(ErrorMessage = "(*)")]
        //[RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format")]
        //[EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        //[Encrypted]
        [Display(Name = "Email")]
        [StringLength(250)]
        public virtual string Email { get; set; }

        [NotMapped]
        [Display(Name = "First Interview Date")]
        public virtual DateTime? FirstInterviewDate { get; set; }

        [NotMapped]
        [Display(Name = "Last Contact Date")]
        public virtual DateTime? LastInterviewDate { get; set; }

        [Display(Name = "CV Source")]
        [Required(ErrorMessage = "(*)")]
        public virtual CVSource CVSource { get; set; }

        [Display(Name = "Status")]
        [Required(ErrorMessage = "(*)")]
        public virtual PreCandidateStatus Status { get; set; }

        [Display(Name = "Current Status")]
        [NotMapped]
        public virtual PreCandidateStatus CurrentStatus { get; set; }

        [Display(Name = "Note")]
        public virtual string Note { get; set; }

        [Display(Name = "Send Email")]
        public virtual bool SendMail { get; set; }

        [NotMapped]
        [Display(Name = "IsCandidate")]
        public bool IsCandidate { get; set; }

        [NotMapped]
        public int? PreCandidateId { get; set; }

        [NotMapped]
        public int? CandidateId { get; set; }

        [NotMapped]
        [Display(Name = "Language")]
        public string Language { get; set; }

        [NotMapped]
        public int SelectedPositionId { get; set; }

        [Display(Name = "Candidate Owner")]
        [NotMapped]
        public string CandidateOwner { get; set; }

        [NotMapped]
        public CandidatePrecandidate CandidatePrecandidate { get; set; }

        public virtual ICollection<PreCandidateMettings> PreCandidateMeetings { get; set; }

    }
}
