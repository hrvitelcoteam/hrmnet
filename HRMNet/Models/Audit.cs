﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models
{

    public class Audit
    {
        public int Id { get; set; }

        [Display(Name = "Table Name")]
        public string TableName { get; set; }
       
        [Display(Name = "Date Time")]
        public DateTime DateTime { get; set; }

        [Display(Name = "Key Values")]
        public string KeyValues { get; set; }

        [Display(Name = "Old Values")]
        public string OldValues { get; set; }

        [Display(Name = "New Values")]
        public string NewValues { get; set; }

        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]  
        [MaxLength(50)]
        [Display(Name = "Action")]
        public string Action { get; set; }
    }

    public class AuditEntry
    {
        public AuditEntry(EntityEntry entry)
        {
            Entry = entry;
        }

        public EntityEntry Entry { get; }
        public string TableName { get; set; }

        public string Username { get; set; }

        public string Action { get; set; }

        public Dictionary<string, object> KeyValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> OldValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> NewValues { get; } = new Dictionary<string, object>();
        public List<PropertyEntry> TemporaryProperties { get; } = new List<PropertyEntry>();

        public bool HasTemporaryProperties => TemporaryProperties.Any();

        public Audit ToAudit()
        {
            var audit = new Audit();
            audit.TableName = TableName;
            audit.DateTime = DateTime.UtcNow;
            audit.KeyValues = JsonConvert.SerializeObject(KeyValues);
            audit.OldValues = OldValues.Count == 0 ? null : JsonConvert.SerializeObject(OldValues); // In .NET Core 3.1+, you can use System.Text.Json instead of Json.NET
            audit.NewValues = NewValues.Count == 0 ? null : JsonConvert.SerializeObject(NewValues);
            audit.Action = Action;
            audit.Username = Username;
            return audit;
        }
    }
}
