﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Sops
{
    public class SopsViewModel
    {
        public SopsViewModel()
        {
            Sops = new HashSet<Sop>();
        }
        public ICollection<Sop> Sops { get; set; }

        public Sop Sop { get; set; }
    }
}
