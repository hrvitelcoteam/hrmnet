﻿using HRMNet.Models.Common;
using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Sops
{
    public class SopPosition: BaseEntity
    {
        public virtual Sop Sop { get; set; }

        public int SopId { get; set; }

        public virtual Position Position { get; set; }

        public int PositionId { get; set; }

    }
}
