﻿using HRMNet.Models.Common;
using HRMNet.Models.Customers;
using HRMNet.Models.Identity;
using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Sops
{
    public class Sop: BaseEntity
    {
        public Sop()
        {
            SopPositions = new HashSet<SopPosition>();
            Attachments = new HashSet<Attachment>();
        }

        [StringLength(450)]
        [Required]
        [Display(Name = "Demand Name")]
        public virtual string DemandName { get; set; }

        [Required]
        [Display(Name = "Demand Type")]
        public virtual SOPDemandType DemandType { get; set; }

        [StringLength(450)]
        [Required]
        [Display(Name = "Contact Person")]
        public virtual string ContactPerson { get; set; }

        [Display(Name = "Owner")]
        public virtual User Owner { get; set; }

        [Display(Name = "Vitelco Owner")]
        public int OwnerId { get; set; }

        [Display(Name = "Priority")]
        public virtual int Priority { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        public virtual DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        public virtual DateTime EndTime { get; set; }

        [Display(Name = "Project Duration (Month)")]
        public virtual int Duration { get; set; }

        [Display(Name = "Status")]
        public virtual SOPStatus Status { get; set; }

        [Display(Name = "Result")]
        public virtual SopsResult Result { get; set; }

        [StringLength(450)]
        [Display(Name = "Comment")]
        public virtual string Comment { get; set; }

        [Display(Name = "Customer")]
        public virtual Customer Customer { get; set; }

        [Display(Name = "Customer")]
        public int? CustomerId { get; set; }

        [NotMapped]
        [Display(Name = "Positions")]
        public string[] selectedPositions { get; set; }

        [NotMapped]
        public string AllPositions { get; set; }

        [NotMapped]
        public AttachmentViewModel AttachmentViewModel { get; set; }

        public virtual ICollection<SopPosition> SopPositions { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }
    }
}
