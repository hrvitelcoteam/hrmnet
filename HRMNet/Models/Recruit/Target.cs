﻿using HRMNet.Models.Common;
using HRMNet.Models.Customers;
using HRMNet.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Recruit
{
    public class Target: BaseEntity
    {
        [Display(Name = "First Interviewed")]
        public virtual int FirstInterviewedCount { get; set; }

        [Display(Name = "Submitted To Management")]
        public virtual int SubmittedToManagement { get; set; }

        [Display(Name = "Shared With Customer")]
        public virtual int SharedWithCustomer { get; set; }

        [Display(Name = "Customer Choosen")]
        public virtual int CustomerChoosen { get; set; }

        [Display(Name = "Proposed")]
        public virtual int Proposed { get; set; }

        [Display(Name = "Accepted Offer")]
        public virtual int AcceptedOffer { get; set; }

        [Display(Name = "Productivity")]
        public virtual double Productivity { get; set; }

        public virtual Customer Customer { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }
    }
}
