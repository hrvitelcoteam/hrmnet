﻿using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Learning
{
    public class Course : BaseEntity
    {

        public Course()
        {
            CandidateCourses = new HashSet<CandidateCourse>();
        }

        [Display(Name = "Name")]
        public virtual string Name { get; set; }

        [NotMapped]
        [Display(Name = "Course Name")]
        public string CourseName { get; set; }

        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        public virtual DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        public virtual DateTime EndDate { get; set; }

        [Display(Name = "Professor")]
        public virtual string Professor { get; set; }

        [Display(Name = "Duration (Day)")]
        public virtual int Duration { get; set; }

        [Display(Name = "Description")]
        public virtual string Description { get; set; }

        //public virtual Candidate Candidate { get; set; }

        //public int CandidateId { get; set; }

        //public virtual User User { get; set; }

        //public int? UserId { get; set; }

        public virtual ICollection<CandidateCourse> CandidateCourses { get; set; }
    }
}
