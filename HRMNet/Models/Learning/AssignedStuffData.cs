﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Learning
{
    public class AssignedStuffData
    {
        public int StuffId { get; set; }

        [Display(Name = "Name Surename")]
        public string Name { get; set; }

        [Display(Name = "Telephone")]
        public string Telephone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Status")]
        public CandidateStatus Status { get; set; }

        [Display(Name = "Assigned")]
        public bool Assigned { get; set; }
    }
}
