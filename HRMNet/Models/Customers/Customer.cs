﻿using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.Positions;
using HRMNet.Models.Recruit;
using HRMNet.Models.Sops;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Customers
{
    public class Customer: BaseEntity
    {
        public Customer()
        {
            Docs = new HashSet<Attachment>();
        }

        [Display(Name = "Customer Name")]
        public virtual string Name { get; set; }

        [Display(Name = "Will the standard mail body be customized?")]
        public virtual bool IsContentUsesForEmail { get; set; }


        [Display(Name = "Document e-mail content")]
        public virtual string Content { get; set; }

        [NotMapped]
        public AttachmentViewModel AttachmentViewModel { get; set; }

        public virtual Target Target { get; set; }

        //public virtual User User { get; set; }

        //public int? UserId { get; set; }

        public virtual ICollection<Position> Positions { get; set; }

        public virtual ICollection<Attachment> Docs { get; set; }

        public virtual ICollection<Sop> Sops { get; set; }

        //public virtual ICollection<Meeting> Meetings { get; set; }
        
        //public virtual ICollection<Offer> Offers { get; set; }

    }
}
