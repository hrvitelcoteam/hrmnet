﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Candidate
{
    public class CandidateViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public CandidateStatus Status { get; set; }

        public string Positions { get; set; }

        public string StartPeriod { get; set; }

        public string TotalITexperience { get; set; }

        public string CurrentWages { get; set; }

        public string ExpectedWages { get; set; }

        public string BriefPersonalInfomation { get; set; }

        public string Telephone { get; set; }

        public string Email { get; set; }

        public bool IsHavingCV { get; set; }

        public string Creator { get; set; }

        public string Modifier { get; set; }

        public DateTime? ModifiedDateTime { get; set; }
    }
}
