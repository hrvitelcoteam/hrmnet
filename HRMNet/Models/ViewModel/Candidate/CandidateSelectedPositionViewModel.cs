﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Candidate
{
    public class CandidateSelectedPositionViewModel
    {
        public string Position { get; set; }

        public string Customer { get; set; }

        public string Project { get; set; }
    }
}
