﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.CustomSetting
{
    public class CustomSettingViewModel
    {
        [Display(Name = "Sending Email Body")]
        public virtual string SendingEmailGeneralBody { get; set; }

        [NotMapped]
        public AttachmentViewModel AttachmentViewModel { get; set; }

        public virtual string AttachFile { get; set; }
    }
}
