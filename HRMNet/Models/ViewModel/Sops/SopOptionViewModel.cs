﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Sops
{
    public class SopOptionViewModel
    {
        public string Name { get; set; }

        public int Value { get; set; }

        public bool Checked { get; set; }
    }
}
