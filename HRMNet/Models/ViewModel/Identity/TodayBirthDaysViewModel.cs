﻿using System.Collections.Generic;
using HRMNet.Models.Identity;

namespace HRMNet.Models.ViewModel.Identity
{
    public class TodayBirthDaysViewModel
    {
        public List<User> Users { set; get; }

        public AgeStatViewModel AgeStat { set; get; }
    }
}