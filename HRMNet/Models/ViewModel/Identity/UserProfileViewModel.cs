﻿using System.ComponentModel.DataAnnotations;
using DNTCommon.Web.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HRMNet.Models.ViewModel.Identity
{
    public class UserProfileViewModel
    {
        public const string AllowedImages = ".png,.jpg,.jpeg,.gif";

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Username")]
        [Remote("ValidateUsername", "UserProfile",
            AdditionalFields = nameof(Email) + "," + ViewModelConstants.AntiForgeryToken + "," + nameof(Pid),
            HttpMethod = "POST")]
        public string UserName { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "(*)")]
        [StringLength(450)]
        public string FirstName { get; set; }

        [Display(Name = "Surename")]
        [Required(ErrorMessage = "(*)")]
        [StringLength(450)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "(*)")]
        [Remote("ValidateUsername", "UserProfile",
            AdditionalFields = nameof(UserName) + "," + ViewModelConstants.AntiForgeryToken + "," + nameof(Pid),
            HttpMethod = "POST")]
        [EmailAddress(ErrorMessage = "Please Enter valid email address.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Picture")]
        [StringLength(maximumLength: 1000, ErrorMessage = "Maximum address length is 1000")]
        public string PhotoFileName { set; get; }

        [UploadFileExtensions(AllowedImages,
            ErrorMessage = "Please send only " + AllowedImages + " pics")]
        [DataType(DataType.Upload)]
        public IFormFile Photo { get; set; }

        [Display(Name = "Mobile")]
        //[StringLength(maximumLength: 11, ErrorMessage = "Please insert valid phone number")]
        public string PhoneNumber { set; get; }

        public int? DateOfBirthYear { set; get; }
        public int? DateOfBirthMonth { set; get; }
        public int? DateOfBirthDay { set; get; }

        [Display(Name = "Location")]
        public string Location { set; get; }

        [Display(Name = "Is Public Email")]
        public bool IsEmailPublic { set; get; }

        [Display(Name = "Two Factor Enable")]
        public bool TwoFactorEnabled { set; get; }

        public bool IsPasswordTooOld { set; get; }

        [HiddenInput]
        public string Pid { set; get; }

        [HiddenInput]
        public bool IsAdminEdit { set; get; }

        [Display(Name = "TitleId")]
        public int? TitleId { get; set; }
    }
}