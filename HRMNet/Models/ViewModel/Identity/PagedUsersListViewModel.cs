﻿using System.Collections.Generic;
using cloudscribe.Web.Pagination;
using HRMNet.Models.Identity;
//using cloudscribe.Web.Pagination;

namespace HRMNet.Models.ViewModel.Identity
{
    public class PagedUsersListViewModel
    {
        public PagedUsersListViewModel()
        {
            Paging = new PaginationSettings();
        }

        public List<User> Users { get; set; }

        public List<Role> Roles { get; set; }

        public PaginationSettings Paging { get; set; }
    }
}
