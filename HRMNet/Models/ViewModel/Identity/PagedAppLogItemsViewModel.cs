﻿using System.Collections.Generic;
using HRMNet.Models.Identity;
//using cloudscribe.Web.Pagination;

namespace HRMNet.Models.ViewModel.Identity
{
    public class PagedAppLogItemsViewModel
    {
        public PagedAppLogItemsViewModel()
        {
            //Paging = new PaginationSettings();
        }

        public string LogLevel { get; set; } = string.Empty;

        public List<AppLogItem> AppLogItems { get; set; }

        //public PaginationSettings Paging { get; set; }
    }
}