﻿using System.ComponentModel.DataAnnotations;

namespace HRMNet.Models.ViewModel.Identity
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "(*)")]
        [EmailAddress(ErrorMessage = "Please enter valid email address.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}