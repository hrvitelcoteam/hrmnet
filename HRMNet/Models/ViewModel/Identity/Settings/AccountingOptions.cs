﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRMNet.Models.ViewModel.Identity.Settings
{
    public class AccountingOptions
    {
        public string TemporaryTourEarningCode { get; set; }

        public string PrimaryTourEarningCode { get; set; }

        public string TemporaryTicketEarningCode { get; set; }

        public string PrimaryTicketEarningCode { get; set; }

        public string ContractAssistantCode { get; set; }

        public string ReceiptCode { get; set; }
    }
}
