﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRMNet.Models.ViewModel.Identity.Settings
{
    public class RataWebServiceOptions
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string ClientId { get; set; }

        public string BaseURI { get; set; }

        public string BaseMethod { get; set; }

        public string DomainName { get; set; }

        public string AppName { get; set; }

        public string ticket { get; set; }

        public string DebitpaymentCode { get; set; }

        public bool PilotMode { get; set; }
    }

    public class AdlWebServiceOptions
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string AgancyCode { get; set; }

        public string BaseURI { get; set; }

        public string BaseMethod { get; set; }

        public bool PilotMode { get; set; }

        public string RedirectAddress { get; set; }
    }

    public class PasargadWebServiceOptions
    {
        public string BaseUri { get; set; }

        public string BaseMethod { get; set; }

        public string PublicKey { get; set; }

        public string PrivateKey { get; set; }

        public string TerminalCode { get; set; }

        public string MerchantCode { get; set; }

        public string RedirectAddress { get; set; }

        public bool PilotMode { get; set; }
    }

    public class AmadeusWebServiceOptions
    {
        public string BaseURI { get; set; }

        public string BaseMethod { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string PNRNo { get; set; }

        public bool PilotMode { get; set; }
    }

    public class Eghamat24WebServiceOptions
    {
        public string Token { get; set; }

        public string BaseURI { get; set; }

        public bool PilotMode { get; set; }

        public string RedirectAddress { get; set; }

    }
}
