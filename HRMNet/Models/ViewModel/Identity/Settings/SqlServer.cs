﻿namespace HRMNet.Models.ViewModel.Identity.Settings
{
    public class SqlServer
    {
        public string ApplicationDbContextConnection { get; set; }
    }
}