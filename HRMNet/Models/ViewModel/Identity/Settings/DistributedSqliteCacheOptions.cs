namespace HRMNet.Models.ViewModel.Identity.Settings
{

    public class DistributedSqliteCacheOptions
    {
        public string ConnectionString { set; get; }
    }
}