﻿namespace HRMNet.Models.ViewModel.Identity.Settings
{
    public enum ActiveDatabase
    {
        LocalDb,
        SqlServer,
        InMemoryDatabase,
        SQLite
    }
}