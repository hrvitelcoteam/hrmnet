﻿using System.ComponentModel.DataAnnotations;

namespace HRMNet.Models.ViewModel.Identity
{
    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Display(Name = "Securıty Code")]
        [Required(ErrorMessage = "(*)")]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "Remember Browser")]
        public bool RememberBrowser { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }
    }
}