﻿using HRMNet.Models.Identity;

namespace HRMNet.Models.ViewModel.Identity.Emails
{
    public class ChangePasswordNotificationViewModel : EmailsBase
    {
        public User User { set; get; }
    }
}