﻿using HRMNet.Models.Identity;

namespace HRMNet.Models.ViewModel.Identity.Emails
{
    public class RegisterEmailConfirmationViewModel : EmailsBase
    {
        public User User { set; get; }
        public string EmailConfirmationToken { set; get; }
    }
}
