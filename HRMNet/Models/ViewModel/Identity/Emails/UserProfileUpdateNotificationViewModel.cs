using HRMNet.Models.Identity;

namespace HRMNet.Models.ViewModel.Identity.Emails
{
    public class UserProfileUpdateNotificationViewModel : EmailsBase
    {
        public User User { set; get; }
    }
}