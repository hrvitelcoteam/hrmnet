﻿using System.Collections.Generic;
using HRMNet.Models.Identity;

namespace HRMNet.Models.ViewModel.Identity
{
    public class OnlineUsersViewModel
    {
        public List<User> Users { set; get; }
        public int NumbersToTake { set; get; }
        public int MinutesToTake { set; get; }
        public bool ShowMoreItemsLink { set; get; }
    }
}