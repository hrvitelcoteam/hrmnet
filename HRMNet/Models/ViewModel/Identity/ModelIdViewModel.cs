﻿namespace HRMNet.Models.ViewModel.Identity
{
    public class ModelIdViewModel
    {
        public int Id { set; get; }
    }
}