﻿using System.ComponentModel.DataAnnotations;

namespace HRMNet.Models.ViewModel.Identity
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }
    }
}