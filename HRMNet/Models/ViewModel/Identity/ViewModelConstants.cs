﻿namespace HRMNet.Models.ViewModel.Identity
{
    public static class ViewModelConstants
    {
        public const string AntiForgeryToken = "__RequestVerificationToken";
    }
}