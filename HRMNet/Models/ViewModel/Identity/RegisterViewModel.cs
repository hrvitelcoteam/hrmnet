﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace HRMNet.Models.ViewModel.Identity
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "(*)")]
        [Display(Name = "Username")]
        [Remote("ValidateUsername", "Register",
            AdditionalFields = nameof(Email) + "," + ViewModelConstants.AntiForgeryToken, HttpMethod = "POST")]
        [RegularExpression("^[a-zA-Z_]*$", ErrorMessage = "Please enter english characters")]
        public string Username { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "(*)")]
        [StringLength(450)]
        //[RegularExpression(@"^[\u0600-\u06FF,\u0590-\u05FF\s]*$",
        //                  ErrorMessage = "لطفا تنها از حروف فارسی استفاده نمائید")]
        public string FirstName { get; set; }

        [Display(Name = "Surename")]
        [Required(ErrorMessage = "(*)")]
        [StringLength(450)]
        //[RegularExpression(@"^[\u0600-\u06FF,\u0590-\u05FF\s]*$",
        //                  ErrorMessage = "لطفا تنها از حروف فارسی استفاده نمائید")]
        public string LastName { get; set; }

        [Display(Name = "Mobile")]
        [Required(ErrorMessage = "(*)")]
        [StringLength(11)]
        //[RegularExpression(@"^[\u0600-\u06FF,\u0590-\u05FF\s]*$",
        //                ErrorMessage = "لطفا تنها از حروف فارسی استفاده نمائید")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "لطفا شماره همراه را بصورت صحیح وارد کنید.")]
        //[Mobile]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "(*)")]
        [Remote("ValidateUsername", "Register",
            AdditionalFields = nameof(Username) + "," + ViewModelConstants.AntiForgeryToken, HttpMethod = "POST")]
        [EmailAddress(ErrorMessage = "Please enter valid email address.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "(*)")]
        [StringLength(100, ErrorMessage = "{0} Must be at least {2} and at most {1} characters.", MinimumLength = 6)]
        [Remote("ValidatePassword", "Register",
            AdditionalFields = nameof(Username) + "," + ViewModelConstants.AntiForgeryToken, HttpMethod = "POST")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "(*)")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare(nameof(Password), ErrorMessage = "The passwords does not match")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Title")]
        public int? TitleId { get; set; }
    }
}