﻿using System.Collections.Generic;
using HRMNet.Models.Identity;
using DNTCommon.Web.Core;

namespace HRMNet.Models.ViewModel.Identity
{
    public class DynamicRoleClaimsManagerViewModel
    {
        public string[] ActionIds { set; get; }

        public int RoleId { set; get; }

        public Role RoleIncludeRoleClaims { set; get; }

        public ICollection<MvcControllerViewModel> SecuredControllerActions { set; get; }
    }
}