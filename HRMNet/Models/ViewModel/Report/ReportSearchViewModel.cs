﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Report
{
    public class ReportSearchViewModel
    {
        public ReportSearchViewModel()
        {
            ChartType = "pie";
        }
        [Display(Name = "Start")]
        [DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End")]
        [DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public DateTime? SingleDate { get; set; }

        [Display(Name = "Date Range")]
        public string DateRange { get; set; }

        [Display(Name = "Customer")]
        public int? CustomerId { get; set; }

        public int? UserId { get; set; }

        public bool IsPosition { get; set; }

        public string ChartType { get; set; }

        [Display(Name = "Position")]
        public int? PositionId { get; set; }

    }
}
