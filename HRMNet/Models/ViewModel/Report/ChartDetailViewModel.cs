﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Report
{
    public class ChartDetailViewModel
    {
        public int NumberOfPhoneCalls { get; set; }

        public int SharedWithCustomer { get; set; }

        public int CustomerChoosen { get; set; }

        public int CustomerOffered { get; set; }

        public Double RecruitmentEffort { get; set; }

        public int RecruitmentTarget { get; set; }

        public Double SuccessPercent { get; set; }

    }
}
