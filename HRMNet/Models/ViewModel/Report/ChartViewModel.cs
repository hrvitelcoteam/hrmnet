﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Report
{
    public class ChartViewModel
    {
        public ChartViewModel()
        {
            Search = new ReportSearchViewModel();
            Detail = new ChartDetailViewModel();
        }

        public Chart Chart { get; set; }

        public ReportSearchViewModel Search { get; set; }

        public ChartDetailViewModel Detail { get; set; }

        public string SerializedChart1 { get; set; }

        public string SerializedChart2 { get; set; }

        public string SerializedChart3 { get; set; }

        public string SerializedChart4 { get; set; }

        public string Message { get; set; }

    }
}
