﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRMNet.Models.Candidates;

namespace HRMNet.Models.ViewModel.Report
{
    public class PrecandidateEvaluateAnalysisViewModel
    {
        public PrecandidateEvaluateAnalysisViewModel()
        {
            Search = new ReportSearchViewModel();
        }
        public ChartViewModel Chart { get; set; }

        public ICollection<PrecandidateEvaluationAnalysisGroupViewModel> Recruiters { get; set; }

        public ReportSearchViewModel Search { get; set; }
    }
}
