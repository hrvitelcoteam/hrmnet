﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Report
{
    public class PrecandidateEvaluationAnalysisGroupViewModel
    {

        public int Id { get; set; }
        [Display(Name = "Name Surename")]
        public string Name { get; set; }
        [Display(Name = "Count")]
        public int Count { get; set; }
    }
}
