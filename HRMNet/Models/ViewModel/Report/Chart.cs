﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Report
{
    public class Chart
    {
        public Chart()
        {
            Labels = new HashSet<string>();
            //Datasets = new HashSet<Dataset>();
        }
        public ICollection<string> Labels { get; set; }

        public Dataset Datasets { get; set; }
    }
}
