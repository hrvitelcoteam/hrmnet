﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRMNet.Models.Workflow;

namespace HRMNet.Models.ViewModel.Report
{
    public class ReportViewModel
    {
        public int Id { get; set; }

        
        public string Key { get; set; }

        public string Category { get; set; }

        public int order { get; set; }

        public double Count { get; set; }


        public int GroupCount { get; set; }

        public DateTime? FirstDate { get; set; }

        public DateTime? LastDate { get; set; }

        public IEnumerable<DateTime> lst { get; set; }

    }
}
