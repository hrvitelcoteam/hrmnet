﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.ViewModel.Report
{
    public class Dataset
    {
        public Dataset()
        {
            Data = new HashSet<int>();
        }
        public string Label { get; set; }

        public string BackgroundColor { get; set; }

        public string BorderColor { get; set; }

        public bool PointRadius { get; set; }

        public string PointColor { get; set; }

        public string PointStrokeColor { get; set; }
        public string PointHighlightFill { get; set; }
        public string PointHighlightStroke { get; set; }

        public ICollection<int> Data { get; set; }
    }
}
