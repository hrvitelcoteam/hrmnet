﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Timesheet
{
    public class AddItemViewModel
    {
        public string Id { get; set; }

        public string Start { get; set; }

        public string End { get; set; }

        public bool AllDay { get; set; }

        public string Title { get; set; }


    }

    public class RenderUpdateItemViewModel
    {
        public int Id { get; set; }

    }
}
