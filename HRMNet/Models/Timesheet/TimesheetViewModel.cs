﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Timesheet
{
    public class TimesheetViewModel
    {
        public int id { get; set; }

        public string title { get; set; }

        //public string groupId { get; set; }

        public bool allDay { get; set; }

        public string start { get; set; }

        public string end { get; set; }

        public string startTime { get; set; }

        public string endTime { get; set; }

        public string startRecur { get; set; }

        public string endRecur { get; set; }

        public string url { get; set; }

        //public string className { get; set; }

        public bool editable { get; set; }

        public bool overlap { get; set; }

        public string color { get; set; }

        //public string backgroundColor { get; set; }

        //public string borderColor { get; set; }

        //public string textColor { get; set; }

        public string extendedProps { get; set; }
    }
}
