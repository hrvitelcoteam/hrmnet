﻿using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Timesheet
{
    public class TimesheetItem: BaseEntity
    {

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Title")]
        [StringLength(450)]
        public virtual string Title { get; set; }

        [Display(Name = "Description")]
        [StringLength(450)]
        public virtual string Description { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Start")]
        public virtual DateTime? Start { get; set; }

        [Display(Name = "End")]
        public virtual DateTime? End { get; set; }


        public virtual DateTime? StartTime { get; set; }

        public virtual DateTime? EndTime { get; set; }


        public virtual int? GroupId { get; set; }

        public virtual bool IsRepeated { get; set; }

        [Display(Name = "Url")]
        [StringLength(450)]
        public virtual string Url { get; set; }

        public virtual int Order { get; set; }

        [Display(Name = "All Day")]
        public virtual bool AllDay { get; set; }

        public virtual DateTime? StartRecur { get; set; }

        public virtual DateTime? EndRecur { get; set; }

        [StringLength(450)]
        public virtual string ClassName { get; set; }

        [StringLength(50)]
        public virtual string BackgroundColor { get; set; }

        [StringLength(450)]
        public virtual string BorderColor { get; set; }

        [StringLength(450)]
        public virtual string TextColor { get; set; }

        [Display(Name = "Overlap")]
        public virtual bool Overlap { get; set; }

        public virtual TimesheetItemType Type { get; set; }

        [Display(Name = "Color")]
        public virtual TimesheetItemColor Color { get; set; }

        public virtual int? ResourseId { get; set; }

        [Display(Name = "Is Public")]
        public virtual bool IsPublic { get; set; }

        public virtual Position Position { get; set; }

        [Display(Name = "Position")]
        public int? PositionId { get; set; }

        [NotMapped]
        [Display(Name = "Position/Project")]
        public string PositionProject { get; set; }

        //[NotMapped]
        //public bool IsDelete { get; set; }

        //public virtual User User { get; set; }

        //public int? UserId { get; set; }

    }
}
