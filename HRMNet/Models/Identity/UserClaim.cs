﻿using HRMNet.Models.AuditableEntity;
using Microsoft.AspNetCore.Identity;

namespace HRMNet.Models.Identity
{
    public class UserClaim : IdentityUserClaim<int>, IAuditableEntity
    {
        public virtual User User { get; set; }
    }
}