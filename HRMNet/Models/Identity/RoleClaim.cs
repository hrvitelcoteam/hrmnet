﻿using HRMNet.Models.AuditableEntity;
using Microsoft.AspNetCore.Identity;

namespace HRMNet.Models.Identity
{
    
    public class RoleClaim : IdentityRoleClaim<int>, IAuditableEntity
    {
        public virtual Role Role { get; set; }
    }
}