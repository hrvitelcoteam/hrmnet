﻿using HRMNet.Models.AuditableEntity;
using Microsoft.AspNetCore.Identity;

namespace HRMNet.Models.Identity
{
    public class UserRole : IdentityUserRole<int>, IAuditableEntity
    {
        public virtual User User { get; set; }

        public virtual Role Role { get; set; }
    }
}