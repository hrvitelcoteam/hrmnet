﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using HRMNet.Models.AuditableEntity;
using HRMNet.Models.Timesheet;
using HRMNet.Models.Recruit;
using HRMNet.Models.Sops;
using HRMNet.Models.Candidates;

namespace HRMNet.Models.Identity
{
    
    public class User : IdentityUser<int>, IAuditableEntity
    {
        public User()
        {
            UserUsedPasswords = new HashSet<UserUsedPassword>();
            UserTokens = new HashSet<UserToken>();
        }

        [StringLength(450)]
        public string FirstName { get; set; }

        [StringLength(450)]
        public string LastName { get; set; }

        [NotMapped]
        public string DisplayName
        {
            get
            {
                var displayName = $"{FirstName} {LastName}";
                return string.IsNullOrWhiteSpace(displayName) ? UserName : displayName;
            }
        }

        [StringLength(450)]
        public string PhotoFileName { get; set; }

        public DateTime? BirthDate { get; set; }

        public DateTime? CreatedDateTime { get; set; }

        public DateTime? LastVisitDateTime { get; set; }

        public bool IsEmailPublic { get; set; }

        public string Location { set; get; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; } = true;

        [Display(Name = "System User")]
        public bool IsSystemUser { get; set; }

        [Display(Name = "Debit User")]
        public bool IsDebitUser { get; set; }

        [Display(Name = "Debit")]
        public int MaxDebitAmount { get; set; }

        [Display(Name = "Accounting Code")]
        public string AccountingCode { get; set; }

        [NotMapped]
        [Display(Name = "Earning")]
        public Int64 Earning { get; set; }

        [NotMapped]
        [Display(Name = "Contracts")]
        public int ContractsCount { get; set; }

        [NotMapped]
        public HashSet<string> ConnectionIds { get; set; }

        public virtual Title Title { get; set; }

        [Display(Name = "Title")]
        public int? TitleId { get; set; }

        //public virtual ICollection<Sop> Sop { get; set; }

        //public virtual ICollection<Candidate> Candidates { get; set; }

        public virtual ICollection<UserUsedPassword> UserUsedPasswords { get; set; }

        public virtual ICollection<UserToken> UserTokens { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }

        public virtual ICollection<UserLogin> Logins { get; set; }

        public virtual ICollection<UserClaim> Claims { get; set; }

    }
}