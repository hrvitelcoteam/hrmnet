﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class DistributedSqliteCacheOptions
    {
        public string ConnectionString { set; get; }
    }
}
