﻿using DNTCommon.Web.Core;
using HRMNet.Services;
using Microsoft.EntityFrameworkCore.DataEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
	public class CustomEncryptionProvider : IEncryptionProvider
	{
		//private readonly IProtectionProviderService _protectionProviderService;
		public CustomEncryptionProvider(
			//IProtectionProviderService protectionProviderService
			)
        {
			//_protectionProviderService = protectionProviderService;
		}
		public string Encrypt(string dataToEncrypt)
		{
			var encryptedText = StringCipher.Encrypt(dataToEncrypt, "12345");
			return encryptedText;
			//return ProtectionProvider.EncryptString(dataToEncrypt, "AAECAwQFBgcICQoLDA0ODw==");
			//return _protectionProviderService.Encrypt(dataToEncrypt);
			// Encrypt data and return as Base64 string
		}

		public string Decrypt(string dataToDecrypt)
		{
			var decryptedText= StringCipher.Decrypt(dataToDecrypt, "12345");
			return decryptedText;
			//return ProtectionProvider.DecryptString(dataToDecrypt, "AAECAwQFBgcICQoLDA0ODw==");
			//return _protectionProviderService.Decrypt(dataToDecrypt);
			// Decrypt a Base64 string to plain string
		}
	}
}
