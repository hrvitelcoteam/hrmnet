﻿using HRMNet.Models.Candidates;
using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class Expertise:BaseEntity
    {
        public Expertise()
        {
            CandidateExpertises = new HashSet<CandidateExpertise>();
        }

        [Display(Name= "Expertise Name")]
        public virtual string ExpertiseName { get; set; }

        

        public virtual ICollection<CandidateExpertise> CandidateExpertises { get; set; }

        public virtual ICollection<PositionType> PositionTypes { get; set; }


    }
}
