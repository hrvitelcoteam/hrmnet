﻿using DNTCommon.Web.Core;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class Lookup : ILookupProtector
    {
        private readonly IProtectionProviderService _protectionProviderService;
        public Lookup(IProtectionProviderService protectionProviderService)
        {
            _protectionProviderService = protectionProviderService;
        }
        public string Protect(string keyId, string data)
        {
            return new string(_protectionProviderService.Encrypt(data)?.Reverse().ToArray());
        }

        public string Unprotect(string keyId, string data)
        {
            return new string(_protectionProviderService.Decrypt(data)?.Reverse().ToArray());
        }
    }

    public class Protector : IPersonalDataProtector
    {
        private readonly IProtectionProviderService _protectionProviderService;
        public Protector(IProtectionProviderService protectionProviderService)
        {
            _protectionProviderService = protectionProviderService;
        }

        public string Protect(string data)
        {
            return new string(_protectionProviderService.Encrypt(data)?.Reverse().ToArray());
        }

        public string Unprotect(string data)
        {
            return new string(_protectionProviderService.Decrypt(data)?.Reverse().ToArray());
            //return new string(data?.Reverse().ToArray());
        }
    }

    public class KeyRing : ILookupProtectorKeyRing
    {
        public string this[string keyId] => "key";

        public string CurrentKeyId => "key";

        public IEnumerable<string> GetAllKeyIds()
        {
            return new string[] { "key" };
        }
    }

}
