﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class SqlServer
    {
        public string ApplicationDbContextConnection { get; set; }
    }
}
