﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class AttachmentViewModel
    {
        public long Id { get; set; }
        public List<string> initialPreview { get; set; }

        public string SerializedInitialPreview { get; set; }

        public List<AttachmentPreviewConfig> initialPreviewConfig { get; set; }

        public string SerializedInitialPreviewConfig { get; set; }

        public bool append { get; set; }
    }

    public class AttachmentPreviewConfig
    {
        public string caption { get; set; }

        public string url { get; set; }

        public string key { get; set; }

        public string width { get; set; }
    }
}
