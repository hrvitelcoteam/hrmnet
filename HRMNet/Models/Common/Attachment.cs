﻿using HRMNet.Models.Candidates;
using HRMNet.Models.Customers;
using HRMNet.Models.Sops;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRMNet.Services.CustomSettingsService;

namespace HRMNet.Models.Common
{
    public class Attachment
    {
        public virtual int Id { get; set; }

        public virtual string Caption { get; set; }

        public virtual string FileName { get; set; }

        public virtual string FilePath { get; set; }

        public virtual string ContentType { get; set; }

        public virtual long Size { get; set; }

        public virtual string Extension { get; set; }

        public virtual bool IsImage { get; set; }

        public virtual Candidate Candidate { get; set; }

        public int? CandidateId { get; set; }

        public virtual Customer Customer { get; set; }

        public int? CustomerId { get; set; }

        public virtual Disciplin Disciplin { get; set; }

        public int? DisciplinId { get; set; }

        public virtual Sop Sop { get; set; }

        public int? SopId { get; set; }



    }
}
