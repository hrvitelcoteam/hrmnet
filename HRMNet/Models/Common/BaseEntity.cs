﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;
using HRMNet.Models.Identity;
using Microsoft.AspNetCore.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;

namespace HRMNet.Models.Common
{

    public class BaseEntity
    {
        public virtual int Id { get; set; }

        //public virtual User Creator { get; set; }

        //public virtual User Creator { get; set; }
        [Display(Name= "Creator Id")]
        public virtual int CreatorId {get; set;}

        [Display(Name = "Created DateTime")]
        public virtual DateTime CreatedDateTime { get; set; }

        [Display(Name = "Created By Browser Name")]
        public virtual string CreatedByBrowserName { get; set; }

        [Display(Name = "Creator Ip")]
        public virtual string CreatorIp { get; set; }

        //public virtual User Modifier { get; set; }

        [Display(Name = "Modifier Id")]
        public virtual int? ModifierId { get; set; }

        [Display(Name = "Modified DateTime")]
        public virtual DateTime? ModifiedDateTime { get; set; }

        [Display(Name = "Modified By Browser Name")]
        public virtual string ModifiedByBrowserName { get; set; }

        [Display(Name = "Modifier Ip")]
        public virtual string ModifierIp { get; set; }

        
    }
}
