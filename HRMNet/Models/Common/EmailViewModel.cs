﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class EmailViewModel
    {
        public string Name { get; set; }

        public string Content { get; set; }

        public string PortalUsername { get; set; }

        public string PortalPassword { get; set; }

        public string Email { get; set; }

        public string EmailPassword { get; set; }

        public string Username { get; set; }

        public string Useremail { get; set; }

        public string Usermobile { get; set; }
    }
}
