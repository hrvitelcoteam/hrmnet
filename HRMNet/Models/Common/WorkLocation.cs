﻿using HRMNet.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class WorkLocation: BaseEntity
    {
        [Display(Name = "Location")]
        public virtual string Name { get; set; }

        [Display(Name = "Address")]
        public virtual string Address { get; set; }

        //public virtual User User { get; set; }

        //public int? UserId { get; set; }
    }
}
