﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class UserAvatarImageOptions
    {
        public int MaxWidth { set; get; }
        public int MaxHeight { set; get; }
    }
}
