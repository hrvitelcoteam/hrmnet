﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public static class ViewModelConstants
    {
        public const string AntiForgeryToken = "__RequestVerificationToken";
    }
}
