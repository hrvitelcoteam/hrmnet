﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public static class JsonExt
    {
        public static string ToJson(this string[] items)
        {
            if (items == null || !items.Any())
                return "[]";
            else
                return JsonConvert.SerializeObject(items);
        }

        public static string ToJson<T> (this List<T> items)
        {
            var setting = new JsonSerializerSettings();
            setting.ContractResolver = new CamelCasePropertyNamesContractResolver();

            if (items == null || !items.Any())
                return "[]";
            else
                return JsonConvert.SerializeObject(items, setting);
        }


    }
}
