﻿using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class Tipe
    {
        public virtual int Id { get; set; }

        public string TypeName { get; set; }

        //public virtual ICollection<PositionType> PositionTypes { get; set; }
    }
}
