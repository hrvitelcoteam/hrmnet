﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public class DataProtectionX509Certificate
    {
        public string FileName { set; get; }
        public string Password { set; get; }
    }
}
