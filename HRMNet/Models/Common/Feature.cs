﻿using HRMNet.Models.Candidates;
using HRMNet.Models.Positions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;

namespace HRMNet.Models.Common
{
    public class Feature: BaseEntity
    {
        public Feature()
        {
            //CandidateFeatures = new HashSet<CandidateFeature>();
        }

        [Display(Name = "Feature Name")]
        public virtual string FeatureName { get; set; }

        public virtual ICollection<CandidateFeature> CandidateFeatures { get; set; }

        public virtual ICollection<PositionFeature> PositionFeatures { get; set; }

        public virtual ICollection<PositionMandatoryFeature> PositionMandatoryFeatures { get; set; }

    }
}
