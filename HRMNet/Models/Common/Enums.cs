﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRMNet.Resources;
using HRMNet.Services;

namespace HRMNet.Models.Common
{

    public enum DisciplinResult
    {
        [Display(Name = "Defense Requested")]
        DefenseRequested = 0,
        [Display(Name = "In The Evaluation Process")]
        InTheEvaluationProcess =1,
        [Display(Name = "Defense Found Sufficient Action Not Taken")]
        DefenseFoundSufficientActionNotTaken = 2,
        [Display(Name = "Warning Has Been Given")]
        WarningHasBeenGiven =3,
        [Display(Name = "Contract Terminated")]
        ContractTerminated =4,
    }

    public enum SopsResult
    {
        [Display(Name = "Please select ...")]
        NoSelect =0,
        [Display(Name = "No Results")]
        NoResults =1,
        [Display(Name = "Didn't Come Back")]
        DidNotComeBack=2,
        [Display(Name = "Position Closed")]
        PositionClosed =3,
        [Display(Name = "Resource Placed")]
        ResourcePlaced =4,
        [Display(Name = "Tender Won")]
        TenderWon = 5
    }

    public enum MyFavoriteKnownColor
    {
        Blue = 37,
        Brown = 39,
        Cyan = 48,
        Gold = 76,
        Green = 79,
        Maroon = 108,
        Navy = 123,
        Olive = 125,
        Orange = 127,
        Pink = 137,
        Red = 141,
        Silver = 150,
        Yellow = 166,
    }
    public enum SOPDemandType
    {
        [Display(Name = "Resource Requested")]
        ResourceRequested =1,
        [Display(Name = "Managed Service")]
        ManagedService = 2,
        [Display(Name = "Tender")]
        Tender =3,
        [Display(Name = "Turnkey Project")]
        TurnkeyProject =4,
    }

    public enum SOPStatus
    {
        [Display(Name = "Open")]
        Open =1,
        [Display(Name = "In Progress")]
        InProgress =2,
        [Display(Name = "Preparing Offer")]
        PreparingOffer =3,
        [Display(Name = "Offer Given")]
        OfferGiven =4,
        [Display(Name = "Waiting For RFP")]
        WaitingForRFP =5,
        [Display(Name = "Technical Meeting Will Be Held")]
        TechnicalMeetingWillBeHeld =6,
        [Display(Name = "Waiting For Tender")]
        WaitingForTender =7,
        [Display(Name = "Closed")]
        Closed =8
    }

    public enum MessageType
    {
        Success,
        Alert,
        Danger,
        Info
    }
    public enum TimesheetItemType
    {
        [Display(Name = "Short")]
        Small = 1,
        [Display(Name = "All")]
        AllDay = 2,
        [Display(Name = "Long")]
        Long = 3
    }

    public enum TimesheetItemColor
    {
        [Display(Name = "Default")]
        Default = 0,
        [Display(Name = "Yellow")]
        Yellow = 1,
        [Display(Name = "Orange")]
        Orange = 2,
        [Display(Name = "Green")]
        Green = 3,
        [Display(Name = "Brown")]
        Brown = 4,
        [Display(Name = "Red")]
        Red = 5
    }
    public enum ContractType
    {
        [Display(Name = "Sub Contracter")]
        SubContracter = 0,
        [Display(Name = "Freelancer")]
        Freelancer=1,
        [Display(Name = "Intern")]
        Intern=2,
        [Display(Name = "Fixed Term Contract")]
        FixedTermContract=3,
        [Display(Name = "Indefinite-Term Employment Contract")]
        IndefiniteTermEmployementContract = 4,
        [Display(Name = "Contractor")]
        Contractor=5
    }

    public enum OperationType
    {
        [Display(Name ="Assignment To Employee")]
        AssignmentToEmployee =1,
        [Display(Name = "Return To Company")]
        ReturnToCompany =2,
    }

    public enum InventoryStatus
    {
        [Display(Name = "Active")]
        Active =1,
        [Display(Name = "Passive")]
        Passive =2
    }

    public enum LeaveType
    {
        [Display(Name = "Legal")]
        Legal =1,
        [Display(Name = "Day off")]
        Dayoff =2
    }

    public enum PermissionType
    {
        [Display(Name = "Paid")]
        Paid =1,
        [Display(Name = "Marriage")]
        Marriage =2,
        [Display(Name = "Decease")]
        Decease =3,
        [Display(Name = "Health")]
        Health =4,
        [Display(Name = "Advance")]
        Advance =5,
        
    }
    public enum MilitaryStatus
    {
        [Display(Name = "Unknown")]
        Unknown =0,
        [Display(Name = "Done")]
        Done =1,
        [Display(Name = "Exempt")]
        Exempt =2,
        [Display(Name = "Not Necessary")]
        NotNecessary =3,
        [Display(Name = "Postponed")]
        Postponed = 4
    }
    //public enum InventoryType
    //{
    //    [Display(Name = "Computer")]
    //    Computer =1,
    //    [Display(Name = "Sim Card")]
    //    SimCard =2,
    //    [Display(Name = "Telephone")]
    //    Telephone =3,

    //}

    public enum CVSource
    {
        Kariyer=0,
        LinkedIn=1,
        [Display(Name = "Customer Suggestion")]
        CustomerSuggestion =2,
        [Display(Name = "Take Award")]
        TakeAward = 3
    }

    public enum PreCandidateStatusByType
    {
        [Display(Name = "Inactive")]
        InActive = 0,
        [Display(Name = "Active")]
        Active = 1,
        [Display(Name = "Submitted To Management")]
        SubmittedToManager = 2,
        [Display(Name = "Unsuitable Technically")]
        UnsuitableTechnically = 3,
        [Display(Name = "Unsuitable In Terms Of Budget")]
        UnsuitableInTermsOfBudget = 4,
        [Display(Name = "Candidate Shared With Customer")]
        SharedWithCustomer = 5,
        [Display(Name = "Cv Not Approved By Customer")]
        CvNotApprovedByCustomer = 6,
        [Display(Name = "Candidate Withdrew Before Meeting")]
        WithdrewBeforeMeeting = 7,
        [Display(Name = "Customer Will Interview")]
        CustomerWillInterview = 8,
        [Display(Name = "Rejected In Customer Interview")]
        RejectedInCustomerInterview = 9,
        [Display(Name = "Didn't Come To Interview")]
        DidnotComeToInterview = 10,
        [Display(Name = "Customer Requested Candidate Withdrew")]
        CustomerRequestedCandidateWithdrew = 11,
        [Display(Name = "Rejected Offer")]
        RejectedOffer = 12,
        [Display(Name = "Offer Made")]
        OfferMade = 13,
        [Display(Name = "Accepted Offer")]
        AcceptedOffer = 14,
        [Display(Name = "Waiting For Paperwork")]
        WaitingForPaperwork = 15,
        [Display(Name = "Vitelco Staff")]
        VitelcoStuff = 80,
        [Display(Name = "Quit")]
        Quit = 81,
        [Display(Name = "Candidate In CV Pool")]
        CandidateInCvPool = 82,


        [Display(Name = "Inactive")]
        PreCandidateInActive = 90,
        [Display(Name = "Not Reachable")]
        PreCandidateNotReachable = 91,
        [Display(Name = "Salary Expectation Is High")]
        PreCandidateSalaryExpectationIsHigh = 92,
        [Display(Name = "Not Qualify")]
        PreCandidateNotQualify = 93,
        [Display(Name = "Not Considering The Position")]
        PreCandidateNotConsideringThePosition = 94,
        [Display(Name = "Eligible Candidate")]
        PreCandidateEligibleCandidate = 95,
        [Display(Name = "Risky Candidate (High Tendency To Withdraw)")]
        PreCandidateRiskyCandidate = 96,
    }

    public enum MeetingStatus
    {
        [Display(Name = "I Don't like")]
        DoNotLike = 0,
        [Display(Name = "I Like")]
        Like =1
       
    }

    public enum CandidatePrecandidate
    {
        [Display(Name = "Precandidate")]
        OnlyPrecandidate = 1,
        [Display(Name = "Candidate")]
        OnlyCandidate =2,
        [Display(Name = "Both")]
        CandidateAndPrecandidate =3
    }

    //[TypeConverter(typeof(EnumToLocalizedName))]
    public enum PreCandidateStatus
    {
        [Display(Name = "Inactive")]
        InActive =0,
        [Display(Name = "Not Reachable")]
        NotReachable =1,
        [Display(Name = "Salary Expectation Is High")]
        SalaryExpectationIsHigh =2,
        [Display(Name = "Not Qualify")]
        NotQualify =3,
        [Display(Name = "Not Considering The Position")]
        NotConsideringThePosition =4,
        [Display(Name = "Eligible Candidate")]
        EligibleCandidate =5,
        [Display(Name = "Risky Candidate (High Tendency To Withdraw)")]
        RiskyCandidate =6,


    }
    public enum CandidateStatus
    {
        [Display(Name = "Inactive")]
        InActive =0,
        [Display(Name = "Active")]
        Active = 1,
        [Display(Name = "Submitted To Management")]
        SubmittedToManager = 2,
        [Display(Name = "Unsuitable Technically")]
        UnsuitableTechnically = 3,
        [Display(Name = "Unsuitable In Terms Of Budget")]
        UnsuitableInTermsOfBudget = 4,
        [Display(Name = "Shared With Customer")]
        SharedWithCustomer = 5,
        [Display(Name = "Cv Not Approved By Customer")]
        CvNotApprovedByCustomer = 6,
        [Display(Name = "Candidate Withdrew Before Meeting")]
        WithdrewBeforeMeeting = 7,
        [Display(Name = "Customer Choosen")]
        CustomerWillInterview = 8,
        [Display(Name = "Rejected In Customer Interview")]
        RejectedInCustomerInterview = 9,
        [Display(Name = "Didn't Come To Interview")]
        DidnotComeToInterview = 10,
        [Display(Name = "Customer Requested Candidate Withdrew")]
        CustomerRequestedCandidateWithdrew = 11,
        [Display(Name = "Rejected Offer")]
        RejectedOffer = 12,
        [Display(Name = "Proposed")]
        OfferMade = 13,
        [Display(Name = "Accepted Offer")]
        AcceptedOffer = 14,
        [Display(Name = "Waiting For Paperwork")]
        WaitingForPaperwork = 15,
        [Display(Name = "Vitelco Staff")]
        VitelcoStuff = 80,
        [Display(Name = "Quit")]
        Quit =81,
        [Display(Name = "Candidate In CV Pool")]
        CandidateInCvPool = 82
    }

    public enum PositionStatus
    {
        [Display(Name = "Passive")]
        Passive =0,
        [Display(Name = "Active")]
        Active =1,
        [Display(Name = "Suspended")]
        Suspended =2,
        [Display(Name = "Closed Positively")]
        ClosedPositively =3,
        [Display(Name = "Closed Negatively")]
        ClosedNegatively =4
    }
   
}
