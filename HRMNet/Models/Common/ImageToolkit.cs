﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Common
{
    public static class ImageToolkit
    {
        public static bool IsImageFile(IFormFile photoFile)
        {
            try
            {
                if (photoFile == null || photoFile.Length == 0)
                    return false;

                using (var img = Image.FromStream(photoFile.OpenReadStream()))
                {
                    return img.Width > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
