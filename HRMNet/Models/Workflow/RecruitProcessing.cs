﻿using Stateless;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Workflow
{
    //enum SubState1
    //{
    //    Initial=2,
    //    UnsuitableTechnically = 3,
    //    UnsuitableInTermsOfBudget = 4,
    //    SharedWithCustomer = 5,
    //}

    //enum Trigger1
    //{
    //    UnsuitableTechnically,
    //    UnsuitableInTermsOfBudget,
    //    ShareWithCustomer
    //}

    //public class State
    //{
    //    public virtual string Name { get; protected set; }
    //    public State(string name)
    //    {
    //        Name = name;
    //    }
    //    protected State()
    //    {

    //    }
    //    public virtual void OnEntry()
    //    {
    //        //Console.WriteLine($"Entering state {Name}");
    //    }
    //    public virtual void OnExit()
    //    {
    //        //Console.WriteLine($"Exiting state {Name}");
    //    }
    //}

    //public class SubmittedToManagerState : State
    //{
    //    private readonly StateMachine<SubState1, Trigger1> _subStateMachineOne;
    //    private readonly StateMachine<SubState1, Trigger1> _subStateMachineTwo;
    //    private readonly StateMachine<SubState1, Trigger1> _subStateMachineThree;
    //    private readonly string _name;

    //    public override string Name { get { return $"{_name}: {_subStateMachineOne.State} + {_subStateMachineTwo.State}"; } }
    //    public SubmittedToManagerState(string name)
    //    {
    //        _name = name;

    //        _subStateMachineOne = new StateMachine<SubState1, Trigger1>(SubState1.Initial);
    //        _subStateMachineOne.Configure(SubState1.Initial)
    //            .Permit(Trigger1.UnsuitableTechnically, SubState1.UnsuitableTechnically);

    //        _subStateMachineTwo = new StateMachine<SubState1, Trigger1>(SubState1.Initial);
    //        _subStateMachineTwo.Configure(SubState1.Initial)
    //            .Permit(Trigger1.UnsuitableInTermsOfBudget, SubState1.UnsuitableInTermsOfBudget);

    //        _subStateMachineThree = new StateMachine<SubState1, Trigger1>(SubState1.Initial);
    //        _subStateMachineTwo.Configure(SubState1.Initial)
    //            .Permit(Trigger1.ShareWithCustomer, SubState1.SharedWithCustomer);


    //        // Ignore unhandled triggers
    //        _subStateMachineOne.OnUnhandledTrigger((s, t) => { });
    //        _subStateMachineTwo.OnUnhandledTrigger((s, t) => { });
    //        _subStateMachineThree.OnUnhandledTrigger((s, t) => { });

    //        //_subStateMachineOne.OnTransitioned(DisplayProgressOne);
    //        //_subStateMachineTwo.OnTransitioned(DisplayProgressTwo);
    //    }

    //    //private void DisplayProgressOne(StateMachine<SubState, Trigger>.Transition obj)
    //    //{
    //    //    Console.WriteLine($"Internal state machine ONE transitioned from {obj.Source} to {obj.Destination}");
    //    //}
    //    //private void DisplayProgressTwo(StateMachine<SubState, Trigger>.Transition obj)
    //    //{
    //    //    Console.WriteLine($"Internal state machine TWO transitioned from {obj.Source} to {obj.Destination}");
    //    //}

    //    internal void Fire(Trigger1 trigger)
    //    {
    //        Console.WriteLine($"ForkState firing internally trigger {trigger}");
    //        _subStateMachineOne.Fire(trigger);
    //        _subStateMachineTwo.Fire(trigger);
    //    }

    //    //internal bool EndGuard()
    //    //{
    //    //    return (_subStateMachineOne.State == SubState.Done && _subStateMachineTwo.State == SubState.Done);
    //    //}
    //}


    public class RecruitProcessing
    {
        public delegate void UnhandledTriggerDelegate(State state, Trigger trigger);
        public delegate void EntryExitDelegate(State state);
        public delegate bool GuardClauseDelegate(State state);

        private readonly StateMachine<State, Trigger> _machine;

        public EntryExitDelegate OnEntry = null;
        public EntryExitDelegate OnExit = null;
        public EntryExitDelegate OnActiveEntry = null;
        public EntryExitDelegate OnActiveExit = null;
        public EntryExitDelegate OnSubmittedToManagerEntry = null;
        public EntryExitDelegate OnSubmittedToManagerExit = null;
        public EntryExitDelegate OnUnsuitableTechnicallyEntry = null;
        public EntryExitDelegate OnUnsuitableTechnicallyExit = null;
        public EntryExitDelegate OnUnsuitableInTermsOfBudgetEntry = null;
        public EntryExitDelegate OnUnsuitableInTermsOfBudgetExit = null;
        public EntryExitDelegate OnSharedWithCustomerEntry = null;
        public EntryExitDelegate OnSharedWithCustomerExit = null;
        public EntryExitDelegate OnCvNotApprovedByCustomerEntry = null;
        public EntryExitDelegate OnCvNotApprovedByCustomerExit = null;
        public EntryExitDelegate OnWithdrewBeforeMeetingEntry = null;
        public EntryExitDelegate OnWithdrewBeforeMeetingExit = null;
        public EntryExitDelegate OnCustomerWillInterviewEntry = null;
        public EntryExitDelegate OnCustomerWillInterviewExit = null;
        public EntryExitDelegate OnRejectedInCustomerInterviewEntry = null;
        public EntryExitDelegate OnRejectedInCustomerInterviewExit = null;
        public EntryExitDelegate OnDidnotComeToInterviewEntry = null;
        public EntryExitDelegate OnDidnotComeToInterviewExit = null;
        public EntryExitDelegate OnCustomerRequestedCandidateWithdrewEntry = null;
        public EntryExitDelegate OnCustomerRequestedCandidateWithdrewExit = null;
        public EntryExitDelegate OnRejectedOfferEntry = null;
        public EntryExitDelegate OnRejectedOfferExit = null;
        public EntryExitDelegate OnOfferMadeEntry = null;
        public EntryExitDelegate OnOfferMadeExit = null;
        public EntryExitDelegate OnAcceptedOfferEntry = null;
        public EntryExitDelegate OnAcceptedOfferExit = null;
        public EntryExitDelegate OnWaitingForPaperworkEntry = null;
        public EntryExitDelegate OnWaitingForPaperworkExit = null;
        public EntryExitDelegate OnVitelcoStuffEntry = null;
        public EntryExitDelegate OnVitelcoStuffExit = null;

        public GuardClauseDelegate GuardClauseFromToUsingTriggerSave = null;
        public GuardClauseDelegate GuardClauseFromToUsingTriggerReject = null;
        public GuardClauseDelegate GuardClauseFromToUsingTriggerWithdrew = null;
        public GuardClauseDelegate GuardClauseFromToUsingTriggerOthers = null;

        public GuardClauseDelegate GuardClauseFromActiveToActiveUsingTriggerSave = null;
        public GuardClauseDelegate GuardClauseFromActiveToSubmittedToManagerUsingTriggerSubmitToManager = null;
        public GuardClauseDelegate GuardClauseFromSubmittedToManagerToSubmittedToManagerUsingTriggerSave = null;
        public GuardClauseDelegate GuardClauseFromSharedWithCustomerToSharedWithCustomerUsingTriggerSave = null;
        public GuardClauseDelegate GuardClauseFromSharedWithCustomerToCustomerWillInterviewUsingTriggerCustomerWillInterview = null;
        public GuardClauseDelegate GuardClauseFromSharedWithCustomerToCvNotApprovedByCustomerUsingTriggerReject = null;
        public GuardClauseDelegate GuardClauseFromSharedWithCustomerToWithdrewBeforeMeetingUsingTriggerWithdrew = null;
        public GuardClauseDelegate GuardClauseFromCustomerWillInterviewToCustomerWillInterviewUsingTriggerSave = null;
        public GuardClauseDelegate GuardClauseFromCustomerWillInterviewToOfferMadeUsingTriggerOfferMake = null;
        public GuardClauseDelegate GuardClauseFromCustomerWillInterviewToRejectedInCustomerInterviewUsingTriggerReject = null;
        public GuardClauseDelegate GuardClauseFromCustomerWillInterviewToDidnotComeToInterviewUsingTriggerWithdrew = null;
        public GuardClauseDelegate GuardClauseFromCustomerWillInterviewToCustomerRequestedCandidateWithdrewUsingTriggerWithdrew = null;
        public GuardClauseDelegate GuardClauseFromOfferMadeToAcceptedOfferUsingTriggerAcceptOffer = null;
        public GuardClauseDelegate GuardClauseFromOfferMadeToRejectedOfferUsingTriggerReject = null;
        public GuardClauseDelegate GuardClauseFromAcceptedOfferToWaitingForPaperworkUsingTriggerWaitForPaperwork = null;
        public GuardClauseDelegate GuardClauseFromWaitingForPaperworkToVitelcoStuffUsingTriggerVitelcoStuff = null;
        public UnhandledTriggerDelegate OnUnhandledTrigger = null;

        //State _state = State.Begin;
        //private readonly StateMachine<State, Trigger>.TriggerWithParameters<int> _changeStatusTrigger;
        private readonly int _start;
        private readonly string _title;

        public RecruitProcessing(int start, string title = "")
        {
            _start = start;
            _title = title;
            _machine = new StateMachine<State, Trigger>((State)_start);
        }

        public void Configure()
        {

            //_machine.Configure(State.Active)
            //.OnEntry(() => { if (OnActiveEntry != null) OnActiveEntry(); })
            //.OnExit(() => { if (OnActiveExit != null) OnActiveExit(); })
            //.PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromActiveToActiveUsingTriggerSave != null) return GuardClauseFromActiveToActiveUsingTriggerSave(); return true; })
            //.PermitIf(Trigger.SubmitToManager, State.SubmittedToManager, () => { if (GuardClauseFromActiveToSubmittedToManagerUsingTriggerSubmitToManager != null) return GuardClauseFromActiveToSubmittedToManagerUsingTriggerSubmitToManager(); return true; });

            //_machine.Configure(State.SubmittedToManager)
            //  .OnEntry(() => { if (OnSubmittedToManagerEntry != null) OnSubmittedToManagerEntry(); })
            //  .OnExit(() => { if (OnSubmittedToManagerExit != null) OnSubmittedToManagerExit(); })
            //  .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromSubmittedToManagerToSubmittedToManagerUsingTriggerSave != null) return GuardClauseFromSubmittedToManagerToSubmittedToManagerUsingTriggerSave(); return true; });

            //_machine.Configure(State.UnsuitableTechnically)
            //  .OnEntry(() => { if (OnUnsuitableTechnicallyEntry != null) OnUnsuitableTechnicallyEntry(); })
            //  .OnExit(() => { if (OnUnsuitableTechnicallyExit != null) OnUnsuitableTechnicallyExit(); });

            //_machine.Configure(State.UnsuitableInTermsOfBudget)
            //  .OnEntry(() => { if (OnUnsuitableInTermsOfBudgetEntry != null) OnUnsuitableInTermsOfBudgetEntry(); })
            //  .OnExit(() => { if (OnUnsuitableInTermsOfBudgetExit != null) OnUnsuitableInTermsOfBudgetExit(); });

            //_machine.Configure(State.SharedWithCustomer)
            //  .OnEntry(() => { if (OnSharedWithCustomerEntry != null) OnSharedWithCustomerEntry(); })
            //  .OnExit(() => { if (OnSharedWithCustomerExit != null) OnSharedWithCustomerExit(); })
            //  .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromSharedWithCustomerToSharedWithCustomerUsingTriggerSave != null) return GuardClauseFromSharedWithCustomerToSharedWithCustomerUsingTriggerSave(); return true; })
            //  .PermitIf(Trigger.CustomerWillInterview, State.CustomerWillInterview, () => { if (GuardClauseFromSharedWithCustomerToCustomerWillInterviewUsingTriggerCustomerWillInterview != null) return GuardClauseFromSharedWithCustomerToCustomerWillInterviewUsingTriggerCustomerWillInterview(); return true; })
            //  .PermitIf(Trigger.Reject, State.CvNotApprovedByCustomer, () => { if (GuardClauseFromSharedWithCustomerToCvNotApprovedByCustomerUsingTriggerReject != null) return GuardClauseFromSharedWithCustomerToCvNotApprovedByCustomerUsingTriggerReject(); return true; })
            //  .PermitIf(Trigger.Withdrew, State.WithdrewBeforeMeeting, () => { if (GuardClauseFromSharedWithCustomerToWithdrewBeforeMeetingUsingTriggerWithdrew != null) return GuardClauseFromSharedWithCustomerToWithdrewBeforeMeetingUsingTriggerWithdrew(); return true; });

            //_machine.Configure(State.CvNotApprovedByCustomer)
            //  .OnEntry(() => { if (OnCvNotApprovedByCustomerEntry != null) OnCvNotApprovedByCustomerEntry(); })
            //  .OnExit(() => { if (OnCvNotApprovedByCustomerExit != null) OnCvNotApprovedByCustomerExit(); });

            //_machine.Configure(State.WithdrewBeforeMeeting)
            //  .OnEntry(() => { if (OnWithdrewBeforeMeetingEntry != null) OnWithdrewBeforeMeetingEntry(); })
            //  .OnExit(() => { if (OnWithdrewBeforeMeetingExit != null) OnWithdrewBeforeMeetingExit(); });

            //_machine.Configure(State.CustomerWillInterview)
            //  .OnEntry(() => { if (OnCustomerWillInterviewEntry != null) OnCustomerWillInterviewEntry(); })
            //  .OnExit(() => { if (OnCustomerWillInterviewExit != null) OnCustomerWillInterviewExit(); })
            //  .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromCustomerWillInterviewToCustomerWillInterviewUsingTriggerSave != null) return GuardClauseFromCustomerWillInterviewToCustomerWillInterviewUsingTriggerSave(); return true; })
            //  .PermitIf(Trigger.OfferMake, State.OfferMade, () => { if (GuardClauseFromCustomerWillInterviewToOfferMadeUsingTriggerOfferMake != null) return GuardClauseFromCustomerWillInterviewToOfferMadeUsingTriggerOfferMake(); return true; })
            //  .PermitIf(Trigger.Reject, State.RejectedInCustomerInterview, () => { if (GuardClauseFromCustomerWillInterviewToRejectedInCustomerInterviewUsingTriggerReject != null) return GuardClauseFromCustomerWillInterviewToRejectedInCustomerInterviewUsingTriggerReject(); return true; })
            //  .PermitIf(Trigger.Withdrew, State.DidnotComeToInterview, () => { if (GuardClauseFromCustomerWillInterviewToDidnotComeToInterviewUsingTriggerWithdrew != null) return GuardClauseFromCustomerWillInterviewToDidnotComeToInterviewUsingTriggerWithdrew(); return true; })
            //  .PermitIf(Trigger.Withdrew, State.CustomerRequestedCandidateWithdrew, () => { if (GuardClauseFromCustomerWillInterviewToCustomerRequestedCandidateWithdrewUsingTriggerWithdrew != null) return GuardClauseFromCustomerWillInterviewToCustomerRequestedCandidateWithdrewUsingTriggerWithdrew(); return true; });

            //_machine.Configure(State.RejectedInCustomerInterview)
            //  .OnEntry(() => { if (OnRejectedInCustomerInterviewEntry != null) OnRejectedInCustomerInterviewEntry(); })
            //  .OnExit(() => { if (OnRejectedInCustomerInterviewExit != null) OnRejectedInCustomerInterviewExit(); });

            //_machine.Configure(State.DidnotComeToInterview)
            //  .OnEntry(() => { if (OnDidnotComeToInterviewEntry != null) OnDidnotComeToInterviewEntry(); })
            //  .OnExit(() => { if (OnDidnotComeToInterviewExit != null) OnDidnotComeToInterviewExit(); });

            //_machine.Configure(State.CustomerRequestedCandidateWithdrew)
            //  .OnEntry(() => { if (OnCustomerRequestedCandidateWithdrewEntry != null) OnCustomerRequestedCandidateWithdrewEntry(); })
            //  .OnExit(() => { if (OnCustomerRequestedCandidateWithdrewExit != null) OnCustomerRequestedCandidateWithdrewExit(); });

            //_machine.Configure(State.RejectedOffer)
            //  .OnEntry(() => { if (OnRejectedOfferEntry != null) OnRejectedOfferEntry(); })
            //  .OnExit(() => { if (OnRejectedOfferExit != null) OnRejectedOfferExit(); });

            //_machine.Configure(State.OfferMade)
            //  .OnEntry(() => { if (OnOfferMadeEntry != null) OnOfferMadeEntry(); })
            //  .OnExit(() => { if (OnOfferMadeExit != null) OnOfferMadeExit(); })
            //  .PermitIf(Trigger.AcceptOffer, State.AcceptedOffer, () => { if (GuardClauseFromOfferMadeToAcceptedOfferUsingTriggerAcceptOffer != null) return GuardClauseFromOfferMadeToAcceptedOfferUsingTriggerAcceptOffer(); return true; })
            //  .PermitIf(Trigger.Reject, State.RejectedOffer, () => { if (GuardClauseFromOfferMadeToRejectedOfferUsingTriggerReject != null) return GuardClauseFromOfferMadeToRejectedOfferUsingTriggerReject(); return true; });

            //_machine.Configure(State.AcceptedOffer)
            //  .OnEntry(() => { if (OnAcceptedOfferEntry != null) OnAcceptedOfferEntry(); })
            //  .OnExit(() => { if (OnAcceptedOfferExit != null) OnAcceptedOfferExit(); })
            //  .PermitIf(Trigger.WaitForPaperwork, State.WaitingForPaperwork, () => { if (GuardClauseFromAcceptedOfferToWaitingForPaperworkUsingTriggerWaitForPaperwork != null) return GuardClauseFromAcceptedOfferToWaitingForPaperworkUsingTriggerWaitForPaperwork(); return true; });

            //_machine.Configure(State.WaitingForPaperwork)
            //  .OnEntry(() => { if (OnWaitingForPaperworkEntry != null) OnWaitingForPaperworkEntry(); })
            //  .OnExit(() => { if (OnWaitingForPaperworkExit != null) OnWaitingForPaperworkExit(); })
            //  .PermitIf(Trigger.VitelcoStuff, State.VitelcoStuff, () => { if (GuardClauseFromWaitingForPaperworkToVitelcoStuffUsingTriggerVitelcoStuff != null) return GuardClauseFromWaitingForPaperworkToVitelcoStuffUsingTriggerVitelcoStuff(); return true; });

            //_machine.Configure(State.VitelcoStuff)
            //  .OnEntry(() => { if (OnVitelcoStuffEntry != null) OnVitelcoStuffEntry(); })
            //  .OnExit(() => { if (OnVitelcoStuffExit != null) OnVitelcoStuffExit(); });


            _machine.Configure(State.Active)
            .OnEntry(() => { if (OnEntry != null) OnEntry(State.Active); })
            .OnExit(() => { if (OnExit != null) OnExit(State.Active); })
            .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.Active); return true; })
            .PermitIf(Trigger.SubmitToManager, State.SubmittedToManager, () => { if (GuardClauseFromToUsingTriggerOthers != null) return GuardClauseFromToUsingTriggerOthers(State.Active); return true; });

            _machine.Configure(State.SubmittedToManager)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.SubmittedToManager); })
              .OnExit(() => { if (OnExit != null) OnExit(State.SubmittedToManager); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.SubmittedToManager); return true; })
              .PermitIf(Trigger.UnsuitableTechnically, State.UnsuitableTechnically, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.SubmittedToManager); return true; })
              .PermitIf(Trigger.UnsuitableInTermsOfBudget, State.UnsuitableInTermsOfBudget, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.SubmittedToManager); return true; })
              .PermitIf(Trigger.ShareWithCustomer, State.SharedWithCustomer, () => { if (GuardClauseFromToUsingTriggerOthers != null) return GuardClauseFromToUsingTriggerOthers(State.SubmittedToManager); return true; });

            _machine.Configure(State.UnsuitableTechnically)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.UnsuitableTechnically); })
              .OnExit(() => { if (OnExit != null) OnExit(State.UnsuitableTechnically); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.UnsuitableTechnically); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.UnsuitableTechnically); return true; });
              

            _machine.Configure(State.UnsuitableInTermsOfBudget)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.UnsuitableInTermsOfBudget); })
              .OnExit(() => { if (OnExit != null) OnExit(State.UnsuitableInTermsOfBudget); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.UnsuitableInTermsOfBudget); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.UnsuitableInTermsOfBudget); return true; });


            _machine.Configure(State.SharedWithCustomer)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.SharedWithCustomer); })
              .OnExit(() => { if (OnExit != null) OnExit(State.SharedWithCustomer); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.SharedWithCustomer); return true; })
              .PermitIf(Trigger.CustomerWillInterview, State.CustomerWillInterview, () => { if (GuardClauseFromToUsingTriggerOthers != null) return GuardClauseFromToUsingTriggerOthers(State.SharedWithCustomer); return true; })
              .PermitIf(Trigger.CvNotApprovedByCustomer, State.CvNotApprovedByCustomer, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.SharedWithCustomer); return true; })
              .PermitIf(Trigger.WithdrewBeforeMeeting, State.WithdrewBeforeMeeting, () => { if (GuardClauseFromToUsingTriggerWithdrew != null) return GuardClauseFromToUsingTriggerWithdrew(State.SharedWithCustomer); return true; });

            _machine.Configure(State.CvNotApprovedByCustomer)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.CvNotApprovedByCustomer); })
              .OnExit(() => { if (OnExit != null) OnExit(State.CvNotApprovedByCustomer); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.CvNotApprovedByCustomer); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.CvNotApprovedByCustomer); return true; });


            _machine.Configure(State.WithdrewBeforeMeeting)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.WithdrewBeforeMeeting); })
              .OnExit(() => { if (OnExit != null) OnExit(State.WithdrewBeforeMeeting); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.WithdrewBeforeMeeting); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.WithdrewBeforeMeeting); return true; });


            _machine.Configure(State.CustomerWillInterview)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.CustomerWillInterview); })
              .OnExit(() => { if (OnExit != null) OnExit(State.CustomerWillInterview); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.CustomerWillInterview); return true; })
              .PermitIf(Trigger.OfferMake, State.OfferMade, () => { if (GuardClauseFromToUsingTriggerOthers != null) return GuardClauseFromToUsingTriggerOthers(State.CustomerWillInterview); return true; })
              .PermitIf(Trigger.RejectInCustomerInterview, State.RejectedInCustomerInterview, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.CustomerWillInterview); return true; })
              .PermitIf(Trigger.DidnotComeToInterview, State.DidnotComeToInterview, () => { if (GuardClauseFromToUsingTriggerWithdrew != null) return GuardClauseFromToUsingTriggerWithdrew(State.CustomerWillInterview); return true; })
              .PermitIf(Trigger.CustomerRequestCandidateWithdrew, State.CustomerRequestedCandidateWithdrew, () => { if (GuardClauseFromToUsingTriggerWithdrew != null) return GuardClauseFromToUsingTriggerWithdrew(State.CustomerWillInterview); return true; });

            _machine.Configure(State.RejectedInCustomerInterview)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.RejectedInCustomerInterview); })
              .OnExit(() => { if (OnExit != null) OnExit(State.RejectedInCustomerInterview); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.RejectedInCustomerInterview); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.RejectedInCustomerInterview); return true; });


            _machine.Configure(State.DidnotComeToInterview)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.DidnotComeToInterview); })
              .OnExit(() => { if (OnExit != null) OnExit(State.DidnotComeToInterview); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.DidnotComeToInterview); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.DidnotComeToInterview); return true; });


            _machine.Configure(State.CustomerRequestedCandidateWithdrew)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.CustomerRequestedCandidateWithdrew); })
              .OnExit(() => { if (OnExit != null) OnExit(State.CustomerRequestedCandidateWithdrew); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.CustomerRequestedCandidateWithdrew); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.CustomerRequestedCandidateWithdrew); return true; });


            _machine.Configure(State.OfferMade)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.OfferMade); })
              .OnExit(() => { if (OnExit != null) OnExit(State.OfferMade); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.OfferMade); return true; })
              .PermitIf(Trigger.AcceptOffer, State.AcceptedOffer, () => { if (GuardClauseFromToUsingTriggerOthers != null) return GuardClauseFromToUsingTriggerOthers(State.OfferMade); return true; })
              .PermitIf(Trigger.RejectOffer, State.RejectedOffer, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.OfferMade); return true; });

            _machine.Configure(State.RejectedOffer)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.RejectedOffer); })
              .OnExit(() => { if (OnExit != null) OnExit(State.RejectedOffer); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.RejectedOffer); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.RejectedOffer); return true; });



            _machine.Configure(State.AcceptedOffer)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.AcceptedOffer); })
              .OnExit(() => { if (OnExit != null) OnExit(State.AcceptedOffer); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.AcceptedOffer); return true; })
              .PermitIf(Trigger.WaitForPaperwork, State.WaitingForPaperwork, () => { if (GuardClauseFromToUsingTriggerOthers != null) return GuardClauseFromToUsingTriggerOthers(State.AcceptedOffer); return true; });

            _machine.Configure(State.WaitingForPaperwork)
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.WaitingForPaperwork); })
              .OnExit(() => { if (OnExit != null) OnExit(State.WaitingForPaperwork); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.WaitingForPaperwork); return true; })
              .PermitIf(Trigger.VitelcoStuff, State.VitelcoStuff, () => { if (GuardClauseFromToUsingTriggerOthers != null) return GuardClauseFromToUsingTriggerOthers(State.WaitingForPaperwork); return true; });

            _machine.Configure(State.VitelcoStuff)
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.VitelcoStuff); return true; })
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.VitelcoStuff); })
              .OnExit(() => { if (OnExit != null) OnExit(State.VitelcoStuff); });

            _machine.Configure(State.Quit)
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.Quit); return true; })
              .OnEntry(() => { if (OnEntry != null) OnEntry(State.Quit); })
              .OnExit(() => { if (OnExit != null) OnExit(State.Quit); })
              .PermitReentryIf(Trigger.Save, () => { if (GuardClauseFromToUsingTriggerSave != null) return GuardClauseFromToUsingTriggerSave(State.Quit); return true; })
              .PermitIf(Trigger.CandidateInCvPool, State.CandidateInCvPool, () => { if (GuardClauseFromToUsingTriggerReject != null) return GuardClauseFromToUsingTriggerReject(State.Quit); return true; });


            _machine.OnUnhandledTrigger((state, trigger) => { if (OnUnhandledTrigger != null) OnUnhandledTrigger(state, trigger); });
        }

        public enum Trigger
        {
            Save=0,
            [Display(Name = "Submitted To Management")]
            SubmitToManager =2,
            [Display(Name = "Unsuitable Technically")]
            UnsuitableTechnically =3,
            [Display(Name = "Unsuitable In Terms Of Budget")]
            UnsuitableInTermsOfBudget =4,
            [Display(Name = "Candidate Shared With Customer")]
            ShareWithCustomer =5,
            [Display(Name = "Cv Not Approved By Customer")]
            CvNotApprovedByCustomer =6,
            [Display(Name = "Candidate Withdrew Before Meeting")]
            WithdrewBeforeMeeting =7,
            [Display(Name = "Customer Will Interview")]
            CustomerWillInterview =8,
            [Display(Name = "Rejected In Customer Interview")]
            RejectInCustomerInterview =9,
            [Display(Name = "Didn't Come To Interview")]
            DidnotComeToInterview =10,
            [Display(Name = "Customer Requested Candidate Withdrew")]
            CustomerRequestCandidateWithdrew =11,
            [Display(Name = "Rejected Offer")]
            RejectOffer =12,
            [Display(Name = "Offer Made")]
            OfferMake =13,
            [Display(Name = "Accepted Offer")]
            AcceptOffer =14,
            [Display(Name = "Waiting For Paperwork")]
            WaitForPaperwork =15,
            [Display(Name = "Vitelco Staff")]
            VitelcoStuff =80,
            [Display(Name = "Quit")]
            Quit = 81,
            [Display(Name = "Candidate In CV Pool")]
            CandidateInCvPool = 82
        }

        public enum State
        {
            [Display(Name = "Active")]
            Active = 1,
            [Display(Name = "Submitted To Management")]
            SubmittedToManager = 2,
            [Display(Name = "Unsuitable Technically")]
            UnsuitableTechnically = 3,
            [Display(Name = "Unsuitable In Terms Of Budget")]
            UnsuitableInTermsOfBudget = 4,
            [Display(Name = "Candidate Shared With Customer")]
            SharedWithCustomer = 5,
            [Display(Name = "Cv Not Approved By Customer")]
            CvNotApprovedByCustomer = 6,
            [Display(Name = "Candidate Withdrew Before Meeting")]
            WithdrewBeforeMeeting = 7,
            [Display(Name = "Customer Will Interview")]
            CustomerWillInterview = 8,
            [Display(Name = "Rejected In Customer Interview")]
            RejectedInCustomerInterview = 9,
            [Display(Name = "Didn't Come To Interview")]
            DidnotComeToInterview = 10,
            [Display(Name = "Customer Requested Candidate Withdrew")]
            CustomerRequestedCandidateWithdrew = 11,
            [Display(Name = "Rejected Offer")]
            RejectedOffer = 12,
            [Display(Name = "Offer Made")]
            OfferMade = 13,
            [Display(Name = "Accepted Offer")]
            AcceptedOffer = 14,
            [Display(Name = "Waiting For Paperwork")]
            WaitingForPaperwork = 15,
            [Display(Name = "Vitelco Staff")]
            VitelcoStuff = 80,
            [Display(Name = "Quit")]
            Quit = 81,
            [Display(Name = "Candidate In CV Pool")]
            CandidateInCvPool = 82
        }


        public List<Trigger> GetNextTransitions()
        {
            return _machine.PermittedTriggers.Where(x=> x.ToString()!="Save").ToList();
        }

        public bool TryFireTrigger(Trigger trigger)
        {
            if (!_machine.CanFire(trigger))
            {
                return false;
            }

            _machine.Fire(trigger);
            return true;
        }

        public State GetState
        {
            get
            {
                return _machine.State;
            }
        }
    }
}
