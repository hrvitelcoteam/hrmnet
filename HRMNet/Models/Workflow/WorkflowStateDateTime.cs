﻿using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Workflow
{
    public class WorkflowStateDateTime: BaseEntity
    {
        public virtual Candidate Candidate { get; set; }

        public int CandidateId { get; set; }

        public virtual CandidateStatus Status { get; set; }

        //public virtual User User { get; set; }

        //public int? UserId { get; set; }


    }
}
