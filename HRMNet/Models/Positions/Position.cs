﻿using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using HRMNet.Models.Customers;
using HRMNet.Models.Sops;
using HRMNet.Models.Timesheet;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using HRMNet.Models.Identity;

namespace HRMNet.Models.Positions
{
    public class Position : BaseEntity
    {
        public Position()
        {
            StartingDate = DateTime.Now;
            //StartingDate = DateTime.Now;
            //PositionTypes = new HashSet<PositionType>();
        }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Position Name")]
        [StringLength(450)]
        public virtual string PositionName { get; set; }

        //[Required(ErrorMessage = "*")]
        //[Display(Name = "Customer Name")]

        //public virtual string CustomerName { get; set; }

        [Display(Name = "Project Name")]
        [Required(ErrorMessage = "*")]
        [StringLength(450)]
        public virtual string ProjectName { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Features")]
        public virtual string Features { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Type")]
        public virtual string PositionType { get; set; }

        [Display(Name = "Must To Have")]
        public virtual string MustToHave { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Starting Date")]
        [DataType(DataType.Date)]
        public virtual DateTime StartingDate { get; set; }

        [Display(Name = "Status")]
        public virtual PositionStatus Status { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Job Description")]
        [StringLength(2000)]
        public virtual string JobDescription {get; set;}

        [Display(Name = "Is Closed")]
        public virtual bool IsClosed { get; set; }

        [Display(Name = "Closing Date")]
        public virtual DateTime? ClosingDate { get; set; }

        //[Required(ErrorMessage = "*")]
        [Display(Name = "Close/Suspend Reason")]
        public virtual string ClosingOrSuspendingReason { get; set; }

        [Display(Name = "Exist On the Kariyer?")]
        public virtual bool ExistOnKariyer { get; set; }

        [Display(Name = "Exist On the LinkedIn?")]
        public virtual bool ExistOnLinkedIn { get; set; }

        [Display(Name = "Responsible User")]
        [StringLength(450)]
        public virtual string ResponsibleMan { get; set; }

        public virtual bool IsBringAppropriateStarted { get; set; }


        public virtual string RelatedPositionExperience { get; set; }

        public virtual string PersonalCharacteristics { get; set; }

        [Display(Name = "CVFileName")]
        public virtual string CVFileName { get; set; }

        [Display(Name = "Is On BlackList")]
        public virtual bool IsOnBlackList { get; set; }

        [StringLength(450)]
        [Display(Name = "BlackList Reason")]
        public virtual string BlackListReason { get; set; }

        public virtual string Expertises { get; set; } 

        public virtual Customer Customer { get; set; }

        [Display(Name = "Customer Name")]
        public int CustomerId { get; set; }

        [NotMapped]
        public string InitialiType { get; set; }

        [NotMapped]
        public string TypesSource { get; set; }

        [NotMapped]
        [Display(Name = "Expertise Types")]
        public string[] SelectedTypes { get; set; }

        [NotMapped]
        public string InitialFeature { get; set; }

        [NotMapped]
        public string InitialMandatoryFeature { get; set; }

        [NotMapped]
        public string FeaturesSource { get; set; }

        [NotMapped]
        [Display(Name = "Features")]
        //[Required(ErrorMessage = "*")]
        public string[] SelectedFeatures { get; set; }

        //public string FeatureList {

        //    get //; set;
        //    {
        //        string list = string.Empty;
        //        //if (PositionFeatures?.Count > 0)
        //        //{
        //        //    PositionFeatures.Select(x => x.Feature).ToList().ForEach(x => list += x.FeatureName + " ");
        //        //}

        //        return list;
        //    }

        //    set { FeatureList = value; }
        //}

        //public string MandatoryFeatureList {
        //    get //; set;
        //    {
        //        string list = string.Empty;
        //        //if (PositionFeatures?.Count > 0)
        //        //{
        //        //    PositionMandatoryFeatures.Select(x => x.Feature).ToList().ForEach(x => list += x.FeatureName + " ");
        //        //}

        //        return list;
        //    }

        //    set { MandatoryFeatureList = value; }
        //}

        //public virtual User User { get; set; }

        //public int? UserId { get; set; }

        [NotMapped]
        [Display(Name = "Must To Have")]
        //[Required(ErrorMessage = "*")]
        public string[] SelectedMandatoryFeatures { get; set; }

        public virtual ICollection<CandidatePosition> CandidatePositions { get; set; }

        public virtual ICollection<CandidatePositionHistory> CandidatePositionsHistory { get; set; }

        public virtual ICollection<PositionFeature> PositionFeatures { get; set; }

        public virtual ICollection<PositionMandatoryFeature> PositionMandatoryFeatures { get; set; }

        public virtual ICollection<PositionType> PositionTypes { get; set; }

        public virtual ICollection<Meeting> Meetings { get; set; }

        public virtual ICollection<Offer> Offers { get; set; }

        public virtual ICollection<PreCandidateMettings> PreCandidateMettings { get; set; }

        public virtual ICollection<TimesheetItem> TimesheetItems { get; set; }

        public virtual ICollection<SopPosition> SopPositions { get; set; }



    }
}
