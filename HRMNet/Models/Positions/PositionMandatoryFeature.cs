﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Positions
{
    public class PositionMandatoryFeature
    {
        public int Id { get; set; }

        public virtual Position Position { get; set; }

        public int PositionId { get; set; }

        public virtual Feature Feature { get; set; }

        public int FeatureId { get; set; }
    }
}
