﻿using HRMNet.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Models.Positions
{
    public class PositionType
    {
        public virtual int Id { get; set; }

        public virtual Position Position { get; set; }

        public int PositionId { get; set; }

        public virtual Expertise Expertise { get; set; }

        public int ExpertiseId { get; set; }


    }
}
