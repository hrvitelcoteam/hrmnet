﻿using HRMNet.Data;
using HRMNet.Models.Common;
using HRMNet.Services.CustomSettingsService;
using HRMNet.Services.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HRMNet.Models.ViewModel.CustomSetting;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Settings")]
    public class CustomSettingsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<CustomSetting> _settings;
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private readonly IWebHostEnvironment _environment;
        public CustomSettingsController(IUnitOfWork uow, IWebHostEnvironment environment)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _settings = uow.Set<CustomSetting>();
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
        }

        // GET: CustomSettings
        public async Task<IActionResult> Index()
        {
            return View(await _settings.ToListAsync());
        }

        // GET: CustomSettings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customSetting = await _settings
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customSetting == null)
            {
                return NotFound();
            }

            return View(customSetting);
        }

        // GET: CustomSettings/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CustomSettings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Type,Value")] CustomSetting customSetting)
        {
            if (ModelState.IsValid)
            {
                _settings.Add(customSetting);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(customSetting);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete Attachment")]
        public IActionResult DeleteAttachment(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var path = Path.Combine(_environment.WebRootPath, "Files", "GeneralSettings", id);
                System.IO.File.Delete(path);
                return Json("ok");
            }
            else
                return Json("not ok");
        }

        private string GetfileType(string extension)
        {
            switch (extension)
            {
                case "pdf":
                    return "pdf";
                case "png":
                    return "image";
                case "jpg":
                    return "image";
                case "jpeg":
                    return "image";
                case "tiff":
                    return "gdocs";
                case "ppt":
                    return "office";
                case "doc":
                    return "office";
                case "docx":
                    return "office";
                case "xls":
                    return "office";
                case "xlsx":
                    return "office";
                case "mp4":
                    return "video";
                case "txt":
                    return "text";
                case "html":
                    return "html";
                default:
                    return "image";
            }
        }

        // GET: CustomSettings/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public IActionResult Edit()
        {
            GeneralSettings setting = new GeneralSettings();
            setting.Load(_uow);
            
            string preview = string.Empty;
            string previewConfig = string.Empty;
            int key = 0;
            if (!string.IsNullOrWhiteSpace(setting.AttachFile))
            {
                var fileExtension = setting.AttachFile.Split(".", StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

                key += 1;
                preview += "\"/Files/GeneralSettings/" + setting.AttachFile + "\",";
                previewConfig += "{type:\"" + GetfileType(fileExtension) + "\", caption:\"" + "" +
                                 "\",size:\"" + "" + "\",width:\"120px\",url:\"/CustomSettings/DeleteAttachment/" +
                                 setting.AttachFile + "\",key:\"" + setting.AttachFile + "\"},";
            }

            var config = new AttachmentViewModel()
            {
                //Id = setting. id.Value,
                SerializedInitialPreview = preview,
                SerializedInitialPreviewConfig = previewConfig,
                append = true
            };

            var returnModel = new CustomSettingViewModel()
            {
                SendingEmailGeneralBody= setting.SendingEmailGeneralBody,
                AttachmentViewModel = config
            };

            return View(returnModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CustomSettingViewModel model, List<IFormFile> files)
        {
            if (ModelState.IsValid)
            {
                var settings = new Settings(_uow);
                settings.General.SendingEmailGeneralBody = model.SendingEmailGeneralBody;

                if (files != null && files.Any())
                {
                    var file = files.FirstOrDefault();
                    var fileName = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
                    var fileExtension = file.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
                    var path = Path.Combine(_environment.WebRootPath, "Files", "GeneralSettings", fileName + "." + fileExtension.LastOrDefault());
                    using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write,
                        FileShare.None,
                        MaxBufferSize,
                        // you have to explicitly open the FileStream as asynchronous
                        // or else you're just doing synchronous operations on a background thread.
                        useAsync: true))
                    {
                        await file.CopyToAsync(fileStream);
                    }

                    //var toAdd = new Attachment { IsImage = ImageToolkit.IsImageFile(item), FileName = fileName + "." + fileExtension.LastOrDefault(), Caption = item.FileName, ContentType = item.ContentType };
                    settings.General.AttachFile= fileName + "." + fileExtension.LastOrDefault();
                }
                else
                {
                    settings.General.AttachFile = string.Empty;
                }

                var result = await settings.Save();
                if (result)
                    return RedirectToAction("Index", "Home");

            }
            return View(model);
        }

        // GET: CustomSettings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customSetting = await _settings
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customSetting == null)
            {
                return NotFound();
            }

            return View(customSetting);
        }

        // POST: CustomSettings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var customSetting = await _settings.FindAsync(id);
            _settings.Remove(customSetting);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CustomSettingExists(int id)
        {
            return _settings.Any(e => e.Id == id);
        }
    }
}
