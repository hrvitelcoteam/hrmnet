﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
//using DNTCommon.Web.Core;
using HRMNet.Hubs;
using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.ViewModel.Identity;
using HRMNet.Models.ViewModel.Identity.Emails;
using HRMNet.Services;
using HRMNet.Services.Contracts.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;

namespace HRMNet.Controllers
{
    [DisplayName("Changing Password")]
    public class ChangePasswordController : Controller
    {
        private readonly IEmailSender _emailSender;
        private readonly IApplicationUserManager _userManager;
        private readonly IApplicationSignInManager _signInManager;
        private readonly IPasswordValidator<User> _passwordValidator;
        private readonly IUsedPasswordsService _usedPasswordsService;
        private readonly IOptionsSnapshot<SiteSettings> _siteOptions;
        private readonly IHubContext<MessageHub> _messageHubContext;
        private readonly IStringLocalizer _localizer;

        public ChangePasswordController(
            IApplicationUserManager userManager,
            IApplicationSignInManager signInManager,
            IEmailSender emailSender,
            IPasswordValidator<User> passwordValidator,
            IHubContext<MessageHub> messageHubContext,
            IUsedPasswordsService usedPasswordsService,
            IStringLocalizerFactory stringLocalizerFactory,
            IOptionsSnapshot<SiteSettings> siteOptions)
        {
            _userManager = userManager;
            _userManager.CheckArgumentIsNull(nameof(_userManager));

            _signInManager = signInManager;
            _signInManager.CheckArgumentIsNull(nameof(_signInManager));

            _passwordValidator = passwordValidator;
            _passwordValidator.CheckArgumentIsNull(nameof(_passwordValidator));

            _usedPasswordsService = usedPasswordsService;
            _usedPasswordsService.CheckArgumentIsNull(nameof(_usedPasswordsService));

            _emailSender = emailSender;
            _emailSender.CheckArgumentIsNull(nameof(_emailSender));

            _siteOptions = siteOptions;
            _siteOptions.CheckArgumentIsNull(nameof(_siteOptions));

            _messageHubContext = messageHubContext ?? throw new ArgumentNullException(nameof(messageHubContext));

            _localizer = stringLocalizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
        }

        public async Task<IActionResult> Index()
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var passwordChangeDate = await _usedPasswordsService.GetLastUserPasswordChangeDateAsync(userId);
            return View(model: new ChangePasswordViewModel
            {
                LastUserPasswordChangeDate = passwordChangeDate
            });
        }

        /// <summary>
        /// For [Remote] validation
        /// </summary>
        [HttpPost, ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> ValidatePassword(string newPassword)
        {
            var user = await _userManager.GetCurrentUserAsync();
            var result = await _passwordValidator.ValidateAsync(
                (UserManager<User>)_userManager, user, newPassword);
            return Json(result.Succeeded ? "true" : result.DumpErrors(useHtmlNewLine: true));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetCurrentUserAsync();
            if (user == null)
            {
                return View("NotFound");
            }

            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                await _userManager.UpdateSecurityStampAsync(user);

                // reflect the changes in the Identity cookie
                await _signInManager.RefreshSignInAsync(user);

                //try
                //{

                //    await _emailSender.SendEmailAsync(
                //           email: user.Email,
                //           subject: "Password change notification",
                //           viewNameOrPath: "~/Views/EmailTemplates/_ChangePasswordNotification.cshtml",
                //           model: new ChangePasswordNotificationViewModel
                //           {
                //               User = user,
                //               EmailSignature = _siteOptions.Value.Smtp.FromName,
                //               MessageDateTime = DateTime.UtcNow.ToLongDateString()
                //           });

                //    await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"success|" + _localizer["The email has been sent successfully"] + "|");

                //    await Task.Delay(5000);
                //}
                //catch (Exception e)
                //{
                //    Console.WriteLine(e);
                //    await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"error|" + _localizer["The email was failed"] + "|");
                //    await Task.Delay(15000);
                //}

                return RedirectToAction(nameof(Index), "UserCard", routeValues: new { id = user.Id });
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }

            return View(model);
        }
    }
}
