﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Workflow;

namespace HRMNet.Controllers
{
    public class WorkflowStateDateTimesController : Controller
    {
        private readonly HRMNetContext _context;

        public WorkflowStateDateTimesController(HRMNetContext context)
        {
            _context = context;
        }

        // GET: WorkflowStateDateTimes
        public async Task<IActionResult> Index()
        {
            var hRMNetContext = _context.WorkflowStateDateTimes.Include(w => w.Candidate);
            return View(await hRMNetContext.ToListAsync());
        }

        // GET: WorkflowStateDateTimes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflowStateDateTime = await _context.WorkflowStateDateTimes
                .Include(w => w.Candidate)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workflowStateDateTime == null)
            {
                return NotFound();
            }

            return View(workflowStateDateTime);
        }

        // GET: WorkflowStateDateTimes/Create
        public IActionResult Create()
        {
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Email");
            return View();
        }

        // POST: WorkflowStateDateTimes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CandidateId,Status,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] WorkflowStateDateTime workflowStateDateTime)
        {
            if (ModelState.IsValid)
            {
                _context.Add(workflowStateDateTime);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Email", workflowStateDateTime.CandidateId);
            return View(workflowStateDateTime);
        }

        // GET: WorkflowStateDateTimes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflowStateDateTime = await _context.WorkflowStateDateTimes.FindAsync(id);
            if (workflowStateDateTime == null)
            {
                return NotFound();
            }
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Email", workflowStateDateTime.CandidateId);
            return View(workflowStateDateTime);
        }

        // POST: WorkflowStateDateTimes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CandidateId,Status,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] WorkflowStateDateTime workflowStateDateTime)
        {
            if (id != workflowStateDateTime.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(workflowStateDateTime);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkflowStateDateTimeExists(workflowStateDateTime.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidateId"] = new SelectList(_context.Candidate, "Id", "Email", workflowStateDateTime.CandidateId);
            return View(workflowStateDateTime);
        }

        // GET: WorkflowStateDateTimes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflowStateDateTime = await _context.WorkflowStateDateTimes
                .Include(w => w.Candidate)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workflowStateDateTime == null)
            {
                return NotFound();
            }

            return View(workflowStateDateTime);
        }

        // POST: WorkflowStateDateTimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var workflowStateDateTime = await _context.WorkflowStateDateTimes.FindAsync(id);
            _context.WorkflowStateDateTimes.Remove(workflowStateDateTime);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WorkflowStateDateTimeExists(int id)
        {
            return _context.WorkflowStateDateTimes.Any(e => e.Id == id);
        }
    }
}
