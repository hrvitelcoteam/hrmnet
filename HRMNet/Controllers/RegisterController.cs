﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DNTCommon.Web.Core;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.ViewModel.Identity;
using HRMNet.Models.ViewModel.Identity.Emails;
using HRMNet.Services;
using HRMNet.Services.Contracts.Identity;
using HRMNet.Services.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace HRMNet.Controllers
{
    //[Authorize]
    [Authorize(Roles = "Admin")]
    public class RegisterController : Controller
    {
        private readonly IEmailSender _emailSender;
        //private readonly ILogger<RegisterController> _logger;
        private readonly IApplicationUserManager _userManager;
        private readonly IPasswordValidator<User> _passwordValidator;
        private readonly IUserValidator<User> _userValidator;
        private readonly IOptionsSnapshot<SiteSettings> _siteOptions;
        private readonly IApplicationSignInManager _signInManager;
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Title> _title;
        public RegisterController
        (
            IUnitOfWork uow,
            IApplicationSignInManager signInManager,
            IApplicationUserManager userManager,
            IPasswordValidator<User> passwordValidator,
            IUserValidator<User> userValidator,
            IEmailSender emailSender,
            IOptionsSnapshot<SiteSettings> siteOptions
            //ILogger<RegisterController> logger
            )
        {
            _userManager = userManager;
            _userManager.CheckArgumentIsNull(nameof(_userManager));

            _passwordValidator = passwordValidator;
            _passwordValidator.CheckArgumentIsNull(nameof(_passwordValidator));

            _userValidator = userValidator;
            _userValidator.CheckArgumentIsNull(nameof(_userValidator));

            _emailSender = emailSender;
            _emailSender.CheckArgumentIsNull(nameof(_emailSender));

            //_logger = logger;
            //_logger.CheckArgumentIsNull(nameof(_logger));

            _siteOptions = siteOptions;
            _siteOptions.CheckArgumentIsNull(nameof(_siteOptions));

            _signInManager = signInManager;
            _signInManager.CheckArgumentIsNull(nameof(_signInManager));
            
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _title = uow.Set<Title>();

        }

        /// <summary>
        /// For [Remote] validation
        /// </summary>
        [AjaxOnly, HttpPost, ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> ValidateUsername(string username, string email)
        {
            var result = await _userValidator.ValidateAsync(
                (UserManager<User>)_userManager, new User { UserName = username, Email = email });
            return Json(result.Succeeded ? "true" : result.DumpErrors(useHtmlNewLine: true));
        }

        /// <summary>
        /// For [Remote] validation
        /// </summary>
        [AjaxOnly, HttpPost, ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> ValidatePassword(string password, string username)
        {
            var result = await _passwordValidator.ValidateAsync(
                (UserManager<User>)_userManager, new User { UserName = username }, password);
            return Json(result.Succeeded ? "true" : result.DumpErrors(useHtmlNewLine: true));
        }

        [DisplayName("Confirm Email")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("NotFound");
            }

            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? nameof(ConfirmEmail) : "Error");
        }

        [DisplayName("Register")]
        public async Task<IActionResult> Index(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewData["TitleId"] = await _title.AsNoTracking().ToListAsync();

            return View();
        }

        public IActionResult ConfirmedRegisteration()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[ValidateDNTCaptcha(CaptchaGeneratorLanguage = DNTCaptcha.Core.Providers.Language.Persian)]
        public async Task<IActionResult> Index(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.Username,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.PhoneNumber,
                    TitleId = model.TitleId
                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //_logger.LogInformation(3, $"{user.UserName} created a new account with password.");

                    if (_siteOptions.Value.EnableEmailConfirmation)
                    {
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        //ControllerExtensions.ShortControllerName<RegisterController>(), //todo: use everywhere .................

                        await _emailSender.SendEmailAsync(
                           email: user.Email,
                           subject: "Please Confirm your account",
                           viewNameOrPath: "~/Views/EmailTemplates/_RegisterEmailConfirmation.cshtml",
                           model: new RegisterEmailConfirmationViewModel
                           {
                               User = user,
                               EmailConfirmationToken = code,
                               EmailSignature = _siteOptions.Value.Smtp.FromName,
                               MessageDateTime = DateTime.UtcNow.ToLongDateString()
                           });

                        return RedirectToAction(nameof(ConfirmYourEmail));
                    }

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        var registeredUser = await _userManager.FindByNameAsync(model.Username);
                        if (user != null)
                        {
                            var siginResult = await _signInManager.PasswordSignInAsync(
                                        model.Username,
                                        model.Password,
                                        false,
                                        lockoutOnFailure: true);

                            if (siginResult.Succeeded)
                            {
                                //_logger.LogInformation(1, $"{model.Username} logged in.");
                                return Redirect(returnUrl);
                            }
                        }
                    }

                    return RedirectToAction(nameof(ConfirmedRegisteration));
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        public IActionResult ConfirmYourEmail()
        {
            return View();
        }
    }
}
