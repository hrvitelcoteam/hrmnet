﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel;
using HRMNet.Services.Identity;
using HRMNet.Models.Customers;
using HRMNet.Models.Common;
using HRMNet.Services;
using System.Text;
using Microsoft.AspNetCore.SignalR;
using HRMNet.Hubs;
using HRMNet.Models.Identity;
using HRMNet.Models.Positions;
using Microsoft.Extensions.Localization;
using HRMNet.Services.Contracts.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Drawing;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using System.Globalization;
using HRMNet.Models.Workflow;

namespace HRMNet.Controllers
{
    [AllowAnonymous]
    public class FileImportController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<PreCandidate> _context;
        private readonly DbSet<Candidate> _candidate;
        private readonly DbSet<CandidatePosition> _candidatePosition;
        private readonly DbSet<Customer> _customer;
        private readonly DbSet<User> _user;
        private readonly DbSet<PreCandidateMettings> _meeting;
        private readonly DbSet<Position> _position;
        private readonly IApplicationSignInManager _signInManager;
        private readonly IApplicationUserManager _userManager;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public FileImportController(IUnitOfWork uow, IApplicationSignInManager signInManager, IApplicationUserManager userManager)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _context = uow.Set<PreCandidate>();
            _customer = uow.Set<Customer>();
            _candidate = uow.Set<Candidate>();
            _candidatePosition = uow.Set<CandidatePosition>();
            _meeting = uow.Set<PreCandidateMettings>();
            _position = uow.Set<Position>();
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(_signInManager));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        [HttpPost]
        public async Task<IActionResult> ImportPreCandidate([FromForm] IFormFile file, [FromHeader] string username, [FromHeader] string password)
        {
            var source = new List<PreCandidateImportModel>();
            try
            {
                await SignInAsGivenUser(username, password);
                source.AddRange(CsvToList<PreCandidateImportModel, PreCandidateImportModelMap>(file));

                if (!source.Any())
                    return Json("The file is empty");

                foreach (var item in source)
                {
                    var position = await _position
                    .Include(x => x.Customer)
                    .Include(x => x.PositionTypes)
                    .Include(x => x.PositionFeatures)
                    .FirstOrDefaultAsync(p =>
                    p.PositionName == item.Position.Trim() &&
                    p.Customer.Name == item.Customer.Trim() &&
                    p.ProjectName == item.ProjectName.Trim());

                    if (position == null)
                    {
                        item.HasError = true;
                        item.ErrorMessage = "Position name is incorrect";
                        continue;
                    }


                    var preCandidate = new PreCandidate
                    {
                        Telephone = item.Telephone,
                        Email = item.Email,
                        Name = item.Name,
                        Family = item.Family,
                        CVSource = (CVSource)item.CVSource,
                        Status = (PreCandidateStatus)item.Status,
                        Note = item.PositionNote,
                        SendMail = false,
                        PreCandidateMeetings = new List<PreCandidateMettings>
                    {
                        new PreCandidateMettings
                        {
                            Note = item.PositionNote,
                            PositionId = position.Id,
                            CreatedDateTime = DateTime.Parse(item.InterviewDate)
                        }
                    }
                    };


                    
                    await _context.AddAsync(preCandidate);
                    item.ErrorMessage = "Successful";
                }

                if (source.Any(s => !s.HasError))
                    await _uow.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return Json($"Error: {ex.Message}");
            }

            return File(ListToExcel(source), XlsxContentType, $"Result-{Guid.NewGuid()}.xlsx");
        }

        [HttpPost]
        public async Task<IActionResult> ImportCandidate([FromForm] IFormFile file, [FromHeader] string username, [FromHeader] string password)
        {
            var source = new List<CandidateImportModel>();

            await SignInAsGivenUser(username, password);
            source.AddRange(CsvToList<CandidateImportModel, CandidateImportModelMap>(file));

            try
            {

                foreach (var item in source)
                {
                    item.ErrorMessage = string.Empty;

                    var candidate = new Candidate
                    {
                        Name = item.Name,
                        Family = item.Family,
                        Telephone = item.Telephone,
                        Email = item.Email.Trim(),
                        StartPeriod = item.StartPeriod,
                        CurrentWages = item.CurrentWages,
                        ExpectedWages = item.ExpectedWages,
                        TotalITexperience = item.TotalITexperience,
                        PositionRelatedExperience = item.PositionRelatedExperience,
                        IsHavingCV = item.IsHavingCV,
                        CVFilename = item.CVfilename,
                        IsOnBlackList = item.IsOnBlackList,
                        IsApproved = false,
                        Status = CandidateStatus.CandidateInCvPool,
                        WorkflowStateDateTimes = new List<WorkflowStateDateTime>()
                        {
                            new WorkflowStateDateTime
                            {
                                Status=CandidateStatus.CandidateInCvPool
                            }
                        }

                    };


                    var position = await _position
                    .Include(x => x.Customer)
                    .Include(x => x.PositionTypes)
                    .Include(x => x.PositionFeatures)
                    .FirstOrDefaultAsync(p =>
                    p.PositionName == item.Position.Trim() &&
                    p.Customer.Name == item.Customer.Trim() &&
                    p.ProjectName == item.ProjectName.Trim());

                    if (position == null)
                    {
                        item.HasError = true;
                        item.ErrorMessage = "Position name is incorrect";
                        //continue;
                    }
                    else
                    {
                        var candidateExpertises = position.PositionTypes.Select(x => new CandidateExpertise()
                        {
                            ExpertiseId = x.ExpertiseId
                        }).ToList();

                        var candidateFeatures = position.PositionFeatures.Select(x => new CandidateFeature()
                        {
                            FeatureId = x.FeatureId
                        }).ToList();

                        candidate.CandidateFeatures = candidateFeatures;
                        candidate.CandidateExpertises = candidateExpertises;
                        candidate.CandidatePositions = new List<CandidatePosition>()
                        {
                            new CandidatePosition()
                            {
                                PositionId=position.Id
                            }
                        };

                        candidate.Meetings = new List<Meeting>()
                        {
                            new Meeting()
                            {
                                MeetingDate=DateTime.Parse(item.InterviewDate),
                                MeetingResult=item.MeetingResult,
                                MeetingSpeakingPerson=item.MeetingSpeakingPerson,
                                MPositionId=position.Id
                            }
                        };

                    }

                    await _candidate.AddAsync(candidate);
                    if (string.IsNullOrEmpty(item.ErrorMessage))
                        item.ErrorMessage = "Successful";

                }
            }
            catch (Exception ex)
            {
                return Json($"Error: {ex.Message}");
            }

            if (source.Any(s => !s.HasError))
                await _uow.SaveChangesAsync();

            return File(ListToExcel(source), XlsxContentType, $"Result-{Guid.NewGuid()}.xlsx");
        }

        [HttpPost]
        public async Task<IActionResult> ImportStaff([FromForm] IFormFile file, [FromHeader] string username, [FromHeader] string password)
        {
            var source = new List<StaffImportModel>();

            await SignInAsGivenUser(username, password);
            source.AddRange(CsvToList<StaffImportModel, StaffImportModelMap>(file));

            foreach (var item in source)
            {
                var position = await _position
                    .Include(x => x.Customer)
                    .Include(x => x.PositionTypes)
                    .Include(x => x.PositionFeatures)
                    .FirstOrDefaultAsync(p =>
                    p.PositionName == item.Position.Trim() &&
                    p.Customer.Name == item.Customer.Trim() &&
                    p.ProjectName == item.ProjectName.Trim());

                if (position == null)
                {
                    item.HasError = true;
                    item.ErrorMessage = "Position name is incorrect";
                    continue;
                }

                var candidateExpertises = position.PositionTypes.Select(x => new CandidateExpertise()
                {
                    ExpertiseId = x.ExpertiseId
                }).ToList();

                var candidateFeatures = position.PositionFeatures.Select(x => new CandidateFeature()
                {
                    FeatureId = x.FeatureId
                }).ToList();

                var workFlows = new List<WorkflowStateDateTime>();
                CandidateStatus status = CandidateStatus.VitelcoStuff;
                workFlows.Add(
                        new WorkflowStateDateTime()
                        {
                            Status = CandidateStatus.VitelcoStuff,
                            CreatedDateTime = item.StartDate
                        });

                if (!item.ActiveWorker && item.EndDate.HasValue)
                {
                    status = CandidateStatus.Quit;
                    workFlows.Add(
                        new WorkflowStateDateTime()
                        {
                            Status = CandidateStatus.Quit,
                            CreatedDateTime = item.EndDate.Value
                        });
                }

                int titleId = 0;
                if (!string.IsNullOrWhiteSpace(item.Title))
                {
                    // TODO
                }

                int? workLocaionId;
                if (!string.IsNullOrWhiteSpace(item.WorkLocation))
                {
                    // TODO
                }

                var candidate = new Candidate
                {
                    Name = item.Name,
                    Family = item.Family,
                    Telephone = item.Telephone,
                    Email = item.Email.Trim(),
                    StartPeriod = item.StartPeriod,
                    CurrentWages = item.CurrentWages,
                    ExpectedWages = item.ExpectedWages,
                    TotalITexperience = item.TotalITexperience,
                    PositionRelatedExperience = item.PositionRelatedExperience,
                    IsHavingCV = item.IsHavingCV,
                    IsOnBlackList = item.IsOnBlackList,
                    IsApproved = false,
                    Status = status,
                    CandidateFeatures = candidateFeatures ?? new List<CandidateFeature>(),
                    CandidateExpertises = candidateExpertises ?? new List<CandidateExpertise>(),
                    StuffDetail = new StuffDetail()
                    {
                        TCNumber = item.TCNumber,
                        AccommodationAddress = item.AccommodationAddress,
                        MilitaryStatus = item.MilitaryStatus,
                        PostponementdDate = item.PostponementdDate,
                        ContractType = item.ContractType,
                        ContractDuration = item.ContractDuration,
                        SchoolOfGraduation = item.SchoolOfGraduation,
                        DepartmentOfGraduation = item.DepartmentOfGraduation,
                        DateOfGraduation = item.DateOfGraduation.HasValue ? item.DateOfGraduation.Value : DateTime.MinValue,
                        IBANNumber = item.IBANNumber,
                        CompanyEmail = item.CompanyEmail,
                        BloodGroup = item.BloodGroup,
                        EmergencyContactInformation = item.EmergencyContactInformation,
                        BirthCertificateNumber = item.BirthCertificateNumber,
                        //TitleId= titleId,
                        //WorkLocationId = workLocaionId.Value
                    },
                    CandidatePositions = new List<CandidatePosition>()
                        {
                            new CandidatePosition()
                            {
                                PositionId = position.Id
                            }
                        },
                    //Meetings = new List<Meeting>()
                    //{
                    //    new Meeting()
                    //    {
                    //        MeetingDate=DateTime.Parse(item.InterviewDate),
                    //        MeetingResult=item.MeetingResult,
                    //        MeetingSpeakingPerson=item.MeetingSpeakingPerson,
                    //        MPositionId=poistion.Id
                    //    }
                    //},
                    WorkflowStateDateTimes = workFlows,



                };

                try
                {
                    await _candidate.AddAsync(candidate);
                    await _uow.SaveChangesAsync();
                    item.ErrorMessage = "Successful";
                }
                catch (Exception ex)
                {
                    item.ErrorMessage = ex.Message;
                    //return Json($"Error: {ex.Message}");
                }

                //if (source.Any(s => !s.HasError))

            }

            return File(ListToExcel(source), XlsxContentType, $"Result-{Guid.NewGuid()}.xlsx");
        }


        private async Task SignInAsGivenUser(string username, string password)
        {
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
                throw new Exception("Username or Password is invalid.");
            var result = await _signInManager.PasswordSignInAsync(username, password, false, lockoutOnFailure: false);
            if (!result.Succeeded)
                throw new Exception("Username or Password is invalid.");
        }
        private List<TModel> CsvToList<TModel, TModelMap>(IFormFile file, string delimiter = ",") where TModelMap : ClassMap<TModel>
        {
            if (file == null || file.Length <= 0)
                throw new Exception("The file is empty");

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = delimiter,
                HeaderValidated = null,
                Encoding = UTF8Encoding.UTF8 // Encoding.GetEncoding("windows-1254") // for turkish characters
            };
            using StreamReader streamReader = new StreamReader(file.OpenReadStream(), UTF8Encoding.UTF8);
            using CsvReader csvReader = new CsvReader(streamReader, config);
            csvReader.Context.RegisterClassMap<TModelMap>();
            var list = csvReader.GetRecords<TModel>().ToList();
            if (!list.Any())
                throw new Exception("The file is empty");
            return list;
        }
        private byte[] ListToExcel<TModel>(List<TModel> source)
        {
            using var package = new ExcelPackage();
            var worksheet = package.Workbook.Worksheets.Add("Result");
            worksheet.Cells.LoadFromCollection(source, true, TableStyles.Dark9);
            worksheet.Cells.AutoFitColumns();
            return package.GetAsByteArray();
        }

    }
}
