﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;
using HRMNet.Models.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;
using HRMNet.Services;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Disciplins")]
    public class DisciplinsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Disciplin> _context;
        private readonly DbSet<Candidate> _candidate;
        private readonly IWebHostEnvironment _environment;
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly DbSet<Attachment> _attachment;
        private readonly IStringLocalizer _localizer;

        public DisciplinsController(IUnitOfWork uow, IWebHostEnvironment environment, IStringLocalizerFactory stringLocalizerFactory)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _context = uow.Set<Disciplin>();
            _candidate = uow.Set<Candidate>();
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
            _attachment = uow.Set<Attachment>();
            _localizer = stringLocalizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
        }

        // GET: Disciplins
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public IActionResult Index()
        {
            var disciplins = _context
                .Include(d => d.Candidate)
                .ThenInclude(x => x.StuffDetail)
                .AsNoTracking()
                .AsEnumerable()
                .GroupBy(c => c.CandidateId)
                .Select(x => new Disciplin
                {
                    CandidateId = x.Key,
                    EmployeeId = x.FirstOrDefault()?.Candidate?.StuffDetail?.BirthCertificateNumber,
                    CandidateName = x.FirstOrDefault()?.Candidate?.Name+ " " + x.FirstOrDefault()?.Candidate?.Family,
                    Total = x.Sum(x=> 1),
                    Candidate= x.FirstOrDefault()?.Candidate,
                });

            return View(disciplins.ToList());
        }

        // GET: Disciplins/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var disciplin = await _context
                .Include(d => d.Candidate)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (disciplin == null)
            {
                return NotFound();
            }

            return View(disciplin);
        }

        // GET: Disciplins/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            ViewData["CandidateId"] = _candidate
                .Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff)
                .AsNoTracking();

            
            return View();
        }

        // POST: Disciplins/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ActionType,Subject,DefenseDate,NotificationDate,Description,CandidateId")] Disciplin disciplin, List<IFormFile> files)
        {
            if (ModelState.IsValid)
            {
                if (files != null && files.Any())
                {
                    //model.Attachments = new List<Attachment>();
                    foreach (var item in files)
                    {
                        var fileName = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
                        var fileExtension = item.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
                        var path = Path.Combine(_environment.WebRootPath, "Files", fileName + "." + fileExtension.LastOrDefault());
                        using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write,
                                       FileShare.None,
                                       MaxBufferSize,
                                       // you have to explicitly open the FileStream as asynchronous
                                       // or else you're just doing synchronous operations on a background thread.
                                       useAsync: true))
                        {
                            await item.CopyToAsync(fileStream);
                        }

                        var toAdd = new Attachment { IsImage = ImageToolkit.IsImageFile(item), FileName = fileName + "." + fileExtension.LastOrDefault(), Caption = item.FileName, ContentType = item.ContentType };
                        disciplin.Attachments.Add(toAdd);
                    }

                }

                _context.Add(disciplin);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["CandidateId"] = _candidate
                .Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff)
                .AsNoTracking();
            
            return View(disciplin);
        }
        private string GetfileTypeByExtension(string extension)
        {
            switch (extension)
            {
                case ".pdf":
                    return "application/pdf";
                case ".png":
                    return "image/png";
                case ".jpg":
                    return "image/jpg";
                case ".jpeg":
                    return "image/jpeg";
                case ".tiff":
                    return "image/tiff";
                case ".docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".doc":
                    return "application/msword";
                case ".xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".xls":
                    return "office";

                case ".mp4":
                    return "video";
                case ".txt":
                    return "text";
                case ".html":
                    return "html";
                default:
                    return "image";
            }
        }

        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Get Files")]
        public IActionResult GetFiles(string id)
        {
            var ext = "." + id.Split(".", StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

            return File("/files/" + id, GetfileTypeByExtension(ext));
        }

        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Get Files")]
        public async Task<IActionResult> GetFilesById(int id)
        {
            var attachFile = await _attachment.FirstOrDefaultAsync(x => x.Id == id);

            var ext = "." + attachFile.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

            return File("/files/" + attachFile.FileName, GetfileTypeByExtension(ext));
        }

        private string GetfileType(string extension)
        {
            switch (extension)
            {
                case "application/pdf":
                    return "pdf";
                case "image/png":
                    return "image";
                case "image/jpg":
                    return "image";
                case "image/jpeg":
                    return "image";
                case "image/tiff":
                    return "gdocs";
                case ".ppt":
                    return "office";
                case "application/msword":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    return "office";
                case ".xls":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    return "office";
                case ".mp4":
                    return "video";
                case ".txt":
                    return "text";
                case ".html":
                    return "html";
                default:
                    return "image";
            }
        }

        // GET: Disciplins/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var disciplin = await _context
                .Include(x => x.Candidate)
                .Include(x=> x.Attachments)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
                
                
            if (disciplin == null)
            {
                return NotFound();
            }

            ViewData["CandidateId"] = _candidate
                .Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff)
                .AsNoTracking();


            string preview = string.Empty;
            string previewConfig = string.Empty;
            int key = 0;
            if (disciplin.Attachments.Any())
                foreach (var item in disciplin.Attachments)
                {
                    key += 1;
                    preview += "\"/Candidates/GetFiles/" + item.FileName + "\",";
                    previewConfig += "{type:\"" + GetfileType(item.ContentType) + "\", caption:\"" + item.Caption + "\",size:" + item.Size + ",width:\"120px\",url:\"/Candidates/DeleteAttachment/" +
                        item.Id + "\",key:" + item.Id + "},";
                }

            var config = new AttachmentViewModel()
            {
                Id = id.Value,
                SerializedInitialPreview = preview,
                SerializedInitialPreviewConfig = previewConfig,
                append = true
            };

            disciplin.AttachmentViewModel = config;

            return View(disciplin);
        }

        // POST: Disciplins/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ActionType,Subject,DefenseDate,NotificationDate,Description,CandidateId,Id")] Disciplin disciplin)
        {
            if (id != disciplin.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(disciplin);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DisciplinExists(disciplin.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["CandidateId"] = _candidate
                .Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff)
                .AsNoTracking();

            return View(disciplin);
        }

        // GET: Disciplins/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var disciplin = await _context
                .Include(d => d.Candidate)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (disciplin == null)
            {
                return NotFound();
            }

            return View(disciplin);
        }

        // POST: Disciplins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var disciplin = await _context.FindAsync(id);
            _context.Remove(disciplin);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> ExportDisciplins()
        {
            var disciplins =await _context
                .Include(d => d.Candidate)
                .ThenInclude(x => x.StuffDetail)
                .AsNoTracking()
                .ToListAsync();

            return File(ExportDisciplinsAsExcel(disciplins), XlsxContentType, "disciplins.xlsx");
        }

        private byte[] ExportDisciplinsAsExcel(List<Disciplin> disciplins)
        {
            byte[] reportBytes;
            using (var package = createDisciplinsExcelPackage(disciplins))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }

        private ExcelPackage createDisciplinsExcelPackage(List<Disciplin> disciplins)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Disciplins Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Disciplins Report";
            package.Workbook.Properties.Keywords = "Disciplins";

            var worksheet = package.Workbook.Worksheets.Add("Disciplins");
            //First add the headers

            worksheet.Cells[1, 1].Value = "Empolyee Id";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 2].Value = "Name Surename";
            worksheet.Cells[1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 3].Value = "Action Type";
            worksheet.Cells[1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 4].Value = "Subject";
            worksheet.Cells[1, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 5].Value = "Defense Date";
            worksheet.Cells[1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 6].Value = "Notification Date";
            worksheet.Cells[1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 7].Value = "Description";
            worksheet.Cells[1, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            
            //Add values

            int rowNumber = 1;
            foreach (var item in disciplins)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Candidate.StuffDetail?.BirthCertificateNumber;
                worksheet.Cells[rowNumber, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[rowNumber, 2].Value = item.Candidate.Name + " " + item.Candidate.Family;
                worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 3].Value = item.ActionType;
                worksheet.Cells[rowNumber, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[rowNumber, 4].Value = item.Subject;
                worksheet.Cells[rowNumber, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 5].Value = item.DefenseDate.ToString("yyyy/MM/dd");
                worksheet.Cells[rowNumber, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[rowNumber, 6].Value = item.NotificationDate.ToString("yyyy/MM/dd");
                worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[rowNumber, 7].Value = item.Description;
                worksheet.Cells[rowNumber, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: disciplins.Count() + 2, toColumn: 7), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;
            
            // AutoFitColumns
            worksheet.Cells[1, 1, disciplins.Count() + 2, 7].AutoFitColumns();
            
            return package;
        }

        [HttpPost]
        public async Task<IActionResult> GetCandidateDisciplins(int? id)
        {

            if (id == null)
                return Json("");

            var candidateLeaves = await _context
                .Where(x => x.CandidateId == id)
                .AsNoTracking()
                .ToListAsync();

            var sb = new StringBuilder();
            sb.Append("<table cellpadding=\"5\" cellspacing=\"0\" class=\"table table-bordered table-striped\" style=\"padding-left:50px;\" >");
            sb.Append("<thead><tr>");
            sb.Append("<td>"+_localizer["Result"]+"</td>");
            sb.Append("<td>"+ _localizer["Subject"] + "</td>");
            sb.Append("<td>"+ _localizer["Defense Date"] + "</td>");
            sb.Append("<td>"+ _localizer["Notification Date"] + "</td>");
            sb.Append("<td>"+ _localizer["Description"] + "</td>");
            sb.Append("<td></td>");
            sb.Append("</tr></thead>");
            sb.Append("<tbody>");

            foreach (var item in candidateLeaves)
            {
                sb.Append("<tr>");
                sb.Append("<td>" + _localizer[EnumHelper<DisciplinResult>.GetDisplayValue(item.ActionType)] + "</td>");
                sb.Append("<td>" + item.Subject + "</td>");
                sb.Append("<td>" + item.DefenseDate.ToString("yyyy/MM/dd") + "</td>");
                sb.Append("<td>" + item.NotificationDate.ToString("yyyy/MM/dd") + "</td>");
                sb.Append("<td>" + item.Description + "</td>");

                sb.Append("<td><a href=\"/Disciplins/Edit/" + item.Id + "\">"+ _localizer["Edit"] + "</a> |" +
                                            //"<a href=\"/Leaves/Detail/" + item.Id + "\"> Details </a> |" +
                                            "<a href=\"/Disciplins/Delete/" + item.Id + "\">"+ _localizer["Delete"] + "</a ></td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            return Json(sb.ToString());
        }


        private bool DisciplinExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
