﻿using HRMNet.Data;
using HRMNet.Hubs;
using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using HRMNet.Models.Customers;
//using DNTCommon.Web.Core;
using HRMNet.Models.Identity;
using HRMNet.Models.Positions;
using HRMNet.Models.ViewModel.Candidate;
using HRMNet.Models.ViewModel.DataTable;
using HRMNet.Models.ViewModel.Identity.Emails;
using HRMNet.Models.Workflow;
using HRMNet.Services;
using HRMNet.Services.Contracts.Identity;
using HRMNet.Services.CustomSettingsService;
using HRMNet.Services.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static HRMNet.Models.Workflow.RecruitProcessing;
using CandidateStatus = HRMNet.Models.Common.CandidateStatus;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Candidates")]
    public class CandidatesController : Controller
    {
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private readonly IUnitOfWork _uow;
        private readonly IWebHostEnvironment _environment;
        private readonly IEmailSender _emailSender;
        private readonly DbSet<Candidate> _context;
        private readonly DbSet<InventoryType> _inventoryType;
        private readonly DbSet<CandidateInventory> _candidateInventory;
        private readonly DbSet<Inventory> _inventory;
        private readonly DbSet<Title> _title;
        private readonly DbSet<WorkLocation> _work;
        private readonly DbSet<StuffDetail> _stuffDetail;
        private readonly DbSet<Position> _position;
        private readonly DbSet<Expertise> _expertise;
        private readonly DbSet<Customer> _customer;
        private readonly DbSet<Feature> _feature;
        private readonly DbSet<Attachment> _attachment;
        private readonly DbSet<User> _user;
        private readonly DbSet<CandidatePosition> _candidatePosition;
        private readonly DbSet<WorkflowStateDateTime> _workflowState;
        private readonly IHubContext<MessageHub> _messageHubContext;
        private readonly IUserValidator<User> _userValidator;
        private readonly IApplicationUserManager _userManager;
        private readonly ISet<string> _passwordsBanList;
        private readonly IOptionsSnapshot<SiteSettings> _siteOptions;
        private readonly IStringLocalizer _localizer;

        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private RecruitProcessing _machine;
        List<Message> messages;

        public CandidatesController(IUnitOfWork uow
            , IWebHostEnvironment environment
            , IHubContext<MessageHub> messageHubContext
            , IUserValidator<User> userValidator
            , IApplicationUserManager userManager
            , IOptionsSnapshot<SiteSettings> configurationRoot
            , IOptionsSnapshot<SiteSettings> siteOptions
            , IStringLocalizerFactory stringLocalizerFactory
            , IEmailSender emailSender)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
            _emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
            _context = uow.Set<Candidate>();
            _inventoryType = uow.Set<InventoryType>();
            _candidateInventory = uow.Set<CandidateInventory>();
            _work = uow.Set<WorkLocation>();
            _title = uow.Set<Title>();
            _stuffDetail = uow.Set<StuffDetail>();
            _inventory = uow.Set<Inventory>();
            _position = uow.Set<Position>();
            _expertise = uow.Set<Expertise>();
            _feature = uow.Set<Feature>();
            _attachment = uow.Set<Attachment>();
            _candidatePosition = uow.Set<CandidatePosition>();
            _customer = uow.Set<Customer>();
            _user = uow.Set<User>();
            _workflowState = uow.Set<WorkflowStateDateTime>();

            _messageHubContext = messageHubContext ?? throw new ArgumentNullException(nameof(messageHubContext));
            messages = new List<Message>();
            _userValidator = userValidator ?? throw new ArgumentNullException(nameof(userValidator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            if (configurationRoot == null) throw new ArgumentNullException(nameof(configurationRoot));
            _passwordsBanList = new HashSet<string>(configurationRoot.Value.PasswordsBanList, StringComparer.OrdinalIgnoreCase);
            _siteOptions = siteOptions ?? throw new ArgumentNullException(nameof(siteOptions));

            _localizer = stringLocalizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
        }

        private void AddToMessageList(MessageType type, string title, string body)
        {
            messages.Add(new Message()
            {
                Type = type,
                Body = body,
                Title = title
            });
        }

        private void ConfigStateMachine(int start)
        {
            // get current state from DB

            _machine = new RecruitProcessing(start);
            _machine.OnEntry = new RecruitProcessing.EntryExitDelegate(this.OnEntry);
            _machine.OnExit = new RecruitProcessing.EntryExitDelegate(this.OnExit);
            _machine.GuardClauseFromToUsingTriggerSave = new GuardClauseDelegate(this.GuardClauseFromToUsingTriggerSave);
            _machine.GuardClauseFromToUsingTriggerOthers = new GuardClauseDelegate(this.GuardClauseFromToUsingTriggerOthers);
            _machine.GuardClauseFromToUsingTriggerReject = new GuardClauseDelegate(this.GuardClauseFromToUsingTriggerReject);
            _machine.GuardClauseFromToUsingTriggerWithdrew = new GuardClauseDelegate(this.GuardClauseFromToUsingTriggerWithdrew);
            _machine.Configure();
        }

        private bool GuardClauseFromToUsingTriggerSave(State state)
        {
            switch (state)
            {
                case State.Active:
                    return true;
                case State.SubmittedToManager:
                    return true;
                case State.UnsuitableTechnically:
                    return true;
                case State.UnsuitableInTermsOfBudget:
                    return true;
                case State.SharedWithCustomer:
                    return true;
                case State.CvNotApprovedByCustomer:
                    return true;
                case State.WithdrewBeforeMeeting:
                    return true;
                case State.CustomerWillInterview:
                    return true;
                case State.RejectedInCustomerInterview:
                    return true;
                case State.DidnotComeToInterview:
                    return true;
                case State.CustomerRequestedCandidateWithdrew:
                    return true;
                case State.RejectedOffer:
                    return true;
                case State.OfferMade:
                    return true;
                case State.AcceptedOffer:
                    return true;
                case State.WaitingForPaperwork:
                    return true;
                case State.VitelcoStuff:
                    return true;
                case State.Quit:
                    return true;
                case State.CandidateInCvPool:
                    return true;
                default:
                    return false;
            }
        }

        private bool GuardClauseFromToUsingTriggerOthers(State state)
        {
            switch (state)
            {
                case State.Active:
                    return true;
                case State.SubmittedToManager:
                    return true;
                case State.UnsuitableTechnically:
                    return true;
                case State.UnsuitableInTermsOfBudget:
                    return true;
                case State.SharedWithCustomer:
                    return true;
                case State.CvNotApprovedByCustomer:
                    return true;
                case State.WithdrewBeforeMeeting:
                    return true;
                case State.CustomerWillInterview:
                    return true;
                case State.RejectedInCustomerInterview:
                    return true;
                case State.DidnotComeToInterview:
                    return true;
                case State.CustomerRequestedCandidateWithdrew:
                    return true;
                case State.RejectedOffer:
                    return true;
                case State.OfferMade:
                    return true;
                case State.AcceptedOffer:
                    return true;
                case State.WaitingForPaperwork:
                    return true;
                case State.VitelcoStuff:
                    return true;
                case State.Quit:
                    return true;
                case State.CandidateInCvPool:
                    return true;
                default:
                    return false;
            }
        }

        private bool GuardClauseFromToUsingTriggerWithdrew(State state)
        {
            switch (state)
            {
                case State.Active:
                    return true;
                case State.SubmittedToManager:
                    return true;
                case State.UnsuitableTechnically:
                    return true;
                case State.UnsuitableInTermsOfBudget:
                    return true;
                case State.SharedWithCustomer:
                    return true;
                case State.CvNotApprovedByCustomer:
                    return true;
                case State.WithdrewBeforeMeeting:
                    return true;
                case State.CustomerWillInterview:
                    return true;
                case State.RejectedInCustomerInterview:
                    return true;
                case State.DidnotComeToInterview:
                    return true;
                case State.CustomerRequestedCandidateWithdrew:
                    return true;
                case State.RejectedOffer:
                    return true;
                case State.OfferMade:
                    return true;
                case State.AcceptedOffer:
                    return true;
                case State.WaitingForPaperwork:
                    return true;
                case State.VitelcoStuff:
                    return true;
                case State.Quit:
                    return true;
                case State.CandidateInCvPool:
                    return true;
                default:
                    return false;
            }
        }

        private bool GuardClauseFromToUsingTriggerReject(State state)
        {
            switch (state)
            {
                case State.Active:
                    return true;
                case State.SubmittedToManager:
                    return true;
                case State.UnsuitableTechnically:
                    return true;
                case State.UnsuitableInTermsOfBudget:
                    return true;
                case State.SharedWithCustomer:
                    return true;
                case State.CvNotApprovedByCustomer:
                    return true;
                case State.WithdrewBeforeMeeting:
                    return true;
                case State.CustomerWillInterview:
                    return true;
                case State.RejectedInCustomerInterview:
                    return true;
                case State.DidnotComeToInterview:
                    return true;
                case State.CustomerRequestedCandidateWithdrew:
                    return true;
                case State.RejectedOffer:
                    return true;
                case State.OfferMade:
                    return true;
                case State.AcceptedOffer:
                    return true;
                case State.WaitingForPaperwork:
                    return true;
                case State.VitelcoStuff:
                    return true;
                case State.Quit:
                    return true;
                case State.CandidateInCvPool:
                    return true;
                default:
                    return false;
            }
        }

        private void OnEntry(State state)
        {
            switch (state)
            {
                case State.Active:
                    break;
                case State.SubmittedToManager:
                    break;
                case State.UnsuitableTechnically:
                    break;
                case State.UnsuitableInTermsOfBudget:
                    break;
                case State.SharedWithCustomer:
                    break;
                case State.CvNotApprovedByCustomer:
                    break;
                case State.WithdrewBeforeMeeting:
                    break;
                case State.CustomerWillInterview:
                    break;
                case State.RejectedInCustomerInterview:
                    break;
                case State.DidnotComeToInterview:
                    break;
                case State.CustomerRequestedCandidateWithdrew:
                    break;
                case State.RejectedOffer:
                    break;
                case State.OfferMade:
                    break;
                case State.AcceptedOffer:
                    break;
                case State.WaitingForPaperwork:
                    break;
                case State.VitelcoStuff:
                    break;
                case State.Quit:
                    break;
                case State.CandidateInCvPool:
                    break;
            }
        }

        private async Task<bool> InsertWorkflowStateDateTime(int candidateId, State state)
        {
            var workflowState = new WorkflowStateDateTime()
            {
                CandidateId = candidateId,
                Status = (CandidateStatus)state
            };

            _workflowState.Add(workflowState);

            var result = await _uow.SaveChangesAsync();
            if (result > 0)
                return true;
            else
                return false;
        }

        private void OnExit(State state)
        {
            switch (state)
            {
                case State.Active:
                    break;
                case State.SubmittedToManager:
                    break;
                case State.UnsuitableTechnically:
                    break;
                case State.UnsuitableInTermsOfBudget:
                    break;
                case State.SharedWithCustomer:
                    break;
                case State.CvNotApprovedByCustomer:
                    break;
                case State.WithdrewBeforeMeeting:
                    break;
                case State.CustomerWillInterview:
                    break;
                case State.RejectedInCustomerInterview:
                    break;
                case State.DidnotComeToInterview:
                    break;
                case State.CustomerRequestedCandidateWithdrew:
                    break;
                case State.RejectedOffer:
                    break;
                case State.OfferMade:
                    break;
                case State.AcceptedOffer:
                    break;
                case State.WaitingForPaperwork:
                    break;
                case State.VitelcoStuff:
                    break;
                case State.Quit:
                    break;
                case State.CandidateInCvPool:
                    break;
            }
        }

        internal async Task<List<Candidate>> PreparingCandidates(List<Candidate> candidates)
        {
            var users = await _user.AsNoTracking().ToListAsync();

            var positionList = string.Empty;
            foreach (var item in candidates)
            {
                positionList = string.Empty;
                item.Creator = users.FirstOrDefault(x => x.Id == item.CreatorId)?.DisplayName;
                item.Modifier = users.FirstOrDefault(x => x.Id == item.ModifierId)?.DisplayName;
                item.CandidatePositions
                    .Select(x => x.Position)
                    .ToList().ForEach(x => positionList += x.PositionName + "-"+ x.ProjectName+ " ("+ x?.Customer?.Name+ "), ");
                item.PositionListString = positionList;
            }

            return candidates;

        }

        internal async Task<Tuple<int, List<Candidate>>> GetCandidateList(CandidateSearchViewModel search, int page, int pageSize) //, DtParameters dtParameters
        {
            var query = _context
                .Include(x => x.CandidatePositions)
                    .ThenInclude(y => y.Position)
                        .ThenInclude(x=> x.Customer)
                .Include(x => x.CandidateFeatures)
                    .ThenInclude(y => y.Feature)
                .Include(x => x.Offers)
                .Include(x => x.Meetings)
                .Include(x => x.Attachments)
                .AsNoTracking()
                .Where(x => x.Status != CandidateStatus.VitelcoStuff
                            && x.Status != CandidateStatus.WaitingForPaperwork
                            && x.Status != CandidateStatus.AcceptedOffer
                            && x.Status != CandidateStatus.Quit);

            if (!string.IsNullOrWhiteSpace(search.DateRange))
            {
                var splittedRange = DateToolkit.SpliteDateRange(search.DateRange);

                var start = splittedRange?.Item1;
                var end = splittedRange?.Item2;
                query = query.Where(x => x.CreatedDateTime >= start && x.CreatedDateTime < end);
            }

            query = search.Sort switch
            {
                "Id" => search.Order == SortOrder.Descending
                    ? query.OrderByDescending(q => q.Id)
                    : query.OrderBy(q => q.Id),
                "Name" => search.Order == SortOrder.Descending
                ? query.OrderByDescending(q => q.Name + " " + q.Family)
                : query.OrderBy(q => q.Name + " " + q.Family),
                "Status" => search.Order == SortOrder.Descending
                    ? query.OrderByDescending(q => q.Status)
                    : query.OrderBy(q => q.Status),
                "StartPeriod" => search.Order == SortOrder.Descending
                    ? query.OrderByDescending(q => q.StartPeriod)
                    : query.OrderBy(q => q.StartPeriod),
                "TotalITexperience" => search.Order == SortOrder.Descending
                    ? query.OrderByDescending(q => q.TotalITexperience)
                    : query.OrderBy(q => q.TotalITexperience),
                "CurrentWages" => search.Order == SortOrder.Descending
                    ? query.OrderByDescending(q => q.CurrentWages)
                    : query.OrderBy(q => q.CurrentWages),
                "ExpectedWages" => search.Order == SortOrder.Descending
                    ? query.OrderByDescending(q => q.ExpectedWages)
                    : query.OrderBy(q => q.ExpectedWages),
                _ => query
            };

            //if (search.Order == SortOrder.Descending)
            //{
            //    search.Order = SortOrder.Ascending;
            //}
            //else
            //{
            //    search.Order = SortOrder.Descending;
            //}

            var offset = (pageSize * page) - pageSize;

            if (!string.IsNullOrWhiteSpace(search.KeySearch))
            {
                var candidatesList = await query.ToListAsync();
                switch (search.KeyType)
                {
                    case 0:
                        candidatesList = candidatesList.Where(x => x.Name.ToLower().Contains(search.KeySearch.ToLower()) || x.Family.ToLower().Contains(search.KeySearch.ToLower())).ToList();
                        break;
                    case 2:
                        candidatesList = candidatesList.Where(x => x.PositionListString.ToLower().Contains(search.KeySearch.ToLower())).ToList();
                        break;
                    case 3:
                        candidatesList = candidatesList.Where(x => x.StartPeriod == search.KeySearch).ToList();
                        break;
                    case 4:
                        candidatesList = candidatesList.Where(x => x.TotalITexperience == search.KeySearch).ToList();
                        break;
                    case 5:
                        candidatesList = candidatesList.Where(x => x.CurrentWages == search.KeySearch).ToList();
                        break;
                    case 6:
                        candidatesList = candidatesList.Where(x => x.ExpectedWages == search.KeySearch).ToList();
                        break;
                    case 8:
                        candidatesList = candidatesList.Where(x => x.Telephone == search.KeySearch).ToList();
                        break;
                    case 9:
                        candidatesList = candidatesList.Where(x => x.Email == search.KeySearch).ToList();
                        break;
                    case 11:
                        candidatesList = candidatesList.Where(x => !string.IsNullOrWhiteSpace(x.Creator) ? x.Creator.ToLower().Contains(search.KeySearch.ToLower()) : 1 == 2).ToList();
                        break;
                    case 12:
                        candidatesList = candidatesList.Where(x => !string.IsNullOrWhiteSpace(x.Modifier) ? x.Modifier.ToLower().Contains(search.KeySearch.ToLower()) : 1 == 2).ToList();
                        break;
                    case 100:
                        candidatesList = candidatesList.Where(x =>
                            x.Email == search.KeySearch ||
                            x.Telephone == search.KeySearch ||
                            x.ExpectedWages == search.KeySearch ||
                            x.CurrentWages == search.KeySearch ||
                            x.TotalITexperience == search.KeySearch ||
                            x.StartPeriod == search.KeySearch ||
                            x.PositionListString.ToLower().Contains(search.KeySearch.ToLower()) ||
                            x.Name.ToLower().Contains(search.KeySearch.ToLower()) ||
                            x.Family.ToLower().Contains(search.KeySearch.ToLower()) ||
                            (!string.IsNullOrWhiteSpace(x.Modifier) ? x.Modifier.ToLower().Contains(search.KeySearch.ToLower()) : 1 == 2) ||
                            (!string.IsNullOrWhiteSpace(x.Creator) ? x.Creator.ToLower().Contains(search.KeySearch.ToLower()) : 1 == 2)).ToList();
                        break;
                    default:
                        candidatesList = candidatesList.Where(x => x.Email == search.KeySearch ||
                            x.Telephone == search.KeySearch ||
                            x.ExpectedWages == search.KeySearch ||
                            x.CurrentWages == search.KeySearch ||
                            x.TotalITexperience == search.KeySearch ||
                            x.StartPeriod == search.KeySearch ||
                            x.PositionListString.ToLower().Contains(search.KeySearch.ToLower()) ||
                            x.Name.ToLower().Contains(search.KeySearch.ToLower()) ||
                            x.Family.ToLower().Contains(search.KeySearch.ToLower()) ||
                            (!string.IsNullOrWhiteSpace(x.Modifier) ? x.Modifier.ToLower().Contains(search.KeySearch.ToLower()) : 1 == 2) ||
                            (!string.IsNullOrWhiteSpace(x.Creator) ? x.Creator.ToLower().Contains(search.KeySearch.ToLower()) : 1 == 2)).ToList();
                        break;
                }

                var total = candidatesList.Count();

                var pageCandidates = candidatesList
                .Skip(offset)
                .Take(pageSize);

                var model = await PreparingCandidates(pageCandidates.ToList());

                return new Tuple<int, List<Candidate>>(total, model);
            }
            
            var totalQuery = query.Count();

            var pageCandidatesQuery = query
                .Skip(offset)
                .Take(pageSize);

            var pagedList =await pageCandidatesQuery.ToListAsync();

            var modelQuery = await PreparingCandidates(pagedList);

            return new Tuple<int, List<Candidate>>(totalQuery, modelQuery);

        }

        [HttpPost]
        public async Task<IActionResult> GetCandidates([FromBody] DtParameters dtParameters) //, CandidateSearchViewModel search, int page, int pageSize
        {
            //var candidates = await GetCandidateList(dtParameters);

            return Json(new DtResult<CandidateViewModel>
            {
                Draw = dtParameters.Draw,
                //RecordsTotal = candidates.Item1,
                RecordsFiltered = dtParameters.Length,
                //Data = candidates.Item2
            });

            //return Json(new
            //{
            //    // this is what datatables wants sending back
            //    //draw = model.draw,
            //    recordsTotal = candidates.Item1,
            //    recordsFiltered = pageSize,
            //    data = candidates.Item2
            //});

        }


        // GET: Candidates
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index(CandidateSearchViewModel search, int page, int pageSize)
        {
            if (page == 0)
                page = 1;

            if (pageSize == 0)
                pageSize = 10;

            var candidateList = await GetCandidateList(search, page, pageSize);

            if (ViewData["Messages"] != null)
            {
                var list = (List<Message>)TempData["Messages"];
                foreach (var item in list)
                {
                    await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", item.Body);
                }
            }

            var returnModel = new CandidatesViewModel()
            {
                Candidates = candidateList.Item2,
                Search = search,
                Paging =
                {
                    TotalItems =candidateList.Item1,
                    CurrentPage=page,
                    ItemsPerPage= pageSize,
                },
            };

            return View(returnModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult ValidateEmail(string email, int id)
        {
            var lst = _context
                //.Select(x => new
                //{
                //    Email = x.Email,
                //    Id = x.Id
                //})
                .AsNoTracking();
            //.ToListAsync();

            if (id == 0)
            {
                var candidates = lst
                    .Where(x => x.Email == email);

                if (candidates != null && candidates.Any())
                    return Json(_localizer["Email has been used"]);
                else
                    return Json("true");
            }
            else
            {
                var candidates = lst
                    .Where(x => x.Email == email && x.Id != id);

                if (candidates != null && candidates.Any())
                    return Json(_localizer["Email has been used"]);
                else
                    return Json("true");
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult ValidatePhone(string telephone, int id)
        {
            var lst = _context
                //.Select(x=> new { 
                //    Telephone= x.Telephone,
                //    Id= x.Id
                //})
                .AsNoTracking();
            //.ToListAsync();

            if (id == 0)
            {
                var candidates = lst
                    .Where(x => x.Telephone == telephone);

                if (candidates != null && candidates.Any())
                    return Json(_localizer["Phone has been used"]);
                else
                    return Json("true");
            }
            else
            {
                var candidates = lst
                    .Where(x => x.Telephone == telephone && x.Id != id);

                if (candidates != null && candidates.Any())
                    return Json(_localizer["Phone has been used"]);
                else
                    return Json("true");
            }
        }

        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        //[DisplayName("Get Files")]
        public async Task<IActionResult> GetFilesById(int id)
        {
            var attachFile = await _attachment.FirstOrDefaultAsync(x => x.Id == id);

            var ext = "." + attachFile.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

            return File("/files/" + attachFile.FileName, GetfileTypeByExtension(ext));
        }

        ////[Authorize]
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        //[DisplayName("Get Files")]
        public IActionResult GetFiles(string id)
        {
            var ext = "." + id.Split(".", StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

            return File("/files/" + id, GetfileTypeByExtension(ext));
        }

        private bool InventoryExists(int id)
        {
            return _inventory.AsParallel().Any(e => e.Id == id);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Start Recruitment")]
        public async Task<IActionResult> InvitedCandidate(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _context
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id.Value);

            if (candidate == null)
            {
                return NotFound();
            }

            ViewData["Customers"] = await _customer.ToListAsync();


            return View(candidate);

        }

        [DisplayName("Accepted Candidates")]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> AcceptedCandidates()
        {
            var model = await _context
                .Include(x => x.CandidatePositions)
                    .ThenInclude(y => y.Position)
                        .ThenInclude(x=> x.Customer)
                .Include(x => x.CandidateFeatures)
                    .ThenInclude(y => y.Feature)
                .Include(x => x.Offers)
                .Include(x => x.Meetings)
                .Where(x => x.Status == CandidateStatus.AcceptedOffer || 
                x.Status == CandidateStatus.WaitingForPaperwork)
                .AsNoTracking()
                .ToListAsync();
            //return View();
            return View(await PreparingCandidates(model));
        }


        [HttpPost]
        public async Task<IActionResult> SendingAttachment(Candidate model)
        {

            var candidate = await _context.AsNoTracking().FirstOrDefaultAsync(x => x.Id == model.Id);

            var attachs = new List<string>() { };
            attachs.Add("\\Files\\AttachFiles\\logo.png");

            var emailModel = new EmailViewModel()
            {
                Name = candidate.Name + " " + candidate.Family,
                Useremail = User.Identity.GetUserEmail(),
                Usermobile = User.Identity.GetUserPhoneNumber(),
                Username = User.Identity.GetUserDisplayName()

            };

            GeneralSettings setting = new GeneralSettings();
            setting.Load(_uow);
            emailModel.Content = setting.SendingEmailGeneralBody;
            if (!string.IsNullOrWhiteSpace(setting.AttachFile))
                attachs.Add("\\Files\\GeneralSettings\\" + setting.AttachFile);

            // getting customer attachments
            if (!string.IsNullOrWhiteSpace(model.CustomerId))
            {
                var customer = await _customer
                        .Include(x => x.Docs)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == int.Parse(model.CustomerId));


                if (customer.IsContentUsesForEmail)
                {
                    emailModel.Content = customer.Content;
                }

                //if (string.IsNullOrWhiteSpace(setting.SendingEmailGeneralBody))
                //    emailModel.Content = customer.Content;

                if (customer.Docs != null && customer.Docs.Any())
                    foreach (var item in customer.Docs)
                    {
                        attachs.Add("\\Files\\" + item.FileName);
                    }
            }

            //await SendEmail<EmailViewModel>(candidate.Email, "Sending Documents", "~/Views/EmailTemplates/_SendingDocument.cshtml", attachs, emailModel);

            try
            {
                await _emailSender.SendEmailAsync(
                    email: candidate.Email,
                    subject: "Sending Documents",
                    viewNameOrPath: "~/Views/EmailTemplates/_SendingDocument.cshtml",
                    attachmentFiles: attachs,
                    model: emailModel);

                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"success|" + _localizer["The email has been sent successfully"] + "|");

                await Task.Delay(5000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"error|" + _localizer["The email was failed. Please send it manually"] + "|");
                await Task.Delay(15000);
            }


            var result = await UpdateStatus(model.Id, CandidateStatus.WaitingForPaperwork);
            if (result)
                return RedirectToAction("AcceptedCandidates");

            return RedirectToAction("InvitedCandidate", new { id = model.Id });
        }

        internal async Task SendEmail<T>(string email, string subject, string viewPath, IEnumerable<string> attachs, T model)
        {
            try
            {
                await _emailSender.SendEmailAsync(
                    email: email,
                    subject: subject,
                    viewNameOrPath: viewPath,
                    attachmentFiles: attachs,
                    model: model);

                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"success|" + _localizer["The email has been sent successfully"] + "|");

                await Task.Delay(5000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"error|" + _localizer["The email was failed"] + "|");
                await Task.Delay(15000);
            }

        }

        public async Task<bool> UpdateStatus(int id, CandidateStatus status)
        {
            var candidate = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidate == null)
            {
                return false;
            }

            if (candidate.Status != status)
            {
                candidate.Status = status;
                candidate.WorkflowStateDateTimes.Add(new WorkflowStateDateTime()
                {
                    //CandidateId = id,
                    Status = (CandidateStatus)status
                });

                var result = await _uow.SaveChangesAsync();

                if (result > 0)
                    return true;
            }
            else
                return true;

            return false;
        }

        // GET: Candidates/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidate == null)
            {
                return NotFound();
            }

            return View(candidate);
        }

        private async Task PopulatePositionDropDownList(Candidate candidate) // object selectedService = null
        {
            var positions = await _position.AsNoTracking().ToListAsync();
            //ViewBag.ServiceId = new SelectList(services, "Id", "Name", selectedService);


            var model = new HashSet<int>(candidate.CandidatePositions.Select(c => c.PositionId));
            var viewModel = new List<AssingedPositionsData>();
            foreach (var item in positions)
            {
                viewModel.Add(new AssingedPositionsData
                {
                    PositionId = item.Id,
                    Title = item.PositionName,
                    Assigned = model.Contains(item.Id)
                });
            }
            ViewData["Positions"] = viewModel;
        }

        // GET: Candidates/Create

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public async Task<IActionResult> Create()
        {


            var positions = await _position
                    .Include(x => x.Customer)
                .Select(x => x.PositionName + "-" + x.ProjectName + " (" + x.Customer.Name + ")")
                .ToArrayAsync();

            var features = await _feature.Select(x => x.FeatureName).ToArrayAsync();
            var types = await _expertise.Select(x => x.ExpertiseName).ToArrayAsync();

            var model = new Candidate()
            {
                PositionsSource = positions.ToJson(),
                FeaturesSource = features.ToJson(),
                TypesSource = types.ToJson()
            };
            //await PopulatePositionDropDownList(new Candidate());
            ConfigStateMachine(1);


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Candidate model, List<IFormFile> files) //IEnumerable<Position> positions, IEnumerable<Feature> features, IEnumerable<Expertise> expertises
        {
            if (ModelState.IsValid)
            {
                if (model.SelectedPositions != null)
                {
                    model.CandidatePositions = new List<CandidatePosition>();

                    List<CandidateSelectedPositionViewModel> modelSelectedPositions = new List<CandidateSelectedPositionViewModel>();
                    foreach (var item in model.SelectedPositions)
                    {
                        //.Net Developer-Vodafone (Amdocs)
                        var splittedItems = item.Split("-");
                        var position = splittedItems.FirstOrDefault().Trim();
                        var customer = item.Substring(item.IndexOf("(") + 1, item.Length - item.IndexOf("(") - 2).Trim();
                        var project = splittedItems.LastOrDefault().Substring(0, splittedItems.LastOrDefault().IndexOf("(")).Trim();

                        modelSelectedPositions.Add(new CandidateSelectedPositionViewModel()
                        {
                            Customer = customer,
                            Position = position,
                            Project = project
                        });
                    }

                    var selectedPositions = _position
                            .Include(x => x.Customer)
                            .AsEnumerable()
                        .Where(x => modelSelectedPositions.Any(y =>
                            y.Position == x.PositionName &&
                            y.Project == x.ProjectName &&
                            y.Customer == x.Customer.Name));

                    //var selectedPositions = _position.Where(x => model.SelectedPositions.Contains(x.PositionName + " ")).AsNoTracking();
                    foreach (var item in selectedPositions)
                    {
                        var toAdd = new CandidatePosition { CandidateId = model.Id, PositionId = item.Id };
                        model.CandidatePositions.Add(toAdd);
                    }
                }

                if (model.SelectedTypes != null)
                {
                    model.CandidateExpertises = new List<CandidateExpertise>();

                    var selectedTypes = _expertise.Where(x => model.SelectedTypes.Contains(x.ExpertiseName)).AsNoTracking();
                    foreach (var item in selectedTypes)
                    {
                        var toAdd = new CandidateExpertise { CandidateId = model.Id, ExpertiseId = item.Id };
                        model.CandidateExpertises.Add(toAdd);
                    }
                }

                if (model.SelectedFeatures != null)
                {
                    model.CandidateFeatures = new List<CandidateFeature>();

                    var selectedFeatures = _feature.Where(x => model.SelectedFeatures.Contains(x.FeatureName)).AsNoTracking();
                    foreach (var item in selectedFeatures)
                    {
                        var toAdd = new CandidateFeature { CandidateId = model.Id, FeatureId = item.Id };
                        model.CandidateFeatures.Add(toAdd);
                    }
                }

                if (files != null && files.Any())
                {
                    //model.Attachments = new List<Attachment>();
                    foreach (var item in files)
                    {
                        var fileName = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
                        var fileExtension = item.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
                        var path = Path.Combine(_environment.WebRootPath, "Files", fileName + "." + fileExtension.LastOrDefault());
                        using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write,
                                       FileShare.None,
                                       MaxBufferSize,
                                       // you have to explicitly open the FileStream as asynchronous
                                       // or else you're just doing synchronous operations on a background thread.
                                       useAsync: true))
                        {
                            await item.CopyToAsync(fileStream);
                        }

                        var toAdd = new Attachment { IsImage = ImageToolkit.IsImageFile(item), FileName = fileName + "." + fileExtension.LastOrDefault(), Caption = item.FileName, ContentType = item.ContentType };
                        model.Attachments.Add(toAdd);
                    }

                    //candidate.CVFilename = fileName + "." + fileExtension.LastOrDefault();
                }

                // model.Status = CandidateStatus.Active;

                model.WorkflowStateDateTimes.Add(new WorkflowStateDateTime()
                {
                    Status = CandidateStatus.Active
                });

                var candidate = _context.Add(model);

                var result = await _uow.SaveChangesAsync();

                if (result > 0)
                {
                    await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "success|" + _localizer["Inserted successfully"]);
                    return RedirectToAction("Index");

                }

            }

            model.InitialPosition = model.SelectedPositions.ToJson();
            model.InitialFeature = model.SelectedFeatures.ToJson();
            model.InitialType = model.SelectedTypes.ToJson();

            //await PopulatePositionDropDownList(new Candidate());

            return View(model);
        }

        private string GetfileType(string extension)
        {
            switch (extension)
            {
                case "application/pdf":
                    return "pdf";
                case "image/png":
                    return "image";
                case "image/jpg":
                    return "image";
                case "image/jpeg":
                    return "image";
                case "image/tiff":
                    return "gdocs";
                case ".ppt":
                    return "office";
                case "application/msword":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    return "office";
                case ".xls":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    return "office";
                case ".mp4":
                    return "video";
                case ".txt":
                    return "text";
                case ".html":
                    return "html";
                default:
                    return "image";
            }
        }


        private string GetfileTypeByExtension(string extension)
        {
            switch (extension)
            {
                case ".pdf":
                    return "application/pdf";
                case ".png":
                    return "image/png";
                case ".jpg":
                    return "image/jpg";
                case ".jpeg":
                    return "image/jpeg";
                case ".tiff":
                    return "image/tiff";
                case ".docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".doc":
                    return "application/msword";
                case ".xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".xls":
                    return "office";

                case ".mp4":
                    return "video";
                case ".txt":
                    return "text";
                case ".html":
                    return "html";
                default:
                    return "image";
            }
        }


        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _context
                .Include(x => x.CandidatePositions)
                    .ThenInclude(x => x.Position)
                        .ThenInclude(x => x.Customer)
                .Include(x => x.CandidateFeatures)
                    .ThenInclude(y => y.Feature)
                .Include(x => x.CandidateExpertises)
                    .ThenInclude(y => y.Expertise)
                .Include(x => x.Meetings)
                    .ThenInclude(y => y.MPosition)
                        .ThenInclude(z => z.Customer)
                .Include(x => x.Offers)
                    .ThenInclude(y => y.Position)
                        .ThenInclude(z => z.Customer)
                .Include(x => x.Attachments)
                    //.Include(x => x.PositionsHistory)
                    .ThenInclude(y => y.Customer)
                .Include(x => x.CandidatePositionsHistory)
                    .ThenInclude(x => x.Position)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id.Value);

            if (candidate == null ||
                candidate.Status == CandidateStatus.AcceptedOffer ||
                candidate.Status == CandidateStatus.WaitingForPaperwork ||
                candidate.Status == CandidateStatus.VitelcoStuff ||
                candidate.Status == CandidateStatus.Quit)
            {
                return Json("Candidate not found or the status is Accepted offer");
            }

            string preview = string.Empty;
            string previewConfig = string.Empty;
            int key = 0;
            if (candidate.Attachments.Any())
                foreach (var item in candidate.Attachments)
                {
                    key += 1;
                    preview += "\"/Candidates/GetFiles/" + item.FileName + "\",";
                    previewConfig += "{type:\"" + GetfileType(item.ContentType) + "\", caption:\"" + item.Caption + "\",size:" + item.Size + ",width:\"120px\",url:\"/Candidates/DeleteAttachment/" +
                        item.Id + "\",key:" + item.Id + "},";
                }

            var config = new AttachmentViewModel()
            {
                Id = id.Value,
                SerializedInitialPreview = preview,
                SerializedInitialPreviewConfig = previewConfig,
                append = true
            };

            candidate.AttachmentViewModel = config;

            var positions = await _position
                .Include(x => x.Customer)
                .Select(x => x.PositionName + "-" + x.ProjectName + " (" + x.Customer.Name + ")")
                .ToArrayAsync();

            var selectedPositions = candidate.CandidatePositions
                .Select(x => x.Position.PositionName + "-" + x.Position.ProjectName + " (" + x.Position.Customer.Name + ")")
                .ToArray();

            candidate.InitialPosition = selectedPositions.ToJson();
            candidate.PositionsSource = positions.ToJson();

            var features = await _feature.Select(x => x.FeatureName).ToArrayAsync();
            var selectedFeatures = candidate.CandidateFeatures.Select(x => x.Feature).Select(x => x.FeatureName).ToArray();
            candidate.InitialFeature = selectedFeatures.ToJson();
            candidate.FeaturesSource = features.ToJson();

            var types = await _expertise.Select(x => x.ExpertiseName).ToArrayAsync();
            var selectedTypes = candidate.CandidateExpertises.Select(x => x.Expertise).Select(x => x.ExpertiseName).ToArray();
            candidate.InitialType = selectedTypes.ToJson();
            candidate.TypesSource = types.ToJson();

            var status = (int)candidate.Status == 0 ? 1 : (int)candidate.Status;
            candidate.CurrentStatus = status;
            ConfigStateMachine(status);
            var st = _machine.GetNextTransitions()
                .Select(x => new SelectListItem()
                {
                    Text = _localizer[x.GetAttribute<DisplayAttribute>().Name],
                    Value = ((int)x).ToString()

                }).ToList();

            if (st == null || !st.Any())
            {
                Enum.TryParse(status.ToString(), out CandidateStatus currentStatus);
                st.Add(new SelectListItem()
                {
                    Value = status.ToString(),
                    Text = _localizer[currentStatus.GetAttribute<DisplayAttribute>().Name]
                });
            }

            //Enum.TryParse("90", out CandidateStatus currentStatus);
            //currentStatus.GetAttribute<DisplayAttribute>();

            ViewData["status"] = st;

            ViewData["Positions"] = new SelectList(await _position.AsNoTracking().ToListAsync(), "Id", "PositionName");

            //await PopulatePositionDropDownList(candidate);
            return View(candidate);
        }

        private async Task UpdateSelectedPosition(string[] selected, Candidate candidate)
        {
            if (selected == null)
            {
                candidate.CandidatePositions = new List<CandidatePosition>();
                return;
            }

            var selectedHS = new HashSet<string>(selected);
            var candidatePositions = new HashSet<int>
                (candidate.CandidatePositions.Select(c => c.PositionId));

            var positions = await _position.AsNoTracking().ToListAsync();
            foreach (var item in positions)
            {
                if (selectedHS.Contains(item.Id.ToString()))
                {
                    if (!candidatePositions.Contains(item.Id))
                    {
                        _candidatePosition.Add(new CandidatePosition { CandidateId = candidate.Id, PositionId = item.Id });
                    }
                }
                else
                {
                    if (candidatePositions.Contains(item.Id))
                    {
                        CandidatePosition toRemove = candidate.CandidatePositions.FirstOrDefault(i => i.PositionId == item.Id);

                        _candidatePosition.Remove(toRemove);
                    }
                }
            }
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete Attachments")]
        public async Task<IActionResult> DeleteAttachment(string id)
        {
            var attachment = await _attachment.FindAsync(int.Parse(id));

            _attachment.Remove(attachment);

            var result = await _uow.SaveChangesAsync();

            if (result > 0)
            {
                var path = Path.Combine(_environment.WebRootPath, "Files", attachment.FileName);
                System.IO.File.Delete(path);
                return Json("ok");
            }
            else
                return Json("not ok");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Candidate model, IEnumerable<CurrentPosition> positions, IEnumerable<Meeting> meetings, IEnumerable<Offer> offers, string[] selected, List<IFormFile> files)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            var candidate = await _context

                    .Include(x => x.CandidatePositions)
                        .ThenInclude(x => x.Position)
                            .ThenInclude(x => x.Customer)
                    .Include(x => x.CandidateFeatures)
                        .ThenInclude(y => y.Feature)
                    .Include(x => x.CandidateExpertises)
                        .ThenInclude(y => y.Expertise)
                    .Include(x => x.Meetings)
                        .ThenInclude(y => y.MPosition)
                            .ThenInclude(z => z.Customer)
                    .Include(x => x.Offers)
                        .ThenInclude(y => y.Position)
                            .ThenInclude(z => z.Customer)
                    .Include(x => x.Attachments)
                        .ThenInclude(y => y.Customer)
                    .Include(x => x.CandidatePositionsHistory)
                        .ThenInclude(x => x.Position)
                    .FirstOrDefaultAsync(x => x.Id == model.Id);

            if (ModelState.IsValid)
            {
                candidate.CandidateFeatures.Clear();
                candidate.CandidateExpertises.Clear();
                var currentCandidatePositionsId = candidate.CandidatePositions.Select(x => x.PositionId).ToList();
                //currentCandidatePositionsId =candidate.CandidatePositions.Select(x => x.PositionId).ToList() ;
                candidate.CandidatePositions.Clear();

                if (model.SelectedPositions != null)
                {
                    //candidate.CandidatePositions.Clear();
                    //if (candidate.CandidatePositions==null)
                    candidate.CandidatePositions = new List<CandidatePosition>();

                    List<CandidateSelectedPositionViewModel> modelSelectedPositions = new List<CandidateSelectedPositionViewModel>();
                    foreach (var item in model.SelectedPositions)
                    {
                        //.Net Developer-Vodafone (Amdocs)
                        var splittedItems = item.Split("-");
                        var position = splittedItems.FirstOrDefault().Trim();
                        var customer = item.Substring(item.IndexOf("(") + 1, item.Length - item.IndexOf("(") - 2).Trim();
                        var project = splittedItems.LastOrDefault().Substring(0, splittedItems.LastOrDefault().IndexOf("(")).Trim();

                        modelSelectedPositions.Add(new CandidateSelectedPositionViewModel()
                        {
                            Customer = customer,
                            Position = position,
                            Project = project
                        });
                    }

                    var selectedPositions = _position
                            .Include(x=> x.Customer)
                            .AsEnumerable()
                        .Where(x => modelSelectedPositions.Any(y=> 
                            y.Position== x.PositionName && 
                            y.Project== x.ProjectName &&
                            y.Customer== x.Customer.Name));

                    foreach (var item in selectedPositions)
                    {
                        var toAdd = new CandidatePosition { CandidateId = model.Id, PositionId = item.Id };
                        candidate.CandidatePositions.Add(toAdd);

                        if (!currentCandidatePositionsId.Contains(item.Id) || !candidate.CandidatePositionsHistory.Any())
                        {
                            var toAddHistory = new CandidatePositionHistory { CandidateId = model.Id, PositionId = item.Id };
                            candidate.CandidatePositionsHistory.Add(toAddHistory);
                        }
                    }
                }

                if (model.SelectedTypes != null)
                {
                    candidate.CandidateExpertises = new List<CandidateExpertise>();

                    var selectedTypes = _expertise.Where(x => model.SelectedTypes.Contains(x.ExpertiseName)).AsNoTracking();
                    foreach (var item in selectedTypes)
                    {
                        var toAdd = new CandidateExpertise { CandidateId = model.Id, ExpertiseId = item.Id };
                        candidate.CandidateExpertises.Add(toAdd);
                    }
                }

                if (model.SelectedFeatures != null)
                {
                    candidate.CandidateFeatures = new List<CandidateFeature>();

                    var selectedFeatures = _feature.Where(x => model.SelectedFeatures.Contains(x.FeatureName)).AsNoTracking();
                    foreach (var item in selectedFeatures)
                    {
                        var toAdd = new CandidateFeature { CandidateId = model.Id, FeatureId = item.Id };
                        candidate.CandidateFeatures.Add(toAdd);
                    }
                }

                if (files != null && files.Any())
                {
                    //candidate.Attachments = new List<Attachment>();
                    foreach (var item in files)
                    {
                        var fileName = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
                        var fileExtension = item.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
                        var path = Path.Combine(_environment.WebRootPath, "Files", fileName + "." + fileExtension.LastOrDefault());
                        using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write,
                                       FileShare.None,
                                       MaxBufferSize,
                                       // you have to explicitly open the FileStream as asynchronous
                                       // or else you're just doing synchronous operations on a background thread.
                                       useAsync: true))
                        {
                            await item.CopyToAsync(fileStream);
                        }

                        var toAdd = new Attachment { IsImage = ImageToolkit.IsImageFile(item), FileName = fileName + "." + fileExtension.LastOrDefault(), Caption = item.FileName, ContentType = item.ContentType };
                        candidate.Attachments.Add(toAdd);
                    }

                    //candidate.CVFilename = fileName + "." + fileExtension.LastOrDefault();
                }

                candidate.CurrentWages = model.CurrentWages;
                candidate.Email = model.Email;
                candidate.ExpectedWages = model.ExpectedWages;
                candidate.Family = model.Family;
                candidate.Name = model.Name;
                candidate.StartPeriod = model.StartPeriod;
                candidate.Status = model.Status;
                candidate.Expertises = model.Expertises;
                candidate.Telephone = model.Telephone;
                candidate.TotalITexperience = model.TotalITexperience;
                candidate.IsHavingCV = model.IsHavingCV;
                candidate.Features = model.Features;
                candidate.PositionRelatedExperience = model.PositionRelatedExperience;
                //candidate.CVFilename = model.CVFilename;
                candidate.IsOnBlackList = model.IsOnBlackList;
                candidate.BlacklistReason = model.BlacklistReason;
                candidate.BriefPersonalInfomation = model.BriefPersonalInfomation;

                if (meetings != null)
                {
                    candidate.Meetings = new List<Meeting>();
                    foreach (var item in meetings)
                    {
                        var toAdd = new Meeting
                        {
                            MeetingDate = item.MeetingDate,
                            MeetingResult = item.MeetingResult,
                            MeetingSpeakingPerson = item.MeetingSpeakingPerson,
                            MPositionId = item.MPositionId
                        };
                        candidate.Meetings.Add(toAdd);
                    }
                }

                if (offers != null)
                {
                    candidate.Offers = new List<Offer>();
                    foreach (var item in offers)
                    {
                        var toAdd = new Offer
                        {
                            PositionId = item.PositionId,
                            OfferContent = item.OfferContent,
                            OfferDate = item.OfferDate,
                            OfferResult = item.OfferResult,
                        };
                        candidate.Offers.Add(toAdd);
                    }
                }

                var newStatus = (int)model.Status;
                if (model.CurrentStatus != newStatus)
                {
                    ConfigStateMachine((int)model.CurrentStatus);

                    if (_machine.TryFireTrigger((Trigger)newStatus))
                    {
                        candidate.WorkflowStateDateTimes.Add(new WorkflowStateDateTime()
                        {
                            Status = (CandidateStatus)newStatus
                        });

                        var result = await _uow.SaveChangesAsync();

                        if (result > 0)
                        {
                            var poitionName = positions.FirstOrDefault()?.ProjectName;
                            var content = poitionName + " Projesi için gerçekleştirmiş olduğunuz görüşmenize istinaden Teknik yetersizlikten dolayı şu an için size olumlu bir geri dönüş sağlayamıyoruz. " +
                                        "İleride doğabilecek uygun roller için sizinle tekrar iletişimde olacağız.";

                            await SendEmailToUnsuccessCandidate((CandidateStatus)newStatus, candidate.Email, candidate.Name + " " + candidate.Family, content);

                            AddToMessageList(MessageType.Success, "Updating Candidate", "Updated successfully");
                            ViewData["Messages"] = messages;
                            //await Task.Delay(300);
                            //await _messageHubContext.Clients.All.SendAsync("InstantMessages", "Updated successfully");
                            return RedirectToAction(nameof(Index));
                        }
                    }
                }
                else
                {
                    var result = await _uow.SaveChangesAsync();

                    if (result > 0)
                    {

                        AddToMessageList(MessageType.Success, "Updating Candidate", "Updated successfully");
                        ViewData["Messages"] = messages;
                        //await Task.Delay(300);
                        //await _messageHubContext.Clients.All.SendAsync("InstantMessages", "Updated successfully");
                        return RedirectToAction(nameof(Index));
                    }
                }


            }

            ConfigStateMachine((int)model.CurrentStatus);
            var st = _machine.GetNextTransitions()
                .Select(x => new SelectListItem()
                {
                    Text = x.ToString(),
                    Value = ((int)x).ToString()
                }).ToList();

            if (st == null || !st.Any())
            {
                Enum.TryParse(model.CurrentStatus.ToString(), out CandidateStatus currentStatus);
                st.Add(new SelectListItem()
                {
                    Value = model.CurrentStatus.ToString(),
                    Text = currentStatus.GetAttribute<DisplayAttribute>().Name
                });
            }

            ViewData["status"] = st;


            string preview = string.Empty;
            string previewConfig = string.Empty;
            int key = 0;
            if (candidate.Attachments.Any())
                foreach (var item in candidate.Attachments)
                {
                    key += 1;
                    preview += "\"/Candidates/GetFiles/" + item.FileName + "\",";
                    previewConfig += "{type:\"" + GetfileType(item.ContentType) + "\", caption:\"" + item.Caption + "\",size:" + item.Size + ",width:\"120px\",url:\"/Candidates/DeleteAttachment/" +
                        item.Id + "\",key:" + item.Id + "},";
                }

            var config = new AttachmentViewModel()
            {
                Id = id,
                SerializedInitialPreview = preview,
                SerializedInitialPreviewConfig = previewConfig,
                append = true
            };

            model.AttachmentViewModel = config;

            model.Offers = candidate.Offers;
            model.Meetings = candidate.Meetings;

            var allfeatures = await _feature.Select(x => x.FeatureName).AsNoTracking().ToArrayAsync();
            var allpositions = await _position.Select(x => x.PositionName + " ").AsNoTracking().ToArrayAsync();
            var allTypes = await _expertise.Select(x => x.ExpertiseName).ToArrayAsync();
            //await PopulatePositionDropDownList(model);
            model.InitialPosition = model.SelectedPositions.ToJson();
            model.PositionsSource = allpositions.ToJson();

            model.InitialFeature = model.SelectedFeatures.ToJson();
            model.FeaturesSource = allfeatures.ToJson();

            model.InitialType = model.SelectedTypes.ToJson();
            model.TypesSource = allTypes.ToJson();

            model.CandidatePositionsHistory = candidate.CandidatePositionsHistory;

            return View(model);
        }

        internal async Task<bool?> SendEmailToUnsuccessCandidate(CandidateStatus status, string email, string name, string content)
        {
            if (
                //status == CandidateStatus.CustomerRequestedCandidateWithdrew ||
                status == CandidateStatus.CvNotApprovedByCustomer ||
                //status == CandidateStatus.DidnotComeToInterview ||
                status == CandidateStatus.RejectedInCustomerInterview ||
                //status == CandidateStatus.RejectedOffer ||
                status == CandidateStatus.UnsuitableTechnically ||
                status == CandidateStatus.UnsuitableInTermsOfBudget
                //status == CandidateStatus.WithdrewBeforeMeeting
                )
            {
                var model = new EmailViewModel()
                {
                    Name = name,
                    Content = content,
                    Useremail = User.Identity.GetUserEmail(),
                    Usermobile = User.Identity.GetUserPhoneNumber(),
                    Username = User.Identity.GetUserDisplayName()
                };

                var attachs = new List<string>() { };
                attachs.Add("\\Files\\AttachFiles\\logo.png");

                await SendEmail<EmailViewModel>(email, "Vitelco-Görüşme Sonucu Bilgilendirmesi ", "~/Views/EmailTemplates/_ApplicancyResult.cshtml", attachs, model);
            }

            return true;
        }
        // GET: Candidates/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _context
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);

            if (candidate == null)
            {
                return NotFound();
            }

            return View(candidate);
        }

        // POST: Candidates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {


            var candidate = await _context.FindAsync(id);
            _context.Remove(candidate);
            await _uow.SaveChangesAsync();
            await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "success|" + _localizer["Deleted successfully"]);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> ChangeApprovedStat(int id, bool approved)
        {
            Candidate candidate = null;

            candidate = await _context
                .Include(x => x.CandidatePositions)
                .ThenInclude(y => y.Position)
                .Include(x => x.CandidateFeatures)
                .ThenInclude(y => y.Feature)
                .Include(x => x.Offers)
                .Include(x => x.Meetings)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (approved)
            {
                candidate.IsApproved = true;
            }
            else
            {
                candidate.IsApproved = false;
            }

            await _uow.SaveChangesAsync();
            await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "success|" + _localizer["The Change Approved successfully"]);
            return RedirectToAction("Index");

            //return PartialView(@"~/Views/Candidates/_item.cshtml", candidate);
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> ApprovedStat(int id, bool approved)
        {
            var candidate = await _context
                .Include(x => x.CandidatePositions)
                .ThenInclude(y => y.Position)
                //.Include(x => x.CandidateFeatures)
                //.ThenInclude(y => y.Feature)
                //.Include(x => x.Offers)
                //.Include(x => x.Meetings)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);


            candidate.Status = CandidateStatus.WaitingForPaperwork;
            ViewData["Customers"] = await _customer.AsNoTracking().ToListAsync();

            return View(candidate);


            //Candidate candidate = null;

            //candidate = await _context.Candidate
            //    .Include(x => x.CandidatePositions)
            //    .ThenInclude(y => y.Position)
            //    .Include(x => x.CandidateFeatures)
            //    .ThenInclude(y => y.Feature)
            //    .Include(x => x.Offers)
            //    .Include(x => x.Meetings)
            //    .FirstOrDefaultAsync(x => x.Id == id);



            //if (approved)
            //{
            //    candidate.IsApproved = true;
            //}
            //else
            //{
            //    candidate.IsApproved = false;
            //}

            //await _context.SaveChangesAsync();

            //return RedirectToAction("Index");

            //return PartialView(@"~/Views/Candidates/_item.cshtml", candidate);
        }



        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export Accepted Candidates")]
        public async Task<IActionResult> ExportAcceptedCandidates()
        {
            var candidates = await _context
                .Where(x => x.Status == CandidateStatus.AcceptedOffer || x.Status == CandidateStatus.WaitingForPaperwork)
                .AsNoTracking()
                .ToListAsync();

            return File(ExportCandidatesAsExcel(candidates), XlsxContentType, "acceptedcandidates.xlsx");
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export Candidates")]
        public async Task<IActionResult> ExportCandidates()
        {
            var candidates = await _context
                //.Include(x => x.StuffDetail)
                //.ThenInclude(y => y.Title)
                //.Include(r => r.StuffDetail)
                //.ThenInclude(t => t.WorkingLocation)
                //.Where(x => x.Status != CandidateStatus.VitelcoStaff)
                .AsNoTracking()
                .ToListAsync();



            return File(ExportCandidatesAsExcel(candidates), XlsxContentType, "candidates.xlsx");
        }

        private byte[] ExportCandidatesAsExcel(List<Candidate> candidates)
        {
            byte[] reportBytes;
            using (var package = createCandidatesExcelPackage(candidates))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;

            //return File(reportBytes, XlsxContentType, "report.xlsx");

        }



        private ExcelPackage createCandidatesExcelPackage(List<Candidate> candidates)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Candidates Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Candidates Report";
            package.Workbook.Properties.Keywords = "Candidates";

            var worksheet = package.Workbook.Worksheets.Add("Candidates");
            //First add the headers
            worksheet.Cells[1, 1].Value = "Name Surename";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 2].Value = "Phone";
            worksheet.Cells[1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 3].Value = "Email";
            worksheet.Cells[1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 4].Value = "Start Period";
            //worksheet.Cells[1, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 4].Value = "Current Wages";
            worksheet.Cells[1, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 5].Value = "Expected Wages";
            worksheet.Cells[1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 6].Value = "Total IT experience";
            worksheet.Cells[1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 7].Value = "Position Related Experiences";
            worksheet.Cells[1, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 8].Value = "Candidate Summary";
            worksheet.Cells[1, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 9].Value = "Is candidate on the Black List";
            worksheet.Cells[1, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 10].Value = "Blacklist Reason";
            worksheet.Cells[1, 10].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 12].Value = "TC. Number";
            //worksheet.Cells[1, 12].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 13].Value = "Accommodation Address";
            //worksheet.Cells[1, 13].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 14].Value = "Military Status";
            //worksheet.Cells[1, 14].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 15].Value = "Postponemented Date";
            //worksheet.Cells[1, 15].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 16].Value = "Contract Type";
            //worksheet.Cells[1, 16].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 17].Value = "Contract Duration";
            //worksheet.Cells[1, 17].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 18].Value = "Title";
            //worksheet.Cells[1, 18].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 19].Value = "Working Location";
            //worksheet.Cells[1, 19].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 20].Value = "School Of Graduation";
            //worksheet.Cells[1, 20].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 21].Value = "Department Of Graduation";
            //worksheet.Cells[1, 21].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 22].Value = "Date Of Graduation";
            //worksheet.Cells[1, 22].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 23].Value = "IBAN Number";
            //worksheet.Cells[1, 23].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 24].Value = "Manager";
            //worksheet.Cells[1, 24].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 25].Value = "Blood Group";
            //worksheet.Cells[1, 25].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 26].Value = "Emergency Contact";
            //worksheet.Cells[1, 26].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells[1, 27].Value = "Employee Id";
            //worksheet.Cells[1, 27].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            //Add values

            //var numberformat = "#,##0";
            //var dataCellStyleName = "TableNumber";
            //var numStyle = package.Workbook.Styles.CreateNamedStyle(dataCellStyleName);
            //numStyle.Style.Numberformat.Format = numberformat;

            int rowNumber = 1;
            foreach (var item in candidates)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Name + " " + item.Family;
                worksheet.Cells[rowNumber, 2].Value = item.Telephone;
                worksheet.Cells[rowNumber, 3].Value = item.Email;
                //worksheet.Cells[rowNumber, 4].Value = item.StartPeriod;
                //worksheet.Cells[rowNumber, 2].Style.Numberformat.Format = numberformat;
                //worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                //worksheet.Cells[rowNumber, 3].Style.Numberformat.Format = numberformat;
                //worksheet.Cells[rowNumber, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                //worksheet.Cells[rowNumber, 4].Style.Numberformat.Format = numberformat;
                //worksheet.Cells[rowNumber, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 4].Value = item.CurrentWages;
                worksheet.Cells[rowNumber, 5].Value = item.ExpectedWages;
                worksheet.Cells[rowNumber, 6].Value = item.TotalITexperience;
                worksheet.Cells[rowNumber, 7].Value = item.PositionRelatedExperience;
                worksheet.Cells[rowNumber, 8].Value = item.BriefPersonalInfomation;
                worksheet.Cells[rowNumber, 9].Value = item.IsOnBlackList;
                worksheet.Cells[rowNumber, 10].Value = item.BlacklistReason;
                //worksheet.Cells[rowNumber, 12].Value = item.StuffDetail.TCNumber;
                //worksheet.Cells[rowNumber, 13].Value = item.StuffDetail.AccommodationAddress;
                //worksheet.Cells[rowNumber, 14].Value = item.StuffDetail.MilitaryStatus;
                //worksheet.Cells[rowNumber, 15].Value = item.StuffDetail.PostponementdDate;
                //worksheet.Cells[rowNumber, 16].Value = item.StuffDetail.ContractType;
                //worksheet.Cells[rowNumber, 17].Value = item.StuffDetail.ContractDuration;
                //worksheet.Cells[rowNumber, 18].Value = item.StuffDetail.Title.Name;
                //worksheet.Cells[rowNumber, 19].Value = item.StuffDetail.WorkingLocation.Name;
                //worksheet.Cells[rowNumber, 20].Value = item.StuffDetail.SchoolOfGraduation;
                //worksheet.Cells[rowNumber, 21].Value = item.StuffDetail.DepartmentOfGraduation;
                //worksheet.Cells[rowNumber, 22].Value = item.StuffDetail.DateOfGraduation;
                //worksheet.Cells[rowNumber, 23].Value = item.StuffDetail.IBANNumber;
                //worksheet.Cells[rowNumber, 24].Value = item.StuffDetail.Manager;
                //worksheet.Cells[rowNumber, 25].Value = item.StuffDetail.BloodGroup;
                //worksheet.Cells[rowNumber, 26].Value = item.StuffDetail.EmergencyContactInformation;
                //worksheet.Cells[rowNumber, 27].Value = item.StuffDetail.BirthCertificateNumber;

            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: candidates.Count() + 2, toColumn: 10), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;
            //tbl.Columns[3].DataCellStyleName = dataCellStyleName;
            //tbl.Columns[1].TotalsRowFunction = RowFunctions.Sum;
            //tbl.Columns[2].TotalsRowFunction = RowFunctions.Sum;
            //tbl.Columns[3].TotalsRowFunction = RowFunctions.Sum;

            //worksheet.Cells[candidates.Count() + 2, 4].Style.Numberformat.Format = numberformat;

            // AutoFitColumns
            worksheet.Cells[1, 1, candidates.Count() + 2, 10].AutoFitColumns();

            //worksheet.HeaderFooter.OddFooter.InsertPicture(
            //    new FileInfo(Path.Combine(_hostingEnvironment.WebRootPath, "images", "captcha.jpg")),
            //    PictureAlignment.Right);

            return package;
        }



        private bool CandidateExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }

        private bool StuffDetailExists(int id)
        {
            return _stuffDetail.AsNoTracking().Any(e => e.Id == id);
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> ValidateUsername(string portalusername, string email)
        {
            var result = await _userValidator.ValidateAsync(
                (UserManager<User>)_userManager, new User { UserName = portalusername, Email = email });

            return Json(result.Succeeded ? "true" : result.DumpErrors(useHtmlNewLine: true));
        }

        private IdentityResult ValidatePassword(string username, string password)
        {
            var errors = new List<IdentityError>();

            if (string.IsNullOrWhiteSpace(password))
            {
                errors.Add(new IdentityError
                {
                    Code = "PasswordIsNotSet",
                    Description = "Password is not set"
                });
                return IdentityResult.Failed(errors.ToArray());
            }


            // Extending the built-in validator
            if (password.Contains(username, StringComparison.OrdinalIgnoreCase))
            {
                errors.Add(new IdentityError
                {
                    Code = "PasswordContainsUserName",
                    Description = "Password cannot consist part of username."
                });
                return IdentityResult.Failed(errors.ToArray());
            }

            if (!isSafePasword(password))
            {
                errors.Add(new IdentityError
                {
                    Code = "PasswordIsNotSafe",
                    Description = "Password is simple and not safe"
                });
                return IdentityResult.Failed(errors.ToArray());
            }

            return !errors.Any() ? IdentityResult.Success : IdentityResult.Failed(errors.ToArray());
        }

        private bool isSafePasword(string data)
        {
            if (string.IsNullOrWhiteSpace(data)) return false;
            if (data.Length < 5) return false;
            if (_passwordsBanList.Contains(data.ToLowerInvariant())) return false;
            if (areAllCharsEqual(data)) return false;

            return true;
        }

        private static bool areAllCharsEqual(string data)
        {
            if (string.IsNullOrWhiteSpace(data)) return false;
            data = data.ToLowerInvariant();
            var firstElement = data.ElementAt(0);
            var euqalCharsLen = data.ToCharArray().Count(x => x == firstElement);
            if (euqalCharsLen == data.Length) return true;
            return false;
        }
    }
}
