﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Common;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Expertise Types")]
    public class ExpertisesController : Controller
    {
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        private readonly IUnitOfWork _uow;
        private readonly DbSet<Expertise> _context;

        public ExpertisesController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<Expertise>();
        }

        // GET: Expertises
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.AsNoTracking().ToListAsync());
        }

        // GET: Expertises/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expertise = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (expertise == null)
            {
                return NotFound();
            }

            return View(expertise);
        }

        // GET: Expertises/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Expertises/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ExpertiseName")] Expertise expertise)
        {
            if (ModelState.IsValid)
            {
                _context.Add(expertise);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(expertise);
        }

        // GET: Expertises/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expertise = await _context.FindAsync(id);
            if (expertise == null)
            {
                return NotFound();
            }
            return View(expertise);
        }

        // POST: Expertises/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ExpertiseName,Id")] Expertise expertise)
        {
            if (id != expertise.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(expertise);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpertiseExists(expertise.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(expertise);
        }

        // GET: Expertises/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expertise = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (expertise == null)
            {
                return NotFound();
            }

            return View(expertise);
        }

        // POST: Expertises/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var expertise = await _context.FindAsync(id);
            _context.Remove(expertise);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> Export()
        {
            var expertises = await _context.AsNoTracking().ToListAsync();

            return File(ExportAsExcel(expertises), XlsxContentType, "expertises.xlsx");
        }

        private byte[] ExportAsExcel(List<Expertise> expertises)
        {
            byte[] reportBytes;
            using (var package = createExcelPackage(expertises))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }

        private ExcelPackage createExcelPackage(List<Expertise> expertises)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Expertises Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Expertises Report";
            package.Workbook.Properties.Keywords = "Expertises";

            var worksheet = package.Workbook.Worksheets.Add("Expertises");
            //First add the headers

            worksheet.Cells[1, 1].Value = "Name";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            
            //Add values

            int rowNumber = 1;
            foreach (var item in expertises)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.ExpertiseName;
                worksheet.Cells[rowNumber, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
               
            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: expertises.Count() + 2, toColumn: 1), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;

            // AutoFitColumns
            worksheet.Cells[1, 1, expertises.Count() + 2, 1].AutoFitColumns();

            return package;
        }

        private bool ExpertiseExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
