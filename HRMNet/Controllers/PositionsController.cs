﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Common;
using HRMNet.Models.Positions;
using HRMNet.Models.Candidates;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using HRMNet.Models.Customers;
using System.ComponentModel;
using System.Data.SqlClient;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Positions")]
    public class PositionsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Position> _context;
        private readonly DbSet<Feature> _feature;
        private readonly DbSet<Expertise> _expertise;
        private readonly DbSet<Customer> _customer;
        private readonly DbSet<Candidate> _candidate;
        public PositionsController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<Position>();
            _feature = uow.Set<Feature>();
            _expertise = uow.Set<Expertise>();
            _customer = uow.Set<Customer>();
            _candidate = uow.Set<Candidate>();
        }

        // GET: Positions
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            var query = await _context
                .Include(x => x.Customer)
                .Include(x => x.PositionFeatures)
                .ThenInclude(x => x.Feature)
                .Include(x => x.PositionMandatoryFeatures)
                .ThenInclude(x => x.Feature)
                .ToListAsync();

            return View(query);
        }

        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> GetDetail(int id)
        {
            var position = await _context
                .Include(x => x.Customer)
                .AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            return Json(new {Customer= position?.Customer?.Name, Project=position?.ProjectName });
        }


        public async Task<IActionResult> GetAllPositions()
        {
            return Json(new SelectList(await _context.AsNoTracking().ToListAsync(), "Id", "PositionName"));
        }

        // GET: Positions/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var position = await _context.AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (position == null)
            {
                return NotFound();
            }

            return View(position);
        }

        // GET: Positions/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public async Task<IActionResult> Create()
        {
            var features = await _feature.Select(x => x.FeatureName).AsNoTracking().ToArrayAsync();
            var types = await _expertise.Select(x => x.ExpertiseName).AsNoTracking().ToArrayAsync();
            var model = new Position()
            {
                FeaturesSource = features.ToJson(),
                TypesSource= types.ToJson()
            };

            //ViewData["Customers"] = new SelectList(await _context.Customers.ToListAsync(), "Id", "Name");
            ViewData["Customers"] = await _customer.AsNoTracking().ToListAsync();

            return View(model);
        }

        [HttpPost]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> GetCustomersPositions(int id)
        {
            if (id == 0)
                return Json(null);

            var positions =await _context
                .Include(x=> x.Customer)
                .Where(x => x.CustomerId == id && x.Status== PositionStatus.Active)
                .AsNoTracking()
                .ToListAsync();

            return Json(new SelectList(positions, "Id", "PositionName"));
        }

        [HttpPost]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> GetPositionDetail(int id)
        {
            if (id == 0)
                return Json(new { Features = "", MustToHaves = "", Expertises = "" });

            var position = await _context
                .Include(x => x.PositionFeatures)
                    .ThenInclude(y => y.Feature)
                .Include(x => x.PositionMandatoryFeatures)
                    .ThenInclude(y => y.Feature)
                .Include(x => x.PositionTypes)
                    .ThenInclude(y => y.Expertise)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            var selectedFeatures = position.PositionFeatures.Select(x => x.Feature).Select(x => x.FeatureName).ToArray();
            var selectedMandatoryFeatures = position.PositionMandatoryFeatures.Select(x => x.Feature).Select(x => x.FeatureName).ToArray();
            var selectedExpertises = position.PositionTypes.Select(x => x.Expertise).Select(x => x.ExpertiseName).ToArray();

            position.InitialFeature = selectedFeatures.ToJson();

            return Json(new {Features = selectedFeatures, MustToHaves = selectedMandatoryFeatures, Expertises = selectedExpertises });
        }

        [HttpPost]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> GetSuitableCandidate(string[] features, int positionId)
        {
            if (features == null || features.Length == 0)
                return PartialView(@"~/Views/Positions/_SuitableCandidate.cshtml", new List<Candidate>());

            //var featuresIds = _feature.Where(x => features.Contains(x.FeatureName)).AsNoTracking().Select(y => y.Id);

            //var suitableCandidates = await _candidate
            //    .Include(x => x.CandidateFeatures)
            //    .ThenInclude(y => y.Feature)
            //    .Where(x => featuresIds.All(y => x.CandidateFeatures.Any(z => z.FeatureId == y)))
                
            //    .ToListAsync();

            //var featuresToStringArray = string.Empty;
            
            //features.ToList().ForEach(x => featuresToStringArray +="'"+x+"',");
            //if (featuresToStringArray.Substring(featuresToStringArray.Length - 1, 1) == ",")
            //    featuresToStringArray=featuresToStringArray.Remove(featuresToStringArray.Length - 1);

            //var stringQuery = "select c.* from Candidate c inner join CandidatePositions cp " +
            //                  "on cp.CandidateId = c.Id " +
            //                  "inner join CandidateFeatures cf " +
            //                  "on cf.CandidateId = c.Id " +
            //                  "inner join Features f " +
            //                  "on cf.FeatureId = f.Id " +
            //                  "where f.FeatureName in ("+ featuresToStringArray + ") and cp.PositionId!="+positionId;

            var featuresIds = _feature.Where(x => features.Contains(x.FeatureName)).AsNoTracking().Select(y => y.Id);
            var suitableCandidates = _candidate
                .Include(x => x.CandidateFeatures)
                .ThenInclude(y => y.Feature)
                .Where(x => !x.CandidatePositions.Any(z=>z.PositionId== positionId) && featuresIds.All(y => x.CandidateFeatures.Any(z => z.FeatureId == y)));

            //var tttt = suitableCandidates1.ToQueryString();
            var result = await suitableCandidates.ToListAsync();

            //var suitableCandidates = _candidate
            //    .FromSqlRaw(stringQuery);
                
            //var queryString = suitableCandidates.ToQueryString();
            //var result = await suitableCandidates.ToListAsync();
            return PartialView(@"~/Views/Positions/_SuitableCandidate.cshtml", result ?? new List<Candidate>());
        }
        
        // POST: Positions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Position position, string checkedCandidates)
        {
            if (ModelState.IsValid)
            {
                var selectedCandidates = checkedCandidates?.Split(",", StringSplitOptions.RemoveEmptyEntries);
                if (position.SelectedFeatures != null)
                {
                    position.PositionFeatures = new List<PositionFeature>();

                    var selectedFeatures = _feature.Where(x => position.SelectedFeatures.Contains(x.FeatureName)).AsNoTracking();
                    foreach (var item in selectedFeatures)
                    {
                        var toAdd = new PositionFeature { PositionId = position.Id, FeatureId = item.Id };
                        position.PositionFeatures.Add(toAdd);
                    }
                }

                if (position.SelectedMandatoryFeatures != null)
                {
                    position.PositionMandatoryFeatures = new List<PositionMandatoryFeature>();

                    var selectedManadatoryFeatures = _feature.Where(x => position.SelectedMandatoryFeatures.Contains(x.FeatureName)).AsNoTracking();
                    foreach (var item in selectedManadatoryFeatures)
                    {
                        var toAdd = new PositionMandatoryFeature { PositionId = position.Id, FeatureId = item.Id };
                        position.PositionMandatoryFeatures.Add(toAdd);
                    }
                }

                if (selectedCandidates != null && selectedCandidates.Any())
                {
                    position.CandidatePositions = new List<CandidatePosition>();
                    foreach (var item in selectedCandidates)
                    {
                        var toAdd = new CandidatePosition { CandidateId = int.Parse(item) };
                        position.CandidatePositions.Add(toAdd);
                    }

                }


                if (position.SelectedTypes != null && position.SelectedTypes.Any())
                {
                    position.PositionTypes = new List<PositionType>();

                    var selectedTypes = _expertise.Where(x => position.SelectedTypes.Contains(x.ExpertiseName)).AsNoTracking();
                    foreach (var item in selectedTypes)
                    {
                        var toAdd = new PositionType { PositionId = position.Id, ExpertiseId = item.Id };
                        position.PositionTypes.Add(toAdd);
                    }
                }



                _context.Add(position);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            var features = await _feature.Select(x => x.FeatureName).AsNoTracking().ToArrayAsync();
            var types = await _expertise.Select(x => x.ExpertiseName).AsNoTracking().ToArrayAsync();
            position.InitialFeature = position.SelectedFeatures.ToJson();
            position.InitialiType = position.SelectedTypes.ToJson();
            position.InitialMandatoryFeature = position.SelectedMandatoryFeatures.ToJson();
            position.TypesSource = types.ToJson();
            position.FeaturesSource = features.ToJson();

            ViewData["Customers"] = await _customer.AsNoTracking().ToListAsync();
            return View(position);
        }

        // GET: Positions/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var position = await _context
                .Include(x => x.PositionFeatures)
                    .ThenInclude(y => y.Feature)
                .Include(x => x.PositionMandatoryFeatures)
                    .ThenInclude(y => y.Feature)
                .Include(x=> x.PositionTypes)
                    .ThenInclude(x=> x.Expertise)
                .Include(x=> x.CandidatePositions)
                    .ThenInclude(x=> x.Candidate)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id.Value);

            if (position == null)
            {
                return NotFound();
            }

            var features = await _feature.Select(x => x.FeatureName).AsNoTracking().ToArrayAsync();
            var types = await _expertise.Select(x => x.ExpertiseName).AsNoTracking().ToArrayAsync();
            var selectedFeatures = position.PositionFeatures.Select(x => x.Feature).Select(x => x.FeatureName).ToArray();
            position.InitialFeature = selectedFeatures.ToJson();
            position.FeaturesSource = features.ToJson();

            var selectedTypes = position.PositionTypes.Select(x => x.Expertise).Select(x => x.ExpertiseName).ToArray();
            position.InitialiType = selectedTypes.ToJson();
            position.TypesSource = types.ToJson();

            var selectedMandatoryFeatures = position.PositionMandatoryFeatures.Select(x => x.Feature).Select(x => x.FeatureName).ToArray();
            position.InitialMandatoryFeature = selectedMandatoryFeatures.ToJson();
            //position.FeaturesSource = features.ToJson();

            ViewData["Customers"] = await _customer.AsNoTracking().ToListAsync();

            return View(position);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Position model, string checkedCandidates)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var selectedCandidates = checkedCandidates?.Split(",", StringSplitOptions.RemoveEmptyEntries);

                var position = _context
                .Include(x => x.PositionFeatures)
                .Include(x => x.PositionMandatoryFeatures)
                .Include(x=> x.PositionTypes)
                .FirstOrDefault(x => x.Id == model.Id);

                position.BlackListReason = model.BlackListReason;
                position.ClosingDate = model.ClosingDate;
                position.ClosingOrSuspendingReason = model.ClosingOrSuspendingReason;
                position.ProjectName = model.ProjectName;
                position.CustomerId = model.CustomerId;
                position.ExistOnKariyer = model.ExistOnKariyer;
                position.ExistOnLinkedIn = model.ExistOnLinkedIn;
                position.Expertises = model.Expertises;
                position.IsClosed = model.IsClosed;
                position.IsOnBlackList = model.IsOnBlackList;
                position.IsBringAppropriateStarted = model.IsBringAppropriateStarted;
                position.JobDescription = model.JobDescription;
                position.Status = model.Status;
                position.MustToHave = model.MustToHave;
                position.PositionName = model.PositionName;
                position.PositionType = model.PositionType;
                position.RelatedPositionExperience = model.RelatedPositionExperience;
                position.ResponsibleMan = model.ResponsibleMan;
                position.StartingDate = model.StartingDate;
                position.PersonalCharacteristics = model.PersonalCharacteristics;

                position.PositionFeatures.Clear();
                position.PositionMandatoryFeatures.Clear();
                position.PositionTypes.Clear();

                if (model.SelectedFeatures != null)
                {
                    position.PositionFeatures = new List<PositionFeature>();

                    var selectedFeatures = _feature.Where(x => model.SelectedFeatures.Contains(x.FeatureName));
                    foreach (var item in selectedFeatures)
                    {
                        var toAdd = new PositionFeature { PositionId = model.Id, FeatureId = item.Id };
                        position.PositionFeatures.Add(toAdd);
                    }
                }

                if (model.SelectedTypes != null)
                {
                    position.PositionTypes = new List<PositionType>();

                    var selectedTypes = _expertise.Where(x => model.SelectedTypes.Contains(x.ExpertiseName));
                    foreach (var item in selectedTypes)
                    {
                        var toAdd = new PositionType { PositionId = model.Id, ExpertiseId = item.Id };
                        position.PositionTypes.Add(toAdd);
                    }
                }

                if (model.SelectedMandatoryFeatures != null)
                {
                    position.PositionMandatoryFeatures = new List<PositionMandatoryFeature>();

                    var selectedMandatoryFeatures = _feature.Where(x => model.SelectedMandatoryFeatures.Contains(x.FeatureName)).AsNoTracking();
                    foreach (var item in selectedMandatoryFeatures)
                    {
                        var toAdd = new PositionMandatoryFeature { PositionId = model.Id, FeatureId = item.Id };
                        position.PositionMandatoryFeatures.Add(toAdd);
                    }
                }

                if (selectedCandidates != null && selectedCandidates.Any())
                {
                    position.CandidatePositions = new List<CandidatePosition>();
                    foreach (var item in selectedCandidates)
                    {
                        var toAdd = new CandidatePosition { CandidateId = int.Parse(item) };
                        position.CandidatePositions.Add(toAdd);
                    }
                }

                var result = await _uow.SaveChangesAsync();

                if (result > 0)
                    return RedirectToAction(nameof(Index));
            }


            var features = await _feature.Select(x => x.FeatureName).AsNoTracking().ToArrayAsync();
            var types = await _expertise.Select(x => x.ExpertiseName).AsNoTracking().ToArrayAsync();

            model.InitialFeature = model.SelectedFeatures.ToJson();
            model.InitialiType = model.SelectedTypes.ToJson();
            model.InitialMandatoryFeature = model.SelectedMandatoryFeatures.ToJson();
            model.TypesSource = types.ToJson();
            model.FeaturesSource = features.ToJson();

            ViewData["Customers"] = await _customer.AsNoTracking().ToListAsync();

            return View(model);
        }

        // GET: Positions/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var position = await _context
                .Include(x=> x.Customer)
                .Include(x=> x.PositionFeatures)
                .ThenInclude(x=> x.Feature)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);

            var features = position.PositionFeatures.Select(x => x.Feature).Select(x => x.FeatureName);
            string featureList = string.Empty;
            features.ToList().ForEach(x => featureList += x+", ");

            position.Features = featureList;
            if (position == null)
            {
                return NotFound();
            }

            return View(position);
        }

        // POST: Positions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var position = await _context.FindAsync(id);
            _context.Remove(position);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PositionExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
