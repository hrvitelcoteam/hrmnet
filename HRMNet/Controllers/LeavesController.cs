﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;
using HRMNet.Models.Common;
using Microsoft.Extensions.Localization;
using HRMNet.Services;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Leaves")]
    public class LeavesController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Leaves> _context;
        private readonly DbSet<Candidate> _candidate;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IStringLocalizer _localizer;

        public LeavesController(IUnitOfWork uow, IStringLocalizerFactory stringLocalizerFactory)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _context = uow.Set<Leaves>();
            _candidate = uow.Set<Candidate>();

            _localizer = stringLocalizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
        }



        // GET: Leaves
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public IActionResult Index()
        {
            var leave = _context
                .Include(x => x.Candidate)
                .ThenInclude(y => y.StuffDetail)
                .AsNoTracking()
                .AsEnumerable()
                .GroupBy(c => c.CandidateId)
                .Select(x => new LeaveViewModel
                {
                    CandidateId = x.Max(z => z.CandidateId),
                    CandidateName = x.Max(z => z.Candidate.Name + " " + z.Candidate.Family),
                    TotalLegalLeave = x.Sum(z => z.LeaveType == Models.Common.LeaveType.Legal ? (z.EndDate - z.StartDate).TotalHours : 0),
                    TotalDayOffLeave = x.Sum(z => z.LeaveType == Models.Common.LeaveType.Dayoff ? (z.EndDate - z.StartDate).TotalHours : 0),
                    BirthNumber = x.Max(z => z.Candidate?.StuffDetail?.BirthCertificateNumber),
                    TotalLeave = x.Sum(z => z.LeaveType == Models.Common.LeaveType.Legal ? (z.EndDate - z.StartDate).TotalHours : 0) + x.Sum(z => z.LeaveType == Models.Common.LeaveType.Dayoff ? (z.EndDate - z.StartDate).TotalHours : 0),
                    
                });

            
            return View(leave);
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> ExportLeaves()
        {
            var leaves=await _context
                .Include(x => x.Candidate)
                .ThenInclude(y => y.StuffDetail)
                .AsNoTracking()
                .ToListAsync();



            return File(ExportLeavesAsExcel(leaves), XlsxContentType, "leaves.xlsx");
        }

        private byte[] ExportLeavesAsExcel(List<Leaves> leaves)
        {
            byte[] reportBytes;
            using (var package = createLeavesExcelPackage(leaves))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;

            //return File(reportBytes, XlsxContentType, "report.xlsx");

        }

        private ExcelPackage createLeavesExcelPackage(List<Leaves> leaves)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Leaves Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Leaves Report";
            package.Workbook.Properties.Keywords = "Leavess";

            var worksheet = package.Workbook.Worksheets.Add("Leaves");
            //First add the headers
            worksheet.Cells[1, 1].Value = "Name Surename";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 2].Value = "Start Date";
            worksheet.Cells[1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 3].Value = "End Date";
            worksheet.Cells[1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 4].Value = "Permission Type";
            worksheet.Cells[1, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 5].Value = "Leave Type";
            worksheet.Cells[1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 6].Value = "Description";
            worksheet.Cells[1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            
            //Add values

            //var numberformat = "#,##0";
            //var dataCellStyleName = "TableNumber";
            //var numStyle = package.Workbook.Styles.CreateNamedStyle(dataCellStyleName);
            //numStyle.Style.Numberformat.Format = numberformat;

            int rowNumber = 1;
            foreach (var item in leaves)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Candidate.Name + " " + item.Candidate.Family;
                worksheet.Cells[rowNumber, 2].Value = item.StartDate.ToString("yyyy/MM/dd hh:mm");
                worksheet.Cells[rowNumber, 3].Value = item.EndDate.ToString("yyyy/MM/dd hh:mm");
                worksheet.Cells[rowNumber, 4].Value = item.PermissionType;
                worksheet.Cells[rowNumber, 5].Value = item.LeaveType;
                worksheet.Cells[rowNumber, 6].Value = item.Description;
                
            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: leaves.Count() + 2, toColumn: 6), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;
            

            // AutoFitColumns
            worksheet.Cells[1, 1, leaves.Count() + 2, 6].AutoFitColumns();

            //worksheet.HeaderFooter.OddFooter.InsertPicture(
            //    new FileInfo(Path.Combine(_hostingEnvironment.WebRootPath, "images", "captcha.jpg")),
            //    PictureAlignment.Right);

            return package;
        }


        [HttpPost]
        public async Task<IActionResult> GetCandidateLeaves(int? id)
        {

            if (id == null)
                return Json("");

            var candidateLeaves =await _context
                .Where(x => x.CandidateId == id)
                .AsNoTracking()
                .ToListAsync();

            var sb = new StringBuilder();
            sb.Append("<table cellpadding=\"5\" cellspacing=\"0\" class=\"table table-bordered table-striped\" style=\"padding-left:50px;\" >");
            sb.Append("<thead><tr>");
            sb.Append("<td>"+_localizer["Start"] +"</td>");
            sb.Append("<td>"+ _localizer["End"] + "</td>");
            sb.Append("<td>"+ _localizer["Permission Type"] + "</td>");
            sb.Append("<td>"+ _localizer["Leave Type"] + "</td>");
            sb.Append("<td></td>");
            sb.Append("</tr></thead>");
            sb.Append("<tbody>");

            foreach (var item in candidateLeaves)
            {
                sb.Append("<tr>");
                sb.Append("<td>"+ item.StartDate+"</td>");
                sb.Append("<td>" + item.EndDate + "</td>");
                sb.Append("<td>" + _localizer[EnumHelper<PermissionType>.GetDisplayValue(item.PermissionType)]  + "</td>");
                sb.Append("<td>" + _localizer[EnumHelper<LeaveType>.GetDisplayValue(item.LeaveType)] + "</td>");
                sb.Append("<td><a href=\"/Leaves/Edit/"+ item.Id + "\">"+ _localizer["Edit"] + "</a> |" +
                                            //"<a href=\"/Leaves/Detail/" + item.Id + "\"> Details </a> |" +
                                            "<a href=\"/Leaves/Delete/" + item.Id + "\">"+ _localizer["Delete"] + "</a ></td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            return Json(sb.ToString());
        }

        // GET: Leaves/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaves = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leaves == null)
            {
                return NotFound();
            }

            return View(leaves);
        }

        // GET: Leaves/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public async Task<IActionResult> Create()
        {

            var stuffs = await _candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff).AsNoTracking().ToListAsync();
            ViewData["CandidateId"] = new SelectList(stuffs, "Id", "DisplayName");

            return View();
        }

        // POST: Leaves/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CandidateId,StartDate,EndDate,PermissionType,LeaveType,Description,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] Leaves leaves)
        {
            if (ModelState.IsValid)
            {
                _context.Add(leaves);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(leaves);
        }

        // GET: Leaves/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaves = await _context.FindAsync(id);
            if (leaves == null)
            {
                return NotFound();
            }

            var stuffs = await _candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff).AsNoTracking().ToListAsync();
            ViewData["CandidateId"] = new SelectList(stuffs, "Id", "DisplayName");

            return View(leaves);
        }

        // POST: Leaves/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CandidateId,StartDate,EndDate,PermissionType,LeaveType,Description,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] Leaves leaves)
        {
            if (id != leaves.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(leaves);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LeavesExists(leaves.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(leaves);
        }

        // GET: Leaves/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaves = await _context
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leaves == null)
            {
                return NotFound();
            }

            return View(leaves);
        }

        // POST: Leaves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var leaves = await _context.FindAsync(id);
            _context.Remove(leaves);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LeavesExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
