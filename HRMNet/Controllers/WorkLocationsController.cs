﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Common;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Work Locations")]
    public class WorkLocationsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<WorkLocation> _context;
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


        public WorkLocationsController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<WorkLocation>();
        }

        // GET: WorkLocations
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.AsNoTracking().ToListAsync());
        }

        // GET: WorkLocations/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workLocation = await _context
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workLocation == null)
            {
                return NotFound();
            }

            return View(workLocation);
        }

        // GET: WorkLocations/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: WorkLocations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Address,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] WorkLocation workLocation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(workLocation);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(workLocation);
        }

        // GET: WorkLocations/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workLocation = await _context.FindAsync(id);
            if (workLocation == null)
            {
                return NotFound();
            }
            return View(workLocation);
        }

        // POST: WorkLocations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,Address,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] WorkLocation workLocation)
        {
            if (id != workLocation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(workLocation);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkLocationExists(workLocation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(workLocation);
        }

        // GET: WorkLocations/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workLocation = await _context
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workLocation == null)
            {
                return NotFound();
            }

            return View(workLocation);
        }

        // POST: WorkLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var workLocation = await _context.FindAsync(id);
            _context.Remove(workLocation);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> Export()
        {
            var locations = await _context.AsNoTracking().ToListAsync();
            
            return File(ExportAsExcel(locations), XlsxContentType, "worklocations.xlsx");
        }

        private byte[] ExportAsExcel(List<WorkLocation> locations)
        {
            byte[] reportBytes;
            using (var package = createExcelPackage(locations))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }

        private ExcelPackage createExcelPackage(List<WorkLocation> locations)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Work Locations Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Work Locations Report";
            package.Workbook.Properties.Keywords = "Work Locations";

            var worksheet = package.Workbook.Worksheets.Add("Work Locations");
            //First add the headers

            worksheet.Cells[1, 1].Value = "Name";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 2].Value = "Address";
            worksheet.Cells[1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            
            //Add values

            int rowNumber = 1;
            foreach (var item in locations)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Name;
                worksheet.Cells[rowNumber, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[rowNumber, 2].Value = item.Address;
                worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                
            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: locations.Count() + 2, toColumn: 2), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;

            // AutoFitColumns
            worksheet.Cells[1, 1, locations.Count() + 2, 2].AutoFitColumns();

            return package;
        }

        private bool WorkLocationExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
