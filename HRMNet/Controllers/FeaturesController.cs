﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Common;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Features")]
    public class FeaturesController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Feature> _context;
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


        public FeaturesController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<Feature>();
        }

        // GET: Features
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.AsNoTracking().ToListAsync());
        }

        // GET: Features/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feature = await _context.AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (feature == null)
            {
                return NotFound();
            }

            return View(feature);
        }

        // GET: Features/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Features/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FeatureName")] Feature feature)
        {
            if (ModelState.IsValid)
            {
                _context.Add(feature);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(feature);
        }

        // GET: Features/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feature = await _context.FindAsync(id);
            if (feature == null)
            {
                return NotFound();
            }
            return View(feature);
        }

        // POST: Features/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FeatureName,Id")] Feature feature)
        {
            if (id != feature.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(feature);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeatureExists(feature.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(feature);
        }

        // GET: Features/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feature = await _context.AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (feature == null)
            {
                return NotFound();
            }

            return View(feature);
        }

        // POST: Features/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var feature = await _context.FindAsync(id);
            _context.Remove(feature);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> Export()
        {
            var features = await _context.AsNoTracking().ToListAsync();

            return File(ExportAsExcel(features), XlsxContentType, "features.xlsx");
        }

        private byte[] ExportAsExcel(List<Feature> features)
        {
            byte[] reportBytes;
            using (var package = createExcelPackage(features))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }

        private ExcelPackage createExcelPackage(List<Feature> features)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Features Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Features Report";
            package.Workbook.Properties.Keywords = "Features";

            var worksheet = package.Workbook.Worksheets.Add("Features");
            //First add the headers

            worksheet.Cells[1, 1].Value = "FeatureName";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            
            //Add values

            int rowNumber = 1;
            foreach (var item in features)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.FeatureName;
                worksheet.Cells[rowNumber, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                
            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: features.Count() + 2, toColumn: 1), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;

            // AutoFitColumns
            worksheet.Cells[1, 1, features.Count() + 2, 1].AutoFitColumns();

            return package;
        }

        private bool FeatureExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
