﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using Newtonsoft.Json;
using System.Reflection.Emit;
using HRMNet.Models.Common;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Inventories")]
    public class InventoriesController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Inventory> _context;
        private readonly DbSet<Candidate> _candidate;
        private readonly DbSet<InventoryType> _inventoryType;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


        public InventoriesController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _context = uow.Set<Inventory>();
            _candidate = uow.Set<Candidate>();
            _inventoryType = uow.Set<InventoryType>();
        }

        [HttpPost]
        public async Task<IActionResult> GetByCategoryId(int id, OperationType type, int candidateId)
        {
            try
            {
                if (type == OperationType.AssignmentToEmployee)
                {
                    var inventories = await _context
                   .Include(x => x.CandidateInventories)
                   //.ToListAsync();
                   .Where(x => x.InventoryTypeId == id && (x.CandidateInventories == null || x.CandidateInventories.Count==0 || x.CandidateInventories.OrderByDescending(x=> x.Id).FirstOrDefault().OperationType == OperationType.ReturnToCompany))
                   //.AsAsyncEnumerable()
                   .AsNoTracking()
                   .ToListAsync();

                    return Json(new SelectList(inventories, "Id", "InventoryNumber"));
                }
                else
                {
                    var inventories = await _context
                   .Include(x => x.CandidateInventories)
                   //.AsAsyncEnumerable()
                   .Where(x => x.InventoryTypeId == id && x.CandidateInventories.Any(y=> y.CandidateId== candidateId) && x.CandidateInventories.OrderByDescending(y => y.Id).FirstOrDefault().OperationType == OperationType.AssignmentToEmployee)
                   //.AsNoTracking()
                   .ToListAsync();

                    return Json(new SelectList(inventories, "Id", "InventoryNumber"));
                }
            }
            catch (Exception e)
            {

                return Json(e.Message);
            }
            
        }

        [HttpPost]
        public async Task<IActionResult> GetInventoryById(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var inventory = await _context
                .AsNoTracking()
                //.Include(i => i.Candidate)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (inventory == null)
            {
                return NotFound();
            }

            //var ttt=JsonConvert.SerializeObject(new {Brand=inventory.Brand });
            //return inventory;
            return Json(inventory);
        }
        
        private bool InventoryExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Inventories()
        {
            var model = await _context
                //.Include(i => i.Candidate)
                .Include(x => x.InventoryType)

                //.Include(x => x.CandidatePositions)
                //.ThenInclude(y => y.Position)
                //.Include(x => x.CandidateFeatures)
                //.ThenInclude(y => y.Feature)
                //.Include(x => x.Offers)
                //.Include(x => x.Meetings)
                .AsNoTracking()
                .Select(x => new Inventory()
                {
                    Id = x.Id,
                    Brand = x.Brand,
                    InventoryNumber = x.InventoryNumber,
                    InventoryType = x.InventoryType,
                    InventoryTypeId = x.InventoryTypeId,
                    Model = x.Model,
                    SerialNumber = x.SerialNumber,
                    IsUsed = (!x.CandidateInventories.Any() || x.CandidateInventories.Any(x => x.OperationType == Models.Common.OperationType.ReturnToCompany)) ? false : true
                })
                .ToListAsync();

            return View(model);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            //var stuffs = await _context.Candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStaff).ToListAsync();
            //ViewData["CandidateId"] = new SelectList(stuffs, "Id", "Name");

            ViewData["InventoryTypeId"] = new SelectList(_inventoryType.AsNoTracking(), "Id", "Name");


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PurchasedLocationAndDate,WindowsPassword,Windows,VirusProgramPassword,VirusProgram,OfficeProgramKey,OfficeProgram,PasswordHint,WindowsUserPassword,WindowsUsername,SerialNumber,Model,Brand,DeliveryDate,InventoryTypeId,InventoryNumber")] Inventory inventory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(inventory);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Inventories));
            }

            //var stuffs = await _context.Candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStaff).ToListAsync();


            //ViewData["CandidateId"] = new SelectList(stuffs, "Id", "Name", inventory.CandidateId);
            ViewData["InventoryTypeId"] = new SelectList(_inventoryType.AsNoTracking(), "Id", "Name");

            return View(inventory);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventory = await _context
                //.Include(x=>x.Candidate)
                //.Include(x=>x.InventoryType)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);

            if (inventory == null)
            {
                return NotFound();
            }

            //var stuffs = await _context.Candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStaff).ToListAsync();


            //ViewData["CandidateId"] = new SelectList(stuffs, "Id", "Name", inventory.CandidateId);
            ViewData["InventoryTypeId"] = new SelectList(_inventoryType.AsNoTracking(), "Id", "Name");

            return View(inventory);
        }

        // POST: Inventories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PurchasedLocationAndDate,WindowsPassword,Windows,VirusProgramPassword,VirusProgram,OfficeProgramKey,OfficeProgram,PasswordHint,WindowsUserPassword,WindowsUsername,SerialNumber,Model,Brand,DeliveryDate,InventoryTypeId,Id,InventoryNumber")] Inventory inventory)
        {
            if (id != inventory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(inventory);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InventoryExists(inventory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Inventories));
            }

            //var stuffs = await _context.Candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStaff).ToListAsync();

            //ViewData["CandidateId"] = new SelectList(stuffs, "Id", "Name", inventory.CandidateId);
            ViewData["InventoryTypeId"] = new SelectList(_inventoryType.AsNoTracking(), "Id", "Name");

            return View(inventory);
        }

        // GET: Inventories/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventory = await _context
                //.Include(i => i.Candidate)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (inventory == null)
            {
                return NotFound();
            }

            return View(inventory);
        }

        // POST: Inventories/Delete/5
        [HttpPost, ActionName("DeleteInventory")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inventory = await _context.FindAsync(id);
            _context.Remove(inventory);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Inventories));
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventory = await _context
                //.Include(i => i.Candidate)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (inventory == null)
            {
                return NotFound();
            }

            return View(inventory);
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> Export()
        {
            var inventories = await _context
                //.Include(i => i.Candidate)
                .Include(x => x.InventoryType)
                .Include(x => x.CandidateInventories)
                .AsNoTracking()
                .ToListAsync();

            return File(ExportInventoriesAsExcel(inventories), XlsxContentType, "inventories.xlsx");
        }

        private byte[] ExportInventoriesAsExcel(List<Inventory> inventories)
        {
            byte[] reportBytes;
            using (var package = createInventoriesExcelPackage(inventories))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }

        private ExcelPackage createInventoriesExcelPackage(List<Inventory> inventories)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Inventories Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Inventories Report";
            package.Workbook.Properties.Keywords = "Inventories";

            var worksheet = package.Workbook.Worksheets.Add("Inventories");
            //First add the headers

            worksheet.Cells[1, 1].Value = "Category Name";
            worksheet.Cells[1, 2].Value = "Number";
            worksheet.Cells[1, 3].Value = "Brand";

            worksheet.Cells[1, 4].Value = "Model";

            worksheet.Cells[1, 5].Value = "Serial Number";

            worksheet.Cells[1, 6].Value = "Is Used";

            //Add values

            int rowNumber = 1;
            foreach (var item in inventories)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.InventoryType.Name;
                worksheet.Cells[rowNumber, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 2].Value = item.InventoryNumber;
                worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 3].Value = item.Brand;
                worksheet.Cells[rowNumber, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 4].Value = item.Model;
                worksheet.Cells[rowNumber, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 5].Value = item.SerialNumber;
                worksheet.Cells[rowNumber, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 6].Value = (!item.CandidateInventories.Any() || item.CandidateInventories.Any(x => x.OperationType == Models.Common.OperationType.ReturnToCompany)) ? false : true;
                worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: inventories.Count() + 2, toColumn: 6), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;

            // AutoFitColumns
            worksheet.Cells[1, 1, inventories.Count() + 2, 6].AutoFitColumns();

            return package;
        }

    }
}
