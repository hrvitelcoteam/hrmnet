﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using HRMNet.Models.Learning;
using System.Text;
using Microsoft.Extensions.Localization;

namespace HRMNet.Controllers
{
    [DisplayName("Employee Courses")]
    [Authorize]
    public class CandidateCoursesController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<CandidateCourse> _context;
        private readonly DbSet<Candidate> _candidate;
        private readonly DbSet<Course> _course;
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IStringLocalizer _localizer;

        public CandidateCoursesController(IUnitOfWork uow, IStringLocalizerFactory stringLocalizerFactory)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<CandidateCourse>();
            _candidate = uow.Set<Candidate>();
            _course = uow.Set<Course>();

            _localizer = stringLocalizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
        }

        // GET: CandidateCourses
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public IActionResult Index()
        {
            var courses = _context
                .Include(c => c.Candidate)
                    .ThenInclude(x => x.StuffDetail)
                .Include(c => c.Course)
                .AsNoTracking()
                .AsEnumerable()
                .GroupBy(c => c.CandidateId)
                .Select(x => new CandidateCourse()
                {
                    EmployeeId = x.Max(z => z.Candidate?.StuffDetail?.BirthCertificateNumber),
                    CandidateId = x.Max(z => z.CandidateId),
                    CandidateDisplayName = x.Max(z => z.Candidate.Name + " " + z.Candidate.Family),
                    TotalExternal = x.Sum(z => z.CourseId.HasValue ? 0 : 1),
                    TotalInternal = x.Sum(z => z.CourseId.HasValue ? 1 : 0),
                    Total = x.Sum(z => 1),
                }).ToList();

            return View(courses);
        }

        // GET: CandidateCourses/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidateCourse = await _context
                .Include(c => c.Candidate)
                .Include(c => c.Course)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidateCourse == null)
            {
                return NotFound();
            }

            return View(candidateCourse);
        }

        // GET: CandidateCourses/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            ViewData["CandidateId"] = _candidate
                .Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff)
                .AsNoTracking();

            ViewData["CourseId"] = _course
                .AsNoTracking();
            return View();
        }

        // POST: CandidateCourses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CandidateId,CourseId,Professor,Name,StartDate,EndDate,Description")] CandidateCourse candidateCourse)
        {
            if (ModelState.IsValid)
            {
                _context.Add(candidateCourse);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidateId"] = _candidate
                .Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff)
                .AsNoTracking();

            ViewData["CourseId"] = _course
                .AsNoTracking();

            return View(candidateCourse);
        }

        // GET: CandidateCourses/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidateCourse = await _context
                .Include(x => x.Candidate)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (candidateCourse == null)
            {
                return NotFound();
            }
            ViewData["CandidateId"] = _candidate
                .Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff)
                .AsNoTracking();

            ViewData["CourseId"] = _course
                .AsNoTracking();

            return View(candidateCourse);
        }

        // POST: CandidateCourses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CandidateId,CourseId,Professor,Name,StartDate,EndDate,Description")] CandidateCourse candidateCourse)
        {
            if (id != candidateCourse.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(candidateCourse);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CandidateCourseExists(candidateCourse.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidateId"] = new SelectList(_candidate, "Id", "DisplayName", candidateCourse.CandidateId);
            ViewData["CourseId"] = new SelectList(_course, "Id", "Id", candidateCourse.CourseId);
            return View(candidateCourse);
        }

        // GET: CandidateCourses/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidateCourse = await _context
                .Include(c => c.Candidate)
                .Include(c => c.Course)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidateCourse == null)
            {
                return NotFound();
            }

            return View(candidateCourse);
        }

        // POST: CandidateCourses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var candidateCourse = await _context.FindAsync(id);
            _context.Remove(candidateCourse);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> ExportCandidatesCourses()
        {
            var courses = await _context
                .Include(c => c.Candidate)
                .Include(c => c.Course)
                .AsNoTracking()
                .ToListAsync();

            return File(ExportCandidatescoursesAsExcel(courses), XlsxContentType, "candidatescourses.xlsx");
        }

        private byte[] ExportCandidatescoursesAsExcel(List<CandidateCourse> candidatescourses)
        {
            byte[] reportBytes;
            using (var package = createCandidatesExcelPackage(candidatescourses))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }

        private ExcelPackage createCandidatesExcelPackage(List<CandidateCourse> candidatescourses)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Candidates Courses Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Candidates Courses Report";
            package.Workbook.Properties.Keywords = "Candidates Courses";

            var worksheet = package.Workbook.Worksheets.Add("CandidatesCourses");
            //First add the headers
            worksheet.Cells[1, 1].Value = "Name Surename";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[1, 2].Value = "Course";
            worksheet.Cells[1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 3].Value = "Trainer";
            worksheet.Cells[1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[1, 4].Value = "Is Internal";
            worksheet.Cells[1, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[1, 5].Value = "Start Date";
            worksheet.Cells[1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 6].Value = "End Date";
            worksheet.Cells[1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 7].Value = "Duration (Days)";
            worksheet.Cells[1, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells[1, 8].Value = "Description";
            worksheet.Cells[1, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

            //Add values
            int rowNumber = 1;
            foreach (var item in candidatescourses)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Candidate.Name + " " + item.Candidate.Family;
                worksheet.Cells[rowNumber, 2].Value = item.Course?.Name ?? item.Name;
                worksheet.Cells[rowNumber, 3].Value = item.Professor;

                worksheet.Cells[rowNumber, 4].Value = item.CourseId > 0 ? "True" : "False";

                worksheet.Cells[rowNumber, 5].Value = item.Course?.StartDate.ToString("yyyy/MM/dd") ?? item.StartDate.Value.ToString("yyyy/MM/dd");
                worksheet.Cells[rowNumber, 6].Value = item.Course?.EndDate.ToString("yyyy/MM/dd") ?? item.EndDate.Value.ToString("yyyy/MM/dd");
                worksheet.Cells[rowNumber, 7].Value = item.Course?.Duration ?? ((item.EndDate.Value - item.StartDate.Value).Days);

                worksheet.Cells[rowNumber, 8].Value = item.Description;
            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: candidatescourses.Count() + 2, toColumn: 8), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;

            // AutoFitColumns
            worksheet.Cells[1, 1, candidatescourses.Count() + 2, 8].AutoFitColumns();


            return package;
        }

        [HttpPost]
        public async Task<IActionResult> GetCandidateCourses(int? id)
        {

            if (id == null)
                return Json("");

            var candidateCourses = await _context
                .Include(x=> x.Course)
                .Where(x => x.CandidateId == id)
                .AsNoTracking()
                .ToListAsync();

            var sb = new StringBuilder();
            sb.Append("<table cellpadding=\"5\" cellspacing=\"0\" class=\"table table-bordered table-striped\" style=\"padding-left:50px;\" >");
            sb.Append("<thead><tr>");
            sb.Append("<td>"+_localizer["Course Name"]+"</td>");
            sb.Append("<td>" + _localizer["Is Internal"] + "</td>");
            sb.Append("<td>" + _localizer["Start"] + "</td>");
            sb.Append("<td>" + _localizer["End"] + "</td>");
            sb.Append("<td>" + _localizer["Duration"] + "</td>");
            sb.Append("<td></td>");
            sb.Append("</tr></thead>");
            sb.Append("<tbody>");

            foreach (var item in candidateCourses)
            {
                sb.Append("<tr>");
                if (item.CourseId > 0)
                {
                    sb.Append("<td>" + item.Course.Name + "</td>");
                    sb.Append("<td><input type='checkbox' checked='true' disabled='disabled' />" + "</td>");
                    sb.Append("<td>" + item.Course.StartDate.ToString("yyyy/MM/dd") + "</td>");
                    sb.Append("<td>" + item.Course.EndDate.ToString("yyyy/MM/dd") + "</td>");
                }
                else
                {
                    sb.Append("<td>" + item.Name + "</td>");
                    sb.Append("<td><input type='checkbox' disabled='disabled' />" + "</td>");
                    sb.Append("<td>" + item.StartDate.Value.ToString("yyyy/MM/dd") + "</td>");
                    sb.Append("<td>" + item.EndDate.Value.ToString("yyyy/MM/dd") + "</td>");
                }

                sb.Append("<td>" + item.Course?.Duration + "</td>");
                sb.Append("<td><a href=\"/CandidateCourses/Edit/" + item.Id + "\">" + _localizer["Edit"] + "</a> |" +
                                            //"<a href=\"/Leaves/Detail/" + item.Id + "\"> Details </a> |" +
                                            "<a href=\"/CandidateCourses/Delete/" + item.Id + "\">" + _localizer["Delete"] + "</a ></td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            return Json(sb.ToString());
        }

        private bool CandidateCourseExists(int id)
        {
            return _context.Any(e => e.Id == id);
        }
    }
}
