﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HRMNet.Models;
using HRMNet.Data;
using Microsoft.EntityFrameworkCore;
using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Microsoft.AspNetCore.Diagnostics;
using HRMNet.Services;
using Microsoft.AspNetCore.SignalR;
using HRMNet.Hubs;
using Microsoft.Extensions.Localization;
using HRMNet.Models.Workflow;

namespace HRMNet.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        //private readonly HRMNetContext _context;
        private readonly IEmailSender _emailSender;
        private readonly IHubContext<MessageHub> _messageHubContext;
        private readonly IStringLocalizer _localizer;
        private readonly IUnitOfWork _uow;
        private readonly DbSet<WorkflowStateDateTime> _workflowState;

        public HomeController(ILogger<HomeController> logger
            //,HRMNetContext context
            , IUnitOfWork uow
            , IEmailSender emailSender
            , IHubContext<MessageHub> messageHubContext
            , IStringLocalizerFactory stringLocalizerFactory
            )
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
            _messageHubContext = messageHubContext ?? throw new ArgumentNullException(nameof(messageHubContext));
            _localizer = stringLocalizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
            //_context = context;

            _workflowState = uow.Set<WorkflowStateDateTime>();
        }

        public IActionResult SetTrLanguage(string returnUrl = null)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(new CultureInfo("tr-TR"))),
                new Microsoft.AspNetCore.Http.CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index");
        }

        public IActionResult SetEnLanguage(string returnUrl = null)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(new CultureInfo("en-US"))),
                new Microsoft.AspNetCore.Http.CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            //_workflowState.Add(new WorkflowStateDateTime()
            //{
            //    CandidateId = 1,
            //    Status = CandidateStatus.AcceptedOffer,
            //    CreatedDateTime = DateTime.Now.AddDays(2)
            //}) ;

            //_uow.SaveChanges();
            //throw new Exception("test");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        ////[HttpPost]
        //public async Task<IActionResult> DeletePosition(int id, int candidateId) //int candidateId
        //{
        //    var candidatePosition= await _context.CandidatePositions.FirstOrDefaultAsync(x=> x.PositionId==id && x.CandidateId==candidateId);
        //    _context.CandidatePositions.Remove(candidatePosition);
        //    await _context.SaveChangesAsync();
        //    return Json("Ok");//,new { id = candidateId }
        //}

        //[HttpPost]
        //public async Task<IActionResult> DeleteFeature(int id) //int candidateId
        //{
        //    var candidateFeature = await _context.CandidateFeatures.FindAsync(id);
        //    _context.CandidateFeatures.Remove(candidateFeature);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Models.Candidates));//,new { id = candidateId }
        //}

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            //Exception exceptionMessage = new Exception();
            //if (User.IsInRole("Admin"))
            //{
            //    exceptionMessage =
            //        HttpContext.Features.Get<IExceptionHandlerPathFeature>().Error;

            //    return View(exceptionMessage.Message);
            //}

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}
