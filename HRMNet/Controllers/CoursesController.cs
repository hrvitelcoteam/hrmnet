﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Learning;
using HRMNet.Models.Common;
using HRMNet.Models.Candidates;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Courses")]
    public class CoursesController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Course> _context;
        private readonly DbSet<Candidate> _candidate;
        private readonly DbSet<CandidateCourse> _candidateCourse;
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public CoursesController(IUnitOfWork uow)
        {
            _uow = uow?? throw new ArgumentNullException(nameof(_uow));
            _context= uow.Set<Course>();
            _candidate = uow.Set<Candidate>();
            _candidateCourse = uow.Set<CandidateCourse>();
        }

        // GET: Courses
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.AsNoTracking().ToListAsync());
        }

        // GET: Courses/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        // GET: Courses/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public async Task<IActionResult> Create()
        {
            var staffs = await PopulateStuffsDropDownList(new Course());
            ViewData["Staffs"] = staffs;
            
            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,StartDate,EndDate,Professor,Duration,Description,Id")] Course course, string[] selected)
        {
            if (ModelState.IsValid)
            {

                if (selected != null)
                {
                    course.CandidateCourses = new List<CandidateCourse>();
                    foreach (var item in selected)
                    {
                        var toAdd = new CandidateCourse { CourseId = course.Id, CandidateId = int.Parse(item) };
                        course.CandidateCourses.Add(toAdd);
                    }
                }

                _context.Add(course);

                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(course);
        }

        [HttpPost]
        public async Task<IActionResult> GetCourseById(int id)
        {
            if (id == 0)
                return NotFound();

            var course = await _context
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);

            if (course == null)
            {
                return NotFound();
            }

            return Json(new { Name=course.Name, StartDate= course.StartDate,EndDate= course.EndDate, Description=course.Description});
        }

        private async Task<List<AssignedStuffData>> PopulateStuffsDropDownList(Course course) // object selectedService = null
        {
            var stuffs = await _candidate
                .Include(x => x.CandidatePositions)
                .Where(x => x.Status == CandidateStatus.VitelcoStuff)
                .AsNoTracking()
                .ToListAsync();

            var model = new HashSet<int>(course.CandidateCourses.Select(c => c.CandidateId));

            var viewModel = new List<AssignedStuffData>();
            foreach (var item in stuffs)
            {
                viewModel.Add(new AssignedStuffData
                {
                    StuffId = item.Id,
                    Name = item.DisplayName,
                    Email = item.Email,
                    Status = item.Status,
                    Telephone = item.Telephone,
                    Assigned = model.Contains(item.Id)
                });
            }

            return viewModel;

            //ViewData["Stuffs"] = viewModel;
        }

        // GET: Courses/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context
                .Include(x => x.CandidateCourses)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
                
                //.FindAsync(id);
            if (course == null)
            {
                return NotFound();
            }

            var staffs=await PopulateStuffsDropDownList(course);
            ViewData["Staffs"] = staffs;

            return View(course);
        }

        private async Task UpdateSelectedCandidateCourse(string[] selected, Course course)
        {
            if (selected == null)
            {
                course.CandidateCourses = new List<CandidateCourse>();
                return;
            }

            var selectedHS = new HashSet<string>(selected);
            var courseStuffs = new HashSet<int>
                (course.CandidateCourses.Select(c => c.CandidateId));


            var stuffs = await _candidate
                 .Include(x => x.CandidatePositions)
                 .Where(x => x.Status == CandidateStatus.VitelcoStuff)
                 .AsNoTracking()
                 .ToListAsync();

            foreach (var item in stuffs)
            {
                if (selectedHS.Contains(item.Id.ToString()))
                {
                    if (!courseStuffs.Contains(item.Id))
                    {
                        _candidateCourse.Add(new CandidateCourse { CourseId = course.Id, CandidateId = item.Id });
                    }
                }
                else
                {
                    if (courseStuffs.Contains(item.Id))
                    {
                        CandidateCourse toRemove = course.CandidateCourses.FirstOrDefault(i => i.CandidateId == item.Id);

                        _candidateCourse.Remove(toRemove);
                    }
                }
            }
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,StartDate,EndDate,Professor,Duration,Description,Id")] Course course, string[] selected)
        {
            if (id != course.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var currentCourse = await _context
                        .Include(z => z.CandidateCourses)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(a => a.Id == course.Id);

                    currentCourse.Name = course.Name;
                    currentCourse.StartDate = course.StartDate;
                    currentCourse.EndDate = course.EndDate;
                    currentCourse.Duration = course.Duration;
                    currentCourse.Description = course.Description;


                    await UpdateSelectedCandidateCourse(selected, currentCourse);
                    //await _providerService.AddRangeAsync(provider.ProviderServices);
                    
                    _context.Update(currentCourse);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CourseExists(course.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            await PopulateStuffsDropDownList(course);
            return View(course);
        }

        // GET: Courses/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var course = await _context.FindAsync(id);
            _context.Remove(course);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> Export()
        {
            var courses = await _context.AsNoTracking().ToListAsync();

            return File(ExportAsExcel(courses), XlsxContentType, "courses.xlsx");
        }

        private byte[] ExportAsExcel(List<Course> courses)
        {
            byte[] reportBytes;
            using (var package = createExcelPackage(courses))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }

        private ExcelPackage createExcelPackage(List<Course> courses)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Courses Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Courses Report";
            package.Workbook.Properties.Keywords = "Courses";

            var worksheet = package.Workbook.Worksheets.Add("Courses");
            //First add the headers

            worksheet.Cells[1, 1].Value = "Name";
            worksheet.Cells[1, 2].Value = "Start Date";
            worksheet.Cells[1, 3].Value = "End Date";
            worksheet.Cells[1, 4].Value = "Duration";
            worksheet.Cells[1, 5].Value = "Trainer";
            worksheet.Cells[1, 6].Value = "Description";


            //Add values

            int rowNumber = 1;
            foreach (var item in courses)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Name;
                worksheet.Cells[rowNumber, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 2].Value = item.StartDate.ToString("yyyy/MM/dd");
                worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[rowNumber, 3].Value = item.EndDate.ToString("yyyy/MM/dd"); 
                worksheet.Cells[rowNumber, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[rowNumber, 4].Value = item.Duration;
                worksheet.Cells[rowNumber, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 5].Value = item.Professor;
                worksheet.Cells[rowNumber, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 6].Value = item.Description;
                worksheet.Cells[rowNumber, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                
            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: courses.Count() + 2, toColumn: 6), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;

            // AutoFitColumns
            worksheet.Cells[1, 1, courses.Count() + 2, 6].AutoFitColumns();

            return package;
        }

        private bool CourseExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
