﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Timesheet;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel;
using HRMNet.Services.Identity;
using HRMNet.Services;
using HRMNet.Models.Common;
using DNTCommon.Web.Core;
using HRMNet.Models.Positions;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Timesheet")]
    public class TimesheetsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<TimesheetItem> _context;
        private readonly DbSet<Position> _position;
        public TimesheetsController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<TimesheetItem>();
            _position = uow.Set<Position>();
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public IActionResult Index()
        {

            return View();

        }

        // GET: Timesheets
        public async Task<IActionResult> Items()
        {
            var query = _context.AsNoTracking();

            if (!User.IsInRole("Admin") && !User.IsInRole("Account Manager") && !User.IsInRole("Sales Operation") && !User.IsInRole("HR Manager") && !User.IsInRole("HR Specialist"))
                query = query.Where(x => x.CreatorId == CurrentUserId.Value);

            var items = await query
                    .Select(x => new TimesheetViewModel()
                    {
                        id = x.Id,
                        allDay = x.AllDay,
                        //backgroundColor = x.BackgroundColor,
                        //borderColor = x.BorderColor,
                        //className = x.ClassName,
                        color = ReturnColor(x.Color),
                        editable = true,
                        start = x.AllDay == true ? x.Start.Value.ToString("yyyy-MM-dd") : x.Start.Value.ToString("yyyy-MM-ddTHH:mm:ss"),
                        end = x.End != null ? (x.AllDay == true ? x.End.Value.ToString("yyyy-MM-dd") : x.End.Value.ToString("yyyy-MM-ddTHH:mm:ss")) : null,
                        extendedProps = "",
                        //groupId = x.GroupId.ToString(),
                        overlap = false,
                        //textColor = x.TextColor,
                        title = string.IsNullOrWhiteSpace(x.Title) ? x.Description : x.Title + (x.AllDay ? " - 1 day" : (x.End.Value - x.Start.Value).Hours + " - hour") ,
                        url = x.Url
                    }).ToListAsync();

            return Json(items);
        }

        [AjaxOnly]
        public async Task<IActionResult> RenderAddItem([FromBody] AddItemViewModel model)
        {
            var positions = _position
                .Include(x => x.Customer)
                .AsNoTracking();


            if (User.IsInRole("Developer"))
            {
                ViewData["PositionId"] = new SelectList(await positions.Select(x => new { Id = x.Id, Name = x.ProjectName }).ToListAsync(), "Id", "Name");
            }
            else
            {
                ViewData["PositionId"] = new SelectList(await positions.Select(x => new { Id = x.Id, Name = x.PositionName + " (" + x.Customer.Name + ") " }).ToListAsync(), "Id", "Name");
            }


            return PartialView("Create", model: new TimesheetItem() { Start = DateTime.Parse(model.Start), End = DateTime.Parse(model.End), Title = model.Title, AllDay = model.AllDay });
        }

        [HttpPost]
        [AjaxOnly]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddItem(TimesheetItem model)
        {
            if (ModelState.IsValid)
            {

                if (model.PositionId == 0)
                    model.PositionId = null;

                //if (model.Title == null || model.Title==string.Empty)
                //    model.Title = model.Description;

                try
                {

                    if ((model.End.Value - model.Start.Value).TotalDays >= 1)
                    {
                        model.Type = TimesheetItemType.AllDay;
                        model.AllDay = true;
                    }
                    else
                    {
                        model.Type = TimesheetItemType.Small;
                        model.AllDay = false;
                    }

                    _context.Add(model);
                    var result = await _uow.SaveChangesAsync();

                    if (result > 0)
                        return Json(new { success = true, id = result, color = ReturnColor(model.Color), title = string.IsNullOrWhiteSpace(model.Title)? model.Description: model.Title });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }

            var positions = await _position
                .Include(x => x.Customer)
                .AsNoTracking()
                .Select(x => new { Id = x.Id, Name = x.PositionName + " (" + x.Customer.Name + ") " })
                .ToListAsync();

            ViewData["PositionId"] = new SelectList(positions, "Id", "Name");
            return PartialView("Create", model: model);
        }

        [AjaxOnly]
        public async Task<IActionResult> RenderUpdateItem([FromBody] AddItemViewModel model)
        {
            var item = await _context
                .FirstOrDefaultAsync(x => x.Id == int.Parse(model.Id));

            if (!string.IsNullOrWhiteSpace(model.End)) // resize mode
            {
                item.End = DateTime.Parse(model.End);
            }

            if (!string.IsNullOrWhiteSpace(model.Start)) // resize mode
            {
                item.Start = DateTime.Parse(model.Start);
            }

            var positions = _position
                .Include(x => x.Customer)
                .AsNoTracking();
            //.Select(x => new { Id = x.Id, Name = x.PositionName + " (" + x.Customer.Name + ") " }).ToListAsync();

            if (User.IsInRole("Developer"))
            {
                ViewData["PositionId"] = new SelectList(await positions.Select(x => new { Id = x.Id, Name = x.ProjectName }).ToListAsync(), "Id", "Name");
            }
            else
            {
                ViewData["PositionId"] = new SelectList(await positions.Select(x => new { Id = x.Id, Name = x.PositionName + " (" + x.Customer.Name + ") " }).ToListAsync(), "Id", "Name");
            }

            //ViewData["PositionId"] = new SelectList(positions, "Id", "Name");

            return PartialView("Edit", model: item);
        }

        [HttpPost]
        [AjaxOnly]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateItem(TimesheetItem model, bool isDelete = false)
        {
            if (ModelState.IsValid)
            {

                if (model.PositionId == 0)
                    model.PositionId = null;

                if (isDelete) // delete mode
                {
                    if (model.Id == 0)
                        return null;

                    var item = await _context.FindAsync(model.Id);
                    _context.Remove(item);
                    var deleteResult = await _uow.SaveChangesAsync();

                    if (deleteResult > 0)
                        return Json(new { success = true, isDelete = true, id = model.Id });
                }

                if ((model.End.Value - model.Start.Value).TotalDays >= 1)
                {
                    model.Type = TimesheetItemType.AllDay;
                    model.AllDay = true;
                }
                else
                {
                    model.Type = TimesheetItemType.Small;
                    model.AllDay = false;
                }



                _context.Update(model);
                var updateResult = await _uow.SaveChangesAsync();

                if (updateResult > 0)
                {
                    return Json(new
                    {
                        success = true,
                        isDelete = false,
                        id = model.Id,
                        title =string.IsNullOrWhiteSpace(model.Title)? model.Description: model.Title,
                        color = ReturnColor(model.Color),
                        allDay = model.AllDay,
                        start = model.AllDay == true ? model.Start.Value.ToString("yyyy-MM-dd") : model.Start.Value.ToString("yyyy-MM-ddTHH:mm"),
                        end = model.End != null ? (model.AllDay == true ? model.End.Value.ToString("yyyy-MM-dd") : model.End.Value.ToString("yyyy-MM-ddTHH:mm")) : null
                    });
                }
            }

            var positions = await _position
                    .Include(x => x.Customer)
                    .AsNoTracking().Select(x => new { Id = x.Id, Name = x.PositionName + " (" + x.Customer.Name + ") " }).ToListAsync();

            ViewData["PositionId"] = new SelectList(positions, "Id", "Name");
            return PartialView("Edit", model: model);
        }

        [AjaxOnly]
        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> DeleteItem(TimesheetItem model)
        {
            if (model.Id == 0)
                return null;

            var item = await _context.FindAsync(model.Id);
            _context.Remove(item);
            var deleteResult = await _uow.SaveChangesAsync();

            if (deleteResult > 0)
                return Json(new { success = true });
            else
                return Json(new { success = false });
        }


        // GET: Timesheets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var timesheetViewModel = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (timesheetViewModel == null)
            {
                return NotFound();
            }

            return View(timesheetViewModel);
        }

        // GET: Timesheets/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Timesheets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,title,groupId,allDay,start,end,startTime,endTime,startRecur,endRecur,url,className,editable,overlap,color,backgroundColor,borderColor,textColor,extendedProps")] TimesheetViewModel timesheetViewModel)
        {
            if (ModelState.IsValid)
            {
                //_context.Add(timesheetViewModel);
                //await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(timesheetViewModel);
        }

        // GET: Timesheets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var timesheetViewModel = await _context.FindAsync(id);
            if (timesheetViewModel == null)
            {
                return NotFound();
            }
            return View(timesheetViewModel);
        }

        // POST: Timesheets/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,title,groupId,allDay,start,end,startTime,endTime,startRecur,endRecur,url,className,editable,overlap,color,backgroundColor,borderColor,textColor,extendedProps")] TimesheetViewModel timesheetViewModel)
        {
            if (id != timesheetViewModel.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                try
                {
                    //_context.Update(timesheetViewModel);
                    //await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TimesheetViewModelExists(timesheetViewModel.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(timesheetViewModel);
        }

        // GET: Timesheets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var timesheetViewModel = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (timesheetViewModel == null)
            {
                return NotFound();
            }

            return View(timesheetViewModel);
        }

        // POST: Timesheets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var timesheetViewModel = await _context.FindAsync(id);
            //_context.TimesheetViewModel.Remove(timesheetViewModel);
            //await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TimesheetViewModelExists(int id)
        {
            return _context.Any(e => e.Id == id);
        }

        private int? CurrentUserId
        {
            get
            {
                var userId = User.Identity.GetUserId<int>();
                return userId;
            }
        }

        private static string ReturnColor(TimesheetItemColor color)
        {
            switch (color)
            {
                case TimesheetItemColor.Default:
                    return "";
                case TimesheetItemColor.Yellow:
                    return "#FFFF00";
                case TimesheetItemColor.Orange:
                    return "#FFA800";
                case TimesheetItemColor.Brown:
                    return "#844004";
                case TimesheetItemColor.Green:
                    return "#07D131";
                case TimesheetItemColor.Red:
                    return "#FF0000";
                default:
                    return "";
            }
        }
    }
}
