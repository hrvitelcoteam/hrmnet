﻿using DNTCommon.Web.Core;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using HRMNet.Models.Customers;
using HRMNet.Models.Timesheet;
using HRMNet.Models.ViewModel.Report;
using HRMNet.Services;
using HRMNet.Services.Contracts.Identity;
using HRMNet.Services.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using HRMNet.Models.Identity;
using OfficeOpenXml.ConditionalFormatting;
using HRMNet.Models.Workflow;
using HRMNet.Models.Positions;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Reports")]
    public class ReportsController : Controller
    {
        private readonly DbSet<PreCandidate> _preCandidate;
        private readonly DbSet<Candidate> _candidate;
        private readonly DbSet<Meeting> _candidateMetting;
        private readonly DbSet<CandidatePosition> _candidatePosition;
        private readonly DbSet<Position> _position;
        private readonly DbSet<PreCandidateMettings> _meeting;
        private readonly DbSet<Customer> _customer;
        private readonly IUnitOfWork _uow;
        private readonly DbSet<TimesheetItem> _timesheet;
        private readonly IApplicationUserManager _userManager;
        private readonly DbSet<Title> _title;
        private readonly DbSet<WorkflowStateDateTime> _workflowState;
        private readonly DbSet<User> _user;
        private IStringLocalizer _localizer;
        public ReportsController(IUnitOfWork uow, IApplicationUserManager userManager, IStringLocalizerFactory localizerFactory)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));

            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));

            _localizer = localizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");

            _preCandidate = uow.Set<PreCandidate>();
            _meeting = uow.Set<PreCandidateMettings>();
            _customer = uow.Set<Customer>();
            _timesheet = uow.Set<TimesheetItem>();
            _candidate = uow.Set<Candidate>();
            _candidatePosition = uow.Set<CandidatePosition>();
            _title = uow.Set<Title>();
            _user = uow.Set<User>();
            _candidateMetting = uow.Set<Meeting>();
            _workflowState = uow.Set<WorkflowStateDateTime>();
            _position = uow.Set<Position>();
        }
        public IActionResult Index()
        {
            return View();
        }

        private string SerializeMultipleDatasets(IEnumerable<ReportViewModel> models)
        {
            var labels = models.ToList().OrderBy(x => x.Id).Select(x => x.Category).Distinct().ToList();

            var names = models.ToList().OrderBy(x => x.Id).Select(x => x.Key).Distinct().ToList();

            var serializedString = string.Empty;

            var randomColors = StringToolkit.GetRandomColorList(names.Count());

            //serializedString += "datasets: [{data:[";
            serializedString += "{datasets: [";

            for (int j = 0; j < names.Count(); j++)
            {
                serializedString += "{ label: \"" + names[j] + "\",";
                serializedString += "data:[";
                var dataItems = models.Where(x => x.Key == names[j]).OrderBy(x => x.order);

                for (int i = 0; i < labels.Count(); i++)
                {
                    if (dataItems.FirstOrDefault(x => x.Category == labels[i]) != null)
                    {
                        serializedString +=
                            ConvertDoubleToString(dataItems.FirstOrDefault(x => x.Category == labels[i]).Count) + ",";
                    }
                    else
                        serializedString += "0,";
                }

                foreach (var dataItem in dataItems)
                {
                    serializedString += ConvertDoubleToString(dataItem.Count) + ",";
                }

                serializedString += "], backgroundColor:\"" + randomColors[j] + "\"},";
            }

            serializedString += "],";

            serializedString += "labels:[";
            foreach (var item in labels)
            {
                serializedString += "\"" + item + "\",";
            };
            serializedString += "]}";

            //foreach (var item in models)
            //{
            //    serializedString += item.Count + ","; //item.count
            //}
            //serializedString += "],";

            //serializedString += "datalabels: {anchor: 'start'},";

            //serializedString += "backgroundColor:[";

            //foreach (var item in models)
            //{
            //    serializedString += "\"" + StringToolkit.RandomColor() + "\",";
            //};

            //serializedString += "]}]}";

            return serializedString;
        }

        private static string ConvertDoubleToString(double number)
        {
            return String.Format("{0:0.####}", number).Replace(",", ".");
        }

        private static string Serialize(IEnumerable<ReportViewModel> models)
        {
            var randomColors = StringToolkit.GetRandomColorList(models.Count());

            var serializedString = string.Empty;
            serializedString += "{labels:[";
            foreach (var item in models)
            {
                serializedString += "\"" + item.Key + "\",";
            };
            serializedString += "],";

            serializedString += "datasets: [{data:[";

            foreach (var item in models)
            {
                serializedString += ConvertDoubleToString(item.Count) + ","; //item.count
            }
            serializedString += "],";

            serializedString += "datalabels: {anchor: 'start'},";

            serializedString += "backgroundColor:[";

            for (int i = 0; i < models.Count(); i++)
            {
                serializedString += "\"" + randomColors[i] + "\",";
            }


            serializedString += "]}]}";

            return serializedString;
        }

        private IQueryable<WorkflowStateDateTime> WorkflowStateDateTimes(DateTime? Start, DateTime? End, int? positionId, int? customerId, int? userId)
        {
            var query = _workflowState
                .Include(x => x.Candidate)
                    .ThenInclude(x => x.CandidatePositions)
                        .ThenInclude(x => x.Position)
                .AsNoTracking();

            if (Start.HasValue && End.HasValue)
            {
                query = query
                    .Where(x => x.CreatedDateTime.Date >= Start.Value && x.CreatedDateTime.Date < End.Value);
            }

            if (positionId != null && positionId > 0)
            {
                query = query
                    .Where(x => x.Candidate.CandidatePositions.Any(z => z.PositionId == positionId));
            }

            if (customerId != null && customerId > 0)
            {
                query = query
                    .Where(x => x.Candidate.CandidatePositions.Any(z => z.Position.CustomerId == customerId));
            }

            if (userId > 0)
            {
                query = query
                    .Where(x => x.CreatorId == userId);
            }

            return query;
        }

        private IQueryable<CandidatePosition> CandidatesPositions(DateTime? Start, DateTime? End, int? positionId, int? customerId, int? userId)
        {
            var query = _candidatePosition
                    .Include(x => x.Position)
                        //.Include(x => x.Position.TimesheetItems)
                        .ThenInclude(x => x.Customer)
                    .Include(x => x.Candidate)
                        .ThenInclude(x => x.WorkflowStateDateTimes)
                    .AsNoTracking();

            if (Start.HasValue && End.HasValue)
            {
                query = query
                    .Where(x => x.Candidate.WorkflowStateDateTimes.Any(y => y.CreatedDateTime >= Start.Value && y.CreatedDateTime < End.Value));

                //var tttt = query.ToQueryString();
            }

            if (positionId != null && positionId > 0)
            {
                query = query
                       .Where(x => x.PositionId == positionId);
            }

            if (customerId != null && customerId > 0)
            {
                query = query
                       .Where(x => x.Position.CustomerId == customerId);
            }

            if (userId > 0)
            {
                query = query
                    .Where(x => x.Candidate.WorkflowStateDateTimes.Any(y => y.CreatorId == userId));
            }

            return query;
        }

        private IEnumerable<Candidate> Candidates(DateTime? Start, DateTime? End, int? positionId, int? customerId, int? userId)
        {
            var query = _candidate
                    .Include(x => x.Meetings)
                    .Include(x => x.CandidatePositions)
                        .ThenInclude(x => x.Position)
                            .ThenInclude(x => x.Customer)
                    .AsNoTracking();

            if (Start.HasValue && End.HasValue)
            {
                query = query
                    .Where(x => x.WorkflowStateDateTimes.Any(z => z.CreatedDateTime >= Start.Value) && x.WorkflowStateDateTimes.Any(z => z.CreatedDateTime < End.Value));
            }

            if (positionId != null && positionId > 0)
            {
                query = query
                       .Where(x => x.CandidatePositions.Any(z => z.PositionId == positionId));
            }

            if (customerId != null && customerId > 0)
            {
                query = query
                       .Where(x => x.CandidatePositions.Select(z => z.Position.Customer).Any(y => y.Id == customerId));
            }

            if (userId > 0)
            {
                query = query
                    .Where(x => x.WorkflowStateDateTimes.Any(y => y.CreatorId == userId));
            }


            return query;
        }

        private IQueryable<PreCandidate> PreCandidates(DateTime? Start, DateTime? End, int? positionId, int? customerId, int? userId)
        {
            var query = _preCandidate
                    .Include(x => x.PreCandidateMeetings)
                    .ThenInclude(x => x.Position)
                    .ThenInclude(x => x.Customer)
                    //.Include(x => x.PreCandidateMeetings)
                    //.ThenInclude(x => x.Position)
                    //.ThenInclude(x => x.TimesheetItems)
                    .AsNoTracking();

            if (Start.HasValue && End.HasValue)
            {
                query = query
                    .Where(x => x.PreCandidateMeetings.Any(z => z.CreatedDateTime >= Start.Value) && x.PreCandidateMeetings.Any(z => z.CreatedDateTime < End.Value));
            }

            if (positionId != null && positionId > 0)
            {
                query = query
                       .Where(x => x.PreCandidateMeetings.Any(x => x.PositionId == positionId));
            }

            if (customerId != null && customerId > 0)
            {
                query = query
                       .Where(x => x.PreCandidateMeetings.Select(x => x.Position.Customer).Any(x => x.Id == customerId));
            }

            if (userId > 0)
            {
                query = query
                    .Where(x => x.CreatorId == userId);
            }

            return query;
        }

        private IQueryable<PreCandidateMettings> PreCandidatesMeetings(DateTime? Start, DateTime? End, int? PositionId, int? customerId, int? userId)
        {
            var query = _meeting
                .Include(x => x.PreCandidate)
                .Include(x => x.Position)
                    .ThenInclude(x => x.Customer)
                    //.Include(x => x.Position)
                    //.ThenInclude(x => x.TimesheetItems)

                    .AsNoTracking();

            if (Start.HasValue && End.HasValue)
            {
                query = query
                    .Where(x => x.CreatedDateTime >= Start.Value && x.CreatedDateTime < End.Value);
            }

            if (PositionId != null && PositionId > 0)
            {
                query = query
                       .Where(x => x.PositionId == PositionId);
            }

            if (customerId != null && customerId > 0)
            {
                query = query
                    .Where(x => x.Position.CustomerId == customerId);
            }

            if (userId > 0)
            {
                query = query
                    .Where(x => x.CreatorId == userId);
            }

            return query;
        }

        private IQueryable<TimesheetItem> Timesheets(DateTime? Start, DateTime? End, int? PositionId, int? userId)
        {
            var query = _timesheet
                .Include(x => x.Position)
                    .ThenInclude(x => x.Customer)
                .AsNoTracking()
                .Where(x => x.PositionId > 0);
            //.Include(x => x.Position)
            //.ThenInclude(x => x.TimesheetItems)


            if (Start.HasValue && End.HasValue)
            {
                query = query
                    .Where(x => x.Start.Value >= Start.Value && x.End.Value < End.Value);
            }

            if (PositionId != null && PositionId > 0)
            {
                query = query
                       .Where(x => x.PositionId == PositionId);
            }

            if (userId > 0)
            {
                query = query
                    .Where(x => x.CreatorId == userId);
            }

            return query;
        }

        private ChartViewModel GenerateChartViewByCustomerPosition(ReportSearchViewModel search)
        {
            //var candidatePositions = CandidatesPositions(search.StartDate, search.EndDate, search.PositionId, null, search.UserId);
            var workFlowStates = WorkflowStateDateTimes(search.StartDate, search.EndDate, search.PositionId, null, search.UserId);

            //var meetings = PreCandidatesMeetings(search.StartDate, search.EndDate, search.PositionId, search.UserId);
            var timeSheets = Timesheets(search.StartDate, search.EndDate, search.PositionId, search.UserId);
            var timeSheetList = timeSheets.ToList();

            ChartViewModel returnModel = new ChartViewModel();
            if (!search.IsPosition)
            {
                // Müşteri ile Paylaşılan Aday Sayısı

                var querySharedWithCustomerCandidates =
                    workFlowStates.Where(x => x.Status == CandidateStatus.SharedWithCustomer);
                //var querySharedWithCustomerCandidates = candidatePositions.Where(x => x.Candidate
                //    .WorkflowStateDateTimes
                //    .Any(z => z.Status == Models.Common.CandidateStatus.SharedWithCustomer));

                var sharedWithCustomerCandidates = from wf in querySharedWithCustomerCandidates
                                                   from cp in wf.Candidate.CandidatePositions
                                                   group new { cp } by new
                                                   {
                                                       cp.Position.Customer.Id,
                                                       cp.Position.Customer.Name
                                                   } into g
                                                   select new ReportViewModel()
                                                   {
                                                       Id = g.Key.Id,
                                                       Key = g.Key.Name,
                                                       Count = g.Count() // 
                                                   };

                // var eeee = sharedWithCustomerCandidates.ToQueryString();

                // Kabul Edilen Aday Sayıları 
                var queryAcceptedOfferCandidates = workFlowStates
                    .Where(x => x.Status == CandidateStatus.AcceptedOffer);
                var acceptedOfferCandidates = from wf in queryAcceptedOfferCandidates
                                              from cp in wf.Candidate.CandidatePositions
                                              group new { cp } by new
                                              {
                                                  cp.Position.Customer.Id,
                                                  cp.Position.Customer.Name
                                              } into g
                                              select new ReportViewModel()
                                              {
                                                  Id = g.Key.Id,
                                                  Key = g.Key.Name,
                                                  Count = g.Count() //p => p.Candidate.Id != null
                                              };

                // Harcanan Süre (gün)

                var elapsedTimes = from t in timeSheetList
                                   group new { t } by new
                                   {
                                       t.Position.Customer.Id,
                                       t.Position.Customer.Name
                                   } into g
                                   select new ReportViewModel()
                                   {
                                       Id = g.Key.Id,
                                       Key = g.Key.Name,
                                       Count = (double)g.Sum(x => x.t.AllDay ? 1 : ((x.t.End.Value - x.t.Start.Value).TotalMinutes / 480))
                                   };

                // Verimlilik
                //var productivities = Math.Round(acceptedOfferCandidates.Count() / elapsedTimes, 1);
                List<ReportViewModel> productivities = new List<ReportViewModel>();

                foreach (var item in acceptedOfferCandidates)
                {
                    var elapsed = elapsedTimes.FirstOrDefault(x => x.Id == item.Id)?.Count;
                    if (elapsed > 0)
                    {
                        productivities.Add(new ReportViewModel()
                        {
                            Id = item.Id,
                            Key = item.Key,
                            Count = item.Count / elapsed.Value
                        });
                    }
                    else
                    {
                        productivities.Add(new ReportViewModel()
                        {
                            Id = item.Id,
                            Key = item.Key,
                            Count = 0
                        });
                    }

                }

                //var productivities1 = from t in timeSheetList
                //                      group new { t } by new
                //                      {
                //                          t.Position.Customer.Id,
                //                          t.Position.Customer.Name
                //                      }
                //    into g
                //                      select new ReportViewModel()
                //                      {
                //                          Id = g.Key.Id,
                //                          Key = g.Key.Name,
                //                          Count = g.GroupBy(x => x.t.CreatorId).Count() / ((double)g.Sum(x =>
                //                                  x.t.AllDay ? 1 : ((x.t.End.Value - x.t.Start.Value).TotalMinutes / 480)))
                //                      };


                returnModel = new ChartViewModel()
                {
                    Search = search,
                    SerializedChart1 = Serialize(sharedWithCustomerCandidates),
                    SerializedChart2 = Serialize(acceptedOfferCandidates),
                    SerializedChart3 = Serialize(elapsedTimes),
                    SerializedChart4 = Serialize(productivities)
                };
            }
            else
            {

                // Müşteri ile Paylaşılan Aday Sayısı
                var querySharedWithCustomerCandidates = workFlowStates
                    .Where(x => x.Status == Models.Common.CandidateStatus.SharedWithCustomer);
                var sharedWithCustomerCandidates = from wf in querySharedWithCustomerCandidates
                                                   from cp in wf.Candidate.CandidatePositions
                                                   group new { cp } by new //, cp.Candidate
                                                   {
                                                       cp.Position.Id,
                                                       cp.Position.PositionName
                                                   } into g
                                                   select new ReportViewModel()
                                                   {
                                                       Id = g.Key.Id,
                                                       Key = g.Key.PositionName,
                                                       Count = g.Count()
                                                   };

                // Kabul Edilen Aday Sayıları 
                var queryAcceptedOfferCandidates = workFlowStates.Where(x => x.Status == CandidateStatus.AcceptedOffer);
                var acceptedOfferCandidates = from wf in queryAcceptedOfferCandidates
                                              from cp in wf.Candidate.CandidatePositions
                                              group new { cp } by new
                                              {
                                                  cp.Position.Id,
                                                  cp.Position.PositionName
                                              } into g
                                              select new ReportViewModel()
                                              {
                                                  Id = g.Key.Id,
                                                  Key = g.Key.PositionName,
                                                  Count = g.Count()
                                              };

                // Harcanan Süre (gün)
                var elapsedTimes = from t in timeSheetList
                                   group new { t } by new
                                   {
                                       t.Position.Id,
                                       t.Position.PositionName
                                   } into g
                                   select new ReportViewModel()
                                   {
                                       Id = g.Key.Id,
                                       Key = g.Key.PositionName,
                                       Count = (double)g.Sum(x => x.t.AllDay ? 1 : (x.t.End.Value - x.t.Start.Value).TotalMinutes / 480)
                                   };

                //// Verimlilik
                //var productivities = from t in timeSheetList
                //                     group new { t } by new
                //                     {
                //                         t.Position.Id,
                //                         t.Position.PositionName
                //                     } into g
                //                     select new ReportViewModel()
                //                     {
                //                         Id = g.Key.Id,
                //                         Key = g.Key.PositionName,
                //                         Count = g.GroupBy(x => x.t.CreatorId).Count() / ((double)g.Sum(x => x.t.AllDay ? 1 : (x.t.End.Value - x.t.Start.Value).TotalMinutes / 480))
                //                     };
                List<ReportViewModel> productivities = new List<ReportViewModel>();

                foreach (var item in acceptedOfferCandidates)
                {
                    var elapsed = elapsedTimes.FirstOrDefault(x => x.Id == item.Id)?.Count;
                    if (elapsed > 0)
                    {
                        productivities.Add(new ReportViewModel()
                        {
                            Id = item.Id,
                            Key = item.Key,
                            Count = item.Count / elapsed.Value
                        });
                    }
                    else
                    {
                        productivities.Add(new ReportViewModel()
                        {
                            Id = item.Id,
                            Key = item.Key,
                            Count = 0
                        });
                    }

                }

                returnModel = new ChartViewModel()
                {
                    Search = search,
                    SerializedChart1 = Serialize(sharedWithCustomerCandidates),
                    SerializedChart2 = Serialize(acceptedOfferCandidates),
                    SerializedChart3 = Serialize(elapsedTimes),
                    SerializedChart4 = Serialize(productivities)
                };
            }

            return returnModel;
        }



        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("All Customers Positions")]
        public IActionResult AllCustomersPositions(ReportSearchViewModel search)
        {
            var splittedRange = DateToolkit.SpliteDateRange(search.DateRange);

            search.StartDate = splittedRange?.Item1;
            search.EndDate = splittedRange?.Item2;

            var model = GenerateChartViewByCustomerPosition(search);

            if (search.ChartType == "bar")
                return View("AllCustomersPositionsBar", model);
            else
                return View("AllCustomersPositionsPie", model);
        }

        private async Task<int> GetTargetAsACandidate()
        {
            var candidate = await _candidate
                .Include(x => x.StuffDetail)
                    .ThenInclude(x => x.Title)
                .FirstOrDefaultAsync(x => x.RelatedUserId == User.Identity.GetUserId<int>());
            if (candidate != null)
                return candidate.StuffDetail?.Title?.Target ?? 0;

            return 0;
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Customer Position Detail Analyze")]
        public async Task<IActionResult> CustomerPositionDetailAnalyze(ReportSearchViewModel search)
        {
            var splittedRange = DateToolkit.SpliteDateRange(search.DateRange);

            search.StartDate = splittedRange?.Item1;
            search.EndDate = splittedRange?.Item2;

            ViewData["Customers"] = await _customer
                .AsNoTracking()
                .Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                })
                .ToListAsync();

            if (search.PositionId > 0)
            {
                ViewData["Positions"] = await _position
                    .Where(x => x.CustomerId == search.CustomerId)
                    .AsNoTracking()
                    .Select(x => new SelectListItem()
                    {
                        Text = x.PositionName,
                        Value = x.Id.ToString()
                    })
                    .ToListAsync();
            }

            var timeSheets = Timesheets(search.StartDate, search.EndDate, search.PositionId, search.UserId);
            var preCandidates = PreCandidates(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);
            var workFlowStates = WorkflowStateDateTimes(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);

            var statuses = from wfs in workFlowStates
                           where wfs.Status == CandidateStatus.AcceptedOffer ||
                                 wfs.Status == CandidateStatus.OfferMade ||
                                 wfs.Status == CandidateStatus.CustomerWillInterview ||
                                 wfs.Status == CandidateStatus.SharedWithCustomer ||
                                 wfs.Status == CandidateStatus.SubmittedToManager
                           group new { wfs } by new
                           {
                               wfs.Status
                           } into g
                           orderby (int)g.Key.Status descending
                           select new ReportViewModel()
                           {
                               Key = _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(g.Key.Status)],
                               Count = g.Count(),
                           };

            // var ttttt = statuses.ToQueryString();

            var statusList = statuses.ToList();


            // Number of Phone Calls
            var preCandidatesCount = preCandidates
                        .Select(x => x.PreCandidateMeetings)?.Count();

            var totalMeetings = preCandidatesCount; //candidatesMeetings +

            // Recruitment Effort  (person/day)
            // Elapsed time
            var timeSheetList = await timeSheets.ToListAsync();
            var timeElapsed = timeSheetList.Sum(x => x.AllDay ? 1 : (x.End.Value - x.Start.Value).TotalMinutes / 480);

            // Recruiter count
            var recruiterCount = timeSheets.Select(x => x.CreatorId).Distinct().Count();
            var recruiterEffort = Math.Round(recruiterCount / timeElapsed, 4);

            List<ReportViewModel> lst = new List<ReportViewModel>();

            lst.AddRange(statusList);

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["First Interviewed"],
                Count = totalMeetings ?? 0
            });

            var chartDetail = new ChartDetailViewModel();
            chartDetail.CustomerChoosen = statusList.FirstOrDefault(x => x.Key == _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(CandidateStatus.CustomerWillInterview)]) != null ? Convert.ToInt32(statusList.FirstOrDefault(x => x.Key == _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(CandidateStatus.CustomerWillInterview)]).Count) : 0;
            chartDetail.CustomerOffered = statusList.FirstOrDefault(x => x.Key == _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(CandidateStatus.OfferMade)]) != null ? Convert.ToInt32(statusList.FirstOrDefault(x => x.Key == _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(CandidateStatus.OfferMade)]).Count) : 0;
            chartDetail.NumberOfPhoneCalls = totalMeetings ?? 0;
            chartDetail.RecruitmentEffort = recruiterEffort;
            chartDetail.SharedWithCustomer = statusList.FirstOrDefault(x => x.Key == _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(CandidateStatus.SharedWithCustomer)]) != null ? Convert.ToInt32(statusList.FirstOrDefault(x => x.Key == _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(CandidateStatus.SharedWithCustomer)]).Count) : 0;

            var returnModel = new ChartViewModel()
            {
                Search = search,
                SerializedChart1 = Serialize(lst),
                Detail = chartDetail
            };

            var acceptedOfferCandidatesCount = statusList.FirstOrDefault(x => x.Key == _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(CandidateStatus.AcceptedOffer)]) != null ? Convert.ToInt32(statusList.FirstOrDefault(x => x.Key == _localizer[EnumHelper<CandidateStatus>.GetDisplayValue(CandidateStatus.AcceptedOffer)]).Count) : 0;
            if (acceptedOfferCandidatesCount == 0)
            {
                returnModel.Message = _localizer["! No placement has been made"];
            }

            return View(returnModel);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Recruiter Analysis")]
        public async Task<IActionResult> RecruiterAnalysis(ReportSearchViewModel search)
        {
            var splittedRange = DateToolkit.SpliteDateRange(search.DateRange);

            search.StartDate = splittedRange?.Item1;
            search.EndDate = splittedRange?.Item2;
            //var candidatePositions = CandidatesPositions(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);
            var preCandidateMettings = PreCandidatesMeetings(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);
            var workFlowStates = WorkflowStateDateTimes(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);
            var timeSheets = Timesheets(search.StartDate, search.EndDate, search.PositionId, search.UserId);

            var users = await _user
                .AsNoTracking()
                .ToListAsync();

            var submittedToManagers = from wfs in workFlowStates
                                      where wfs.Status == CandidateStatus.SubmittedToManager
                                      group new { wfs } by new
                                      {
                                          wfs.CreatorId
                                      } into g
                                      select new ReportViewModel()
                                      {
                                          Id = g.Key.CreatorId,
                                          Count = g.Count(),
                                          Category = _localizer["Submitted To Management"],
                                          order = 2
                                      };

            var sharedWithCustomers = from wfs in workFlowStates
                                      where wfs.Status == CandidateStatus.SharedWithCustomer
                                      group new { wfs } by new
                                      {
                                          wfs.CreatorId
                                      } into g
                                      select new ReportViewModel()
                                      {
                                          Id = g.Key.CreatorId,
                                          Count = g.Count(),
                                          Category = _localizer["Shared With Customer"],
                                          order = 3
                                      };

            var customerWillInterviews = from wfs in workFlowStates
                                         where wfs.Status == CandidateStatus.CustomerWillInterview
                                         group new { wfs } by new
                                         {
                                             wfs.CreatorId
                                         } into g
                                         select new ReportViewModel()
                                         {
                                             Id = g.Key.CreatorId,
                                             Count = g.Count(),
                                             Category = _localizer["Customer Choosen"],
                                             order = 4
                                         };

            var offerMades = from wfs in workFlowStates
                             where wfs.Status == CandidateStatus.OfferMade
                             group new { wfs } by new
                             {
                                 wfs.CreatorId
                             } into g
                             select new ReportViewModel()
                             {
                                 Id = g.Key.CreatorId,
                                 Count = g.Count(),
                                 Category = _localizer["Proposed"],
                                 order = 5
                             };

            var acceptedOffers = from wfs in workFlowStates
                                 where wfs.Status == CandidateStatus.AcceptedOffer
                                 group new { wfs } by new
                                 {
                                     wfs.CreatorId
                                 } into g
                                 select new ReportViewModel()
                                 {
                                     Id = g.Key.CreatorId,
                                     Count = g.Count(),
                                     Category = _localizer["Accepted Offer"],
                                     order = 6
                                 };


            var firstInterviewedCandidates = from c in preCandidateMettings
                                             group new { c } by new
                                             {
                                                 c.CreatorId,
                                             } into g
                                             select new ReportViewModel()
                                             {
                                                 Id = g.Key.CreatorId,
                                                 Count = g.Count(),
                                                 Category = _localizer["First Interviewed"],
                                                 order = 1
                                             };

            var timesheetList = await timeSheets.ToListAsync();

            // Verimlilik
            var productivities = from t in timesheetList
                                 group new { t } by new
                                 {
                                     t.CreatorId,
                                 } into g
                                 select new ReportViewModel()
                                 {
                                     Id = g.Key.CreatorId,
                                     Count = g.GroupBy(x => x.t.CreatorId).Count() / ((double)g.Sum(x => x.t.AllDay ? 1 : (x.t.End.Value - x.t.Start.Value).TotalMinutes / 480))
                                 };

            List<ReportViewModel> lst = new List<ReportViewModel>();
            lst.AddRange(firstInterviewedCandidates);
            lst.AddRange(submittedToManagers);
            lst.AddRange(customerWillInterviews);
            lst.AddRange(offerMades);

            foreach (var item in lst)
            {
                item.Key = users.FirstOrDefault(x => x.Id == item.Id)?.DisplayName;
            }

            var sharedWithCustomersList = await sharedWithCustomers.ToListAsync();
            foreach (var item in sharedWithCustomersList)
            {
                item.Key = users.FirstOrDefault(x => x.Id == item.Id)?.DisplayName;
            }

            lst.AddRange(sharedWithCustomersList);

            var acceptedOffersList = await acceptedOffers.ToListAsync();
            foreach (var item in acceptedOffersList)
            {
                item.Key = users.FirstOrDefault(x => x.Id == item.Id)?.DisplayName;
            }

            lst.AddRange(acceptedOffersList);

            var productivitiesList = productivities.ToList();
            foreach (var item in productivitiesList)
            {
                item.Key = users.FirstOrDefault(x => x.Id == item.Id)?.DisplayName;
            }

            //var model = GenerateChartViewByRecruiter(search);
            var model = new ChartViewModel()
            {
                Search = search,
                SerializedChart1 = Serialize(sharedWithCustomersList),
                SerializedChart2 = Serialize(acceptedOffersList),
                SerializedChart3 = SerializeMultipleDatasets(lst),
                SerializedChart4 = Serialize(productivitiesList)
            };

            return View(model);
        }

        internal static double TargetCalculation(int target, DateTime? start, DateTime? end)
        {
            double duration = 0;
            if (start.HasValue && end.HasValue)
            {
                duration = (end - start).Value.TotalDays/31;
            }

            var roundedDuration = Math.Ceiling(duration);


            return roundedDuration * target;

        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Recruiter Target Analysis")]
        public async Task<IActionResult> RecruiterTargetAnalysis(ReportSearchViewModel search)
        {

            var splittedRange = DateToolkit.SpliteDateRange(search.DateRange);

            search.StartDate = splittedRange?.Item1;
            search.EndDate = splittedRange?.Item2;

            //(Kabul Eden Aday/Hedef)*100
            var query = _workflowState
                .Where(x => x.Status == CandidateStatus.AcceptedOffer)
                .OrderBy(x => x.CreatedDateTime)
                .AsNoTracking();

            if (search.StartDate.HasValue && search.EndDate.HasValue)
            {
                query = query
                    .Where(x => x.CreatedDateTime >= search.StartDate.Value && x.CreatedDateTime < search.EndDate.Value);
            }

            var users = await _user
                .Include(x => x.Title)
                .AsNoTracking()
                .ToListAsync();

            var queryList =await query.ToListAsync();

            var rateOfAchievements =from wf in queryList
                                    group new { wf } by new
                                     {
                                         wf.CreatorId,
                                     } into g
                                     select new ReportViewModel()
                                     {
                                         Id = g.Key.CreatorId,
                                         GroupCount = g.Count(),
                                         lst = g.Select(x=> x.wf.CreatedDateTime),
                                         FirstDate = g.FirstOrDefault().wf.CreatedDateTime,
                                         LastDate = g.LastOrDefault().wf.CreatedDateTime
                                         //Key = _userManager.GetUserDisplayName(g.Key.CreatorId),
                                         //Count = _userManager.GetUserTitle(g.Key.CreatorId).Target > 0 ? Math.Round((double)g.Count() / _userManager.GetUserTitle(g.Key.CreatorId).Target, 2) * 100 : 0,
                                         //Category = g.Count().ToString() + "-" + _userManager.GetUserTitle(g.Key.CreatorId).Target,
                                         //lst = g.Key
                                     };


            var rateOfAchievementsList = rateOfAchievements.ToList();
            double target = 0;
            foreach (var item in rateOfAchievementsList)
            {
                var selectedUser = users.FirstOrDefault(x => x.Id == item.Id);

                if (selectedUser != null)
                {
                    item.Key = selectedUser?.DisplayName;
                    item.Category = item.GroupCount.ToString() + "-" + selectedUser?.Title?.Target;

                    if (selectedUser?.Title?.Target > 0)
                    {
                        if (search.StartDate.HasValue && search.EndDate.HasValue)
                        {
                            target = TargetCalculation(selectedUser.Title.Target, search.StartDate.Value,
                                search.EndDate.Value);
                        }
                        else
                        {
                            target = TargetCalculation(selectedUser.Title.Target, item.lst.FirstOrDefault(),
                                item.lst.LastOrDefault());
                        }

                        item.Count = target > 0
                            ? Math.Round((double)item.GroupCount / target, 2) * 100
                            : 0;
                    }
                }
                else
                {
                    item.Key = "Unknown";
                    item.Count = 0;
                    item.Category = item.GroupCount.ToString();
                }
            }

            var model = new ChartViewModel()
            {
                Search = search,
                SerializedChart1 = Serialize(rateOfAchievementsList),
                //Detail = chartDetail
            };

            return View(model);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Recruiter Report")]
        public async Task<IActionResult> RecruiterReport(ReportSearchViewModel search)
        {
            var splittedRange = DateToolkit.SpliteDateRange(search.DateRange);

            search.StartDate = splittedRange?.Item1;
            search.EndDate = splittedRange?.Item2;

            //var candidates = Candidates(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);
            //var candidatePositions = CandidatesPositions(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);
            var timeSheets = Timesheets(search.StartDate, search.EndDate, search.PositionId, search.UserId);
            //var preCandidates = PreCandidates(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);
            var preCandidateMeetings = PreCandidatesMeetings(search.StartDate, search.EndDate, search.PositionId, search.CustomerId, search.UserId);
            var workFlowStateDateTimes = WorkflowStateDateTimes(search.StartDate, search.EndDate, search.PositionId,
                search.CustomerId, search.UserId);

            var userId = User.Identity.GetUserId<int>();

            // First interviewed cv
            var allFirstInterviewedCandidatesCount = preCandidateMeetings.Count();
            var firstInterviewedCandidatesCount = preCandidateMeetings
                .Where(x => x.CreatorId == userId)?.Count();

            var allFirstInterviewedCandidatesUserCount =
                preCandidateMeetings.Select(x => x.CreatorId).Distinct().Count();

            var firstInterviewedCandidatesAverage =
               Math.Round((double)allFirstInterviewedCandidatesCount / allFirstInterviewedCandidatesUserCount, 1);


            // Submitted To Management 
            var allSubmittedToManagementCandidates = workFlowStateDateTimes
                                              .Where(x => x.Status == CandidateStatus.SubmittedToManager);


            var allSubmittedToManagementCandidatesUserCount = allSubmittedToManagementCandidates.Select(x => x.CreatorId).Distinct().Count();

            var submittedToManagementCandidatesCount = allSubmittedToManagementCandidates
                .Where(x => x.CreatorId == userId)?.Count();

            var submittedToManagementCandidatesAverage =
               Math.Round((double)allSubmittedToManagementCandidates.Count() / allSubmittedToManagementCandidatesUserCount, 1);

            // Sharing CV with Customer
            var allSharedWithCustomerCandidates = workFlowStateDateTimes
                                              .Where(x => x.Status == CandidateStatus.SharedWithCustomer);

            var allSharedWithCustomerCandidatesUserCount = allSharedWithCustomerCandidates
                .Select(x => x.CreatorId).Distinct().Count();

            var sharedWithCustomerCandidatesCount = allSharedWithCustomerCandidates
                .Where(x => x.CreatorId == userId)?.Count();

            var sharedWithCustomerCandidatesAverage =
               Math.Round((double)allSharedWithCustomerCandidates.Count() / allSharedWithCustomerCandidatesUserCount, 1);


            //Teklif Yapılan Aday
            var allProposedCandidates = workFlowStateDateTimes
                                              .Where(x => x.Status == CandidateStatus.OfferMade);

            var allProposedCandidatesUserCount = allProposedCandidates.Select(x => x.CreatorId).Distinct().Count();

            var proposedCandidatesCount = allProposedCandidates
                .Where(x => x.CreatorId == userId)?.Count();

            var proposedCandidatesAverage = Math.Round((double)allProposedCandidates.Count() / allProposedCandidatesUserCount, 1);


            // Customer's chosen candidate
            var allCustomerChoosenCandidates = workFlowStateDateTimes
                                              .Where(x => x.Status == CandidateStatus.CustomerWillInterview);

            var allCustomerChoosenCandidatesUserCount = allCustomerChoosenCandidates
                .Select(x => x.CreatorId).Distinct().Count();

            var customerChoosenCandidatesCount = allCustomerChoosenCandidates
                .Where(x => x.CreatorId == userId)?.Count();

            var customerChoosenCandidatesAverage =
               Math.Round((double)allCustomerChoosenCandidates.Count() / allCustomerChoosenCandidatesUserCount, 1);


            // Acceptted PreCandidate
            var allAcceptedOfferCandidates = workFlowStateDateTimes
                                              .Where(x => x.Status == CandidateStatus.AcceptedOffer);

            var allAcceptedOfferCandidatesUserCount = allAcceptedOfferCandidates
                .Select(x => x.CreatorId).Distinct().Count();

            var acceptedOfferCandidatesCount = allAcceptedOfferCandidates
                .Where(x => x.CreatorId == userId)?.Count();

            var acceptedOfferCandidatesAverage =
                Math.Round((double)allAcceptedOfferCandidates.Count() / allAcceptedOfferCandidatesUserCount, 1);

            // Verimlilik
            //Recruiter Eforu (adam/gün) 
            // Accepted Offer/Toplam 
            // Elapsed time
            var timeSheetList = await timeSheets.ToListAsync();
            var allTimeElapsed = timeSheetList.Sum(x => x.AllDay ? 1 : (x.End.Value - x.Start.Value).TotalMinutes / 480);
            var timeElapsed = timeSheetList.Where(x => x.CreatorId == userId)
                .Sum(x => x.AllDay ? 1 : (x.End.Value - x.Start.Value).TotalMinutes / 480);
            // Recruiter count
            //var recruiterCount = timeSheets.Select(x => x.CreatorId).Distinct().Count();
            var allRecruiterEffort = Math.Round(allAcceptedOfferCandidates.Count() / allTimeElapsed, 1);
            // verimlilik
            var recruiterEffort = Math.Round(acceptedOfferCandidatesCount.Value / timeElapsed, 1);

            // ise alim effor
            var recruiterCount = timeSheets.Select(x => x.CreatorId).Distinct().Count();
            var allRecruiterEffortPerDay = Math.Round(recruiterCount / timeElapsed, 2);
            // Recruitment Target (Monthly)
            int recruitmentTarget = _userManager.GetUserTitle(User.Identity.GetUserId<int>()).Target;

            // Success Percentage
            // (Kabul Eden Aday/Hedef)*100
            var successPercent = Math.Round((double)acceptedOfferCandidatesCount.Value / recruitmentTarget, 2) * 100;


            List<ReportViewModel> lst = new List<ReportViewModel>();
            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Status"],
                Count = firstInterviewedCandidatesCount.Value,
                Category = _localizer["First Interviewed"],
            });
            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Average"],
                Count = firstInterviewedCandidatesAverage,
                Category = _localizer["First Interviewed"],
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Status"],
                Count = submittedToManagementCandidatesCount ?? 0,
                Category = _localizer["Submitted To Management"],
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Average"],
                Count = submittedToManagementCandidatesAverage,
                Category = _localizer["Submitted To Management"],
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Status"],
                Count = sharedWithCustomerCandidatesCount ?? 0,
                Category = _localizer["Shared With Customer"]
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Average"],
                Count = sharedWithCustomerCandidatesAverage,
                Category = _localizer["Shared With Customer"]
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Status"],
                Count = proposedCandidatesCount ?? 0,
                Category = _localizer["Proposed"]
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Average"],
                Count = proposedCandidatesAverage,
                Category = _localizer["Proposed"]
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Status"],
                Count = customerChoosenCandidatesCount ?? 0,
                Category = _localizer["Customer Choosen"]
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Average"],
                Count = customerChoosenCandidatesAverage,
                Category = _localizer["Customer Choosen"]
            });


            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Status"],
                Count = acceptedOfferCandidatesCount ?? 0,
                Category = _localizer["Accepted Offer"]
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Average"],
                Count = acceptedOfferCandidatesAverage,
                Category = _localizer["Accepted Offer"]
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Status"],
                Count = recruiterEffort,
                Category = _localizer["Productivity"]
            });

            lst.Add(new ReportViewModel()
            {
                Key = _localizer["Average"],
                Count = allRecruiterEffort,
                Category = _localizer["Productivity"]
            });


            var chartDetail = new ChartDetailViewModel();
            chartDetail.CustomerChoosen = customerChoosenCandidatesCount ?? 0;
            chartDetail.CustomerOffered = proposedCandidatesCount ?? 0;
            chartDetail.NumberOfPhoneCalls = firstInterviewedCandidatesCount.Value;
            chartDetail.RecruitmentEffort = allRecruiterEffortPerDay;
            chartDetail.SharedWithCustomer = sharedWithCustomerCandidatesCount ?? 0;
            chartDetail.RecruitmentTarget = recruitmentTarget;
            chartDetail.SuccessPercent = successPercent;

            var returnModel = new ChartViewModel()
            {
                Search = search,
                SerializedChart1 = SerializeMultipleDatasets(lst),
                Detail = chartDetail
            };

            return View(returnModel);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("PreCandidate Evaluation Analysis")]
        public async Task<IActionResult> PreCandidateEvaluationAnalysis(ReportSearchViewModel search)
        {
            if (search.SingleDate == null)
            {
                search.SingleDate = DateTime.Now;
            }

            var start = search.SingleDate.Value.Date;
            var end = search.SingleDate?.AddDays(1).Date;

            var users = await _user.AsNoTracking().ToListAsync();

            var preByCreatorAndModifier = _preCandidate
                //.Include(x => x.PreCandidate)
                .AsNoTracking()
                .Where(x => (x.CreatedDateTime >= start && x.CreatedDateTime < end) || (x.ModifiedDateTime != null && x.ModifiedDateTime >= start && x.ModifiedDateTime < end && x.ModifierId.Value != x.CreatorId));

            //var preByModifier = _preCandidate
            //    //.Include(x => x.PreCandidate)
            //    .AsNoTracking()
            //    .Where(x =>x.ModifiedDateTime!=null &&  x.ModifiedDateTime >= start && x.ModifiedDateTime < end);

            //var preTotal = preByCreator.Concat(preByModifier);


            //var ttt= from p in preTotal
            //    group new { p} by new
            //    {
            //        p.Id
            //    } into g
            //        select new PreCandidate()
            //        {
            //            Id= g.Key.Id,
            //            CreatorId = g.Count()>1 ? g.OrderByDescending(x=> x.p.CreatedDateTime).FirstOrDefault().p.ModifierId.Value: g.FirstOrDefault().p.CreatorId,
            //            Status = 

            //        }

            //.GroupBy()


            //.DistinctBy(x=> x.Id)

            //.OrderByDescending(x => x.CreatedDateTime)
            //.Select(x => x.Id).Distinct();

            // کوئری جالب ------------------------------------
            //var query =
            //    from p in parents
            //    from c in p.Children
            //    group p by c.Value;

            var preCandidateMeetings = from m in preByCreatorAndModifier
                                       group new { m } by new
                                       {
                                           m.CreatorId,
                                           //m.ModifierId,
                                           m.Status
                                       }
                into g
                                       select new ReportViewModel()
                                       {
                                           Id = g.Key.CreatorId,
                                           //Key = _userManager.GetUserDisplayName(x.CreatorId), //_localizer[EnumHelper<PreCandidateStatus>.GetDisplayValue(x.Key)],
                                           Count = g.Count(),
                                           Category = _localizer[EnumHelper<PreCandidateStatus>.GetDisplayValue(g.Key.Status)],
                                       };



            //var preCandidateMeetingsGrouped = from m in preCandidateMeetings
            //                                  group new { m } by new
            //                                  {
            //                                      m.Status
            //                                  } into g
            //                                  orderby g.Key.Status
            //                                  select new ReportViewModel()
            //                                  {
            //                                      Id = g.Key.CreatorId,
            //                                      //Key = _userManager.GetUserDisplayName(x.CreatorId), //_localizer[EnumHelper<PreCandidateStatus>.GetDisplayValue(x.Key)],
            //                                      Count = g.Count(),
            //                                      Category = _localizer[EnumHelper<PreCandidateStatus>.GetDisplayValue(g.Key.Status)],
            //                                  };

            //var preCandidateMeetingsByModifier = from m in preByCreator
            //                                     where m.ModifierId != null
            //                                     group new { m } by new
            //                                     {
            //                                         m.ModifierId,
            //                                         m.Status
            //                                     } into g
            //                                     orderby g.Key.Status
            //                                     select new ReportViewModel()
            //                                     {
            //                                         Id = g.Key.ModifierId.Value,
            //                                         //Key = _userManager.GetUserDisplayName(x.CreatorId), //_localizer[EnumHelper<PreCandidateStatus>.GetDisplayValue(x.Key)],
            //                                         Count = g.Count(),
            //                                         Category = _localizer[EnumHelper<PreCandidateStatus>.GetDisplayValue(g.Key.Status)],
            //                                     };

            //var preCandidateMettings1 = _meeting
            //    //.Include(x => x.Position)
            //    //.ThenInclude(x => x.Customer)
            //    .Include(x => x.PreCandidate)
            //    .AsNoTracking()
            //    .Where(x => x.CreatedDateTime >= start && x.CreatedDateTime < end)
            //    .GroupBy(x => new { x.CreatorId, x.PreCandidate.Status }, (x, y) => new
            //    {
            //        Status = x.Status,
            //        Mettings = y,
            //        CreatorId = x.CreatorId

            //    })
            //    .OrderBy(x => x.Status)
            //    .Select(x => new ReportViewModel()
            //    {
            //        Id = x.CreatorId,
            //        //Key = _userManager.GetUserDisplayName(x.CreatorId), //_localizer[EnumHelper<PreCandidateStatus>.GetDisplayValue(x.Key)],
            //        Count = x.Mettings.Count(),
            //        Category = _localizer[EnumHelper<PreCandidateStatus>.GetDisplayValue(x.Status)],

            //    });


            var candidates = _candidate
                .AsNoTracking()
                .Where(x => x.CreatedDateTime >= start && x.CreatedDateTime < end);
            //.ToListAsync();

            var candidatesByRecruiters = from m in candidates
                                         group new { m } by new
                                         {
                                             m.CreatorId
                                         } into g
                                         select new PrecandidateEvaluationAnalysisGroupViewModel()
                                         {
                                             Id = g.Key.CreatorId,
                                             //Name = _userManager.GetUserDisplayName(g.Key.CreatorId),
                                             Count = g.Count()
                                         };

            var preCandidateMeetingsList = await preCandidateMeetings.ToListAsync();
            //var preCandidateMeetingsListByModifier = await preCandidateMeetingsByModifier
            //    .ToListAsync();

            //preCandidateMeetingsList.AddRange(preCandidateMeetingsListByModifier);

            var preCandidateListGroupedByAgain = preCandidateMeetingsList
                .GroupBy(x => new { x.Id, x.Category })
                .Select(x => new ReportViewModel()
                {
                    Id = x.Key.Id,
                    Count = x.Sum(z => z.Count),
                    Category = x.Key.Category
                }).ToList();

            foreach (var item in preCandidateListGroupedByAgain)
            {
                item.Key = users.FirstOrDefault(x => x.Id == item.Id)?.DisplayName;
            }


            var candidatesByRecruitersList = await candidatesByRecruiters.ToListAsync();
            foreach (var item in candidatesByRecruitersList)
            {
                item.Name = users.FirstOrDefault(x => x.Id == item.Id)?.DisplayName;
            }


            var returnModel = new PrecandidateEvaluateAnalysisViewModel()
            {
                Chart = new ChartViewModel()
                {
                    SerializedChart1 = SerializeMultipleDatasets(preCandidateListGroupedByAgain),
                },
                Recruiters = candidatesByRecruitersList

            };

            return View(returnModel);
        }


        [HttpPost]
        public async Task<IActionResult> GetRecruiterCandidateAnlysis(int? id, DateTime? date)
        {
            if (id == null)
                return Json("");

            if (date == null)
            {
                date = DateTime.Now;
            }

            var start = date.Value.Date;
            var end = date.Value.AddDays(1).Date;


            var candidates = await _candidatePosition
                .Include(x => x.Candidate)
                    .ThenInclude(x => x.Attachments)
                .Include(x => x.Position)
                    .ThenInclude(x => x.Customer)
                .Include(x => x.Candidate)
                .Where(x => x.Candidate.CreatorId == id && x.Candidate.CreatedDateTime >= start && x.Candidate.CreatedDateTime < end)
                .AsNoTracking()
                .ToListAsync();

            //var candidates = await _candidate
            //        .Include(x => x.Attachments)
            //        .Include(x => x.CandidatePositions)
            //        .ThenInclude(x => x.Position)
            //            .ThenInclude(x => x.Customer)
            //    .Where(x => x.CreatorId == id && x.CreatedDateTime >= start && x.CreatedDateTime < end)
            //    .AsNoTracking()
            //    .ToListAsync();


            var sb = new StringBuilder();
            sb.Append("<table cellpadding=\"5\" cellspacing=\"0\" class=\"table table-bordered table-striped\" style=\"padding-left:50px;\" >");
            sb.Append("<thead><tr>");
            sb.Append("<td></td>");
            sb.Append("<td>" + _localizer["Customer Name"] + "</td>");
            sb.Append("<td>" + _localizer["Position Name"] + "</td>");
            sb.Append("<td>" + _localizer["Name Surename"] + "</td>");
            sb.Append("<td>" + _localizer["Position Related Exp(Year)"] + "</td>");
            sb.Append("<td>" + _localizer["Expected Salary(TL)"] + "</td>");
            sb.Append("<td>" + _localizer["Having CV"] + "</td>");
            sb.Append("</tr></thead>");
            sb.Append("<tbody>");

            var serial = 0;
            foreach (var item in candidates)
            {
                serial += 1;
                var haveCv = item.Candidate.Attachments.Any() ? _localizer["Yes"] : _localizer["No"];
                sb.Append("<tr>");
                sb.Append("<td>" + serial + "</td>");
                sb.Append("<td>" + item.Position?.Customer?.Name + "</td>");
                sb.Append("<td>" + item.Position?.PositionName + "</td>");
                sb.Append("<td>" + item.Candidate.DisplayName + "</td>");
                sb.Append("<td>" + item.Candidate.PositionRelatedExperience + "</td>");
                sb.Append("<td>" + item.Candidate.ExpectedWages + "</td>");
                sb.Append("<td>" + haveCv + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            return Json(sb.ToString());
        }

    }


}
