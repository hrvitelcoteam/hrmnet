﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;

namespace HRMNet.Controllers
{

    [DisplayName("Employee Inventories")]
    [Authorize]
    public class CandidateInventoriesController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<CandidateInventory> _context;
        private readonly DbSet<Candidate> _candidate;
        private readonly DbSet<Inventory> _inventory;
        private readonly DbSet<InventoryType> _inventoryType;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


        public CandidateInventoriesController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _context = uow.Set<CandidateInventory>();
            _candidate= uow.Set<Candidate>();
            _inventoryType= uow.Set<InventoryType>();
            _inventory = uow.Set<Inventory>();
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> Export()
        {
            var inventories = await _context
                                .Include(i => i.Candidate)
                                .Include(x => x.Inventory)
                                .ThenInclude(x => x.InventoryType)
                                .AsNoTracking()
                                .ToListAsync();

            //var inventories = await _context.Inventories
            //    .Include(i => i.CandidateInventories)
            //    .ThenInclude(x => x.Candidate)
            //    .AsNoTracking()
            //    .ToListAsync();

            return File(ExportAsExcel(inventories), XlsxContentType, "deliveredInventories.xlsx");
        }

        private byte[] ExportAsExcel(List<CandidateInventory> inventories)
        {
            byte[] reportBytes;
            using (var package = createExcelPackage(inventories))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;

            //return File(reportBytes, XlsxContentType, "report.xlsx");

        }

        private ExcelPackage createExcelPackage(List<CandidateInventory> inventories)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Delivered Inventories Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Delivered Inventories Report";
            package.Workbook.Properties.Keywords = "Delivered Inventories";

            var worksheet = package.Workbook.Worksheets.Add("Delivered Inventories");
            //First add the headers
            worksheet.Cells[1, 1].Value = "Employee Name";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 2].Value = "Operation Type";
            worksheet.Cells[1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 3].Value = "Category Name";
            worksheet.Cells[1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 4].Value = "Inventory Number";
            worksheet.Cells[1, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 5].Value = "Delivery Date";
            worksheet.Cells[1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 6].Value = "Status";
            worksheet.Cells[1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 7].Value = "Brand";
            worksheet.Cells[1, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 8].Value = "Model";
            worksheet.Cells[1, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 9].Value = "Serial Number";
            worksheet.Cells[1, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //Add values

            //var numberformat = "#,##0";
            //var dataCellStyleName = "TableNumber";
            //var numStyle = package.Workbook.Styles.CreateNamedStyle(dataCellStyleName);
            //numStyle.Style.Numberformat.Format = numberformat;

            int rowNumber = 1;
            foreach (var item in inventories)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Candidate.Name+" "+ item.Candidate.Family;
                worksheet.Cells[rowNumber, 2].Value = item.OperationType;
                worksheet.Cells[rowNumber, 3].Value = item.Inventory.InventoryType.Name;
                worksheet.Cells[rowNumber, 4].Value = item.Inventory.InventoryNumber;
                //worksheet.Cells[rowNumber, 2].Style.Numberformat.Format = numberformat;
                //worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                //worksheet.Cells[rowNumber, 3].Style.Numberformat.Format = numberformat;
                //worksheet.Cells[rowNumber, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                //worksheet.Cells[rowNumber, 4].Style.Numberformat.Format = numberformat;
                //worksheet.Cells[rowNumber, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[rowNumber, 5].Value = item.DeliveryDate.ToShortDateString();
                worksheet.Cells[rowNumber, 6].Value = item.Status;
                worksheet.Cells[rowNumber, 7].Value = item.Inventory.Brand;
                worksheet.Cells[rowNumber, 8].Value = item.Inventory.Model;
                worksheet.Cells[rowNumber, 9].Value = item.Inventory.SerialNumber;
            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: inventories.Count() + 2, toColumn: 9), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;
            //tbl.Columns[3].DataCellStyleName = dataCellStyleName;
            //tbl.Columns[1].TotalsRowFunction = RowFunctions.Sum;
            //tbl.Columns[2].TotalsRowFunction = RowFunctions.Sum;
            //tbl.Columns[3].TotalsRowFunction = RowFunctions.Sum;

            //worksheet.Cells[candidates.Count() + 2, 4].Style.Numberformat.Format = numberformat;

            // AutoFitColumns
            worksheet.Cells[1, 1, inventories.Count() + 2, 9].AutoFitColumns();

            //worksheet.HeaderFooter.OddFooter.InsertPicture(
            //    new FileInfo(Path.Combine(_hostingEnvironment.WebRootPath, "images", "captcha.jpg")),
            //    PictureAlignment.Right);

            return package;
        }

        
        private bool CandidateInventoryExists(int id)
        {
            return _context.Any(e => e.Id == id);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public async Task<IActionResult> Create()
        {
            var stuffs = await _candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff).ToListAsync();
            ViewData["CandidateId"] = new SelectList(stuffs, "Id", "DisplayName");

            ViewData["InventoryTypeId"] = _inventoryType.AsNoTracking();
            return View();
        }

        // POST: CandidateInventories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CandidateId,InventoryId,DeliveryDate,Id,OperationType,Status")] CandidateInventory candidateInventory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(candidateInventory);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(DeliveredInventories));
            }

            var stuffs = await _candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff).ToListAsync();
            ViewData["CandidateId"] = new SelectList(stuffs, "Id", "Name");

            ViewData["InventoryTypeId"] = _inventoryType;
            return View(candidateInventory);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidateInventory = await _context
                .Include(c => c.Candidate)
                .Include(c => c.Inventory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidateInventory == null)
            {
                return NotFound();
            }

            return View(candidateInventory);
        }

        // POST: CandidateInventories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var candidateInventory = await _context.FindAsync(id);
            _context.Remove(candidateInventory);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(DeliveredInventories));
        }

        // GET: CandidateInventories/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidateInventory = await _context
                .Include(x => x.Inventory)
                //.ThenInclude(x=> x.InventoryType)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (candidateInventory == null)
            {
                return NotFound();
            }
            var stuffs = await _candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff).AsNoTracking().ToListAsync();
            ViewData["CandidateId"] = new SelectList(stuffs, "Id", "Name");

            ViewData["InventoryTypeId"] = _inventoryType.AsNoTracking();

            //var ttt = _inventory.Where(x => x.InventoryTypeId == candidateInventory.Inventory.InventoryTypeId).ToList();
            ViewData["InventoryNumbers"] = _inventory.Where(x => x.InventoryTypeId == candidateInventory.Inventory.InventoryTypeId).AsNoTracking();

            return View(candidateInventory);
        }

        // POST: CandidateInventories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CandidateId,InventoryId,DeliveryDate,Id,OperationType,Status")] CandidateInventory candidateInventory)
        {
            if (id != candidateInventory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(candidateInventory);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    //if (!CandidateInventoryExists(candidateInventory.Id))
                    //{
                    //    return NotFound();
                    //}
                    //else
                    //{
                    //    throw;
                    //}
                }
                return RedirectToAction(nameof(DeliveredInventories));
            }

            var stuffs = await _candidate.Where(x => x.Status == Models.Common.CandidateStatus.VitelcoStuff).AsNoTracking().ToListAsync();
            ViewData["CandidateId"] = new SelectList(stuffs, "Id", "Name");

            ViewData["InventoryTypeId"] = _inventoryType.AsNoTracking();
            return View(candidateInventory);
        }

        [DisplayName("Delivered Inventories")]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> DeliveredInventories()
        {
            var model = await _context
                                .Include(i => i.Candidate)
                                .Include(x => x.Inventory)
                                .ThenInclude(x => x.InventoryType)
                .AsNoTracking()
                .ToListAsync();

            return View(model);
        }

    }
}
