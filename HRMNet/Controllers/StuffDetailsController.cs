﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using HRMNet.Models.Common;
using System.ComponentModel;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Stuff Detail")]
    public class StuffDetailsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<StuffDetail> _context;
        private readonly DbSet<Candidate> _candidate;
        private readonly DbSet<Title> _title;
        private readonly DbSet<WorkLocation> _workLocation;

        public StuffDetailsController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<StuffDetail>();
            _candidate = uow.Set<Candidate>();
            _title = uow.Set<Title>();
            _workLocation = uow.Set<WorkLocation>();
        }

        // GET: StuffDetails
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            var hRMNetContext = _context.Include(s => s.Manager).Include(s => s.Title).Include(s => s.WorkingLocation).AsNoTracking();
            return View(await hRMNetContext.ToListAsync());
        }

        // GET: StuffDetails/Details/5
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stuffDetail = await _context
                .Include(s => s.Manager)
                .Include(s => s.Title)
                .Include(s => s.WorkingLocation)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (stuffDetail == null)
            {
                return NotFound();
            }

            return View(stuffDetail);
        }

        // GET: StuffDetails/Create
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            ViewData["ManagerId"] = new SelectList(_candidate.AsNoTracking(), "Id", "Email");
            ViewData["TitleId"] = new SelectList(_title.AsNoTracking(), "Id", "Id");
            ViewData["WorkLocationId"] = new SelectList(_workLocation.AsNoTracking(), "Id", "Id");
            return View();
        }

        // POST: StuffDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TCNumber,AccommodationAddress,MilitaryStatus,PostponementdDate,ContractType,ContractDuration,TitleId,WorkLocationId,SchoolOfGraduation,DepartmentOfGraduation,DateOfGraduation,IBANNumber,CompanyEmail,ManagerId,BloodGroup,EmergencyContactInformation,BirthCertificateNumber,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] StuffDetail stuffDetail)
        {
            if (ModelState.IsValid)
            {
                _context.Add(stuffDetail);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["ManagerId"] = new SelectList(_context.Candidate, "Id", "Email", stuffDetail.ManagerId);
            ViewData["TitleId"] = new SelectList(_title.AsNoTracking(), "Id", "Id", stuffDetail.TitleId);
            ViewData["WorkLocationId"] = new SelectList(_workLocation.AsNoTracking(), "Id", "Id", stuffDetail.WorkLocationId);
            return View(stuffDetail);
        }

        // GET: StuffDetails/Edit/5
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stuffDetail = await _context.FindAsync(id);
            if (stuffDetail == null)
            {
                return NotFound();
            }
            //ViewData["ManagerId"] = new SelectList(_context.Candidate, "Id", "Email", stuffDetail.ManagerId);
            ViewData["TitleId"] = new SelectList(_title.AsNoTracking(), "Id", "Id", stuffDetail.TitleId);
            ViewData["WorkLocationId"] = new SelectList(_workLocation.AsNoTracking(), "Id", "Id", stuffDetail.WorkLocationId);
            return View(stuffDetail);
        }

        // POST: StuffDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TCNumber,AccommodationAddress,MilitaryStatus,PostponementdDate,ContractType,ContractDuration,TitleId,WorkLocationId,SchoolOfGraduation,DepartmentOfGraduation,DateOfGraduation,IBANNumber,CompanyEmail,ManagerId,BloodGroup,EmergencyContactInformation,BirthCertificateNumber,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] StuffDetail stuffDetail)
        {
            if (id != stuffDetail.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(stuffDetail);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StuffDetailExists(stuffDetail.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["ManagerId"] = new SelectList(_context.Candidate, "Id", "Email", stuffDetail.ManagerId);
            ViewData["TitleId"] = new SelectList(_title.AsNoTracking(), "Id", "Id", stuffDetail.TitleId);
            ViewData["WorkLocationId"] = new SelectList(_workLocation.AsNoTracking(), "Id", "Id", stuffDetail.WorkLocationId);
            return View(stuffDetail);
        }

        // GET: StuffDetails/Delete/5
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stuffDetail = await _context
                .Include(s => s.Manager)
                .Include(s => s.Title)
                .Include(s => s.WorkingLocation)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (stuffDetail == null)
            {
                return NotFound();
            }

            return View(stuffDetail);
        }

        // POST: StuffDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var stuffDetail = await _context.FindAsync(id);
            _context.Remove(stuffDetail);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StuffDetailExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
