﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Customers;
using Microsoft.AspNetCore.Http;
using System.IO;
using HRMNet.Models.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Customers")]
    public class CustomersController : Controller
    {
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private readonly IUnitOfWork _uow;
        private readonly IWebHostEnvironment _environment;
        private readonly DbSet<Customer> _context;
        private readonly DbSet<Attachment> _attachment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


        public CustomersController(IUnitOfWork uow, IWebHostEnvironment environment)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
            _context = uow.Set<Customer>();
            _attachment = uow.Set<Attachment>();
        }

        // GET: Customers
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.AsNoTracking().ToListAsync());
        }

        // GET: Customers/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // GET: Customers/Create
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Content")] Customer customer, List<IFormFile> files)
        {
            if (ModelState.IsValid)
            {

                if (files != null && files.Any())
                {
                    //customer.Docs = new List<Attachment>();
                    foreach (var item in files)
                    {
                        var fileName = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
                        var fileExtension = item.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
                        var path = Path.Combine(_environment.WebRootPath, "Files","Customer", fileName + "." + fileExtension.LastOrDefault());
                        using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write,
                                       FileShare.None,
                                       MaxBufferSize,
                                       // you have to explicitly open the FileStream as asynchronous
                                       // or else you're just doing synchronous operations on a background thread.
                                       useAsync: true))
                        {
                            await item.CopyToAsync(fileStream);
                        }

                        var toAdd = new Attachment { IsImage = ImageToolkit.IsImageFile(item), FileName = fileName + "." + fileExtension.LastOrDefault(), Caption = item.FileName, ContentType = item.ContentType };
                        customer.Docs.Add(toAdd);
                    }

                    //candidate.CVFilename = fileName + "." + fileExtension.LastOrDefault();
                }

                _context.Add(customer);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete Attachment")]
        public async Task<IActionResult> DeleteAttachment(string id)
        {
            var attachment = await _attachment.FindAsync(int.Parse(id));

            _attachment.Remove(attachment);

            var result = await _uow.SaveChangesAsync();

            if (result > 0)
            {
                var path = Path.Combine(_environment.WebRootPath, "Files", attachment.FileName);
                System.IO.File.Delete(path);
                return Json("ok");
            }
            else
                return Json("not ok");
        }

        // GET: Customers/Edit/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context
                         .Include(x => x.Docs)
                         .AsNoTracking()
                         .FirstOrDefaultAsync(x => x.Id == id);
                         
            if (customer == null)
            {
                return NotFound();
            }

            string preview = string.Empty;
            string previewConfig = string.Empty;
            int key = 0;
            if (customer.Docs.Any())
                foreach (var item in customer.Docs)
                {
                    key += 1;
                    preview += "\"/Files/Customer/" + item.FileName + "\",";
                    previewConfig += "{type:\"" + GetfileType(item.ContentType) + "\", caption:\"" + item.Caption + "\",size:" + item.Size + ",width:\"120px\",url:\"/Customers/DeleteAttachment/" +
                        item.Id + "\",key:" + item.Id + "},";
                }

            var config = new AttachmentViewModel()
            {
                Id = id.Value,
                SerializedInitialPreview = preview,
                SerializedInitialPreviewConfig = previewConfig,
                append = true
            };

            customer.AttachmentViewModel = config;

            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Customer customer, List<IFormFile> files)
        {
            if (id != customer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var currentCustomer = _context
                        .Include(x=> x.Docs)
                        .FirstOrDefault(x => x.Id ==id);

                    if (files != null && files.Any())
                    {
                        //currentCustomer.Docs = new List<Attachment>();
                        foreach (var item in files)
                        {
                            var fileName = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
                            var fileExtension = item.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
                            var path = Path.Combine(_environment.WebRootPath, "Files",fileName + "." + fileExtension.LastOrDefault());
                            using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write,
                                           FileShare.None,
                                           MaxBufferSize,
                                           // you have to explicitly open the FileStream as asynchronous
                                           // or else you're just doing synchronous operations on a background thread.
                                           useAsync: true))
                            {
                                await item.CopyToAsync(fileStream);
                            }

                            var toAdd = new Attachment { IsImage = ImageToolkit.IsImageFile(item), FileName = fileName + "." + fileExtension.LastOrDefault(), Caption = item.FileName, ContentType = item.ContentType };
                            currentCustomer.Docs.Add(toAdd);
                        }

                        //candidate.CVFilename = fileName + "." + fileExtension.LastOrDefault();
                    }

                    currentCustomer.Name = customer.Name;
                    currentCustomer.Content = customer.Content;
                    currentCustomer.IsContentUsesForEmail = customer.IsContentUsesForEmail;
                    //currentCustomer.Id

                    var result = await _uow.SaveChangesAsync();

                    if (result>0)
                        return RedirectToAction(nameof(Index));
                    //_context.Update(customer);
                    //await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(customer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                //return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // GET: Customers/Delete/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var customer = await _context.FindAsync(id);
            _context.Remove(customer);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private string GetfileType(string extension)
        {
            switch (extension)
            {
                case "application/pdf":
                    return "pdf";
                case "image/png":
                    return "image";
                case "image/jpg":
                    return "image";
                case "image/jpeg":
                    return "image";
                case "image/tiff":
                    return "gdocs";
                case ".ppt":
                    return "office";
                case "application/msword":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    return "office";
                case ".xls":
                    return "office";
                case ".xlsx":
                    return "office";
                case ".mp4":
                    return "video";
                case ".txt":
                    return "text";
                case ".html":
                    return "html";
                default:
                    return "image";
            }
        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export")]
        public async Task<IActionResult> Export()
        {
            var locations = await _context.AsNoTracking().ToListAsync();

            return File(ExportAsExcel(locations), XlsxContentType, "customers.xlsx");
        }

        private byte[] ExportAsExcel(List<Customer> customers)
        {
            byte[] reportBytes;
            using (var package = createExcelPackage(customers))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }

        private ExcelPackage createExcelPackage(List<Customer> customers)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Customers Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Customers Report";
            package.Workbook.Properties.Keywords = "Customers";

            var worksheet = package.Workbook.Worksheets.Add("Customers");
            //First add the headers

            worksheet.Cells[1, 1].Value = "Name";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 2].Value = "Content";
            worksheet.Cells[1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            //Add values

            int rowNumber = 1;
            foreach (var item in customers)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Name;
                worksheet.Cells[rowNumber, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[rowNumber, 2].Value = item.Content;
                worksheet.Cells[rowNumber, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: customers.Count() + 2, toColumn: 2), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;

            // AutoFitColumns
            worksheet.Cells[1, 1, customers.Count() + 2, 2].AutoFitColumns();

            return package;
        }


        private bool CustomerExists(int id)
        {
            return _context.AsNoTracking().Any(e => e.Id == id);
        }
    }
}
