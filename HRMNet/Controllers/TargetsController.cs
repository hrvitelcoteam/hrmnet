﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Recruit;
using HRMNet.Models.Customers;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Targets")]
    public class TargetsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Target> _context;
        private readonly DbSet<Customer> _customer;
        public TargetsController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<Target>();
            _customer = uow.Set<Customer>();
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            var hRMNetContext = _context
                .Include(t => t.Customer);
            return View(await hRMNetContext.ToListAsync());
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var target = await _context
                .Include(t => t.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (target == null)
            {
                return NotFound();
            }

            return View(target);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            ViewData["CustomerId"] = new SelectList(_customer, "Id", "Name");
            return View();
        }

        // POST: Targets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FirstInterviewedCount,SubmittedToManagement,SharedWithCustomer,CustomerChoosen,Proposed,AcceptedOffer,Productivity,CustomerId,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] Target target)
        {
            if (ModelState.IsValid)
            {
                _context.Add(target);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerId"] = new SelectList(_customer, "Id", "Name", target.CustomerId);
            return View(target);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var target = await _context.FindAsync(id);
            if (target == null)
            {
                return NotFound();
            }
            ViewData["CustomerId"] = new SelectList(_customer, "Id", "Name", target.CustomerId);
            return View(target);
        }

        // POST: Targets/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FirstInterviewedCount,SubmittedToManagement,SharedWithCustomer,CustomerChoosen,Proposed,AcceptedOffer,Productivity,CustomerId,Id,CreatorId,CreatedDateTime,CreatedByBrowserName,CreatorIp,ModifierId,ModifiedDateTime,ModifiedByBrowserName,ModifierIp")] Target target)
        {
            if (id != target.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(target);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TargetExists(target.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerId"] = new SelectList(_customer, "Id", "Name", target.CustomerId);
            return View(target);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var target = await _context
                .Include(t => t.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (target == null)
            {
                return NotFound();
            }

            return View(target);
        }

        // POST: Targets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var target = await _context.FindAsync(id);
            _context.Remove(target);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TargetExists(int id)
        {
            return _context.Any(e => e.Id == id);
        }
    }
}
