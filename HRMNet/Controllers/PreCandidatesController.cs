﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Candidates;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel;
using HRMNet.Services.Identity;
using HRMNet.Models.Customers;
using HRMNet.Models.Common;
using HRMNet.Services;
using System.Text;
using Microsoft.AspNetCore.SignalR;
using HRMNet.Hubs;
using HRMNet.Models.Identity;
using HRMNet.Models.Positions;
using Microsoft.Extensions.Localization;
using HRMNet.Services.Contracts.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Drawing;
using System.ComponentModel.DataAnnotations;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("PreCandidates")]
    public class PreCandidatesController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<PreCandidate> _context;
        private readonly DbSet<Candidate> _candidate;
        private readonly DbSet<CandidatePosition> _candidatePosition;
        private readonly DbSet<Customer> _customer;
        private readonly DbSet<User> _user;
        private readonly DbSet<PreCandidateMettings> _meeting;
        private readonly DbSet<Position> _position;
        private readonly DbSet<PreCandidateAndCandidateViewModel> _preCandidateAndCandidate;
        private readonly IHubContext<MessageHub> _messageHubContext;
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private readonly IEmailSender _emailSender;
        private IStringLocalizer _localizer;
        private readonly IApplicationUserManager _userManager;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public PreCandidatesController(IUnitOfWork uow
            , IHubContext<MessageHub> messageHubContext
            , IEmailSender emailSender
            , IApplicationUserManager userManager
            , IStringLocalizerFactory localizerFactory)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _context = uow.Set<PreCandidate>();
            _customer = uow.Set<Customer>();
            _candidate = uow.Set<Candidate>();
            _candidatePosition = uow.Set<CandidatePosition>();
            _meeting = uow.Set<PreCandidateMettings>();
            _position = uow.Set<Position>();

            _preCandidateAndCandidate = uow.Set<PreCandidateAndCandidateViewModel>();

            _messageHubContext = messageHubContext ?? throw new ArgumentNullException(nameof(messageHubContext));
            //_emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
            _user = uow.Set<User>();
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _localizer = localizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
        }

        // GET: PreCandidates
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            ViewData["Customers"] = await _customer.AsNoTracking()
                .Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                })
                .ToListAsync();

            ViewData["Users"] = await _user.AsNoTracking()
                .Select(x => new SelectListItem()
                {
                    Text = x.DisplayName,
                    Value = x.Id.ToString()
                })
                .ToListAsync();

            return View(new PreCandidatesResultViewModel());

            //var model = await GetPreCandidatesByPositionAndUser(search, page, pageSize);

            //return View(model);

            //if (id > 0)
            //{
            //    var position = await _position
            //        .AsNoTracking()
            //        .FirstOrDefaultAsync(x => x.Id == id.Value);

            //    var customerId = position.CustomerId;

            //    var customerPositions = await _position
            //        .AsNoTracking()
            //        .Where(x => x.CustomerId == position.CustomerId)
            //        .Select(x => new SelectListItem()
            //        {
            //            Text = x.PositionName,
            //            Value = x.Id.ToString()
            //        })
            //        .ToListAsync();

            //    ViewData["Positions"] = customerPositions;

            //    search.CustomerId = position.CustomerId;
            //    search.PositionId = id.Value;
            //    //search.UserId=

            //}

            //var model = new PreCandidatesViewModel()
            //{
            //    PreCandidates = new List<PreCandidateViewModel>(),
            //    //await _context
            //    //.AsNoTracking().Select(x => new PreCandidateViewModel()
            //    //{
            //    //    CVSource = x.CVSource,
            //    //    Email = x.Email,
            //    //    Family = x.Family,
            //    //    Id = x.Id,
            //    //    Name = x.Name,
            //    //    Note = x.Note,
            //    //    Status = (PreCandidateStatusByType)x.Status,
            //    //    Telephone = x.Telephone,
            //    //    IsCandidate = false


            //    //}).ToListAsync(),
            //    //.AsEnumerable()
            //    //.GroupBy(c => c.Telephone)
            //    //.Select(x => new PreCandidate()
            //    //{
            //    //    Name = x.Max(z => z.Name),
            //    //    Family = x.Max(z => z.Family),
            //    //    Telephone= x.Key,
            //    //    Note = x.Max(z => z.Note),
            //    //    Status = x.Max(z => z.Status),

            //    //}).,

            //    Search = search
            //};
            //return View(model);
        }

        internal string GenerateQueryText(int page, int pageSize, int customerId, int positionId, string ownerId, string phone, DateTime? start, DateTime? end)
        {
            var preCandidateQueryText =
                " select pr.CVSource, pr.Email, pr.Family, pr.Id as PreCandidateId, CandidateId = 0, pr.Id, pr.Name, IsCandidate =0, " +
                " pr.Status as GeneralStatus, pr.Telephone, CandidatePrecandidate = 1, " +
                " t1.CreatedDateTime as LastInterviewDate, " +
                " pr.Name+' '+pr.Family as DisplayName, " +
                " pr.CreatorId, pr.ModifierId,  " +
                " pr.Status, CandidateOwner='', Creator='',CustomerName='',IsInBlackList=0 ,Modifier='',Note='',PositionName='',Result='' " +
                " from PreCandidate pr " +
                " LEFT JOIN (select prcm.CreatedDateTime,prcm.PreCandidateId from PreCandidateMettings prcm inner join Positions prp on prcm.PositionId=prp.Id) as t1 on pr.Id=t1.PreCandidateId " +
                " inner join PreCandidateMettings prm " +
                " on pr.Id = prm.PreCandidateId " +
                " inner join Positions po " +
                " on prm.PositionId = po.Id " +
                " where pr.Status != 5 ";

            if (positionId > 0)
            {
                preCandidateQueryText += " and prm.PositionId=" + positionId;
            }

            if (customerId > 0)
            {
                preCandidateQueryText += " and po.CustomerId=" + customerId;
            }

            if (start.HasValue && end.HasValue)
            {
                preCandidateQueryText += " and prm.CreatedDateTime>='" + start.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' and prm.CreatedDateTime<'" + end.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }


            if (!string.IsNullOrWhiteSpace(ownerId) && ownerId != "0")
            {
                preCandidateQueryText += " and (pr.CreatorId=" + ownerId + " or pr.ModifierId=" + ownerId + ")";
            }

            if (phone != null)
            {
                preCandidateQueryText += " and pr.Telephone='" + phone+"'";
            }

            var candidateQueryText =
                " select CVSource=0,c.Email, c.Family,PreCandidateId=0,c.Id as CandidateId,c.Id,c.Name,IsCandidate =1, " +
                " c.Status as GeneralStatus,c.Telephone,CandidatePrecandidate = 2, " +
                " t2.CreatedDateTime as LastInterviewDate, " +
                " c.Name+' '+c.Family as DisplayName, " +
                " c.CreatorId,c.ModifierId, " +
                " c.Status , CandidateOwner='', Creator='',CustomerName='',IsInBlackList=0,Modifier='',Note='',PositionName='',Result='' " +
                " from CandidatePositions cp " +
                " inner join Candidate c  " +
                " on cp.CandidateId = c.Id  " +
                " LEFT JOIN (select cam.CreatedDateTime, cam.CandidateId from Meetings cam inner join Positions cap on cam.MPositionId=cap.Id) as t2 on c.Id=t2.CandidateId " +
                " inner join Positions p  " +
                " on cp.PositionId = p.Id  " +
                " left outer join Meetings m  " +
                " on m.CandidateId = c.Id  " +
                " Where c.Status != 14 and c.Status != 15 and c.Status != 80 ";

            if (positionId > 0)
            {
                candidateQueryText += " and cp.PositionId=" + positionId;
            }

            if (customerId > 0)
            {
                candidateQueryText += " and p.CustomerId=" + customerId;
            }

            if (start.HasValue && end.HasValue)
            {
                candidateQueryText += " and m.CreatedDateTime>='" + start.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' and m.CreatedDateTime<'" + end.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }


            if (!string.IsNullOrWhiteSpace(ownerId) && ownerId != "0")
            {
                candidateQueryText += " and (c.CreatorId=" + ownerId + " or c.ModifierId=" + ownerId + ")";
            }

            if (phone != null)
            {
                candidateQueryText += " and c.Telephone='" + phone+"'";
            }

            var totalQueryText = " ;WITH CTE1 AS ( select ROW_NUMBER() OVER (ORDER BY Id) AS 'RowNumber', * from  (" +
                preCandidateQueryText + " union " + candidateQueryText +
                " ) as A )";

            var offset = (pageSize * page) - pageSize;

            totalQueryText += "SELECT" +
                " RowNumber, CVSource, Email, Family, PreCandidateId, CandidateId, Id, Name, IsCandidate, " +
                " GeneralStatus, Telephone, CandidatePrecandidate, " + " LastInterviewDate, " +
                " CreatorId, ModifierId, " + " Status, CandidateOwner, Creator, CustomerName, " +
                " DisplayName, IsInBlackList, Modifier, " +
                " Note, PositionName, Result, (SELECT COUNT(1) FROM CTE1)  as RecordCount " +
                " FROM CTE1 where RowNumber>" + offset + " and RowNumber<= " + pageSize * page;

            return totalQueryText;
        }

        internal async Task<PreCandidatesAndCandidateResultViewModel> GetPreCandidatesByPositionAndUser(PreCandidateSearchViewModel search, int page, int pageSize)
        {
            if (page == 0)
                page = 1;

            if (pageSize == 0)
                pageSize = 10;

            var splittedRange = DateToolkit.SpliteDateRange(search.DateRange);

            var start = splittedRange?.Item1;
            var end = splittedRange?.Item2;

            if (search.CustomerId == 0 && search.PositionId == 0 && search.OwnerId == "0" && start == null && end == null)
                return new PreCandidatesAndCandidateResultViewModel()
                {
                     Result="empty"
                };

            //var preCandidateQuery1 = _context
            //    .Include(x => x.PreCandidateMeetings)
            //        .ThenInclude(x => x.Position)
            //        .Where(x => x.Status != PreCandidateStatus.EligibleCandidate)
            //    .AsNoTracking();
            //.AsEnumerable();

            //if (search.OwnerId != null && search.OwnerId != "0")
            //    preCandidateQuery = preCandidateQuery
            //        .Where(x => x.CreatorId.ToString() == search.OwnerId || x.ModifierId.ToString() == search.OwnerId);


            //if (search.PositionId > 0)
            //    preCandidateQuery = preCandidateQuery
            //        .Where(x => x.PreCandidateMeetings.Any(z => z.PositionId == search.PositionId));

            //if (search.CustomerId > 0)
            //    preCandidateQuery = preCandidateQuery
            //        .Where(x => x.PreCandidateMeetings.Any(z => z.Position.CustomerId == search.CustomerId));

            //if (start.HasValue && end.HasValue)
            //    preCandidateQuery1 = preCandidateQuery1
            //        .Where(x => x.PreCandidateMeetings.Any(m => m.CreatedDateTime >= start.Value && m.CreatedDateTime < end.Value));

            //var tttt = preCandidateQuery1.ToQueryString();

            //var tttttt = ";";

            //var preCandidatesGrouped = preCandidateQuery
            //    //.GroupBy(x => x.Telephone)
            //    .Select(x => new PreCandidateViewModel()
            //    {
            //        CVSource = x.CVSource,
            //        Email = x.Email,
            //        Family = x.Family,
            //        PreCandidateId = x.Id,
            //        Id = x.Id,
            //        Name = x.Name,
            //        IsCandidate = false,
            //        //Note = string.Join(" - ", x.PreCandidateMeetings.Select(z => z.Note)),
            //        GeneralStatus = GetGeneralPrecandidateStatus(x.Status),
            //        Telephone = x.Telephone,
            //        CandidatePrecandidate = CandidatePrecandidate.OnlyPrecandidate,
            //        //LastInterviewDate = x.PreCandidateMeetings != null && x.PreCandidateMeetings.FirstOrDefault() != null ? x.PreCandidateMeetings.OrderByDescending(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime : null,
            //        CreatorId = x.CreatorId,
            //        ModifierId = x.ModifierId
            //    });
            //.ToListAsync();



            //var candidateQuery = _candidatePosition
            //    .Include(x => x.Position)
            //    .Include(x => x.Candidate)
            //    .Where(x => x.Candidate.Status != CandidateStatus.AcceptedOffer
            //    //&& x.Candidate.Status != CandidateStatus.OfferMade
            //    && x.Candidate.Status != CandidateStatus.VitelcoStuff
            //    && x.Candidate.Status != CandidateStatus.WaitingForPaperwork)
            //    .AsNoTracking();

            //if (search.OwnerId != null && search.OwnerId != "0")
            //    candidateQuery = candidateQuery
            //        .Where(x => x.Candidate.CreatorId.ToString() == search.OwnerId || x.Candidate.ModifierId.ToString() == search.OwnerId);

            //if (search.PositionId > 0)
            //    candidateQuery = candidateQuery
            //        .Where(x => x.PositionId == search.PositionId);

            //if (search.CustomerId > 0)
            //    candidateQuery = candidateQuery
            //        .Where(x => x.Position.CustomerId == search.CustomerId);

            //if (start.HasValue && end.HasValue)
            //    candidateQuery = candidateQuery
            //        .Where(x => x.Candidate.Meetings.Any(m => m.CreatedDateTime >= start.Value && m.CreatedDateTime < end.Value));

            //var candidatesGrouped = candidateQuery
            //    //.GroupBy(x => x.Candidate.Telephone)
            //    .Select(x => new PreCandidateViewModel()
            //    {
            //        //CVSource = x.CVSource,
            //        Email = x.Candidate.Email,
            //        Family = x.Candidate.Family,
            //        //PreCandidateId = x.Id,
            //        CandidateId = x.Candidate.Id,
            //        Id = x.Candidate.Id,
            //        Name = x.Candidate.Name,
            //        IsCandidate = true,
            //        //Note = x.Note,
            //        GeneralStatus = (PreCandidateStatusByType)x.Candidate.Status,
            //        Telephone = x.Candidate.Telephone,
            //        CandidatePrecandidate = CandidatePrecandidate.OnlyCandidate,
            //        //LastInterviewDate = x.Candidate != null && x.Candidate.Meetings != null && x.Candidate.Meetings.FirstOrDefault() != null ? x.Candidate.Meetings.OrderByDescending(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime : null,
            //        CreatorId = x.Candidate != null ? x.Candidate.CreatorId : 0,
            //        ModifierId = x.Candidate != null ? x.Candidate.ModifierId : 0
            //    });

            //var unionPreCandidateCandidate = preCandidateQuery
            //    .Union(candidateQuery);

            //var lst = unionPreCandidateCandidate
            //    .GroupBy(x => x.Telephone)
            //    .Select(x => new PreCandidateViewModel()
            //    {
            //        CVSource = x.Max(z => z.CVSource),
            //        Email = x.Max(z => z.Email),
            //        Family = x.Max(z => z.Family),
            //        Name = x.Max(z => z.Name),
            //        PreCandidateId = x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate) != null ? x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate).Id : 0,
            //        CandidateId = x.FirstOrDefault(z => z.CandidatePrecandidate != CandidatePrecandidate.OnlyPrecandidate) != null ? x.FirstOrDefault(z => z.CandidatePrecandidate != CandidatePrecandidate.OnlyPrecandidate).Id : 0,
            //        Id = x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate) != null ? x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate).Id : x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyCandidate || z.CandidatePrecandidate == CandidatePrecandidate.CandidateAndPrecandidate).Id,
            //        Note = string.Join(" - ", x.Where(x => x.Note != null).Select(z => z.Note)),
            //        Telephone = x.Max(z => z.Telephone),
            //        CandidatePrecandidate = x.Any(y => y.IsCandidate) ? CandidatePrecandidate.OnlyCandidate : CandidatePrecandidate.OnlyPrecandidate,
            //        LastInterviewDate = x.Max(z => z.LastInterviewDate),
            //        GeneralStatus = x.Any(y => y.IsCandidate) ? 
            //        x.FirstOrDefault(y => y.CandidatePrecandidate != CandidatePrecandidate.OnlyPrecandidate).GeneralStatus :
            //        x.FirstOrDefault(y => y.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate).GeneralStatus ,
            //        CreatorId = x.Max(z => z.CreatorId),
            //        ModifierId = x.Max(z => z.ModifierId)
            //    });

            var totalQueryText = GenerateQueryText(page, pageSize, search.CustomerId, search.PositionId, search.OwnerId, search.Phone, start, end);

            var preCandidateAnCandidateQuery = _preCandidateAndCandidate
                .FromSqlRaw(totalQueryText)
                .AsNoTracking();

            var pagedListResult = await preCandidateAnCandidateQuery.ToListAsync();

            var preCandidateAndCandidateList = await PreparingPreCandidatesAndCandidates(pagedListResult);

            var totalCount = preCandidateAndCandidateList.FirstOrDefault()?.RecordCount;

            return new PreCandidatesAndCandidateResultViewModel()
            {
                Search = search,
                PreCandidatesAndCandidate = preCandidateAndCandidateList,
                Result="ok",
                Paging =
                {
                    TotalItems =totalCount ?? 10, //totalCount,
                    CurrentPage=page,
                    ItemsPerPage= pageSize,
                },
            };
        }

        internal async Task<List<PreCandidateViewModel>> GetPreCandidatesByPosition(int positionId)
        {
            if (positionId == 0)
                return new List<PreCandidateViewModel>();

            var preCandidates = await _context
                            .Include(x => x.PreCandidateMeetings)
                                .ThenInclude(x => x.Position)
                            .AsNoTracking()
                            .Where(x => x.PreCandidateMeetings.Any(z => z.PositionId == positionId))
                            .Select(x => new PreCandidateViewModel()
                            {
                                CVSource = x.CVSource,
                                Email = x.Email,
                                Family = x.Family,
                                PreCandidateId = x.Id,
                                Id = x.Id,
                                Name = x.Name,
                                IsCandidate = false,
                                Note = x.Note,
                                GeneralStatus = GetGeneralPrecandidateStatus(x.Status),
                                Telephone = x.Telephone,
                                CandidatePrecandidate = CandidatePrecandidate.OnlyPrecandidate,
                                LastInterviewDate = x.PreCandidateMeetings.OrderByDescending(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime,
                                CreatorId = x.CreatorId,
                                ModifierId = x.ModifierId
                            }).ToListAsync();

            var candidates = await _candidate
                            .Include(x => x.Meetings)
                                .ThenInclude(x => x.MPosition)
                            .AsNoTracking()
                            .Where(x => x.Meetings.Any(z => z.MPositionId == positionId))
                            .Select(x => new PreCandidateViewModel()
                            {
                                //CVSource = x.CVSource,
                                Email = x.Email,
                                Family = x.Family,
                                //PreCandidateId = x.Id,
                                CandidateId = x.Id,
                                Id = x.Id,
                                Name = x.Name,
                                IsCandidate = true,
                                //Note = x.Note,
                                GeneralStatus = (PreCandidateStatusByType)x.Status,
                                Telephone = x.Telephone,
                                CandidatePrecandidate = CandidatePrecandidate.OnlyCandidate,
                                LastInterviewDate = x.Meetings.OrderByDescending(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime,
                                CreatorId = x.CreatorId,
                                ModifierId = x.ModifierId
                            }).ToListAsync();

            preCandidates.AddRange(candidates);
            var lst = preCandidates
                .GroupBy(x => x.Telephone)
                .Select(x => new PreCandidateViewModel()
                {
                    CVSource = x.Max(z => z.CVSource),
                    //GeneralStatus = x.Where(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyCandidate || z.CandidatePrecandidate == CandidatePrecandidate.CandidateAndPrecandidate).FirstOrDefault()?.GeneralStatus,
                    Email = x.Max(z => z.Email),
                    Family = x.Max(z => z.Family),
                    Name = x.Max(z => z.Name),
                    PreCandidateId = x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate) != null ? x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate).Id : 0,
                    CandidateId = x.Where(z => z.CandidatePrecandidate != CandidatePrecandidate.OnlyPrecandidate).FirstOrDefault()?.Id ?? 0,
                    Id = x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate) != null ? x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate).Id : x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyCandidate || z.CandidatePrecandidate == CandidatePrecandidate.CandidateAndPrecandidate).Id,
                    Note = x.Max(z => z.Note),
                    Telephone = x.Max(z => z.Telephone),
                    CandidatePrecandidate = GetCandidatePrecandidate(x.AsEnumerable()),
                    LastInterviewDate = x.Max(z => z.LastInterviewDate),
                    GeneralStatus = GetCandidatePrecandidate(x.AsEnumerable()) == CandidatePrecandidate.OnlyPrecandidate ? x.FirstOrDefault(z => z.CandidatePrecandidate == CandidatePrecandidate.OnlyPrecandidate).GeneralStatus : x.FirstOrDefault(z => z.CandidatePrecandidate != CandidatePrecandidate.OnlyPrecandidate).GeneralStatus,
                    CreatorId = x.Max(z => z.CreatorId),
                    ModifierId = x.Max(z => z.ModifierId)
                });
            var returnModel = await PreparingPrecandidates(lst.ToList());
            return returnModel.ToList();
        }

        internal async Task<List<PreCandidateAndCandidateViewModel>> PreparingPreCandidatesAndCandidates(List<PreCandidateAndCandidateViewModel> preCandidatesAndCandidate)
        {
            var users = await _user.AsNoTracking().ToListAsync();

            foreach (var item in preCandidatesAndCandidate)
            {
                item.Creator = users.FirstOrDefault(x => x.Id == item.CreatorId)?.DisplayName;
                item.Modifier = users.FirstOrDefault(x => x.Id == item.ModifierId)?.DisplayName;
                item.DisplayName = StringCipher.Decrypt(item.Name, "12345") + " " + StringCipher.Decrypt(item.Family, "12345");
               
                if (item.IsCandidate == 1)
                {
                    item.GeneralStatus = (PreCandidateStatusByType)item.Status;
                }
                else
                {
                    item.GeneralStatus = GetGeneralPrecandidateStatus(item.Status);
                }
                //item.CandidateOwner= users.FirstOrDefault(x => x.Id == item.ModifierId)?.DisplayName;
            }

            return preCandidatesAndCandidate;

        }

        internal async Task<List<PreCandidateViewModel>> PreparingPrecandidates(List<PreCandidateViewModel> preCandidates)
        {
            var users = await _user.AsNoTracking().ToListAsync();

            foreach (var item in preCandidates)
            {
                item.Creator = users.FirstOrDefault(x => x.Id == item.CreatorId)?.DisplayName;
                item.Modifier = users.FirstOrDefault(x => x.Id == item.ModifierId)?.DisplayName;
                //item.CandidateOwner= users.FirstOrDefault(x => x.Id == item.ModifierId)?.DisplayName;
            }

            return preCandidates;

        }

        [HttpPost]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> GetPreCandidatesByPositionId(int id)
        {
            var search = new PreCandidateSearchViewModel()
            {
                PositionId = id
            };

            return PartialView("_List", await GetPreCandidatesByPositionAndUser(search, 1, int.MaxValue));
        }

        //[HttpPost]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> GetPreCandidatesByPositionIdAndUserId(PreCandidateSearchViewModel search, int page, int pageSize)
        {
            return PartialView("_List",
                    await GetPreCandidatesByPositionAndUser(search, page, pageSize)
                );

        }


        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Details")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preCandidate = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (preCandidate == null)
            {
                return NotFound();
            }

            return View(preCandidate);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Create")]
        public IActionResult Create()
        {
            return View();
        }

        internal bool IsInBlackListByPhone(string phone)
        {
            var candidates = _candidate
                .AsNoTracking()
                .Where(x => x.Telephone == phone);

            if (candidates != null && candidates.Any() && candidates.FirstOrDefault().IsOnBlackList)
                return true;
            else
                return false;

        }

        //private async Task<bool> CheckCandidateIsInPosition(int id, CandidatePrecandidate type )
        //{
        //    if (type== CandidatePrecandidate.OnlyCandidate || type== CandidatePrecandidate.CandidateAndPrecandidate)
        //    {

        //    }
        //}

        //private bool IsCandidateAccepted(PreCandidate preCandidate)
        //{
        //    if (preCandidate.Status == PreCandidateStatus.EligibleCandidate)
        //    {

        //    }
        //}

        // POST: PreCandidates/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PreCandidate preCandidate, PreCandidateSearchViewModel search)
        {
            ModelState.Remove("Id");
            if ((int)preCandidate.CandidatePrecandidate == 0)
            {
                preCandidate.CandidatePrecandidate = CandidatePrecandidate.OnlyPrecandidate;
            }

            ModelState.Remove("CandidatePrecandidate");

            if (preCandidate.Telephone.Contains("_"))
            {
                ModelState.AddModelError("Error", _localizer["The Phone number is incorrect"]);
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["The Phone number is incorrect"]);
                return Json("false");
            }

            if (preCandidate.Status == PreCandidateStatus.EligibleCandidate &&
                string.IsNullOrWhiteSpace(preCandidate.Email))
            {
                ModelState.AddModelError("Error", _localizer["For eligible precandidate inserting email is necessary"]);
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["For eligible precandidate inserting email is necessary"]);
                return Json("false");
            }

            if (ModelState.IsValid)
            {
                if (IsInBlackListByPhone(preCandidate.Telephone))
                {
                    ViewData["Customers"] = await _customer
                        .AsNoTracking()
                        .ToListAsync();

                    ModelState.AddModelError("Error", "The Candidate is in Black list");
                    await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["The Candidate is in Black list"]);
                    return Json("false");
                }


                if (preCandidate.CandidatePrecandidate == CandidatePrecandidate.OnlyCandidate ||
                    preCandidate.CandidatePrecandidate == CandidatePrecandidate.CandidateAndPrecandidate)
                {
                    var can = _candidate
                            .Include(x => x.Meetings)
                            .Include(x => x.CandidatePositions)
                            .Include(x => x.CandidatePositionsHistory)
                            .FirstOrDefault(x => x.Id == preCandidate.CandidateId);

                    var pre = _context
                        .Include(x => x.PreCandidateMeetings)
                        .ThenInclude(x => x.Position)
                        .FirstOrDefault(x => x.Id == preCandidate.PreCandidateId);


                    if (can != null && can.CandidatePositions.Any(x => x.Id == search.PositionId))
                    {
                        await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["The Candidate is in this position"]);
                        return Json("false");
                    }

                    if (pre != null && pre.PreCandidateMeetings.Any(x => x.PositionId == search.PositionId))
                    {
                        await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["The Candidate is in this position"]);
                        return Json("false");
                    }

                    if (preCandidate.Status == PreCandidateStatus.EligibleCandidate)
                    {
                        if (pre != null)
                        {
                            pre.PreCandidateMeetings.Add(new PreCandidateMettings()
                            {
                                PositionId = search.PositionId,
                                Note = preCandidate.Note
                            });
                        }

                        if (can != null)
                        {
                            if (can.CandidatePositions == null)
                                can.CandidatePositions = new List<CandidatePosition>();

                            can.CandidatePositions.Add(new CandidatePosition()
                            {
                                CandidateId = preCandidate.Id,
                                PositionId = search.PositionId
                            });
                        }

                        //var currentCandidatePositionsId = can.CandidatePositions.Select(x => x.PositionId).ToList();
                        //can.CandidatePositions.Clear();
                        //can.CandidatePositions = new List<CandidatePosition>();


                        //can.Meetings.Add(new Meeting()
                        //{
                        //    MeetingDate = DateTime.Now,
                        //    MeetingResult = preCandidate.Note,
                        //    MeetingSpeakingPerson = User.Identity.GetUserDisplayName(),
                        //    MPositionId = search.PositionId
                        //});


                        //if (!currentCandidatePositionsId.Contains(search.PositionId) || !can.CandidatePositionsHistory.Any())
                        //{
                        //    var toAddHistory = new CandidatePositionHistory { CandidateId = preCandidate.Id, PositionId = search.PositionId };
                        //    can.CandidatePositionsHistory.Add(toAddHistory);
                        //}
                    }
                    else
                    {

                        if (pre != null) // if exist
                        {
                            if (pre.PreCandidateMeetings.Any(x => x.PositionId == search.PositionId))
                            {
                                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["The Candidate is in this position"]);
                                return Json("false");
                            }

                            pre.PreCandidateMeetings.Add(new PreCandidateMettings()
                            {
                                PositionId = search.PositionId,
                                Note = preCandidate.Note
                            });

                        }
                        else
                        {
                            var preCanidateMeetings = new List<PreCandidateMettings>();

                            preCanidateMeetings.Add(new PreCandidateMettings()
                            {
                                PositionId = search.PositionId,
                                Note = preCandidate.Note
                            });

                            preCandidate.PreCandidateMeetings = preCanidateMeetings;

                            _context.Add(preCandidate);
                        }
                    }

                }
                else
                {
                    var pre = _context
                            .Include(x => x.PreCandidateMeetings)
                                .ThenInclude(x => x.Position)
                            .FirstOrDefault(x => x.Id == preCandidate.PreCandidateId);

                    if (pre != null) // if exist
                    {

                        if (pre.PreCandidateMeetings.Any(x => x.PositionId == search.PositionId))
                        {
                            await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["The Candidate is in this position"]);
                            return Json("false");
                        }

                        pre.PreCandidateMeetings.Add(new PreCandidateMettings()
                        {
                            PositionId = search.PositionId,
                            Note = preCandidate.Note
                        });
                    }
                    else
                    {
                        var preCanidateMeetings = new List<PreCandidateMettings>();

                        preCanidateMeetings.Add(new PreCandidateMettings()
                        {
                            PositionId = search.PositionId,
                            Note = preCandidate.Note
                        });

                        preCandidate.PreCandidateMeetings = preCanidateMeetings;
                        //preCandidate.CreatorId = int.Parse(User.Identity.GetUserId());

                        _context.Add(preCandidate);
                    }



                    if (preCandidate.Status == PreCandidateStatus.EligibleCandidate)
                    {

                        var position = _position
                        .Include(x => x.PositionFeatures)
                            .ThenInclude(x => x.Feature)
                        .Include(x => x.PositionTypes)
                            .ThenInclude(x => x.Expertise)
                        .AsNoTracking()
                        .FirstOrDefault(x => x.Id == search.PositionId);

                        var candidateExpertises = position.PositionTypes.Select(x => new CandidateExpertise()
                        {
                            ExpertiseId = x.ExpertiseId
                        }).ToList();

                        var candidateFeatures = position.PositionFeatures.Select(x => new CandidateFeature()
                        {
                            FeatureId = x.FeatureId
                        }).ToList();

                        // adding to candidate
                        var candidate = new Candidate()
                        {
                            Email = preCandidate.Email,
                            Status = CandidateStatus.Active,
                            Family = preCandidate.Family,
                            Name = preCandidate.Name,
                            Telephone = preCandidate.Telephone,
                            CandidateFeatures = candidateFeatures,
                            CandidateExpertises = candidateExpertises,
                            ExpectedWages = "1000"
                        };

                        candidate.CandidatePositions = new List<CandidatePosition>();
                        candidate.CandidatePositions.Add(new CandidatePosition()
                        {
                            //CandidateId = preCandidate.Id,
                            PositionId = search.PositionId
                        });

                        //candidate.Meetings.Add(new Meeting()
                        //{
                        //    MeetingDate = DateTime.Now,
                        //    MeetingResult = preCandidate.Note,
                        //    MeetingSpeakingPerson = User.Identity.GetUserDisplayName(),
                        //    MPositionId = search.PositionId
                        //});

                        _candidate.Add(candidate);
                    }
                }

                var insertResult = await _uow.SaveChangesAsync();
                if (insertResult > 0)
                {

                    await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "success|" + _localizer["Inserted successfully"]);

                    if (preCandidate.Status == PreCandidateStatus.EligibleCandidate)
                    {
                        await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "success|" + _localizer["Added to the candidates table. You can continue the process from the Candidates page"]);
                        // Send email
                        var attachs = new List<string>() { };
                        attachs.Add("\\Files\\AttachFiles\\logo.png");
                        //attachs.Add("\\Files\\AttachFiles\\Form-012- Work Documents List.doc");

                        var emailModel = new EmailViewModel()
                        {
                            Name = preCandidate.Name + " " + preCandidate.Family,
                            Useremail = User.Identity.GetUserEmail(),
                            Usermobile = User.Identity.GetUserPhoneNumber(),
                            Username = User.Identity.GetUserDisplayName()

                        };

                        string template = string.Empty;
                        if (preCandidate.Language == "tr")
                        {
                            template = "~/Views/EmailTemplates/_CvRequest-tr.cshtml";
                        }
                        else
                        {
                            template = "~/Views/EmailTemplates/_CvRequest-en.cshtml";
                        }

                        //await _emailSender.SendEmailAsync(
                        //       email: preCandidate.Email,
                        //       subject: preCandidate.Language == "en" ? "Cv Request" : "Özgeçmiş Talebi",
                        //       viewNameOrPath: template,
                        //       attachmentFiles: attachs,
                        //       model: emailModel);

                        //await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "success|" + _localizer["The email has been sent successfully"]);

                    }

                    return Json("true");

                    //return RedirectToAction(nameof(Index), new { id = search.PositionId });
                }

            }
            //return View(preCandidate);
            return Json("false");
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id, int type, int positionId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preCandidate = await _context.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (preCandidate == null)
            {
                return NotFound();
            }

            preCandidate.SelectedPositionId = positionId;
            preCandidate.CurrentStatus = preCandidate.Status;
            preCandidate.CandidatePrecandidate = (CandidatePrecandidate)type;
            return View(preCandidate);
        }

        // POST: PreCandidates/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Status,Name,Family,Email,CVSource,Note,Id,CandidatePrecandidate,CurrentStatus,SelectedPositionId")] PreCandidate preCandidate, int? positionId)
        {
            if (id != preCandidate.Id)
            {
                return NotFound();
            }

            ModelState.Remove("Telephone");

            if (preCandidate.Status == PreCandidateStatus.EligibleCandidate &&
                string.IsNullOrWhiteSpace(preCandidate.Email))
            {
                ModelState.AddModelError(string.Empty, _localizer["For eligible precandidate inserting email is necessary"]);
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["For eligible precandidate inserting email is necessary"]);
                return View(preCandidate);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (preCandidate.CandidatePrecandidate == CandidatePrecandidate.OnlyCandidate)
                    {
                        ModelState.AddModelError(string.Empty, _localizer["Eligible Precandidates can not be modified"]);
                        return View(preCandidate);
                    }
                    else
                    {
                        var currentPre = await _context
                            .Include(x => x.PreCandidateMeetings)
                            .FirstOrDefaultAsync(x => x.Id == id);


                        if (preCandidate.CurrentStatus != PreCandidateStatus.EligibleCandidate &&
                            preCandidate.Status == PreCandidateStatus.EligibleCandidate)
                        {
                            // Create Candidate
                            var position = await _position
                                .Include(x => x.PositionFeatures)
                                    .ThenInclude(x => x.Feature)
                                .Include(x => x.PositionTypes)
                                    .ThenInclude(x => x.Expertise)
                                .AsNoTracking()
                                .FirstOrDefaultAsync(x => x.Id == preCandidate.SelectedPositionId);

                            var candidateExpertises = position.PositionTypes.Select(x => new CandidateExpertise()
                            {
                                ExpertiseId = x.ExpertiseId
                            }).ToList();

                            var candidateFeatures = position.PositionFeatures.Select(x => new CandidateFeature()
                            {
                                FeatureId = x.FeatureId
                            }).ToList();

                            // adding to candidate
                            var candidate = new Candidate()
                            {
                                Email = preCandidate.Email,
                                Status = CandidateStatus.Active,
                                Family = preCandidate.Family,
                                Name = preCandidate.Name,
                                Telephone = currentPre.Telephone,
                                CandidateFeatures = candidateFeatures,
                                CandidateExpertises = candidateExpertises,
                                ExpectedWages = "1000"
                            };

                            candidate.CandidatePositions = new List<CandidatePosition>();
                            candidate.CandidatePositions.Add(new CandidatePosition()
                            {
                                //CandidateId = preCandidate.Id,
                                PositionId = preCandidate.SelectedPositionId
                            });

                            //candidate.Meetings.Add(new Meeting()
                            //{
                            //    MeetingDate = DateTime.Now,
                            //    MeetingResult = preCandidate.Note,
                            //    MeetingSpeakingPerson = User.Identity.GetUserDisplayName(),
                            //    MPositionId = search.PositionId
                            //});

                            _candidate.Add(candidate);

                            currentPre.Name = preCandidate.Name;
                            currentPre.Family = preCandidate.Family;
                            currentPre.CVSource = preCandidate.CVSource;
                            currentPre.Email = preCandidate.Email;
                            currentPre.Note = preCandidate.Note;
                            currentPre.Status = preCandidate.Status;

                            _context.Update(currentPre);
                            await _uow.SaveChangesAsync();

                        }
                        else if (preCandidate.CurrentStatus == PreCandidateStatus.EligibleCandidate &&
                                 preCandidate.Status != PreCandidateStatus.EligibleCandidate)
                        {
                            ModelState.AddModelError(string.Empty, _localizer["Changing status from Eligible to the other is Forbidden"]);
                            return View(preCandidate);
                        }
                        else // Other to the other except Eligible
                        {
                            currentPre.Name = preCandidate.Name;
                            currentPre.Family = preCandidate.Family;
                            currentPre.CVSource = preCandidate.CVSource;
                            currentPre.Email = preCandidate.Email;
                            currentPre.Note = preCandidate.Note;
                            currentPre.Status = preCandidate.Status;
                            _context.Update(currentPre);
                            await _uow.SaveChangesAsync();
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PreCandidateExists(preCandidate.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(preCandidate);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preCandidate = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (preCandidate == null)
            {
                return NotFound();
            }

            return View(preCandidate);
        }

        // POST: PreCandidates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var preCandidate = await _context.FindAsync(id);
            _context.Remove(preCandidate);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private static PreCandidateStatusByType GetGeneralPrecandidateStatus(PreCandidateStatus status)
        {
            switch (status)
            {
                case PreCandidateStatus.InActive:
                    return PreCandidateStatusByType.PreCandidateInActive;
                case PreCandidateStatus.NotReachable:
                    return PreCandidateStatusByType.PreCandidateNotReachable;
                case PreCandidateStatus.SalaryExpectationIsHigh:
                    return PreCandidateStatusByType.PreCandidateSalaryExpectationIsHigh;
                case PreCandidateStatus.NotQualify:
                    return PreCandidateStatusByType.PreCandidateNotQualify;
                case PreCandidateStatus.NotConsideringThePosition:
                    return PreCandidateStatusByType.PreCandidateNotConsideringThePosition;
                case PreCandidateStatus.EligibleCandidate:
                    return PreCandidateStatusByType.PreCandidateEligibleCandidate;
                case PreCandidateStatus.RiskyCandidate:
                    return PreCandidateStatusByType.PreCandidateRiskyCandidate;
                default:
                    return PreCandidateStatusByType.PreCandidateInActive;
            }
        }

        private static CandidatePrecandidate GetCandidatePrecandidate(IEnumerable<PreCandidateViewModel> model)
        {
            if (model.Any(x => x.IsCandidate) && model.Any(x => x.IsCandidate == false))
            {
                return CandidatePrecandidate.CandidateAndPrecandidate;
            }
            else if (model.Any(x => x.IsCandidate) && !model.Any(x => x.IsCandidate == false))
            {
                return CandidatePrecandidate.OnlyCandidate;
            }
            else
                return CandidatePrecandidate.OnlyPrecandidate;
        }

        private static PreCandidateStatusByType GetCandidateByPhoneStatus(CandidateStatus status)
        {
            if (status == CandidateStatus.CustomerRequestedCandidateWithdrew ||
                status == CandidateStatus.DidnotComeToInterview ||
                status == CandidateStatus.WithdrewBeforeMeeting)
            {
                return PreCandidateStatusByType.PreCandidateRiskyCandidate;
            }
            else
            {
                return (PreCandidateStatusByType)status;
            }
        }

        internal bool CandidateIsInPosition(IQueryable<Candidate> candidates, string phone, int positionId)
        {
            var isCandidateInPosition = candidates
                        .Any(x => x.Telephone == phone && x.Meetings.Any(z => z.MPositionId == positionId));

            return isCandidateInPosition;
        }

        internal bool PreCandidateIsInPosition(IQueryable<PreCandidate> preCandidates, string phone, int positionId)
        {
            var isPreCandidateInPosition = preCandidates
                    .Any(x => x.Telephone == phone && x.PreCandidateMeetings.Any(z => z.PositionId == positionId));

            return isPreCandidateInPosition;
        }


        [HttpPost]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> GetCandidateByphone(PreCandidateSearchViewModel search, int page, int pageSize)
        {
            if (string.IsNullOrWhiteSpace(search.Phone) || search.Phone.Contains("_"))
            {
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|The phone number is incorrect|" + search.PositionId);

                var result = await GetPreCandidatesByPositionAndUser(search, page, pageSize);
                result.Result = "emptyphone";
                return PartialView(@"~/Views/PreCandidates/_List.cshtml", result);
            }
            var candidates = _candidate
                .Include(x => x.Meetings)
                .AsNoTracking();

            if (CandidateIsInPosition(candidates, search.Phone, search.PositionId))
            {
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["Interview has been made"] + "|" + search.PositionId);

                var returnModel = await GetPreCandidatesByPositionAndUser(search, page, pageSize);
                returnModel.Result = "inposition";

                return PartialView(@"~/Views/PreCandidates/_List.cshtml", returnModel);

            }

            var lstCandidates = candidates
                .Where(x => x.Telephone == search.Phone
                && x.Status != CandidateStatus.AcceptedOffer
                && x.Status != CandidateStatus.OfferMade
                && x.Status != CandidateStatus.VitelcoStuff
                && x.Status != CandidateStatus.WaitingForPaperwork);

            if (lstCandidates.Count() > 0 && lstCandidates.FirstOrDefault().IsOnBlackList)
            {
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", search.PositionId + "warning|" + _localizer["The Candidate is in Black list"]);

                var returnModel = await GetPreCandidatesByPositionAndUser(search, page, pageSize);
                returnModel.Result = "blacklist";

                return PartialView(@"~/Views/PreCandidates/_List.cshtml", returnModel);
            }

            var precandidates = _context
                .Include(x => x.PreCandidateMeetings)
                .AsNoTracking();

            if (PreCandidateIsInPosition(precandidates, search.Phone, search.PositionId))
            {
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["Interview has been made"] + "|" + search.PositionId);

                var returnModel = await GetPreCandidatesByPositionAndUser(search, page, pageSize);
                returnModel.Result = "in position";

                return PartialView(@"~/Views/PreCandidates/_List.cshtml", returnModel);
            }

            var totalQueryText = GenerateQueryText(page, pageSize, search.CustomerId, search.PositionId, "", search.Phone, null, null);

            //var lstPreCandidates = precandidates
            //    .Where(x => x.Telephone == phone
            //    && x.Status != PreCandidateStatus.EligibleCandidate)
            //    .Select(x => new PreCandidateViewModel()
            //    {
            //        CVSource = x.CVSource,
            //        GeneralStatus = GetGeneralPrecandidateStatus(x.Status),
            //        Email = x.Email,
            //        Family = x.Family,
            //        Name = x.Name,
            //        Id = x.Id,
            //        Note = x.Note,
            //        Telephone = x.Telephone,
            //        IsCandidate = false,
            //        CandidatePrecandidate = CandidatePrecandidate.OnlyPrecandidate,
            //        LastInterviewDate = x.PreCandidateMeetings != null && x.PreCandidateMeetings.FirstOrDefault() != null ? x.PreCandidateMeetings.OrderByDescending(z => z.CreatedDateTime).FirstOrDefault().CreatedDateTime : null
            //    }).AsEnumerable();

            var preCandidateAnCandidateQuery = _preCandidateAndCandidate
                .FromSqlRaw(totalQueryText)
                .AsNoTracking();

            var pagedListResult = await preCandidateAnCandidateQuery.ToListAsync();

            var preCandidateAndCandidateList = await PreparingPreCandidatesAndCandidates(pagedListResult);

            if (preCandidateAndCandidateList.FirstOrDefault()?.GeneralStatus == PreCandidateStatusByType.PreCandidateRiskyCandidate)
            {
                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "warning|" + _localizer["The candidate is a Risky candidate (High tendency to withdraw)"] + "|");
            }

            var model = new PreCandidatesAndCandidateResultViewModel()
            {
                Search = search, 
                Result="ok",
                PreCandidatesAndCandidate = preCandidateAndCandidateList,
                Paging =
                {
                    TotalItems =preCandidateAndCandidateList.FirstOrDefault()!=null ?preCandidateAndCandidateList.FirstOrDefault().RecordCount: 10, //totalCount,
                    CurrentPage=page,
                    ItemsPerPage= pageSize,
                },
            };

            return PartialView(@"~/Views/PreCandidates/_List.cshtml", model);

        }

        [HttpPost]
        public async Task<IActionResult> GetCandidates(int? candidateId, int? precandidateId, CandidatePrecandidate candidatePrecandidate)
        {
            if (candidateId == null && precandidateId == null)
                return Json("");

            var viewModel = new List<PreCandidateViewModel>();

            if (candidateId > 0)
            {
                var candidate = _candidatePosition
                    .Include(x => x.Candidate)
                        .ThenInclude(x => x.Meetings)
                    .Include(x => x.Position)
                        .ThenInclude(x => x.Customer)
                    .AsNoTracking()
                    .Where(x => x.CandidateId == candidateId);

                //var candidate = _candidate
                //    .Include(x => x.Meetings)
                //    .ThenInclude(x => x.MPosition)
                //    .ThenInclude(x => x.Customer)
                //    .AsNoTracking()
                //    .Where(x => x.Id == candidateId.Value);
                //.Where(x=> !x.IsOnBlackList)
                //.FirstOrDefaultAsync(x => x.Id == candidateId.Value);

                var model = candidate
                    .Select(x => new PreCandidateViewModel()
                    {
                        Name = x.Candidate.Name,
                        Family = x.Candidate.Family,
                        Telephone = x.Candidate.Telephone,
                        Id = x.Candidate.Id,
                        Email = x.Candidate.Email,
                        GeneralStatus = (PreCandidateStatusByType)x.Candidate.Status,
                        CandidatePrecandidate = CandidatePrecandidate.OnlyCandidate,
                        InterviewDate = x.Candidate.Meetings != null && x.Candidate.Meetings.Any() ? (x.Candidate.Meetings.OrderByDescending(z => z.CreatedDateTime).FirstOrDefault().CreatedDateTime) : null,
                        CustomerName = x.Position.Customer.Name,
                        PositionName = x.Position.PositionName,
                        Note = x.Candidate.Meetings != null && x.Candidate.Meetings.Any() ? (x.Candidate.Meetings.OrderByDescending(z => z.CreatedDateTime).FirstOrDefault().MeetingResult) : null
                    });

                //var viewModel1 = candidate.Meetings.Select(x => new PreCandidateViewModel()
                //{
                //    Name = x.Candidate.Name,
                //    Family = x.Candidate.Family,
                //    Telephone = x.Candidate.Telephone,
                //    Id = x.CandidateId,
                //    Email = x.Candidate.Email,
                //    GeneralStatus = (PreCandidateStatusByType)x.Candidate.Status,
                //    CandidatePrecandidate = CandidatePrecandidate.OnlyCandidate,
                //    InterviewDate = x.MeetingDate,
                //    CustomerName = x.MPosition.Customer.Name,
                //    PositionName = x.MPosition.PositionName,
                //    Note = x.MeetingResult

                //}).ToList();

                viewModel.AddRange(model);

            }

            if (precandidateId > 0)
            {
                var preCandidate = await _context
                        .Include(x => x.PreCandidateMeetings)
                            .ThenInclude(x => x.Position)
                                .ThenInclude(x => x.Customer)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == precandidateId.Value);

                var model1 = preCandidate.PreCandidateMeetings.Select(x => new PreCandidateViewModel()
                {
                    Name = x.PreCandidate.Name,
                    Family = x.PreCandidate.Family,
                    Telephone = x.PreCandidate.Telephone,
                    Id = x.PreCandidateId,
                    Email = x.PreCandidate.Email,
                    GeneralStatus = GetGeneralPrecandidateStatus(x.PreCandidate.Status),
                    CandidatePrecandidate = CandidatePrecandidate.OnlyPrecandidate,
                    InterviewDate = x.CreatedDateTime,
                    CustomerName = x.Position.Customer.Name,
                    PositionName = x.Position.PositionName,
                    Note = x.Note

                }).ToList();

                viewModel.AddRange(model1);
            }

            var sb = new StringBuilder();
            sb.Append("<table id=\"reza\" style=\"width: 100 %;\"  cellpadding=\"5\" cellspacing=\"0\" class=\"table table-bordered table-striped\" style=\"padding-left:50px;\" >");
            sb.Append("<thead><tr>");
            sb.Append("<td>" + _localizer["Cutomer"] + "</td>");
            sb.Append("<td>" + _localizer["Position"] + "</td>");
            sb.Append("<td>" + _localizer["Date"] + "</td>");
            sb.Append("<td>" + _localizer["Note"] + "</td>");
            sb.Append("<td></td>");
            sb.Append("</tr></thead>");
            sb.Append("<tbody>");

            foreach (var item in viewModel)
            {
                sb.Append("<tr>");
                sb.Append("<td>" + item.CustomerName + "</td>");
                sb.Append("<td>" + item.PositionName + "</td>");
                sb.Append("<td>" + item.InterviewDate + "</td>");
                sb.Append("<td>" + item.Note + "</td>");
                if (item.CandidatePrecandidate == CandidatePrecandidate.OnlyCandidate || item.CandidatePrecandidate == CandidatePrecandidate.CandidateAndPrecandidate)
                {
                    sb.Append("<td><a href=\"/Candidates/Details/" + item.Id + "\">" + _localizer["Detail"] + "</a>" +
                                            //"<a href=\"/Leaves/Detail/" + item.Id + "\"> Details </a> |" +
                                            //"<a href=\"/Leaves/Delete/" + item.Id + "\">Delete</a >" +
                                            "</td>");
                }
                else
                {
                    sb.Append("<td><a href=\"/PreCandidates/Details/" + item.Id + "\">" + _localizer["Detail"] + "</a>" +
                                            //"<a href=\"/Leaves/Detail/" + item.Id + "\"> Details </a> |" +
                                            //"<a href=\"/Leaves/Delete/" + item.Id + "\">Delete</a >" +
                                            "</td>");
                }

                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            return Json(sb.ToString());
        }

        private bool PreCandidateExists(int id)
        {
            return _context.Any(e => e.Id == id);
        }


        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export PreCandidates")]
        public async Task<IActionResult> ExportPreCandidates(int customerId, int positionId, int userId, string dateRange)
        {
            var search = new PreCandidateSearchViewModel()
            {
                CustomerId = customerId,
                PositionId = positionId,
                DateRange = dateRange,
                OwnerId = userId.ToString()
            };

            var preCandidates = await GetPreCandidatesByPositionAndUser(search, 1, int.MaxValue);
            return File(await ExportPreCandidatesAsExcelAsync(preCandidates.PreCandidatesAndCandidate.ToList()), XlsxContentType, _localizer["PreCandidates Report"] + ".xlsx");
        }

        private async Task<List<PreCandidateViewModel>> GetMeetingsOfPreCandidatesAsync(int? candidateId, int? precandidateId, CandidatePrecandidate candidatePrecandidate)
        {
            var viewModel = new List<PreCandidateViewModel>();

            if (candidateId == null && precandidateId == null)
                return viewModel;

            if (candidateId > 0)
            {
                var candidate = await _candidate
                                        .Include(x => x.Meetings)
                                            .ThenInclude(x => x.MPosition)
                                                .ThenInclude(x => x.Customer)
                                        .AsNoTracking()
                                        //.Where(x=> !x.IsOnBlackList)
                                        .FirstOrDefaultAsync(x => x.Id == candidateId.Value);

                var viewModel1 = candidate.Meetings.Select(x => new PreCandidateViewModel()
                {
                    Name = x.Candidate.Name,
                    Family = x.Candidate.Family,
                    Telephone = x.Candidate.Telephone,
                    Id = x.CandidateId,
                    Email = x.Candidate.Email,
                    GeneralStatus = (PreCandidateStatusByType)x.Candidate.Status,
                    CandidatePrecandidate = CandidatePrecandidate.OnlyCandidate,
                    InterviewDate = x.MeetingDate,
                    CustomerName = x.MPosition.Customer.Name,
                    PositionName = x.MPosition.PositionName,
                    Note = x.MeetingResult

                }).ToList();

                viewModel.AddRange(viewModel1);

            }

            if (precandidateId > 0)
            {
                var preCandidate = await _context
                        .Include(x => x.PreCandidateMeetings)
                            .ThenInclude(x => x.Position)
                                .ThenInclude(x => x.Customer)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == precandidateId.Value);

                var viewModel2 = preCandidate.PreCandidateMeetings.Select(x => new PreCandidateViewModel()
                {
                    Name = x.PreCandidate.Name,
                    Family = x.PreCandidate.Family,
                    Telephone = x.PreCandidate.Telephone,
                    Id = x.PreCandidateId,
                    Email = x.PreCandidate.Email,
                    GeneralStatus = GetGeneralPrecandidateStatus(x.PreCandidate.Status),
                    CandidatePrecandidate = CandidatePrecandidate.OnlyPrecandidate,
                    InterviewDate = x.CreatedDateTime,
                    CustomerName = x.Position.Customer.Name,
                    PositionName = x.Position.PositionName,
                    Note = x.Note

                }).ToList();

                viewModel.AddRange(viewModel2);
            }
            return viewModel;
        }
        private async Task<byte[]> ExportPreCandidatesAsExcelAsync(List<PreCandidateAndCandidateViewModel> preCandidates)
        {
            byte[] reportBytes;
            using (var package = await CreatePreCandidatesExcelPackageAsync(preCandidates))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;
        }
        internal async Task<ExcelPackage> CreatePreCandidatesExcelPackageAsync(List<PreCandidateAndCandidateViewModel> preCandidates)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = _localizer["PreCandidates"];
            package.Workbook.Properties.Author = _localizer["Vitelco"];
            package.Workbook.Properties.Subject = _localizer["PreCandidates"];
            package.Workbook.Properties.Keywords = _localizer["PreCandidates"];

            var worksheet = package.Workbook.Worksheets.Add(_localizer["PreCandidates"]);
            //First add the headers
            worksheet.Cells[1, 1].Value = _localizer["Name Surename"];
            worksheet.Cells[1, 2].Value = _localizer["Phone"];
            worksheet.Cells[1, 3].Value = _localizer["Email"];
            worksheet.Cells[1, 4].Value = _localizer["CV Source"];
            worksheet.Cells[1, 5].Value = _localizer["Note"];
            worksheet.Cells[1, 6].Value = _localizer["Candidate/Precandidate"];
            worksheet.Cells[1, 7].Value = _localizer["Last Interview Date"];
            worksheet.Cells[1, 8].Value = _localizer["Status"];
            worksheet.Cells[1, 9].Value = _localizer["Candidate Owner"];
            worksheet.Cells[1, 10].Value = _localizer["Modifier"];

            worksheet.Cells[1, 11].Value = _localizer["Cutomer"];
            worksheet.Cells[1, 12].Value = _localizer["Position"];
            worksheet.Cells[1, 13].Value = _localizer["Date"];
            worksheet.Cells[1, 14].Value = _localizer["Note"];
            int rowNumber = 1;
            bool isOddRow = false;
            foreach (var item in preCandidates)
            {
                rowNumber += 1;
                int firstRow = rowNumber;
                //worksheet.Cells[rowNumber, 1].Value = item.Name;
                worksheet.Cells[rowNumber, 1].Value = item.DisplayName;
                worksheet.Cells[rowNumber, 2].Value = item.Telephone;
                worksheet.Cells[rowNumber, 3].Value = item.Email;
                worksheet.Cells[rowNumber, 4].Value = item.CVSource;
                worksheet.Cells[rowNumber, 5].Value = item.Note;
                worksheet.Cells[rowNumber, 6].Value = _localizer[item.CandidatePrecandidate.GetAttribute<DisplayAttribute>().Name];
                worksheet.Cells[rowNumber, 7].Value = item.LastInterviewDate?.ToString("dd/MM/yyyy HH:mm");
                worksheet.Cells[rowNumber, 8].Value = _localizer[item.GeneralStatus.GetAttribute<DisplayAttribute>().Name];
                worksheet.Cells[rowNumber, 9].Value = item.CandidateOwner;
                worksheet.Cells[rowNumber, 10].Value = item.Modifier;
                var meetings = await GetMeetingsOfPreCandidatesAsync(item.CandidateId, item.PreCandidateId, item.CandidatePrecandidate);
                foreach (var meetingItem in meetings)
                {
                    worksheet.Cells[rowNumber, 11].Value = meetingItem.CustomerName;
                    worksheet.Cells[rowNumber, 12].Value = meetingItem.PositionName;
                    worksheet.Cells[rowNumber, 13].Value = meetingItem.InterviewDate?.ToString("dd/MM/yyyy HH:mm");
                    worksheet.Cells[rowNumber, 14].Value = meetingItem.Note;
                    rowNumber += 1;
                }
                int lastRow = --rowNumber;

                //Merge rows
                worksheet.Cells[firstRow, 1, lastRow, 1].Merge = true;
                worksheet.Cells[firstRow, 2, lastRow, 2].Merge = true;
                worksheet.Cells[firstRow, 3, lastRow, 3].Merge = true;
                worksheet.Cells[firstRow, 4, lastRow, 4].Merge = true;
                worksheet.Cells[firstRow, 5, lastRow, 5].Merge = true;
                worksheet.Cells[firstRow, 6, lastRow, 6].Merge = true;
                worksheet.Cells[firstRow, 7, lastRow, 7].Merge = true;
                worksheet.Cells[firstRow, 8, lastRow, 8].Merge = true;
                worksheet.Cells[firstRow, 9, lastRow, 9].Merge = true;
                worksheet.Cells[firstRow, 10, lastRow, 10].Merge = true;
                //Set background color
                worksheet.Cells[firstRow, 1, lastRow, 14].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[firstRow, 1, lastRow, 14].Style.Fill.BackgroundColor.SetColor(isOddRow ? Color.FromArgb(180, 196, 228) : Color.FromArgb(219, 228, 244));
                isOddRow = !isOddRow;
            }

            //Set border style 
            worksheet.Cells[1, 1, rowNumber, 14].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[1, 1, rowNumber, 14].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[1, 1, rowNumber, 14].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[1, 1, rowNumber, 14].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[1, 1, rowNumber, 14].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //Set header color
            worksheet.Cells[1, 1, 1, 14].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            worksheet.Cells[1, 1, 1, 14].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(223, 130, 68));
            worksheet.Cells[1, 1, rowNumber, 14].AutoFilter = true;
            // AutoFitColumns
            worksheet.Cells[1, 1, rowNumber, 14].AutoFitColumns();
            return package;
        }

    }
}
