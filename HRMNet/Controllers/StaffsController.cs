﻿using HRMNet.Data;
using HRMNet.Hubs;
using HRMNet.Models.Candidates;
using HRMNet.Models.Common;
using HRMNet.Models.Identity;
using HRMNet.Models.ViewModel.Identity.Emails;
using HRMNet.Models.Workflow;
using HRMNet.Services;
using HRMNet.Services.Contracts.Identity;
using HRMNet.Services.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Employees")]
    public class StaffsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Candidate> _candidate;
        private readonly IStringLocalizer _localizer;
        private readonly DbSet<Title> _title;
        private readonly DbSet<WorkLocation> _work;
        private readonly DbSet<StuffDetail> _stuffDetail;
        private readonly IWebHostEnvironment _environment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private readonly IEmailSender _emailSender;
        private readonly IHubContext<MessageHub> _messageHubContext;
        private readonly IUserValidator<User> _userValidator;
        private readonly IApplicationUserManager _userManager;
        private readonly ISet<string> _passwordsBanList;
        private readonly IOptionsSnapshot<SiteSettings> _siteOptions;
        private readonly DbSet<Attachment> _attachment;
        public StaffsController(IUnitOfWork uow
            , IStringLocalizerFactory stringLocalizerFactory
            , IEmailSender emailSender
            , IUserValidator<User> userValidator
            , IApplicationUserManager userManager
            , IOptionsSnapshot<SiteSettings> siteOptions
            , IHubContext<MessageHub> messageHubContext
            , IWebHostEnvironment environment)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
            _candidate = uow.Set<Candidate>();
            _work = uow.Set<WorkLocation>();
            _title = uow.Set<Title>();
            _stuffDetail = uow.Set<StuffDetail>();
            _emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
            _messageHubContext = messageHubContext ?? throw new ArgumentNullException(nameof(messageHubContext));
            _userValidator = userValidator ?? throw new ArgumentNullException(nameof(userValidator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _attachment = uow.Set<Attachment>();
            _siteOptions = siteOptions ?? throw new ArgumentNullException(nameof(siteOptions));
            _passwordsBanList = new HashSet<string>(_siteOptions.Value.PasswordsBanList, StringComparer.OrdinalIgnoreCase);
            _localizer = stringLocalizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Personal Information")]
        public async Task<IActionResult> Staffs(int page, int pageSize)
        {

            if (page == 0)
                page = 1;

            if (pageSize == 0)
                pageSize = 10;

            var model = _candidate
                .Include(x => x.CandidatePositions)
                .ThenInclude(y => y.Position)
                .Where(x => x.Status == CandidateStatus.VitelcoStuff || x.Status == CandidateStatus.Quit)
                .AsNoTracking();

            model = model.OrderByDescending(x => x.Id);

            var offset = (pageSize * page) - pageSize;
            var total = await model.CountAsync();

            var returnModel = new CandidatesViewModel()
            {
                Candidates = await model
                .Skip(offset)
                .Take(pageSize)
                .ToListAsync(),
                Paging =
                {
                    TotalItems =total,
                    CurrentPage=page,
                    ItemsPerPage= pageSize,
                },
            };


            return View(returnModel);
        }

        
        [HttpPost]
        public async Task<IActionResult> StaffDetails(int id, Candidate model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (model.StuffDetail.Id == 0)
            {
                ModelState.Remove("StuffDetail.Id");
            }

            if (ModelState.IsValid)
            {
                if (model.StuffDetail.Id == 0)
                {
                    model.StuffDetail.CandidateId = model.Id;
                    _stuffDetail.Add(model.StuffDetail);
                }
                else
                {
                    var stuff = await _stuffDetail
                    .Include(x => x.Candidate)
                    .FirstOrDefaultAsync(x => x.Id == model.StuffDetail.Id);

                    //stuff.CandidateId = model.Id;
                    stuff.AccommodationAddress = model.StuffDetail.AccommodationAddress;
                    stuff.BirthCertificateNumber = model.StuffDetail.BirthCertificateNumber;
                    stuff.BloodGroup = model.StuffDetail.BloodGroup;
                    //stuff.CandidateId = model.StuffDetail.CandidateId;
                    stuff.CompanyEmail = model.StuffDetail.CompanyEmail;
                    stuff.ContractDuration = model.StuffDetail.ContractDuration;
                    stuff.ContractType = model.StuffDetail.ContractType;
                    stuff.DateOfGraduation = model.StuffDetail.DateOfGraduation;
                    stuff.DepartmentOfGraduation = model.StuffDetail.DepartmentOfGraduation;
                    stuff.EmergencyContactInformation = model.StuffDetail.EmergencyContactInformation;
                    stuff.IBANNumber = model.StuffDetail.IBANNumber;
                    stuff.Manager = model.StuffDetail.Manager;
                    stuff.MilitaryStatus = model.StuffDetail.MilitaryStatus;
                    stuff.PostponementdDate = model.StuffDetail.PostponementdDate;
                    stuff.SchoolOfGraduation = model.StuffDetail.SchoolOfGraduation;
                    stuff.TCNumber = model.StuffDetail.TCNumber;
                    stuff.TitleId = model.StuffDetail.TitleId;
                    stuff.WorkLocationId = model.StuffDetail.WorkLocationId;
                }


                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Staffs));
            }

            ViewData["TitleId"] = await _title.AsNoTracking().ToListAsync();
            ViewData["WorkLocationId"] = await _work.AsNoTracking().ToListAsync();

            return View(model);
        }

        

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Revert To Staff")]
        [HttpPost]
        public async Task<IActionResult> RevertToStaff(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _candidate
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id.Value);

            if (candidate == null || (candidate.Status != CandidateStatus.WaitingForPaperwork &&
                candidate.Status != CandidateStatus.VitelcoStuff))
            {
                return Json("Not found or The status is incorrect");
            }

            //candidate.VitelcoEmail = candidate.Email;

            return View(candidate);

        }

        private IdentityResult ValidatePassword(string username, string password)
        {
            var errors = new List<IdentityError>();

            if (string.IsNullOrWhiteSpace(password))
            {
                errors.Add(new IdentityError
                {
                    Code = "PasswordIsNotSet",
                    Description = "Password is not set"
                });
                return IdentityResult.Failed(errors.ToArray());
            }


            // Extending the built-in validator
            if (password.Contains(username, StringComparison.OrdinalIgnoreCase))
            {
                errors.Add(new IdentityError
                {
                    Code = "PasswordContainsUserName",
                    Description = "Password cannot consist part of username."
                });
                return IdentityResult.Failed(errors.ToArray());
            }

            if (!isSafePasword(password))
            {
                errors.Add(new IdentityError
                {
                    Code = "PasswordIsNotSafe",
                    Description = "Password is simple and not safe"
                });
                return IdentityResult.Failed(errors.ToArray());
            }

            return !errors.Any() ? IdentityResult.Success : IdentityResult.Failed(errors.ToArray());
        }

        private bool isSafePasword(string data)
        {
            if (string.IsNullOrWhiteSpace(data)) return false;
            if (data.Length < 5) return false;
            if (_passwordsBanList.Contains(data.ToLowerInvariant())) return false;
            if (areAllCharsEqual(data)) return false;

            return true;
        }

        private static bool areAllCharsEqual(string data)
        {
            if (string.IsNullOrWhiteSpace(data)) return false;
            data = data.ToLowerInvariant();
            var firstElement = data.ElementAt(0);
            var euqalCharsLen = data.ToCharArray().Count(x => x == firstElement);
            if (euqalCharsLen == data.Length) return true;
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmRevertToStaff(int? id, Candidate model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            ModelState.Remove("Email");
            ModelState.Remove("ExpectedWages");
            ModelState.Remove("Telephone");


            var emailPasswordResult = ValidatePassword(model.VitelcoEmail, model.EmailPassword);

            if (!emailPasswordResult.Succeeded)
            {
                ModelState.AddModelError("", emailPasswordResult.DumpErrors(useHtmlNewLine: true));
            }

            var portalPasswordResult = ValidatePassword(model.PortalUsername, model.PortalPassword);

            if (!portalPasswordResult.Succeeded)
            {
                ModelState.AddModelError("", portalPasswordResult.DumpErrors(useHtmlNewLine: true));
            }

            var validationResult = await _userValidator.ValidateAsync(
                (UserManager<User>)_userManager, new User { UserName = model.PortalUsername, Email = model.VitelcoEmail });

            if (!validationResult.Succeeded)
            {
                ModelState.AddModelError("", validationResult.DumpErrors(useHtmlNewLine: true));
            }

            if (!string.IsNullOrWhiteSpace(model.VitelcoEmail) && model.VitelcoEmail.IsValidEmail())
            {
                var splittedEmail = model.VitelcoEmail.Split("@");
                if (splittedEmail[1].ToLower() != "vitelco.com.tr")
                {
                    ModelState.AddModelError("", _localizer["Please Set Vitelco email"]);
                }
            }
            else
            {
                ModelState.AddModelError("", _localizer["Email is null or incorrect"]);
            }

            if (ModelState.IsValid)
            {
                var candidate = await _candidate
                .FirstOrDefaultAsync(m => m.Id == id.Value);

                if (candidate == null)
                {
                    return NotFound();
                }

                //var aaa = model.Email.Substring(model.Email.Length - 14, 14);


                candidate.Email = model.VitelcoEmail.ToLower();
                candidate.VitelcoEmail = model.VitelcoEmail.ToLower();
                candidate.PortalUsername = model.PortalUsername.ToLower();
                candidate.PortalPassword = model.PortalPassword;
                candidate.EmailPassword = model.EmailPassword;

                if (candidate.Status != CandidateStatus.VitelcoStuff)
                {
                    candidate.Status = CandidateStatus.VitelcoStuff;

                    candidate.WorkflowStateDateTimes.Add(new WorkflowStateDateTime()
                    {
                        Status = (CandidateStatus)candidate.Status
                    });
                }

                var attachs = new List<string>();
                attachs.Add("\\Files\\AttachFiles\\logo.png");

                if (model.CreateAUser && model.RelatedUserId == 0)
                {
                    var user = new User
                    {
                        UserName = model.PortalUsername,
                        Email = model.VitelcoEmail,
                        FirstName = model.Name,
                        LastName = model.Family,
                        PhoneNumber = model.Telephone
                    };
                    var userCreationResult = await _userManager.CreateAsync(user, model.PortalPassword);
                    if (userCreationResult.Succeeded)
                    {

                        // TODO adding public role to the user

                        var registeredUser = await _userManager.FindByNameAsync(model.PortalUsername);
                        candidate.RelatedUserId = registeredUser.Id;

                        await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"success|{user.UserName} " + _localizer["created a new account with password."] + "|");

                        //_logger.LogInformation(3, $"{user.UserName} created a new account with password.");

                        if (_siteOptions.Value.EnableEmailConfirmation)
                        {
                            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                            //ControllerExtensions.ShortControllerName<RegisterController>(), //todo: use everywhere .................

                            try
                            {
                                await _emailSender.SendEmailAsync(
                                    email: user.Email,
                                    subject: "Please Confirm your account",
                                    viewNameOrPath: "~/Views/EmailTemplates/_RegisterEmailConfirmation.cshtml",
                                    attachmentFiles: attachs,
                                    model: new RegisterEmailConfirmationViewModel
                                    {
                                        User = user,
                                        EmailConfirmationToken = code,
                                        EmailSignature = _siteOptions.Value.Smtp.FromName,
                                        MessageDateTime = DateTime.Now.ToLongDateString()
                                    });

                                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"success|" + _localizer["Email confirmation has been sent."] + "");
                                await Task.Delay(5000);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"error|" + _localizer["Email confirmation was failed"] + "");
                                await Task.Delay(15000);
                            }
                            
                        }
                    }

                    if (userCreationResult.Errors.Any())
                    {
                        foreach (var error in userCreationResult.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }

                        await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"warning|" + _localizer["User creation was failed"]);

                        await Task.Delay(10000);
                        return View("RevertToStaff", model);
                    }
                }

                var result = await _uow.SaveChangesAsync();
                var emailModel = new EmailViewModel()
                {
                    Name = candidate.Name + " " + candidate.Family,
                    PortalUsername = candidate.PortalUsername,
                    PortalPassword = candidate.PortalPassword,
                    Email = candidate.VitelcoEmail,
                    EmailPassword = candidate.EmailPassword,
                    Useremail = User.Identity.GetUserEmail(),
                    Usermobile = User.Identity.GetUserPhoneNumber(),
                    Username = User.Identity.GetUserDisplayName()
                };

                try
                {
                    await _emailSender.SendEmailAsync(
                        email: candidate.Email,
                        subject: "E-Posta & VITELCO HRM Portal Hesap Bilgileriniz",
                        viewNameOrPath: "~/Views/EmailTemplates/_SendingWelcome.cshtml",
                        attachmentFiles: attachs,
                        model: emailModel);
                    
                    await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"success|" + _localizer["Welcome email has been sent"] + "|");

                    await Task.Delay(5000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", $"error|" + _localizer["Welcome email was failed. Please send the email manually"] + "|");
                    await Task.Delay(15000);
                }
                
                return RedirectToAction(nameof(Documents));

            }

            return View("RevertToStaff", model);

        }

        [DisplayName("Employee Documents")]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        public async Task<IActionResult> Documents()
        {
            var model = await _candidate
                .Include(x => x.CandidatePositions)
                .ThenInclude(y => y.Position)
                .Where(x => x.Status == CandidateStatus.WaitingForPaperwork || x.Status == CandidateStatus.VitelcoStuff)
                .AsNoTracking()
                .OrderBy(x => x.Status)
                .ToListAsync();

            return View(model);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Employee Detail")]
        public async Task<IActionResult> StaffDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _candidate
                .Include(x => x.StuffDetail)
                    .Include(y => y.CandidatePositions)
                .ThenInclude(z => z.Position)
                .Include(x => x.CandidateInventories)
                    .ThenInclude(x => x.Inventory)
                        .ThenInclude(x => x.InventoryType)
                .Include(x => x.Leaves)
                .Include(x => x.CandidateCourses)
                    .ThenInclude(x => x.Course)
                .AsNoTracking()
                .FirstOrDefaultAsync(t => t.Id == id);

            var statusName = EnumHelper<CandidateStatus>.GetDisplayValue(candidate.Status);

            candidate.StatusName = _localizer[statusName ?? "Unknown"];
            

            ViewData["TitleId"] = await _title.AsNoTracking().ToListAsync();
            ViewData["WorkLocationId"] = await _work.AsNoTracking().ToListAsync();


            return View(candidate);
        }

        private string GetfileType(string extension)
        {
            switch (extension)
            {
                case "application/pdf":
                    return "pdf";
                case "image/png":
                    return "image";
                case "image/jpg":
                    return "image";
                case "image/jpeg":
                    return "image";
                case "image/tiff":
                    return "gdocs";
                case ".ppt":
                    return "office";
                case "application/msword":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    return "office";
                case ".xls":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    return "office";
                case ".mp4":
                    return "video";
                case ".txt":
                    return "text";
                case ".html":
                    return "html";
                default:
                    return "image";
            }
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit Documents")]
        public async Task<IActionResult> EditDocuments(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidate = await _candidate
                .Include(x => x.Attachments)
                .Include(x => x.CandidatePositions)
                    .ThenInclude(x => x.Position)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id.Value);

            if (candidate == null)
            {
                return NotFound();
            }

            string preview = string.Empty;
            string previewConfig = string.Empty;
            int key = 0;
            if (candidate.Attachments.Any())
                foreach (var item in candidate.Attachments)
                {
                    key += 1;
                    preview += "\"/Candidates/GetFiles/" + item.FileName + "\",";
                    previewConfig += "{type:\"" + GetfileType(item.ContentType) + "\", caption:\"" + item.Caption + "\",size:" + item.Size + ",width:\"120px\",url:\"/Candidates/DeleteAttachment/" +
                        item.Id + "\",key:" + item.Id + "},";
                }

            var config = new AttachmentViewModel()
            {
                Id = id.Value,
                SerializedInitialPreview = preview,
                SerializedInitialPreviewConfig = previewConfig,
                append = true
            };

            candidate.AttachmentViewModel = config;
            var statusName = EnumHelper<CandidateStatus>.GetDisplayValue(candidate.Status);

            candidate.StatusName = _localizer[statusName ?? "Unknown"];


            return View(candidate);
        }

        [HttpPost]
        public async Task<IActionResult> EditDocuments(Candidate model, List<IFormFile> files)
        {

            if (ModelState.IsValid)
            {
                var candidate = await _candidate
                    .Include(x => x.Attachments)
                .FirstOrDefaultAsync(x => x.Id == model.Id);

                if (files != null && files.Any())
                {
                    //List<Attachment> newAttachs = new List<Attachment>();
                    foreach (var item in files)
                    {
                        var fileName = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
                        var fileExtension = item.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
                        var path = Path.Combine(_environment.WebRootPath, "Files", fileName + "." + fileExtension.LastOrDefault());
                        using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write,
                                       FileShare.None,
                                       MaxBufferSize,
                                       // you have to explicitly open the FileStream as asynchronous
                                       // or else you're just doing synchronous operations on a background thread.
                                       useAsync: true))
                        {
                            await item.CopyToAsync(fileStream);
                        }

                        var toAdd = new Attachment { IsImage = ImageToolkit.IsImageFile(item), FileName = fileName + "." + fileExtension.LastOrDefault(), Caption = item.FileName, ContentType = item.ContentType };
                        candidate.Attachments.Add(toAdd);
                    }

                    //candidate.CVFilename = fileName + "." + fileExtension.LastOrDefault();
                }

                var result = await _uow.SaveChangesAsync();

                if (result > 0)
                    return RedirectToAction(nameof(Documents));

            }

            return View(model);

        }

        [HttpPost]
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Export Stuffs")]
        public async Task<IActionResult> ExportStaffs()
        {
            var candidates = await _candidate
                .Include(x => x.StuffDetail)
                .ThenInclude(y => y.Title)
                .Include(r => r.StuffDetail)
                .ThenInclude(t => t.WorkingLocation)
                .Where(x => x.Status == CandidateStatus.VitelcoStuff)
                .AsNoTracking()
                .ToListAsync();

            return File(ExportStuffsAsExcel(candidates), XlsxContentType, "staffs.xlsx");
        }

        private byte[] ExportStuffsAsExcel(List<Candidate> stuffs)
        {
            byte[] reportBytes;
            using (var package = createStuffsExcelPackage(stuffs))
            {
                reportBytes = package.GetAsByteArray();
            }

            return reportBytes;

        }

        private ExcelPackage createStuffsExcelPackage(List<Candidate> stuffs)
        {
            var package = new ExcelPackage();
            package.Workbook.Properties.Title = "Staffs Report";
            package.Workbook.Properties.Author = "Vitelco";
            package.Workbook.Properties.Subject = "Staffs Report";
            package.Workbook.Properties.Keywords = "Staffs";

            var worksheet = package.Workbook.Worksheets.Add("Staffs");
            //First add the headers
            worksheet.Cells[1, 1].Value = "Name Surename";
            worksheet.Cells[1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 2].Value = "Phone";
            worksheet.Cells[1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 3].Value = "Email";
            worksheet.Cells[1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 4].Value = "Start Period";
            worksheet.Cells[1, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 5].Value = "Current Wages";
            worksheet.Cells[1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 6].Value = "Expected Wages";
            worksheet.Cells[1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 7].Value = "Total IT experience";
            worksheet.Cells[1, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 8].Value = "Position Related Experiences";
            worksheet.Cells[1, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 9].Value = "Candidate Summary";
            worksheet.Cells[1, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 10].Value = "Is candidate on the Black List";
            worksheet.Cells[1, 10].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 11].Value = "Blacklist Reason";
            worksheet.Cells[1, 11].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 12].Value = "TC. Number";
            worksheet.Cells[1, 12].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 13].Value = "Accommodation Address";
            worksheet.Cells[1, 13].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 14].Value = "Military Status";
            worksheet.Cells[1, 14].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 15].Value = "Postponemented Date";
            worksheet.Cells[1, 15].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 16].Value = "Contract Type";
            worksheet.Cells[1, 16].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 17].Value = "Contract Duration";
            worksheet.Cells[1, 17].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 18].Value = "Title";
            worksheet.Cells[1, 18].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 19].Value = "Working Location";
            worksheet.Cells[1, 19].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 20].Value = "School Of Graduation";
            worksheet.Cells[1, 20].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 21].Value = "Department Of Graduation";
            worksheet.Cells[1, 21].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 22].Value = "Date Of Graduation";
            worksheet.Cells[1, 22].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 23].Value = "IBAN Number";
            worksheet.Cells[1, 23].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 24].Value = "Manager";
            worksheet.Cells[1, 24].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 25].Value = "Blood Group";
            worksheet.Cells[1, 25].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 26].Value = "Emergency Contact";
            worksheet.Cells[1, 26].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 27].Value = "Employee Id";
            worksheet.Cells[1, 27].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            int rowNumber = 1;
            foreach (var item in stuffs)
            {
                rowNumber += 1;
                worksheet.Cells[rowNumber, 1].Value = item.Name + " " + item.Family;
                worksheet.Cells[rowNumber, 2].Value = item.Telephone;
                worksheet.Cells[rowNumber, 3].Value = item.Email;
                worksheet.Cells[rowNumber, 4].Value = item.StartPeriod;
                worksheet.Cells[rowNumber, 5].Value = item.CurrentWages;
                worksheet.Cells[rowNumber, 6].Value = item.ExpectedWages;
                worksheet.Cells[rowNumber, 7].Value = item.TotalITexperience;
                worksheet.Cells[rowNumber, 8].Value = item.PositionRelatedExperience;
                worksheet.Cells[rowNumber, 9].Value = item.BriefPersonalInfomation;
                worksheet.Cells[rowNumber, 10].Value = item.IsOnBlackList;
                worksheet.Cells[rowNumber, 11].Value = item.BlacklistReason;
                worksheet.Cells[rowNumber, 12].Value = item.StuffDetail?.TCNumber;
                worksheet.Cells[rowNumber, 13].Value = item.StuffDetail?.AccommodationAddress;
                worksheet.Cells[rowNumber, 14].Value = item.StuffDetail?.MilitaryStatus;
                worksheet.Cells[rowNumber, 15].Value = item.StuffDetail?.PostponementdDate;
                worksheet.Cells[rowNumber, 16].Value = item.StuffDetail?.ContractType;
                worksheet.Cells[rowNumber, 17].Value = item.StuffDetail?.ContractDuration;
                worksheet.Cells[rowNumber, 18].Value = item.StuffDetail?.Title.Name;
                worksheet.Cells[rowNumber, 19].Value = item.StuffDetail?.WorkingLocation?.Name;
                worksheet.Cells[rowNumber, 20].Value = item.StuffDetail?.SchoolOfGraduation;
                worksheet.Cells[rowNumber, 21].Value = item.StuffDetail?.DepartmentOfGraduation;
                worksheet.Cells[rowNumber, 22].Value = item.StuffDetail?.DateOfGraduation;
                worksheet.Cells[rowNumber, 23].Value = item.StuffDetail?.IBANNumber;
                worksheet.Cells[rowNumber, 24].Value = item.StuffDetail?.Manager;
                worksheet.Cells[rowNumber, 25].Value = item.StuffDetail?.BloodGroup;
                worksheet.Cells[rowNumber, 26].Value = item.StuffDetail?.EmergencyContactInformation;
                worksheet.Cells[rowNumber, 27].Value = item.StuffDetail?.BirthCertificateNumber;

            }

            // Add to table / Add summary row
            var tbl = worksheet.Tables.Add(new ExcelAddressBase(fromRow: 1, fromCol: 1, toRow: stuffs.Count() + 2, toColumn: 27), "Data");
            tbl.ShowHeader = true;
            tbl.TableStyle = TableStyles.Dark9;
            tbl.ShowTotal = true;
            //tbl.Columns[3].DataCellStyleName = dataCellStyleName;
            //tbl.Columns[1].TotalsRowFunction = RowFunctions.Sum;
            //tbl.Columns[2].TotalsRowFunction = RowFunctions.Sum;
            //tbl.Columns[3].TotalsRowFunction = RowFunctions.Sum;

            //worksheet.Cells[candidates.Count() + 2, 4].Style.Numberformat.Format = numberformat;

            // AutoFitColumns
            worksheet.Cells[1, 1, stuffs.Count() + 2, 27].AutoFitColumns();

            //worksheet.HeaderFooter.OddFooter.InsertPicture(
            //    new FileInfo(Path.Combine(_hostingEnvironment.WebRootPath, "images", "captcha.jpg")),
            //    PictureAlignment.Right);

            return package;
        }

        public async Task<IActionResult> GetFilesById(int id)
        {
            var attachFile = await _attachment.FirstOrDefaultAsync(x => x.Id == id);

            var ext = "." + attachFile.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

            return File("/files/" + attachFile.FileName, GetfileTypeByExtension(ext));
        }

        private string GetfileTypeByExtension(string extension)
        {
            switch (extension)
            {
                case ".pdf":
                    return "application/pdf";
                case ".png":
                    return "image/png";
                case ".jpg":
                    return "image/jpg";
                case ".jpeg":
                    return "image/jpeg";
                case ".tiff":
                    return "image/tiff";
                case ".docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".doc":
                    return "application/msword";
                case ".xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".xls":
                    return "office";

                case ".mp4":
                    return "video";
                case ".txt":
                    return "text";
                case ".html":
                    return "html";
                default:
                    return "image";
            }
        }

    }
}
