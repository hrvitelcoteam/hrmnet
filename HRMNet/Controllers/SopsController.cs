﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models.Sops;
using HRMNet.Models.Identity;
using HRMNet.Models.Common;
using HRMNet.Models.Positions;
using HRMNet.Models.Customers;
using HRMNet.Services;
using Microsoft.AspNetCore.SignalR;
using HRMNet.Hubs;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;
using System.Text;
using HRMNet.Models.ViewModel.Sops;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Sops")]
    public class SopsController : Controller
    {
        private const int MaxBufferSize = 0x10000; // 64K. The artificial constraint due to win32 api limitations. Increasing the buffer size beyond 64k will not help in any circumstance, as the underlying SMB protocol does not support buffer lengths beyond 64k.
        private readonly IUnitOfWork _uow;
        private readonly IWebHostEnvironment _environment;
        private readonly DbSet<Sop> _context;
        private readonly DbSet<User> _user;
        private readonly DbSet<Position> _position;
        private readonly DbSet<Customer> _customer;
        private readonly DbSet<SopPosition> _sopPosition;
        private readonly DbSet<Attachment> _attachment;
        private readonly IHubContext<MessageHub> _messageHubContext;
        private readonly IStringLocalizer _localizer;

        public SopsController(IUnitOfWork uow
            , IHubContext<MessageHub> messageHubContext
            , IStringLocalizerFactory stringLocalizerFactory
            , IWebHostEnvironment environment)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));

            _user = uow.Set<User>();
            _position = uow.Set<Position>();
            _customer = uow.Set<Customer>();
            _sopPosition = uow.Set<SopPosition>();
            _attachment = uow.Set<Attachment>();
            _messageHubContext = messageHubContext ?? throw new ArgumentNullException(nameof(messageHubContext));
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
            _context = uow.Set<Sop>();
            _localizer = stringLocalizerFactory.Create(
                baseName: "SharedResource",
                location: "HRMNet");
        }

        private async Task CreateDefaultRequiredList()
        {
            var positions = await _position
                .Include(x => x.Customer)
                .AsNoTracking()
                //.Where(x=> x.Status== PositionStatus.Active)
                .Select(x => new SelectListItem()
                {
                    Text = x.PositionName + " (" + x.Customer.Name + ")",
                    Value = x.Id.ToString()
                }).ToListAsync();

            var customers = await _customer
                .AsNoTracking()
                .Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                })
                .ToListAsync();

            var users = await _user
                .Include(x => x.Roles)
                .AsNoTracking()
                //.Where(x=> x.IsInRole(""))
                .Select(x => new SelectListItem()
                {
                    Text = x.DisplayName,
                    Value = x.Id.ToString()
                })
                .ToListAsync();

            ViewData["OwnerId"] = users;

            ViewData["Positions"] = positions;
            ViewData["Customers"] = customers;
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            //var sops = await _context
            //    .Include(s => s.Owner).ToListAsync();

            await CreateDefaultRequiredList();

            return View(new SopsViewModel()
            {
                // Sops= sops

            });
        }

        private async Task<List<Sop>> GetSops(int? customerId, int? type)
        {
            if (customerId == 0 || customerId is null)
                return null;

            var query = _context
                .Include(x => x.Customer)
                .AsNoTracking()
                .Where(x => x.CustomerId == customerId.Value);

            if (type > 0)
            {
                query = query.Where(x => x.DemandType == (SOPDemandType)type);
            }

            var sops = await query
                .ToListAsync();

            return sops;
        }

        public async Task<IActionResult> GetSopsByCustomerId(int? id)
        {
            return PartialView("_List", new SopsViewModel()
            {
                Sops = await GetSops(id, null)
            });
        }

        public async Task<IActionResult> GetSopsByDemandType(int? id, int? type)
        {
            return PartialView("_List", new SopsViewModel()
            {
                Sops = await GetSops(id, type)
            });
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Details")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sop = await _context
                .Include(s => s.Owner)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sop == null)
            {
                return NotFound();
            }

            return View(sop);
        }

        // GET: Sops/Create
        public IActionResult Create()
        {

            return View();
        }

        // POST: Sops/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Sop sop) //[Bind("DemandName,DemandType,ContactPerson,OwnerId,Priority,StartDate,EndTime,Duration,Status,Result,Comment,CustomerId,selectedPositions")]
        {
            if (ModelState.IsValid)
            {
                try
                {


                    if (sop.selectedPositions != null && sop.selectedPositions.Any())
                    {
                        foreach (var item in sop.selectedPositions)
                        {
                            sop.SopPositions.Add(new SopPosition()
                            {
                                PositionId = int.Parse(item)
                            });
                        }
                    }

                    _context.Add(sop);
                    var result = await _uow.SaveChangesAsync();
                    if (result > 0)
                    {
                        await _messageHubContext.Clients.User(User.Identity.GetUserId()).SendAsync("InstantMessages", "success|" + _localizer["Inserted successfully"] + "|");
                        return Json("true");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            await CreateDefaultRequiredList();
            return Json("false");
        }

        private async Task<string> GenerateOptions(List<SopPosition> sopPositions)
        {
            var positions = await _position
                .Include(x => x.Customer)
                .AsNoTracking()
                .ToListAsync();

            int[] sopPositionsIds = sopPositions.Select(x => x.PositionId).ToArray();

            List<SopOptionViewModel> sopOptions = new List<SopOptionViewModel>();
            foreach (var item in positions)
            {
                if (sopPositionsIds.Contains(item.Id))
                {
                    sopOptions.Add(new SopOptionViewModel()
                    {
                        Name = item.PositionName + " (" + item.Customer.Name + ")",
                        Value = item.Id,
                        Checked = true
                    });
                }
                else
                {
                    sopOptions.Add(new SopOptionViewModel()
                    {
                        Name = item.PositionName + " (" + item.Customer.Name + ")",
                        Value = item.Id,
                        Checked = false
                    });
                }
            }

            return sopOptions.ToJson<SopOptionViewModel>();

        }

        private string GetfileType(string extension)
        {
            switch (extension)
            {
                case "application/pdf":
                    return "pdf";
                case "image/png":
                    return "image";
                case "image/jpg":
                    return "image";
                case "image/jpeg":
                    return "image";
                case "image/tiff":
                    return "gdocs";
                case ".ppt":
                    return "office";
                case "application/msword":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    return "office";
                case ".xls":
                    return "office";
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    return "office";
                case ".mp4":
                    return "video";
                case ".txt":
                    return "text";
                case ".html":
                    return "html";
                default:
                    return "image";
            }
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sop = await _context
                .Include(x => x.SopPositions)
                .Include(x => x.Attachments)
                //.AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id.Value);

            sop.AllPositions = await GenerateOptions(sop.SopPositions.ToList());

            if (sop == null)
            {
                return NotFound();
            }
            await CreateDefaultRequiredList();

            string preview = string.Empty;
            string previewConfig = string.Empty;
            int key = 0;

            if (sop.Attachments.Any())
                foreach (var item in sop.Attachments)
                {
                    key += 1;
                    preview += "\"/Candidates/GetFiles/" + item.FileName + "\",";
                    previewConfig += "{type:\"" + GetfileType(item.ContentType) + "\", caption:\"" + item.Caption + "\",size:" + item.Size + ",width:\"120px\",url:\"/Candidates/DeleteAttachment/" +
                        item.Id + "\",key:" + item.Id + "},";
                }

            var config = new AttachmentViewModel()
            {
                Id = id.Value,
                SerializedInitialPreview = preview,
                SerializedInitialPreviewConfig = previewConfig,
                append = true
            };

            sop.AttachmentViewModel = config;

            return View(sop);
        }

        // POST: Sops/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DemandName,DemandType,ContactPerson,OwnerId,Priority,StartDate,EndTime,Duration,Status,Result,Comment,Id,CustomerId,selectedPositions")] Sop sop, List<IFormFile> files)
        {
            if (id != sop.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var currentSopPositions = _sopPosition.Where(x => x.SopId == sop.Id);

                    _sopPosition.RemoveRange(currentSopPositions);

                    if (sop.selectedPositions != null && sop.selectedPositions.Any())
                    {
                        foreach (var item in sop.selectedPositions)
                        {
                            _sopPosition.Add(new SopPosition()
                            {
                                SopId = sop.Id,
                                PositionId = int.Parse(item)
                            });
                        }
                    }

                    var currentAttachment = _attachment.Where(x => x.SopId == sop.Id);
                    _attachment.RemoveRange(currentAttachment);

                    if (files != null && files.Any())
                    {
                        //candidate.Attachments = new List<Attachment>();
                        foreach (var item in files)
                        {
                            var fileName = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
                            var fileExtension = item.FileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
                            var path = Path.Combine(_environment.WebRootPath, "Files", fileName + "." + fileExtension.LastOrDefault());
                            using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write,
                                           FileShare.None,
                                           MaxBufferSize,
                                           // you have to explicitly open the FileStream as asynchronous
                                           // or else you're just doing synchronous operations on a background thread.
                                           useAsync: true))
                            {
                                await item.CopyToAsync(fileStream);
                            }

                            var toAdd = new Attachment { SopId = sop.Id, IsImage = ImageToolkit.IsImageFile(item), FileName = fileName + "." + fileExtension.LastOrDefault(), Caption = item.FileName, ContentType = item.ContentType };
                            _attachment.Add(toAdd);
                        }
                    }

                    _context.Update(sop);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SopExists(sop.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            await CreateDefaultRequiredList();
            return View(sop);
        }

        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sop = await _context
                .Include(s => s.Owner)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sop == null)
            {
                return NotFound();
            }

            return View(sop);
        }

        // POST: Sops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sop = await _context.FindAsync(id);
            _context.Remove(sop);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> GetDetail(int? id)
        {
            if (id == null)
                return Json("");

            var sop = await _context
                .Include(s => s.Owner)
                //.Include(x => x.Customer)
                //.Where(x => x.Id == id.Value)
                .FirstOrDefaultAsync(x => x.Id == id.Value);
            //.ToListAsync();

            var sb = new StringBuilder();
            sb.Append("<table id=\"reza\" cellpadding=\"5\" cellspacing=\"0\" class=\"table table-bordered table-striped\" style=\"padding-left:50px;\" >");
            sb.Append("<thead><tr>");
            sb.Append("<td>" + _localizer["Contact Person"] + "</td>");
            sb.Append("<td>" + _localizer["Vitelco Owner"] + "</td>");
            sb.Append("<td>" + _localizer["Start Date"] + "</td>");
            sb.Append("<td>" + _localizer["End Date"] + "</td>");
            sb.Append("<td>" + _localizer["Project Duration"] + "</td>");
            sb.Append("<td>" + _localizer["Result"] + "</td>");
            sb.Append("</tr></thead>");
            sb.Append("<tbody>");

            sb.Append("<tr>");
            sb.Append("<td>" + sop.ContactPerson + "</td>");
            sb.Append("<td>" + sop.Owner.DisplayName + "</td>");
            sb.Append("<td>" + sop.StartDate.ToString("MM/dd/yyyy") + "</td>");
            sb.Append("<td>" + sop.EndTime.ToString("MM/dd/yyyy") + "</td>");
            sb.Append("<td>" + sop.Duration + "</td>");

            if (sop.Result!= SopsResult.NoSelect)
                sb.Append("<td>" + _localizer[EnumHelper<SopsResult>.GetDisplayValue(sop.Result)]  + "</td>");
            else
                sb.Append("<td></td>");

            sb.Append("<td><a href=\"/Sops/Edit/" + sop.Id + "\">" + _localizer["Edit"] + "</a></td>");
            sb.Append("</tr>");

            //foreach (var item in sops)
            //{

            //}

            sb.Append("</tbody>");
            sb.Append("</table>");

            return Json(sb.ToString());
        }

        private bool SopExists(int id)
        {
            return _context.Any(e => e.Id == id);
        }
    }
}
