﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using HRMNet.Models;
using Microsoft.AspNetCore.Authorization;
using HRMNet.Services.Identity;
using System.ComponentModel;

namespace HRMNet.Controllers
{
    [Authorize]
    [DisplayName("Audits")]
    public class AuditsController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<Audit> _context;

        public AuditsController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(_uow));
            _context = uow.Set<Audit>();
        }

        // GET: Audits
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("List")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Where(x=> x.TableName!="AppUsers").ToListAsync());
        }

        // GET: Audits/Details/5
        [Authorize(Policy = ConstantPolicies.DynamicPermission)]
        [DisplayName("Detail")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var audit = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (audit == null)
            {
                return NotFound();
            }

            return View(audit);
        }

        // GET: Audits/Create
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        //[DisplayName("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Audits/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,TableName,DateTime,KeyValues,OldValues,NewValues,Username,Action")] Audit audit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(audit);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(audit);
        }

        // GET: Audits/Edit/5
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        //[DisplayName("Edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var audit = await _context.FindAsync(id);
            if (audit == null)
            {
                return NotFound();
            }
            return View(audit);
        }

        // POST: Audits/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,TableName,DateTime,KeyValues,OldValues,NewValues,Username,Action")] Audit audit)
        {
            if (id != audit.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(audit);
                    await _uow.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AuditExists(audit.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(audit);
        }

        // GET: Audits/Delete/5
        //[Authorize(Policy = ConstantPolicies.DynamicPermission)]
        //[DisplayName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var audit = await _context
                .FirstOrDefaultAsync(m => m.Id == id);
            if (audit == null)
            {
                return NotFound();
            }

            return View(audit);
        }

        // POST: Audits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var audit = await _context.FindAsync(id);
            _context.Remove(audit);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AuditExists(int id)
        {
            return _context.Any(e => e.Id == id);
        }
    }
}
