﻿using HRMNet.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMNet.ViewComponents
{
    public class SuitableCandidatesViewComponent : ViewComponent
    {
        private readonly HRMNetContext _context;
        public SuitableCandidatesViewComponent(HRMNetContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string features)
        {
            var candidateFeatures = features.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList();


            var suitableCandidates =await _context
                .Candidate
                .Include(x=> x.CandidateFeatures)
                .Where(x=> x.CandidateFeatures.Any(y=> candidateFeatures.Contains(y.FeatureId.ToString()))).ToListAsync();

            return View(viewName: "~/Views/Shared/Components/_SuitableCandidate.cshtml",
                        model: suitableCandidates);
        }
    }
}
