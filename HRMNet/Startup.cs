﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using HRMNet.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using HRMNet.Services;
using HRMNet.Models.Identity;
using HRMNet.Services.Identity;
using Microsoft.AspNetCore.Http;
using HRMNet.Models.Common;
using DNTCommon.Web.Core;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using HRMNet.Hubs;
using Microsoft.AspNetCore.Mvc.Razor;
using HRMNet.Resources;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Newtonsoft.Json.Converters;

namespace HRMNet
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<SiteSettings>(options => Configuration.Bind(options));

            //services.Configure<ContentSecurityPolicyConfig>(options => Configuration.GetSection("ContentSecurityPolicyConfig").Bind(options));
            services.AddCustomIdentityServices();

            services.AddMvc(options =>
            {
                //options.UseYeKeModelBinder();
                //options.UsePersianDateModelBinder();
                //options.UseCommaSeparatorModelBinder();
                //options.AllowEmptyInputInBodyModelBinding = true
            })
            .AddRazorRuntimeCompilation()
            .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
            .AddDataAnnotationsLocalization(options =>
            {
                options.DataAnnotationLocalizerProvider = (type, factory) =>
                    factory.Create(baseName: "SharedResource",
                                    location: "HRMNet");
            });

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddDNTCommonWeb();
            //services.AddDNTCaptcha();

            //services.AddCloudscribePagination();
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                }); 

            services.AddRazorPages(options =>
            {
                //options.Conventions.AllowAnonymousToFolder("/wwwroot/adminlte");
                //options.Conventions.AllowAnonymousToFolder("/wwwroot/avatars");
                //options.Conventions.AllowAnonymousToFolder("/wwwroot/css");
                //options.Conventions.AllowAnonymousToFolder("/wwwroot/cvs");
                //options.Conventions.AllowAnonymousToFolder("/wwwroot/images");
                //options.Conventions.AllowAnonymousToFolder("/wwwroot/lib");
            })
            .AddRazorRuntimeCompilation();

            services.AddCloudscribePagination();

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }
            //app.UseHttpsRedirection();

            var requestLocalizationOptions = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(new CultureInfo("tr-TR")),
                SupportedCultures = new[]
                {
                    new CultureInfo("en-US"),
                    new CultureInfo("tr-TR")
                },
                SupportedUICultures = new[]
                {
                    new CultureInfo("en-US"),
                    new CultureInfo("tr-TR")
                }
            };
            //requestLocalizationOptions.RequestCultureProviders.Insert(0, new FaRequestCultureProvider());
            app.UseRequestLocalization(requestLocalizationOptions);

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //         Path.Combine(env.ContentRootPath,"wwwroot", "Files")),
            //    RequestPath = "/Files"
            //});

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();

                endpoints.MapHub<MessageHub>("/message");
            });
        }
    }
}
