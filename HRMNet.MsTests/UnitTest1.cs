using System.Collections.Generic;
using System.Linq;
using HRMNet.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace HRMNet.MsTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Random_Color()
        {
            var colors = StringToolkit.GetRandomColorList(25);
            
            colors.Should().NotBeNull();
        }
    }
}
